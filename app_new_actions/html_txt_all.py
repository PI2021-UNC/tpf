"""
Este script se encarga de llamar a los demás para realizar la conversión de los archivos html extraidos del Petrinator a txt
"""

import state_deadlock as sd
import matricesI_html as mih
import siphons_traps as st
import invariantes_html as inv

def main(html_path, txt_path):
    sd.main(html_path, txt_path)
    mih.main(html_path, txt_path)
    st.main(html_path, txt_path)
    inv.main(html_path, txt_path)
