import numpy as np
from scipy.linalg import block_diag
from xml.etree.ElementTree import Element, SubElement, ElementTree
import time

# Genero
document = Element('document')
subnet = SubElement(document, 'subnet')
SubElement(subnet, 'x').text = '0'
SubElement(subnet, 'y').text = '0'
# Los ejes del dibujo del petrinator.
# 0,0 es el medio de la imagen, por eso empiezo en -200, -200.
axis_x = -200
axis_y = -200
place_index = 1
transition_index = 1
# Generate empty matrix
ip = np.empty((0, 0), dtype=int)
im = np.empty((0, 0), dtype=int)
loops = []


def pflow_add_trans_places(ip_matrix):
    global axis_x
    global axis_y
    global transition_index
    global place_index
    global subnet
    for idx, place in enumerate(ip_matrix):
        for transition in place:
            if transition == 1:
                # TODO Esta parte solo funcion cuando hay una sola transicion en la fila de la plaza
                xml_transition = SubElement(subnet, 'transition')
                SubElement(xml_transition, 'id').text = 'T' + str(transition_index)
                SubElement(xml_transition, 'label').text = 'T' + str(transition_index)
                transition_index += 1  # Sumo uno para la proxima transicion
                SubElement(xml_transition, 'x').text = str(axis_x)
                SubElement(xml_transition, 'y').text = str(axis_y)
                # Lo proximo que agrego es una transicion que esta a 75 pixeles de distancia
                axis_y = axis_y + 75
                SubElement(xml_transition, 'timed').text = 'false'
                SubElement(xml_transition, 'rate').text = '1.0'
                SubElement(xml_transition, 'automatic').text = 'false'
                SubElement(xml_transition, 'informed').text = 'true'
                SubElement(xml_transition, 'enableWhenTrue').text = 'false'
                SubElement(xml_transition, 'guard').text = 'none'
                SubElement(xml_transition, 'stochasticProperties', {
                    'distribution': 'Exponential',
                    'labelVar1': 'Rate (λ)',
                    'labelVar2': ' ',
                    'var1': '1.0',
                    'var2': '1.0'
                })
        xml_place = SubElement(subnet, 'place')
        SubElement(xml_place, 'id').text = 'P' + str(place_index)
        SubElement(xml_place, 'label').text = 'P' + str(place_index)
        place_index += 1  # Para la proxima plaza que agregue
        xml_axis_x = SubElement(xml_place, 'x')
        xml_axis_y = SubElement(xml_place, 'y')

        # Si es la ultima plaza la dibujo al medio del tren
        if idx == len(ip_matrix[:, 0]) - 1:
            xml_axis_x.text = str(axis_x - 75)  # Para correrlo a la izquierda del tren
            xml_axis_y.text = str(axis_y / 2)  # Para poner la plaza al medio en el eje y
        else:
            xml_axis_x.text = str(axis_x)
            xml_axis_y.text = str(axis_y)

        axis_y = axis_y + 75  # Lo proximo que agrego es una transicion que esta a 75 pixeles de distancia
        SubElement(xml_place, 'tokens').text = '0'  # TODO aca podriamos agregar la cantidad de tokens de la plaza inicial
        SubElement(xml_place, 'isStatic').text = 'false'


def pflow_add_arc(ip_matrix, im_matrix):
    global subnet
    # Recorro matriz I+ para dibujar arcos desde las transiciones a las plazas
    for place_idx, place in enumerate(ip_matrix):
        xml_arc = SubElement(subnet, 'arc')
        SubElement(xml_arc, 'type').text = 'regular'
        for transition_idx, transition in enumerate(place):
            if transition == 1:
                SubElement(xml_arc, 'sourceId').text = 'T' + str(transition_idx + 1)
                SubElement(xml_arc, 'destinationId').text = 'P' + str(place_idx + 1)

    # Recorro matriz I- para dibujar arcos desde las plazas a las transiciones
    for place_idx, place in enumerate(im_matrix):
        xml_arc = SubElement(subnet, 'arc')
        SubElement(xml_arc, 'type').text = 'regular'
        for transition_idx, transition in enumerate(place):
            if transition == 1:
                SubElement(xml_arc, 'sourceId').text = 'P' + str(place_idx + 1)
                SubElement(xml_arc, 'destinationId').text = 'T' + str(transition_idx + 1)


# Generate random matrixs I+ and I- of carousels
# And concatenates them diagonally
for rand in np.random.randint(low=3, high=5, size=5):
    plus_matrix = np.identity(rand)
    minus_matrix = np.roll(np.identity(rand), -1, axis=0)
    # print("\nDim: [{}]".format(rand))
    # print("Plus matrix:\n {}".format(plus_matrix))
    # print("Minus matrix:\n {}".format(minus_matrix))
    # Guardo la matriz de cada uno de los loop para despues agregarlos al .pflow
    loops.append(plus_matrix)
    # Guardo los nuevos trenes en una matriz comun
    ip = block_diag(ip, plus_matrix)
    im = block_diag(im, minus_matrix)
    # print("I+\n{}".format(ip))
    # print("I-\n{}".format(im))

for loop in loops:
    pflow_add_trans_places(loop)
    # Reincio ejes y me corro 100 pixeles a la derecha para dibujar el nuevo tren.
    axis_x += 125
    axis_y = -200

pflow_add_arc(ip, im)
ElementTree(document).write( time.strftime("%Y%m%d-%H%M%S")+".pflow", xml_declaration=True, encoding='utf-8', method="xml")

# Generate random shared resources and add to I+ and I- matrix
num_resources = np.random.randint(low=3, high=10)
ip = np.vstack((ip, np.random.choice([0, 1], size=(num_resources, np.shape(ip)[0]), p=[9. / 10, 1. / 10])))
im = np.vstack((im, np.random.choice([0, 1], size=(num_resources, np.shape(im)[0]), p=[9. / 10, 1. / 10])))

# print("I+ with resources. Shape: {}\n{}".format(np.shape(ip), ip))
# print("I- with resources. Shape: {}\n{}".format(np.shape(im), im))

# save to txt
# np.savetxt()
