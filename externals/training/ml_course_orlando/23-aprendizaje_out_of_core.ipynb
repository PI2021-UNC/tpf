{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Aprendizaje *out-of-core*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problemas de escalabilidad"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Las clases `sklearn.feature_extraction.text.CountVectorizer` y `sklearn.feature_extraction.text.TfidfVectorizer` tienen una serie de problemas de escalabilidad que provienen de la forma en que se utiliza, a nivel interno, el atributo `vocabulary_` (que es un diccionario Python) para convertir los nombres de las características (cadenas) a índices enteros de características.\n",
    "\n",
    "Los principales problemas de escalabilidad son:\n",
    "\n",
    "- **Uso de memoria del vectorizador de texto**: todas las representaciones textuales de características se cargan en memoria.\n",
    "- **Problemas de paralelización para extracción de características**: el atributo `vocabulary_` es compartido, lo que conlleva que sea difícil la sincronización y por tanto que se produzca una sobrecarga.\n",
    "- **Imposibilidad de realizar aprendizaje *online*, *out-of-core* o *streaming***: el atributo `vocabulary_` tiene que obtenerse a partir de los datos y su tamaño no se puede conocer hasta que no realizamos una pasada completa por toda la base de datos de entrenamiento.\n",
    "\n",
    "Para entender mejor estos problemas, analicemos como trabaja el atributo `vocabulary_`. En la fase de `fit` se identifican los tokens del corpus de forma unívoca, mediante un índice entero, y esta correspondencia se guarda en el vocabulario:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 194,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'cat': 0, 'mat': 1, 'on': 2, 'sat': 3, 'the': 4}"
      ]
     },
     "execution_count": 194,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sklearn.feature_extraction.text import CountVectorizer\n",
    "\n",
    "vectorizer = CountVectorizer(min_df=1)\n",
    "\n",
    "vectorizer.fit([\n",
    "    \"The cat sat on the mat.\",\n",
    "])\n",
    "vectorizer.vocabulary_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El vocabulario se utiliza en la fase `transform` para construir la matriz de ocurrencias:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 195,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5\n",
      "['cat', 'mat', 'on', 'sat', 'the']\n",
      "[[1 1 1 1 2]\n",
      " [2 0 0 0 0]]\n"
     ]
    }
   ],
   "source": [
    "X = vectorizer.transform([\n",
    "    \"The cat sat on the mat.\",\n",
    "    \"This cat is a nice cat.\",\n",
    "]).toarray()\n",
    "\n",
    "print(len(vectorizer.vocabulary_))\n",
    "print(vectorizer.get_feature_names())\n",
    "print(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vamos a realizar un nuevo `fit` con un corpus algo más grande:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 196,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'brown': 0,\n",
       " 'cat': 1,\n",
       " 'dog': 2,\n",
       " 'fox': 3,\n",
       " 'jumps': 4,\n",
       " 'lazy': 5,\n",
       " 'mat': 6,\n",
       " 'on': 7,\n",
       " 'over': 8,\n",
       " 'quick': 9,\n",
       " 'sat': 10,\n",
       " 'the': 11}"
      ]
     },
     "execution_count": 196,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vectorizer = CountVectorizer(min_df=1)\n",
    "\n",
    "vectorizer.fit([\n",
    "    \"The cat sat on the mat.\",\n",
    "    \"The quick brown fox jumps over the lazy dog.\",\n",
    "])\n",
    "vectorizer.vocabulary_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El atributo `vocabulary_` crece (en escala logarítmica) con respecto al tamaño del conjunto de entrenamiento. Observa que no podemos construir los vocabularios en paralelo para cada documento de texto ya que hay algunas palabras que son comunes y necesitaríamos alguna estructura compartida o barrera de sincronización (aumentando la complejidad de implementar el entrenamiento, sobre todo si queremos distribuirlo en un cluster).\n",
    "\n",
    "Con este nuevo vocabulario, la dimensionalidad del espacio de salida es mayor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 197,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "12\n",
      "['brown', 'cat', 'dog', 'fox', 'jumps', 'lazy', 'mat', 'on', 'over', 'quick', 'sat', 'the']\n",
      "[[0 1 0 0 0 0 1 1 0 0 1 2]\n",
      " [0 2 0 0 0 0 0 0 0 0 0 0]]\n"
     ]
    }
   ],
   "source": [
    "X = vectorizer.transform([\n",
    "    \"The cat sat on the mat.\",\n",
    "    \"This cat is a nice cat.\",\n",
    "]).toarray()\n",
    "\n",
    "print(len(vectorizer.vocabulary_))\n",
    "print(vectorizer.get_feature_names())\n",
    "print(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The IMDb movie dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para mostrar los problemas de escalabilidad con los vocabularios basados en vectorizadores, vamos a cargar un dataset realista que proviene de una tarea típica de clasificación de textos: análisis de sentimientos en texto. El objetivo es discernir entre revisiones positivas y negativas a partir de la base de datos de [Internet Movie Database](http://www.imdb.com) (IMDb).\n",
    "\n",
    "En las siguientes secciones, vamos a usar el siguiente dataset [subset](http://ai.stanford.edu/~amaas/data/sentiment/) de revisiones de películas de IMDb, que has sido recolectado por Maas et al. \n",
    "\n",
    "- A. L. Maas, R. E. Daly, P. T. Pham, D. Huang, A. Y. Ng, and C. Potts. Learning Word Vectors for Sentiment Analysis. In the proceedings of the 49th Annual Meeting of the Association for Computational Linguistics: Human Language Technologies, pages 142–150, Portland, Oregon, USA, June 2011. Association for Computational Linguistics. \n",
    "\n",
    "Este dataset contiene 50,000 revisiones de películas, divididas en 25,000 ejemplos de entrenamiento y 25,000 ejemplos de test. Las revisiones se etiquetan como negativas (`neg`) o positivas (`pos`). De hecho, las negativas recibieron $\\le 4$ estrellas en IMDb; las positivas recibieron $\\ge 7$ estrellas. Las revisiones neutrales no se incluyeron en el dataset.\n",
    "\n",
    "Asumiendo que ya habéis ejecutado el *script* `../fetch_data.py`, deberías tener disponibles los siguientes ficheros:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 198,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "train_path = os.path.join('datasets', 'IMDb', 'aclImdb', 'train')\n",
    "test_path = os.path.join('datasets', 'IMDb', 'aclImdb', 'test')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora, vamos a cargarlos en nuestra sesión activa usando la función `load_files` de scikit-learn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 199,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_files\n",
    "\n",
    "train = load_files(container_path=(train_path),\n",
    "                   categories=['pos', 'neg'])\n",
    "\n",
    "test = load_files(container_path=(test_path),\n",
    "                  categories=['pos', 'neg'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\">\n",
    "    <b>NOTA</b>:\n",
    "     <ul>\n",
    "      <li>\n",
    "      Ya que el dataset de películas contiene 50,000 ficheros individuales de texto, ejecutar el código anterior puede llevar bastante tiempo.\n",
    "      </li>\n",
    "    </ul>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La función `load_files`  ha cargado los datasets en objetos `sklearn.datasets.base.Bunch`, que son diccionarios de Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 200,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['target', 'DESCR', 'data', 'filenames', 'target_names'])"
      ]
     },
     "execution_count": 200,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "train.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En particular, solo estamos interesados en los arrays `data` y `target`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 201,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "ENTRENAMIENTO\n",
      "Número de documentos: 25000\n",
      "\n",
      "1er documento:\n",
      " b\"Zero Day leads you to think, even re-think why two boys/young men would do what they did - commit mutual suicide via slaughtering their classmates. It captures what must be beyond a bizarre mode of being for two humans who have decided to withdraw from common civility in order to define their own/mutual world via coupled destruction.<br /><br />It is not a perfect movie but given what money/time the filmmaker and actors had - it is a remarkable product. In terms of explaining the motives and actions of the two young suicide/murderers it is better than 'Elephant' - in terms of being a film that gets under our 'rationalistic' skin it is a far, far better film than almost anything you are likely to see. <br /><br />Flawed but honest with a terrible honesty.\"\n",
      "\n",
      "1era etiqueta: 1\n",
      "\n",
      "Nombre de las clases: ['neg', 'pos']\n",
      "Conteo de las clases: [0 1]  ->  [12500 12500]\n",
      "\n",
      "\n",
      "TEST\n",
      "Número de documentos: 25000\n",
      "\n",
      "1er documento:\n",
      " b\"Don't hate Heather Graham because she's beautiful, hate her because she's fun to watch in this movie. Like the hip clothing and funky surroundings, the actors in this flick work well together. Casey Affleck is hysterical and Heather Graham literally lights up the screen. The minor characters - Goran Visnjic {sigh} and Patricia Velazquez are as TALENTED as they are gorgeous. Congratulations Miramax & Director Lisa Krueger!\"\n",
      "\n",
      "1era etiqueta: 1\n",
      "\n",
      "Nombre de las clases: ['neg', 'pos']\n",
      "Conteo de las clases: [0 1]  ->  [12500 12500]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "for label, data in zip(('ENTRENAMIENTO', 'TEST'), (train, test)):\n",
    "    print('\\n\\n%s' % label)\n",
    "    print('Número de documentos:', len(data['data']))\n",
    "    print('\\n1er documento:\\n', data['data'][0])\n",
    "    print('\\n1era etiqueta:', data['target'][0])\n",
    "    print('\\nNombre de las clases:', data['target_names'])\n",
    "    print('Conteo de las clases:', \n",
    "          np.unique(data['target']), ' -> ',\n",
    "          np.bincount(data['target']))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como puedes comprobar, el array `'target'` consiste en valores `0` y `1`, donde el `0` es una revisión negativa y el `1` representa una positiva."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## El truco del *hashing*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recuerda la representación *bag-of-words* que se obtenía usando un vectorizador basado en vocabulario:\n",
    "\n",
    "<img src=\"figures/bag_of_words.svg\" width=\"100%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para solventar las limitaciones de los vectorizadores basados en vocabularios, se puede utilizar el truco del *hashing*. En lugar de construir y almacenar una conversión explícita de los nombres de las características a los índices de las mismas dentro de un diccionario Python, podemos aplicar una función de *hash* y el operador módulo:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"figures/hashing_vectorizer.svg\" width=\"100%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Podemos acceder a más información y a las referencias a los artículos originales en el siguiente [sitio web](http://www.hunch.net/~jl/projects/hash_reps/index.html), y una descripción más sencilla en este otro [sitio](http://blog.someben.com/2013/01/hashing-lang/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 202,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "b'the' => 761698\n",
      "b'cat' => 300839\n",
      "b'sat' => 122804\n",
      "b'on' => 735689\n",
      "b'the' => 761698\n",
      "b'mat' => 122997\n"
     ]
    }
   ],
   "source": [
    "from sklearn.utils.murmurhash import murmurhash3_bytes_u32\n",
    "\n",
    "# Codificado para compatibilidad con Python 3\n",
    "for word in \"the cat sat on the mat\".encode(\"utf-8\").split():\n",
    "    print(\"{0} => {1}\".format(\n",
    "        word, murmurhash3_bytes_u32(word, 0) % 2 ** 20))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La conversión no tiene estado y la dimensionalidad del espacio de salida se fijo a prior (aquí usamos módulo `2 ** 20`, que significa aproximadamente que tenemos un millón de dimensiones, $2^{20}$). Esto hace posible evitar las limitaciones del vectorizador de vocabulario, tanto a nivel de paralelización como de poder aplicar aprendizaje *online*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La clase `HashingVectorizer` es una alternativa a `CountVectorizer` (o a `TfidfVectorizer` si consideramos `use_idf=False`) que aplica internamente la función de *hash* llamada ``murmurhash``:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 203,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "HashingVectorizer(analyzer='word', binary=False, decode_error='strict',\n",
       "         dtype=<class 'numpy.float64'>, encoding='latin-1',\n",
       "         input='content', lowercase=True, n_features=1048576,\n",
       "         ngram_range=(1, 1), non_negative=False, norm='l2',\n",
       "         preprocessor=None, stop_words=None, strip_accents=None,\n",
       "         token_pattern='(?u)\\\\b\\\\w\\\\w+\\\\b', tokenizer=None)"
      ]
     },
     "execution_count": 203,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sklearn.feature_extraction.text import HashingVectorizer\n",
    "\n",
    "h_vectorizer = HashingVectorizer(encoding='latin-1')\n",
    "h_vectorizer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparte la misma estructura de preprocesamiento, generación de tokens y análisis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 204,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['esta', 'es', 'una', 'frase', 'de', 'prueba']"
      ]
     },
     "execution_count": 204,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "analyzer = h_vectorizer.build_analyzer()\n",
    "analyzer('Esta es una frase de prueba.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Podemos vectorizar nuestros datasets en matriz dispersa de scipy de la misma forma que hubiéramos hecho con `CountVectorizer` o `TfidfVectorizer`, excepto que podemos llamar directamente al método `transform`: no hay necesidad de llamar a `fit` porque el `HashingVectorizer` no tiene estado:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 205,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "docs_train, y_train = train['data'], train['target']\n",
    "docs_valid, y_valid = test['data'][:12500], test['target'][:12500]\n",
    "docs_test, y_test = test['data'][12500:], test['target'][12500:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La dimensión de salida se fija de antemano a `n_features=2 ** 20` (valor por defecto) para minimizar la probabilidad de colisión en la mayoría de problemas de clasificación, teniendo todavía modelos lineales de un tamaño razonable (1M de pesos en el atributo `coef_`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 206,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<25000x1048576 sparse matrix of type '<class 'numpy.float64'>'\n",
       "\twith 3446628 stored elements in Compressed Sparse Row format>"
      ]
     },
     "execution_count": 206,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "h_vectorizer.transform(docs_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora vamos a comparar la eficiencia computacional de `HashingVectorizer` con respecto a `CountVectorizer`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 207,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The slowest run took 4.36 times longer than the fastest. This could mean that an intermediate result is being cached.\n",
      "1 loop, best of 3: 8.69 µs per loop\n"
     ]
    }
   ],
   "source": [
    "h_vec = HashingVectorizer(encoding='latin-1')\n",
    "%timeit -n 1 -r 3 h_vec.fit(docs_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 208,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1 loop, best of 3: 5.56 s per loop\n"
     ]
    }
   ],
   "source": [
    "count_vec =  CountVectorizer(encoding='latin-1')\n",
    "%timeit -n 1 -r 3 count_vec.fit(docs_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como puedes observar, ``HashingVectorizer`` es mucho más rápido que ``Countvectorizer``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Por último, vamos a entrenar un clasificador ``LogisticRegression`` en los datos de entrenamiento de IMDb:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 209,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Pipeline(steps=[('vec', HashingVectorizer(analyzer='word', binary=False, decode_error='strict',\n",
       "         dtype=<class 'numpy.float64'>, encoding='latin-1',\n",
       "         input='content', lowercase=True, n_features=1048576,\n",
       "         ngram_range=(1, 1), non_negative=False, norm='l2',\n",
       "         preprocessor=None, sto...nalty='l2', random_state=1, solver='liblinear', tol=0.0001,\n",
       "          verbose=0, warm_start=False))])"
      ]
     },
     "execution_count": 209,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.pipeline import Pipeline\n",
    "\n",
    "h_pipeline = Pipeline([\n",
    "    ('vec', HashingVectorizer(encoding='latin-1')),\n",
    "    ('clf', LogisticRegression(random_state=1)),\n",
    "])\n",
    "\n",
    "h_pipeline.fit(docs_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 210,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Accuracy de entrenamiento 0.87848\n",
      "Accuracy de validación 0.86208\n"
     ]
    }
   ],
   "source": [
    "print('Accuracy de entrenamiento', h_pipeline.score(docs_train, y_train))\n",
    "print('Accuracy de validación', h_pipeline.score(docs_valid, y_valid))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 211,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "11459"
      ]
     },
     "execution_count": 211,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import gc\n",
    "\n",
    "del count_vec\n",
    "del h_pipeline\n",
    "\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Aprendizaje *Out-of-Core*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El aprendizaje *Out-of-Core* consiste en entrenar un modelo de aprendizaje automático usando un dataset que no cabe en memoria RAM. Requiere las siguientes condiciones:\n",
    "    \n",
    "- Una capa de **extracción de características** con una **dimensionalidad de salida fija**.\n",
    "- Saber la lista de clases de antemano (en esta caso, sabemos que hay *tweets* positivos y negativos).\n",
    "- Un algoritmo de aprendizaje automático que soporte **aprendizaje incremental** (método `partial_fit` en scikit-learn).\n",
    "\n",
    "En la siguientes secciones, vamos a configurar una función simple de entrenamiento iterativo de un `SGDClassifier`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pero primero cargamos los nombres de los ficheros en una lista de Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 212,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['datasets\\\\IMDb\\\\aclImdb\\\\train\\\\pos\\\\0_9.txt',\n",
       " 'datasets\\\\IMDb\\\\aclImdb\\\\train\\\\pos\\\\10000_8.txt',\n",
       " 'datasets\\\\IMDb\\\\aclImdb\\\\train\\\\pos\\\\10001_10.txt']"
      ]
     },
     "execution_count": 212,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "train_path = os.path.join('datasets', 'IMDb', 'aclImdb', 'train')\n",
    "train_pos = os.path.join(train_path, 'pos')\n",
    "train_neg = os.path.join(train_path, 'neg')\n",
    "\n",
    "fnames = [os.path.join(train_pos, f) for f in os.listdir(train_pos)] +\\\n",
    "         [os.path.join(train_neg, f) for f in os.listdir(train_neg)]\n",
    "\n",
    "fnames[:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora vamos a crear el array de etiquetas:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 213,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([12500, 12500], dtype=int64)"
      ]
     },
     "execution_count": 213,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y_train = np.zeros((len(fnames), ), dtype=int)\n",
    "y_train[:12500] = 1\n",
    "np.bincount(y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora vamos a implementar la función `batch_train function`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 214,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from sklearn.base import clone\n",
    "\n",
    "def batch_train(clf, fnames, labels, iterations=25, batchsize=1000, random_seed=1):\n",
    "    vec = HashingVectorizer(encoding='latin-1')\n",
    "    idx = np.arange(labels.shape[0])\n",
    "    c_clf = clone(clf)\n",
    "    rng = np.random.RandomState(seed=random_seed)\n",
    "    \n",
    "    for i in range(iterations):\n",
    "        rnd_idx = rng.choice(idx, size=batchsize)\n",
    "        #print(np.max(rnd_idx),np.min(rnd_idx))\n",
    "        documents = []\n",
    "        for i in rnd_idx:\n",
    "            with open(fnames[i], 'r', encoding=\"utf8\") as f:\n",
    "                documents.append(f.read())\n",
    "        X_batch = vec.transform(documents)\n",
    "        batch_labels = labels[rnd_idx]\n",
    "        c_clf.partial_fit(X=X_batch, \n",
    "                          y=batch_labels, \n",
    "                          classes=[0, 1])\n",
    "      \n",
    "    return c_clf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 215,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.base import clone\n",
    "\n",
    "def batch_train_todos(clf, fnames, labels, iterations=25, batchsize=1000, random_seed=1):\n",
    "    vec = HashingVectorizer(encoding='latin-1')\n",
    "    idx = np.arange(labels.shape[0])\n",
    "    c_clf = clone(clf)\n",
    "    rng = np.random.RandomState(seed=random_seed)\n",
    "    \n",
    "    for i in range(iterations):\n",
    "        rnd_idx = range(i*batchsize, batchsize*(i+1))  \n",
    "        print(np.max(rnd_idx),np.min(rnd_idx))\n",
    "        documents = []\n",
    "        for i in rnd_idx:\n",
    "            with open(fnames[i], 'r', encoding=\"utf8\") as f:\n",
    "                documents.append(f.read())\n",
    "        X_batch = vec.transform(documents)\n",
    "        batch_labels = labels[rnd_idx]\n",
    "       \n",
    "        c_clf.partial_fit(X=X_batch, \n",
    "                          y=batch_labels, \n",
    "                          classes=[0, 1])\n",
    "      \n",
    "    return c_clf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 216,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def batch_train_uno_uno(clf, fnames, labels, iterations=25, batchsize=1000, random_seed=1):\n",
    "    vec = HashingVectorizer(encoding='latin-1')\n",
    "    idx = np.arange(labels.shape[0])\n",
    "    c_clf = clone(clf)\n",
    "    rng = np.random.RandomState(seed=random_seed)\n",
    "    \n",
    "    rnd_idx_1=range(0, 1000)\n",
    "    rnd_idx_2=range(20000, 21000)\n",
    "    a=iterations\n",
    "    b=0\n",
    "    for i in range(iterations):\n",
    "        if i%2 ==1:\n",
    "            rnd_idx = range(batchsize*(a-1), batchsize*(a))\n",
    "            a=a-1\n",
    "        else:\n",
    "            rnd_idx = range(b*batchsize, batchsize*(b+1))\n",
    "            b=b+1\n",
    "       \n",
    "        print(np.max(rnd_idx),np.min(rnd_idx))\n",
    "        documents = []\n",
    "        for i in rnd_idx:\n",
    "            with open(fnames[i], 'r', encoding=\"utf8\") as f:\n",
    "                documents.append(f.read())\n",
    "        X_batch = vec.transform(documents)\n",
    "        batch_labels = labels[rnd_idx]\n",
    "       \n",
    "        c_clf.partial_fit(X=X_batch, \n",
    "                          y=batch_labels, \n",
    "                          classes=[0, 1])\n",
    "      \n",
    "    return c_clf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora vamos a utilizar la clase `LogisticRegression` como hicimos anteriormente, pero usaremos un `SGDClassifier` con un coste logístico en su lugar. SGD proviene de *stochastic gradient descent*, un algoritmo de optimización que optimiza los pesos de forma iterativa ejemplo a ejemplo, lo que nos permite pasarle los datos en grupos."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como empleamos el `SGDClassifier` con la configuración por defecto, entrenará el clasificador en 25\\*1000=25000 documentos (lo que puede llevar algo de tiempo)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 217,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(25000,) 25000\n",
      "999 0\n",
      "1999 1000\n",
      "2999 2000\n",
      "3999 3000\n",
      "4999 4000\n",
      "5999 5000\n",
      "6999 6000\n",
      "7999 7000\n",
      "8999 8000\n",
      "9999 9000\n",
      "10999 10000\n",
      "11999 11000\n",
      "12999 12000\n",
      "13999 13000\n",
      "14999 14000\n",
      "15999 15000\n",
      "16999 16000\n",
      "17999 17000\n",
      "18999 18000\n",
      "19999 19000\n",
      "20999 20000\n",
      "21999 21000\n",
      "22999 22000\n",
      "23999 23000\n",
      "24999 24000\n",
      "999 0\n",
      "24999 24000\n",
      "1999 1000\n",
      "23999 23000\n",
      "2999 2000\n",
      "22999 22000\n",
      "3999 3000\n",
      "21999 21000\n",
      "4999 4000\n",
      "20999 20000\n",
      "5999 5000\n",
      "19999 19000\n",
      "6999 6000\n",
      "18999 18000\n",
      "7999 7000\n",
      "17999 17000\n",
      "8999 8000\n",
      "16999 16000\n",
      "9999 9000\n",
      "15999 15000\n",
      "10999 10000\n",
      "14999 14000\n",
      "11999 11000\n",
      "13999 13000\n",
      "12999 12000\n"
     ]
    }
   ],
   "source": [
    "from sklearn.linear_model import SGDClassifier\n",
    "\n",
    "sgd = SGDClassifier(loss='log', random_state=1, n_jobs=-1)\n",
    "sgd_t = SGDClassifier(loss='log', random_state=1, n_jobs=-1)\n",
    "sgd_u= SGDClassifier(loss='log', random_state=1, n_jobs=-1)\n",
    "print(y_train.shape, len(fnames))\n",
    "sgd = batch_train(clf=sgd, fnames=fnames, labels=y_train)\n",
    "sgd_t = batch_train_todos(clf=sgd, fnames=fnames, labels=y_train)\n",
    "sgd_u = batch_train_uno_uno(clf=sgd, fnames=fnames, labels=y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Al terminar, evaluemos el rendimiento:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 218,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.82848\n",
      "0.49848\n",
      "0.74192\n"
     ]
    }
   ],
   "source": [
    "vec = HashingVectorizer(encoding='latin-1')\n",
    "print(sgd.score(vec.transform(docs_test), y_test))\n",
    "print(sgd_t.score(vec.transform(docs_test), y_test))\n",
    "print(sgd_u.score(vec.transform(docs_test), y_test))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Limitaciones de los vectorizadores basados en *hash*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Utilizar este tipo de vectorizadores nos permiten una mejor escalabilidad y trabajar *on-line*, pero también tienen algunos inconvenientes:\n",
    "    \n",
    "- Las colisiones podrían introducir ruido en los datos y degradar la calidad de las predicciones.\n",
    "- La clase `HashingVectorizer` no nos permite emplear \"*Inverse Document Frequency*\" (no existe la opción `use_idf=True`).\n",
    "- No hay una forma simple de realizar una conversión inversa y encontrar los nombres de las características a partir del índice.\n",
    "\n",
    "Las colisiones pueden minimizarse incrementando el parámetro `n_features`.\n",
    "\n",
    "El peso IDF podría reintroducirse si añadimos un objeto de la clase `TfidfTransformer` a la salida del vectorizador. Sin embargo, obtener el estadístico `idf_` utilizado para sopesar las características implicaría al menos una pasada adicional por los datos de entrenamiento antes de empezar a entrenar el clasificador: ya no podríamos afrontar un escenario *on-line*.\n",
    "\n",
    "La falta de una conversión inversa (método `get_feature_names()` de `TfidfVectorizer`) es mucho más difícil de evitar. Tendríamos que extender la clase `HashingVectorizer` para añadir un modo \"traza\" que nos permitiera guardar la conversión de las características más importantes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>EJERCICIO</b>:\n",
    "     <ul>\n",
    "      <li>\n",
    "      En la implementación propuesta de la función ``batch_train``, se tomaron aleatoriamente *k* ejemplos de entrenamiento como *batch* en cada iteración, lo que puede considerarse un muestreo aleatorio con reemplazamiento. Modifica la función `batch_train` de forma que itere sobre todos los documentos ***sin*** reemplazamiento, es decir, que se usa cada documento ***una sola vez*** por iteración?\n",
    "      </li>\n",
    "    </ul>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>Respueste</b>:\n",
    "     <ul>\n",
    "      <li>\n",
    "     Se implementaron dos funciones ``batch_train_todo`` y ``batch_train_uno_uno`` . <br>\n",
    "     La primera itera sobre todos los documentos ***sin*** reemplazamiento, para lo cual se realizaron 25 particiones de 1000 elementos consecutivas, es decir  se usa cada documento ***una sola vez*** por iteración.<br>\n",
    "      La segunda itera sobre todos los documentos ***sin*** reemplazamiento, para lo cual se realizaron 25 particiones de 1000 elementos y las recorre entrelazadas (0,24,1,23,2,22,....) es decir  se usa cada documento ***una sola vez*** por iteración.\n",
    "      </li>\n",
    "    </ul>\n",
    "    <ul>\n",
    "      <li>\n",
    "      El score para batch_train, cuando tomamos los datos aleatoriamente con reposicion, es de 0.82848<br>\n",
    "      El score para batch_train_todo, cuando tomamos los datos secuencialmente sin reposicion es de 0.49848<br>\n",
    "      El score para batch_train_uno_uno, cuando tomamos los datos entrelazados sin reposicion es de 0.74192<br>\n",
    "       </li>\n",
    "    </ul>\n",
    "      <ul>\n",
    "      <li>\n",
    "      Dado que el entrenamiento es incremental y si entrenamos con muchos datos con igual etique primero el sistema encuentra un minimo local y no puede salir.<br>\n",
    "      </li>\n",
    "    </ul>\n",
    "      \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
