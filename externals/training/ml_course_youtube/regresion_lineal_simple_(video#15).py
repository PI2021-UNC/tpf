# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# Importando librerias a utilizar
import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt

# Imprimiendo el dataset
boston = datasets.load_boston()
print (boston)
print ()

# Verificando informacion contenida en el dataset 
print ('Informacion del dataset:')
print (boston.keys())
print ()
print ('Caracteristicas del dataset:')
print (boston.DESCR)
print ()
print ('Cantidad de datos:')
print (boston.data.shape)
print ()
print ('Nombres de las columnas:')
print (boston.feature_names)

x = boston.data[:, np.newaxis, 5]
y = boston.target

plt.scatter(x,y)
plt.xlabel('Numero de habitaciones')
plt.ylabel('Valor medio')
plt.show()

### IMPLEMENTACION REGRESION LINEAL SIMPLE ###
from sklearn.model_selection import train_test_split

# separo los datos de train en entrenamiento y prueba 
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

# defino el algoritmo a utilizar
lr = linear_model.LinearRegression()

# entreno al modelo
lr.fit(x_train, y_train)

# realizo una prediccion 
y_pred = lr.predict(x_test)

# graficamos los datos junto con el modelo
plt.scatter (x_test,y_test)
plt.plot (x_test,y_pred, color='red', linewidth=3)
plt.title ('Regresion lineal simple')
plt.xlabel ('Numero de habitaciones')
plt.ylabel ('Valor medio')
plt.show()

print ()
print ('Datos del modelo de regresion lineal simple')
print ()
print ('Valor de la pendiente del coeficiente "a":')
print (lr.coef_)
print ()
print ('Valor de la interseccion o del coeficiente "b":')
print (lr.intercept_)

print ()
print ('Precision del modelo:')
print (lr.score(x_train, y_train))
