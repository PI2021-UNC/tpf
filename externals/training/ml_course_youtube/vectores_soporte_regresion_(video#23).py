# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 23:11:36 2021

@author: cnicolaide
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model

# Importamos los datos de Boston Housing
boston = datasets.load_boston()

x_svr = boston.data[:, np.newaxis, 5]
y_svr = boston.target

plt.scatter(x_svr, y_svr)
plt.show()

from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x_svr, y_svr, test_size=0.2)

from sklearn.svm import SVR
svr = SVR(kernel='linear', C=1.0, epsilon=0.2)

svr.fit(x_train, y_train)

# Realizo una prediccion
y_pred = svr.predict(x_test)

plt.scatter(x_test, y_test)
plt.plot (x_test, y_pred, color='red', linewidth=3)
plt.show()

print(svr.score(x_train, y_train))

