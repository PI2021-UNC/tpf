# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 16:19:39 2021

@author: cnicolaide
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model

# Importamos los datos de Boston Housing
boston = datasets.load_boston()
print (boston)
print ()

# Verificando la informacion contenida en el dataset
print ('Informacion en el dataset:')
print (boston.keys())
print (boston.data.shape)
print ()

x_polinomio = boston.data[:, np.newaxis, 5]
y_polinomio = boston.target

plt.scatter(x_polinomio, y_polinomio)
plt.show()

from sklearn.model_selection import train_test_split
x_train_polinomio, x_test_polinomio, y_train_polinomio, y_test_polinomio = train_test_split(x_polinomio, y_polinomio, test_size=0.2)

from sklearn.preprocessing import PolynomialFeatures
poli_reg = PolynomialFeatures(degree = 2)

x_train_poli = poli_reg.fit_transform(x_train_polinomio)
x_test_poli = poli_reg.fit_transform(x_test_polinomio)

pr = linear_model.LinearRegression()

pr.fit(x_train_poli, y_train_polinomio)

# Realizo una prediccion

y_pred_polinomio = pr.predict(x_test_poli)

plt.scatter(x_test_polinomio, y_test_polinomio)
plt.plot (x_test_polinomio, y_pred_polinomio, color='red', linewidth=3)
plt.show()

print(pr.coef_)
print(pr.intercept_)
print(pr.score(x_train_poli, y_train_polinomio))

