# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 12:03:03 2021

@author: cnicolaide
"""

from sklearn import datasets, linear_model

# Importamos los datos de Boston Housing
boston = datasets.load_boston()
print (boston)
print ()

# Verificando la informacion contenida en el dataset
print ('Informacion en el dataset:')
print (boston.keys())
print (boston.data.shape)
print ()

x_multiple = boston.data[:, 5:8]
print (x_multiple)

y_multiple = boston.target

# Implementacion regresion lineal multiple

from sklearn.model_selection import train_test_split
# Separo los datos de train en entrenamiento y prueba para probar los algoritmos

x_train, x_test, y_train, y_test = train_test_split(x_multiple, y_multiple, test_size=0.2)

# Defino el algoritmo a utilizar
lr_multiple = linear_model.LinearRegression()

# Entreno el modelo
lr_multiple.fit(x_train, y_train)

# Realizo una prediccion
y_pred_multiple = lr_multiple.predict(x_test)

print ('Valor de las pendientes o coeficientes a:')
print (lr_multiple.coef_)

print ('Valor de la interseccion o coeficiente b:')
print (lr_multiple.intercept_)

print ('Precision del modelo:')
print (lr_multiple.score(x_train, y_train))
