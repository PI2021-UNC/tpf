15/09/2021 21:39:30[INFO] Iniciando la clasificación serial de todos las redes... 
  
15/09/2021 21:40:33[INFO] Clasificando los resultados... 
  
15/09/2021 21:40:43[INFO] Generando estadísticas... 
  
15/09/2021 21:40:43[RESULT] Se analizaron 30 redes de petri, de las cuales:   
15/09/2021 21:40:43[RESULT] 30 se pudieron clasificar   
15/09/2021 21:40:43[RESULT] 20 no contenian DEADLOCK inicialmente   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210915-213836_21 |    00h:00m:01s    |
| 20210915-213836_2  |    00h:00m:01s    |
| 20210915-213836_20 |    00h:00m:01s    |
| 20210915-213836_7  |    00h:00m:01s    |
| 20210915-213835_0  |    00h:00m:01s    |
| 20210915-213836_24 |    00h:00m:01s    |
| 20210915-213836_18 |    00h:00m:01s    |
| 20210915-213836_28 |    00h:00m:02s    |
| 20210915-213836_15 |    00h:00m:02s    |
| 20210915-213836_12 |    00h:00m:03s    |
+--------------------+-------------------+ 

15/09/2021 21:40:43[RESULT] 0 no eran del tipo S3PR   
15/09/2021 21:40:43[RESULT] 0 no pudieron clasificarse debido a corte por timeout del petrinator   
15/09/2021 21:40:43[RESULT] 0 expirementaron fallas inesperadas durante el análisis   
15/09/2021 21:40:43[RESULT] 40 cumplen las condiciones de tener DEADLOCK y ser tipo S3PR   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210915-213836_19 |    00h:00m:01s    |
| 20210915-213836_23 |    00h:00m:01s    |
| 20210915-213836_25 |    00h:00m:01s    |
| 20210915-213836_13 |    00h:00m:01s    |
| 20210915-213836_17 |    00h:00m:01s    |
| 20210915-213836_16 |    00h:00m:01s    |
| 20210915-213836_1  |    00h:00m:01s    |
| 20210915-213836_8  |    00h:00m:01s    |
| 20210915-213836_22 |    00h:00m:01s    |
| 20210915-213836_6  |    00h:00m:01s    |
| 20210915-213836_9  |    00h:00m:02s    |
| 20210915-213836_14 |    00h:00m:02s    |
| 20210915-213836_27 |    00h:00m:02s    |
| 20210915-213836_10 |    00h:00m:02s    |
| 20210915-213836_29 |    00h:00m:02s    |
| 20210915-213836_26 |    00h:00m:02s    |
| 20210915-213836_5  |    00h:00m:02s    |
| 20210915-213836_4  |    00h:00m:02s    |
| 20210915-213836_3  |    00h:00m:02s    |
| 20210915-213836_11 |    00h:00m:03s    |
+--------------------+-------------------+ 

15/09/2021 21:40:43[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
15/09/2021 21:40:43[RESULT] - 133.33% de las redes válidas cumplen las condiciones esperadas -- Calidad del Algoritmo   
15/09/2021 21:40:43[RESULT] - 66.67% no cumplen condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
15/09/2021 21:40:43[RESULT] - 0.00% se debieron descartar por problemas petrinator -- Incidencias debido a factores externos   
15/09/2021 21:40:43[INFO] La ejecución masiva ha concluído exitosamente, luego de 00h:01m:12s 
  
