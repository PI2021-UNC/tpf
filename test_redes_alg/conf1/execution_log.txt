18/09/2021 15:02:22[INFO] Iniciando la clasificación serial de todos las redes... 
  
18/09/2021 15:04:21[INFO] Clasificando los resultados... 
  
18/09/2021 15:04:32[INFO] Generando estadísticas... 
  
18/09/2021 15:04:32[RESULT] Se analizaron 30 redes de petri, de las cuales:   
18/09/2021 15:04:32[RESULT] 18 se pudieron clasificar   
18/09/2021 15:04:32[RESULT] 6 no contenian DEADLOCK inicialmente   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210918-150202_15 |    00h:00m:03s    |
| 20210918-150202_8  |    00h:00m:03s    |
| 20210918-150202_25 |    00h:00m:05s    |
+--------------------+-------------------+ 

18/09/2021 15:04:32[RESULT] 0 no eran del tipo S3PR   
18/09/2021 15:04:32[RESULT] 0 no pudieron clasificarse debido a corte por timeout del petrinator   
18/09/2021 15:04:32[RESULT] 12 expirementaron fallas inesperadas durante el análisis   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210918-150202_11 |    00h:00m:02s    |
| 20210918-150202_20 |    00h:00m:02s    |
| 20210918-150202_26 |    00h:00m:03s    |
| 20210918-150202_0  |    00h:00m:03s    |
| 20210918-150202_3  |    00h:00m:04s    |
| 20210918-150202_22 |    00h:00m:05s    |
+--------------------+-------------------+ 

18/09/2021 15:04:32[RESULT] 42 cumplen las condiciones de tener DEADLOCK y ser tipo S3PR   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210918-150202_10 |    00h:00m:01s    |
| 20210918-150202_16 |    00h:00m:01s    |
| 20210918-150202_2  |    00h:00m:01s    |
| 20210918-150202_18 |    00h:00m:01s    |
| 20210918-150202_27 |    00h:00m:01s    |
| 20210918-150202_12 |    00h:00m:01s    |
| 20210918-150202_17 |    00h:00m:01s    |
| 20210918-150202_28 |    00h:00m:01s    |
| 20210918-150202_4  |    00h:00m:01s    |
| 20210918-150202_29 |    00h:00m:01s    |
| 20210918-150202_6  |    00h:00m:01s    |
| 20210918-150202_19 |    00h:00m:02s    |
| 20210918-150202_5  |    00h:00m:02s    |
| 20210918-150202_24 |    00h:00m:02s    |
| 20210918-150202_7  |    00h:00m:02s    |
| 20210918-150202_9  |    00h:00m:02s    |
| 20210918-150202_1  |    00h:00m:03s    |
| 20210918-150202_23 |    00h:00m:03s    |
| 20210918-150202_21 |    00h:00m:04s    |
| 20210918-150202_13 |    00h:00m:05s    |
| 20210918-150202_14 |    00h:00m:34s    |
+--------------------+-------------------+ 

18/09/2021 15:04:32[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
18/09/2021 15:04:32[RESULT] - 233.33% de las redes válidas cumplen las condiciones esperadas -- Calidad del Algoritmo   
18/09/2021 15:04:32[RESULT] - 20.00% no cumplen condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
18/09/2021 15:04:32[RESULT] - 40.00% se debieron descartar por problemas petrinator -- Incidencias debido a factores externos   
18/09/2021 15:04:32[INFO] La ejecución masiva ha concluído exitosamente, luego de 00h:02m:09s 
  
