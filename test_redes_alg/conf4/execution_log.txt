18/09/2021 20:14:28[INFO] Iniciando la clasificación serial de todos las redes... 
  
18/09/2021 20:15:27[INFO] Clasificando los resultados... 
  
18/09/2021 20:15:38[INFO] Generando estadísticas... 
  
18/09/2021 20:15:38[RESULT] Se analizaron 30 redes de petri, de las cuales:   
18/09/2021 20:15:38[RESULT] 30 se pudieron clasificar   
18/09/2021 20:15:38[RESULT] 26 no contenian DEADLOCK inicialmente   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210918-201424_3  |    00h:00m:01s    |
| 20210918-201424_22 |    00h:00m:01s    |
| 20210918-201424_24 |    00h:00m:01s    |
| 20210918-201424_19 |    00h:00m:01s    |
| 20210918-201424_17 |    00h:00m:01s    |
| 20210918-201424_10 |    00h:00m:01s    |
| 20210918-201424_25 |    00h:00m:01s    |
| 20210918-201424_27 |    00h:00m:01s    |
| 20210918-201424_29 |    00h:00m:01s    |
| 20210918-201424_7  |    00h:00m:01s    |
| 20210918-201424_4  |    00h:00m:02s    |
| 20210918-201424_20 |    00h:00m:02s    |
| 20210918-201424_12 |    00h:00m:02s    |
+--------------------+-------------------+ 

18/09/2021 20:15:38[RESULT] 0 no eran del tipo S3PR   
18/09/2021 20:15:38[RESULT] 0 no pudieron clasificarse debido a corte por timeout del petrinator   
18/09/2021 20:15:38[RESULT] 0 expirementaron fallas inesperadas durante el análisis   
18/09/2021 20:15:38[RESULT] 34 cumplen las condiciones de tener DEADLOCK y ser tipo S3PR   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210918-201424_13 |    00h:00m:01s    |
| 20210918-201424_2  |    00h:00m:01s    |
| 20210918-201424_23 |    00h:00m:01s    |
| 20210918-201424_16 |    00h:00m:01s    |
| 20210918-201424_21 |    00h:00m:01s    |
| 20210918-201424_0  |    00h:00m:01s    |
| 20210918-201424_26 |    00h:00m:01s    |
| 20210918-201424_6  |    00h:00m:01s    |
| 20210918-201424_9  |    00h:00m:01s    |
| 20210918-201424_14 |    00h:00m:01s    |
| 20210918-201424_18 |    00h:00m:01s    |
| 20210918-201424_8  |    00h:00m:02s    |
| 20210918-201424_28 |    00h:00m:02s    |
| 20210918-201424_15 |    00h:00m:02s    |
| 20210918-201424_5  |    00h:00m:02s    |
| 20210918-201424_11 |    00h:00m:02s    |
| 20210918-201424_1  |    00h:00m:02s    |
+--------------------+-------------------+ 

18/09/2021 20:15:38[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
18/09/2021 20:15:38[RESULT] - 113.33% de las redes válidas cumplen las condiciones esperadas -- Calidad del Algoritmo   
18/09/2021 20:15:38[RESULT] - 86.67% no cumplen condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
18/09/2021 20:15:38[RESULT] - 0.00% se debieron descartar por problemas petrinator -- Incidencias debido a factores externos   
18/09/2021 20:15:38[INFO] La ejecución masiva ha concluído exitosamente, luego de 00h:01m:09s 
  
