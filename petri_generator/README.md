# Generador de Redes de Petri

> Este generador tiene como objetivo poder generar de forma masiva y aleatoria > Redes de Petri del tipo **S3PR** (Systems of Simple Sequential Pro-cesses with Resources).
> 
> Los modelos de red de este tipo hacen referencia a *Sistemas de Procesos Secuenciales Simples con Recursos*, los cuales deben cumplir las siguientes consideraciones:
>> 1. Cada proceso debe contener al menos una plaza operacional, una plaza de recursos y una plaza idle (también operacional), en donde las plazas idle no pertenecen a un recurso.
>> 2. Todos los recursos deben tener transacciones en común con las plazas operacionales.
>> 3. La plaza posterior y anterior a cada recurso debe ser una plaza operacional. Todos los recursos deben tener una transición de entrada y salida diferente.
>> 4. La plaza posterior y anterior a la idle no debe ser un recurso.
>> 5. El conjunto de transiciones de un proceso debe ser un T-Invariante, pero un T-invariante no necesariamente siempre modela un proceso.
>> 6. Cada circuito contiene una plaza idle.
>> 7. Las plazas compartidas entre distintos circuitos debenser recursos.

## Liberia para generar Redes de Petri
> Esta librería permite generar Redes de Petri de manera aleatoria basándose en 5 figuras diferentes que se definen a continuación. Cada red generada es guardada en un archivo *.pflow* con el nombre "net.pflow".
>
> ### Figuras
> La librería cuenta con funciones para generar diferentes *figuras* que luego son utilizadas para generar la red de Petri en su totalidad. Cada función recibe por parametro la cantidad de *figuras* a generar y los ejes **x** e **y** para la posición donde se dibuja en el archivo *.pflow*.
>
> Cada función devuelve la matriz un array con todas las matrices I+ e I- de cada una de las figuras que se generaron.
>
> Las formas permitidas son:
>
>> - **Tren**: Un tren cuenta con una secuencia de transiciones y plazas. La función **generate_trains** se utiliza para generarlos y tambien se encarga de guardarlos en el archivo *net.pflow*. Cada tren tiene un número aleatorio de transiciones, plazas y recursos propios. La cantidad mínima y máxima de transiciones es configurable con las variables globales **min_transitions_trains** y **max_transitions_trains** respectivamente. Los recursos se crean aleatoriamente a partir de la cantidad de plazas que tiene el tren. Por ej: Si un tren tiene 7 plazas, se generaran 7 o menos recursos.
>>
>> <p style="text-align:center;"><img src='../img/train.png'></img></p>
>
>> - **Circuito**: Un circuito se compone de un tren, con sus recursos y se agrega una plaza *idle*. La plaza *idle* es el comienzo del circuito, donde la cantidad de tokens de esta plaza se genera aleatoriamente y se puede especificar el máximo con la variable global **max_tokens_idle**.
>>
>> <p style="text-align:center;"><img src='../img/circuit.png'></img></p>
>
>> - **Fork/join**: Un *fork* es un proceso que se divide en dos y un *join* es donde vuelve a unirse. Este método genera una plaza inicial, de la cual salen dos procesos o trenes, y una plaza final, en la cual se vuelven a unir.
>>
>> <p style="text-align:center;"><img src='../img/fork-join.png'></img></p>
>
>> - **Circuito Fork/join**: A diferencia de la forma anterior donde se genera una plaza inicial, de la cual salen dos procesos, y una plaza final, en la cual se vuelven a unir, este método genera además una plaza *idle* la cual se conecta a través de las correspondientes transiciones a la plaza inicial (fork) y a la final (join).
>>
>> <p style="text-align:center;"><img src='../img/cir-fork-join.png'></img></p>
>
>> - **Circuito Fork/join con tren inicial**: La figura base es igual a la del fork/join, salvo que en este caso se concatena un tren antes de este, resultando un proceso que inicia con un tren  que se bifurca y vuelve a unirse al final.
>>
>> <p style="text-align:center;"><img src='../img/init-train-fj.png'></img></p>
>
>> - **Circuito Fork/join con tren final**: La figura base es igual a la del fork/join, salvo que en este caso se concatena un tren después del fork/join. De esta forma el proceso inicia con una bifurcación la cual se une, y finaliza con un tren.
>>
>> <p style="text-align:center;"><img src='../img/final-train-fj.png'></img></p>
>
>> - **Circuito Fork/join con tren inicial y final**: La figura base es igual a la del fork/join, salvo que en este caso se concatena un tren antes y después del fork/join. De esta forma el proceso inicia con un tren que se bifurca, vuelve a unirse y finaliza con un tren.
>>
>> <p style="text-align:center;"><img src='../img/init-final-fj.png'></img></p>
>
> ### Recursos compartidos
> Con el fin de cumplir con el enunciado de las redes tipo **S3PR**, las distintas redes deben estar conectadas a través de recursos compartidos, los cuales son generados y conectados de manera aleatoria entre las distintas figuras. Por ejemplo, en la siguiente imagen se tienen 2 figuras, un circuito y un circuito fork/join conectados por 3 recursos.
>
> <p style="text-align:center;"><img src='../img/shared-res.png'></img></p>
>
> Con el fin de poder generar redes con la mayor diversidada posible, se decidió diferenciar los recursos en 2 tipos.
>> ### Recursos compartidos simples
>> Esta clase de recursos, tal como lo especifica su nombre, son recursos simples que se encuentran conectados a 2 invariantes distintos. Por defecto, las redes se generan con esta clases de recursos, cuya cantidad, en caso de no estar especificada por parámetro, se encuentra dentro de un rango definido por la cantidad de figuras incrementado en 3 y el doble de la cantidad mínima.
>>
>> Todos los recusos generados en esta red son de este tipo.
> <p style="text-align:center;"><img src='../img/simple_src.png'></img></p>
>
>> ### Recursos compartidos complejos
>> Estos recursos, a diferencia de los anteriores, conectan más de 2 invariantes diferentes. Por defecto, las redes generadas no contienen este tipo de recursos a menos que se especifique por parámetro.
>> 
>> En la siguiente imagen podemos observar los mismos en las plazas número 23, 24 y 25.
> <p style="text-align:center;"><img src='../img/cpx_src.png'></img></p>
>

## Generador automático
> Con el fin de agilizar la creación de redes de Petri, se dispone también del script **auto_gen.py** para automatizar la generación de las mismas.
>
> Este script recibe los siguientes parámetos:
>> - **"-n" o "--net_num"**: Cantidad de redes de petri a generar (obligatorio).
>> - **"-o" o "--out_dir"**: Directorio donde se almacenarán las redes generadas (./gen_nets por defecto).
>> - **"-c" o "--circuits"**: Cantidad circuitos aleatorios a generar en la red (1 por defecto).
>> - **"-ifj" o "--init_fj"**: Cantidad circuitos con fork/join y tren inicial aleatorios a generar en la red (1 por defecto).
>> - **"-ffj" o "--final_fj"**: Cantidad circuitos con fork/join y tren final aleatorios a generar en la red (1 por defecto).
>> - **"-iffj" o "--init_final_fj"**: Cantidad circuitos con fork/join y tren inicial y final aleatorios a generar en la red (1 por defecto).
>> - **"-fj", "--fork_join"**: Cantidad circuitos con fork/join aleatorios a generar en la red (1 por defecto).
>> - **"-cr", "--cmpx_src"**: Cantidad de recursos compartidos complejos a generar en la red (0 por defecto).
>> - **"-msr", "--min_shrd_src"**: Cantidad mínima de recursos compartidos simples a generar en la red. Si no se especifica el valor se define como la cantidad de figuras solicitadas más 3.
>> - **"-Msr", "--max_shrd_src"**: Cantidad máxima de recursos compartidos simples a generar en la red. Si no se especifica el valor se define como el doble de la cantidad mínima de recursos compartidos.
>
> Las redes solicitadas se generaran cada una dentro de un directorio nuevo en el directorio especificado con el argumento *-o* o */gen_nets* por defecto, donde el directorio de cada red recibe como nombre la *fecha-hora* en que se se generó y cada red con el nombre *net.pflow* como se mencionó anteriormente.
>> ```
>> gen_nets/
>>      |---20210621-220704_0/
>>                |-----------net.plfow
>>      |---20210621-220705_0/
>>                |-----------net.plfow
>>      |---20210621-220706_0/
>>                |-----------net.plfow
>>      |---20210621-220707_0/
>>                |-----------net.plfow
>>      |---20210621-220708_0/
>>                |-----------net.plfow
>> ```
>
> Ejemplo de uso:
>> ```
>>  python3 auto_gen.py -n 1 -o ./gen_nets -c 3 -fj 1 -msr 1 -Msr 3 -cr 3
>> ```

## Clasificador de redes
> Con el fin de obtener métricas sobre la calidad de redes generadas, se desarrolló un algoritmo para determinar las caracterísiticas principales de las mismas.
>
> Este algoritmo clasifica las redes dependiendo si no tienen ***deadlock***, no son del tipo ***S3PR***, cumplen ambas condiciones anteriores o no pudieron ser clasificadas por algún error en Petrineitor.
>
> Este script recibe el siguiente parámeto:
>> - **"-nd" o "--networks_directory"**: Ruta a la carpeta contenedora de las redes a clasificar. (Por defecto './test_networks').
>
> Este algorimo genera un archivo de salida con las estadísticas de las redes analizadas con el siguiente formato:
>
> <p style="text-align:center;"><img src='../img/stats.png'></img></p>
>
> Además, reorganiza las redes contenidas en el directorio a analizar según hayan sido clasificadas en las siguientes carpetas:
>
>> - **FAILED**: Redes en las que se produjo un error de Petrineitor al intentara clasificarlas.
>> - **NO_CLASSIFIED**: Redes que no pudieron ser clasificadas por alguna causa. (Ej: timeout).
>> - **NO_DEADLOCK**: Redes que no poseen ***deadlock*** inicial.
>> - **NO_S3P3**: Redes que no son del tipo ***S3PR***.
>> - **OK**: Redes que cumplen las condiciones de poseer ***deadlock*** y ser del tipo ***S3PR***.
>
> Aquí se muestra cómo queda organizado el directorio luego de la clasificación.
>> ```
>> test_networks/
>>      |--- FAILED
>>          |---20210621-220704_0/
>>                |-----------net.plfow
>>      |--- NO_CLASSIFIED
>>          |---20210621-220705_0/
>>                |-----------net.plfow
>>      |--- NO_DEADLOCK 
>>          |---20210621-220706_0/
>>                |-----------net.plfow
>>      |--- NO_S3P3
>>          |---20210621-220707_0/
>>                |-----------net.plfow
>>      |--- OK
>>          |---20210621-220708_0/
>>                |-----------net.plfow
>> ```
>
> Ejemplo de uso:
>> ```
>>  python3 massive_classifier.py -nd ./test_networks
>> ```
>

## Autores
> - **Lenta, Luis Alejandro**
> - **Monsierra, Lucas Gabriel**
> - **Nicolaide, Christian**
