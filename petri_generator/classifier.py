#!/usr/bin/python3

"""
Clasificación automática de una red de Petri

Analiza las RdP y determina si dicha red posee deadlock y si es del tipo S3PR.

Autores:
- Lenta, Luis Alejandro
- Monsierra, Lucas Gabriel
- Nicolaide, Christian
"""

import os
import sys
import argparse
import subprocess
from datetime import datetime

##### INICIO CONFIGURACION GLOBAL #####

# PRINT OPTIONS: Se puede agregar o quitar labels de debug al igual que excepciones
custom_print_level = ["[INFO]", "[ACTION]", "[REWARD]", "[STATE]", "[INPUT]", "[ERROR]", "[DEBUG]",
                      "[RESULT]"]       # Tags que se desean loguear, omitimos [INSTRUCTION]
custom_print_exceptions = ["---", "\n"] # Excepciones del custom_print
custom_print_datefmt = "%d/%m/%Y %H:%M:%S"  # Formato del timestamp del log
show_label = True                       # Mostrar/ocultar el label del tag
log_activity = False                    # Habilitar/deshabilitar el logueo a archivo de la salida por consola
format_text = True                      # Habilitar/deshabilitar el formateo de texto en base a los tags
INFO = '\033[94m'                       # Azul
ACTION = '\033[96m'                     # Cyan
REWARD = '\033[92m'                     # Verde
STATE = '\033[93m'                      # Amarillo
INPUT = '\033[0m'                       # Sin formato especial
ERROR = '\033[91m'                      # Rojo
DEBUG = '\033[0;37;45m'                 # Fondo magenta letra blanca
RESULT = '\033[6;30;43m'                # Fondo amarillo letra negra
ENDC = '\033[0m'                        # Clear format

# PETRINATOR OPTIONS: Se construye el comando para ejecutar la red: java_command<string> petrinator_path<string> pflow_file<string> output_folder<string> bring_gui<boolean> auto_close<boolean> current_iteration<int> clasify<boolean> redirect_output<string>
java_command = "java -Xms12G -Xmx12G -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=20 -XX:ConcGCThreads=5 -XX:InitiatingHeapOccupancyPercent=70 -jar"                                   # Comando java
petrinator_path = "../auto_petrinator/Petrinator-1.0-jar-with-dependencies.jar"  # Path al jar automizado
pflow_file = "../test_path/net.pflow"   # Ruta al pflow por defecto (ojo esta hardcodeado net.pflow en el jar)
output_folder = "../test_path"          # Carpeta de salida al pflow (ojo esta hardcodeado net.pflow en el jar)
bring_gui = "false"                     # Habilitar o deshabilitar la gui de petrinator para ver la red
auto_close = "true"                     # Con auto_close en false y bring_gui en true se puede hacer debug paso a paso viendo evolución graficamente
current_iteration = 0                   # Para saber si se corre el análisis S3PR y deadlock o no
clasify = "true"                        # Flag para indicar si hago sólo la clasificación de redes o corro el análisis incial también
redirect_output = "> /dev/null"         # Quitar "> /dev/null" para debug del jar
petrinator_timeout = 300                # Si tarda más que esto, corta la ejecucion de petrinator y marca la red como NO.CLASSIFIED

# ALGORITHM OPTIONS: Variables que usa el algoritmo para operar
name_pflow = ""                         # Nombre del pflow
txt_path = "../test_path/txt_info"      # Carpeta de salida de los txt
logfile_name = "/execution_log.txt"     # Nombre del archivo de log (se guarda en carpeta de txt)
auto_createdir = True                   # Permite creación automática del output_folder

###### FIN CONFIGURACION GLOBAL ######


def custom_print(text1, text2="", text3="", text4="", show_label=show_label, log_activity=log_activity,
                 format_text=format_text, log_path=None):
    """
    Imprime hasta 4 strings si el tag del primero esta incluido en la lista custom_print_level.

    Parameters
    ----------
        text1          -- Requerido: texto tageado a imprimir
        text2          -- Opcional: texto adicional (Default: "")
        text3          -- Opcional: texto adicional (Default: "")
        text4          -- Opcional: texto adicional (Default: "")
        show_label     -- Opcional: flag para activar/desactivar impresion del tag (Default: "True")
        log_activity   -- Opcional: flag para activar/desactivar logueo en archivo de texto (Default: "False")
        format_text    -- Opcional: flag para activar/desactivar el formateo de texto (Default: "True")
        log_path       -- Opcional: path al archivo de log que se quiere escribir
    """
    if log_path is None and log_activity:
        dir_path(txt_path, True)
        log_path = txt_path + logfile_name

    # Registro la hora
    timestamp = datetime.now().strftime(custom_print_datefmt)

    # El label siempre viene en el primer string, entonces lo spliteo por espacios
    # y me quedo con el primer elemento del array que seria el tag
    substring = text1.split(" ")

    # Si el texto es una excepción no hago el chequeo de tags
    for exception in custom_print_exceptions:
        # Solo verifico que sea un unico string y que la excepción este contenida en ese texto
        if ((exception in text1) and text2 == "" and text3 == "" and text4 == ""):
            print(text1, text2, text3, text4)
            # Si el log esta activado, escribo archivo
            if (log_activity):
                with open(log_path, "a+") as log:
                    print(text1, text2, text3, text4, file=log)
            return

    # Le saco los corchetes al tag para luego llamar la variable global de mismo nombre que contiene el formato a usar
    format = substring[0].replace("[", "").replace("]", "")

    # Chequeo si el tag esta en la lista de lo que deseo imprimir
    if (substring[0] in (custom_print_level)):
        # Chequeo si tengo que imprimir etiqueta
        if (show_label):
            if (format_text):
                # En caso positivo, imprimo timestamp + formato desado + text1..n + vuelvo formato default
                print(timestamp + globals()[format], text1, text2, text3, text4, ENDC)
            else:
                # En caso negativo, imprimo timestamp + text1..n
                print(timestamp + text1, text2, text3, text4)

            # Si el log esta activado, escribo archivo también (sin formato independientemente de format_text)
            if (log_activity):
                with open(log_path, "a+") as log:
                    print(timestamp + text1, text2, text3, text4, file=log)
        else:
            if (format_text):
                # En caso negativo, le saco el tag y el espacio separador después de el y agrego timestamp y formato deseado
                print(timestamp + globals()[format], text1.replace(substring[0] + " ", ""), text2, text3, text4, ENDC)
            else:
                # En caso negativo, le saco el tag y el espacio separador después de el y agrego timestamp
                print(timestamp + text1.replace(substring[0] + " ", ""), text2, text3, text4)

            # Si el log esta activado, escribo archivo también (sin formato independientemente de format_text)
            if (log_activity):
                with open(log_path, "a+") as log:
                    print(timestamp + text1.replace(substring[0] + " ", ""), text2, text3, text4, file=log)


def call_petrinator():

    # Armo el comando a ejecutar
    petrinator_cmd = java_command + " " + petrinator_path + " " + pflow_file + " " + output_folder + " " + bring_gui + " " + auto_close + " " \
                        + "0" + " " + clasify + " " + redirect_output

    custom_print("\n--------------------------------------------------------------------------")
    custom_print("[INFO] Ejecutando petrinator para conocer estado de la red...")
    custom_print("--------------------------------------------------------------------------\n")

    # Lo ejecuto, y controlo si se llega al timeout
    petrinator = subprocess.Popen(petrinator_cmd, shell=True)
    try:
        code = petrinator.wait(petrinator_timeout)
        if code == 1:
            # Si no es S3PR abandono el análisis
            if os.path.isfile(output_folder + "/NO.S3PR"):
                custom_print("[STATE] La red ingresada no es S3PR, cerrando análisis", "\n")
                exit(1)

            # Si no hay DEADLOCK abandono el análisis
            if os.path.isfile(output_folder + "/NO.DEADLOCK"):
                custom_print("[STATE] La red ingresada no tiene deadlock, cerrando análisis", "\n")
                exit(1)

            custom_print("[ERROR] Se produjo un error en el petrinator. ", "\n")
            subprocess.Popen("touch " + output_folder + "/FAILED", shell=True).communicate()
            exit(1)
    except subprocess.TimeoutExpired:
        petrinator.kill()
        custom_print("[ERROR] Se alcanzo el limite de " + str(
            petrinator_timeout) + " segundos para la ejecucion de petrinator, cerrando análisis", "\n")
        subprocess.Popen("touch " + output_folder + "/NO.CLASSIFIED", shell=True).communicate()
        exit(1)

    # En caso de no salir por ninguna de las opciones anteriores se trata de una red con DEADLOCK y S3PR
    subprocess.Popen("touch " + output_folder + "/NET.OK", shell=True).communicate()
    custom_print("[STATE] La red ingresada posee DEADLOCK y es S3PR", "\n")

def dir_path(string, auto_createdir=auto_createdir):
    if os.path.isdir(string):
        return string

    # Crear directorio si no existe y auto_createdir = True
    if ((not os.path.exists(string)) and auto_createdir):
        os.makedirs(string)
        return string
    else:
        raise NotADirectoryError(string)
    

def file_path(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)


def main(pf, of):
    global pflow_file, output_folder

    pflow_file = pf
    output_folder = of

    custom_print("[DEBUG] pflow_file: ", pflow_file)
    custom_print("[DEBUG] output_folder: ", output_folder)

    custom_print(
        "[INSTRUCTION] Bienvenido: Este programa es capaz de clasificar una red según posea DEADLOCK o sea del tipo S3PR.")
    custom_print("[INSTRUCTION] Sólo necesita:")
    custom_print("[INSTRUCTION]    - Una red de petri en formato .pflow.")
    custom_print("[INSTRUCTION]    - Programa Petrinator (versión automatizada) para el análisis de la red.")
    custom_print("[INSTRUCTION] Una vez ingresado el nombre del archivo .pflow, el programa obtiene la información de la red.")

    call_petrinator()


if __name__ == "__main__":
    # Si se corre el script sin argumentos usa los default del setup inicial
    # si no verifica que se pasen los 2 argumentos
    if len(sys.argv) > 1:
        ap = argparse.ArgumentParser(description='Paths de entrada y salida del algoritmo')
        required = ap.add_argument_group('Argummentos requeridos')

        required.add_argument("-pf", "--pflow_file", type=file_path, required=True, default=pflow_file,
                                help="Ruta al archivo net.pflow que se usará durante el anális")
        required.add_argument("-of", "--output_folder", type=dir_path, required=True, default=output_folder,
                                help="Directorio de salida donde se escribirá net.pflow procesado")

        args = vars(ap.parse_args())

        pflow_file = args['pflow_file']
        output_folder = args['output_folder']

        main(pflow_file, output_folder)

    else:
        main(pflow_file, output_folder)
