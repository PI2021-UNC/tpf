\documentclass[../main.tex]{subfiles}

\begin{document}

El presente informe tiene por objetivo describir el trabajo realizado como Proyecto Integrador de la carrera Ingeniería en Computación de la Facultad de Ciencias Exactas, Físicas y Naturales (FCEFyN) de la Universidad Nacional de Córdoba (UNC). A continuación, se desarrollará la formulación del problema que se pretende abordar, los objetivos propuestos, la metodología empleada para alcanzarlos, como así también los resultados y conclusiones a los cuales se arribaron.\\

Las redes de Petri se utilizan ampliamente en el campo del modelado de sistemas de fabricación flexible (FMS) debido a sus características intrínsecas como concurrencia, sincronización, conflictos y activación de eventos. Sin embargo, diseñar el modelo para una FMS a través de una Red de Petri puede resultar extremadamente tedioso y difícil de depurar. Por lo tanto, un generador automático de redes de Petri que permiten modelar sistemas FMS es una herramienta práctica y de utilidad para estudiar dichos sistemas.

En este trabajo final se desarrolla un generador automático, aleatorio y configurable por el usuario, que de manera sistemática crea redes de Petri. Estas redes generadas modelan sistemas sintéticos y abstractos que poseen las mismas características que los modelos de FMS. El término “Automático” significa que el usuario no necesita diseñar la Red de Petri por sí mismo, sino que el generador será quien la diseñe en base a las definiciones que el usuario realice.

De esta forma se facilitará la posibilidad de realizar un sinfín de estudios en donde la cantidad de redes a analizar debe ser importante para producir resultados concluyentes. Entre los que encontramos, por ejemplo, un algoritmo encargado de solucionar problemas en redes de Petri de tipo S3PR con deadlock mediante la utilización de una red neuronal, la cual necesita de una cierta cantidad de redes para poder ser entrenada.

% introducción general
\section{Antecedentes}
\label{ss:antec}
Este trabajo se encuentra fuertemente ligado a varios proyectos integradores (PI) que fueron desarrollados en el ámbito del Laboratorio de Arquitecturas de Computadoras con anterioridad, e incluso algunos de ellos que aún se encuentran en vías de desarrollo. Surge como emergente durante el desarrollo del PI “Determinación automática mediante inteligencia artificial del control de una red de Petri” publicado en forma paralela por los autores Lenta y Nicolaide \cite{tesislentanicolaide} en el cual se trabajó colaborativamente. Cuando se comenzaron a explorar diferentes alternativas de solución y a validar algunas que habían sido propuestas previamente, aparece la necesidad de contar con una mayor cantidad de redes.

Por otro lado, para el desarrollo de este proyecto se utiliza como punto de partida el trabajo “Integración Módulos Petrinator” de los autores Bosack y Oliva \cite{tesisbosack}, desarrollado en el año 2021, en el cual se introducen mejoras al software Petrinator (descrito en la sección \ref{ss:integra_petri}) necesarias para validar si la red propuesta cumple con las condiciones del subtipo S3PR o no.

El software analizador y editor de redes Petrinator \cite{petrinator} fue a su vez completamente desarrollado por diferentes estudiantes a lo largo de más de 5 años dentro del laboratorio. Esta herramienta provee la salida de diferentes tipos de análisis sobre la red, los cuales son post procesados por el algoritmo del generador. Se debe mencionar que el generador que aquí se presenta es completamente independiente del software Petrinator, pudiendo este ser reemplazado por cualquier otro software analizador de redes de Petri disponible capaz de verificar que se cumplen las condiciones suficientes para ser del subtipo S3PR y si la red cuenta con deadlock o no.

\section{Relevancia}
\label{ss:relev}
En diversos sectores productivos, la programación de actividades reviste especial importancia por su incidencia en la productividad y en la competitividad de las empresas. En los últimos cinco años los países desarrollados han presenciado un avance más rápido en la aplicación de alta tecnología de manufactura, que en los 10 años anteriores. Muchas de esas aplicaciones han ocurrido bajo la insignia de un nuevo tipo de manufactura llamado Sistemas de Manufactura Flexible (FMS). Con la implementación de los nuevos FMS está llegando un nuevo despertar de las oportunidades para la reducción de costos. 
 
Sin embargo, debido a la complejidad de un FMS surgen algunos problemas de diseño y operacionales. Es por ello que existe un interés creciente en el desarrollo e implementación de los sistemas de manufactura flexible. Hoy por hoy se han implementado cientos de esos sistemas por todo el mundo; siendo Japón el líder en términos de la cantidad de aplicaciones y de los éxitos administrativos y organizacionales asociados a éstas implementaciones. Dicho interés creciente en los FMS ha traído muchos problemas nuevos. Los cuales los podemos resumir en dos grandes grupos:

\begin{enumerate}
\item el de diseño
\item el operacional
\end{enumerate}

El primer grupo tiene que ver con la selección óptima de todos los componentes del FMS, y el segundo con su uso óptimo.\\

Las redes de Petri son una herramienta gráfica y matemática de modelación que se puede aplicar en muchos sistemas. Particularmente son ideales para describir y estudiar sistemas que procesan información y con características concurrentes, asíncronas, distribuidas, paralelas, no determinísticas y/o estocásticas. Es por ello que en la actualidad existen muchos trabajos en los que se proponen sistemas FMS modelados mediante redes de Petri.

\section{Motivaciones}
\label{ss:motiv}
Al diseñar un procedimiento, se tiene la incertidumbre si este permitirá, luego de su aplicación, obtener los resultados esperados. Es muy frecuente que, durante su aplicación, emerjan dificultades de coordinación entre las diferentes actividades, tareas y pasos. 

Las redes de Petri fueron utilizadas inicialmente para el análisis de algoritmos en la computación paralela o concurrente, pero dada la complejidad de los procesos productivos actuales, estas constituyen un método alternativo de diseño tanto para el proceso industrial como para el controlador. La fortaleza del modelado de las redes de Petri radica en sus propiedades, que se dividen en dos grandes áreas, las dependientes del marcado inicial llamadas propiedades dinámicas o del comportamiento y las propiedades independientes del marcado, llamadas estructurales o estáticas. 

Este proyecto se basa principalmente en el estudio de la ejecución concurrente de procesos de trabajo en sistemas de fabricación flexible a través de una clase especial de redes de Petri que se denominan S3PR. Por lo general un sistema FMS está estructurado como un grupo de estaciones de trabajo interconectadas por medio de un sistema de transporte de materiales automatizado. Los procesos de trabajo en un FMS son ejecutados al mismo tiempo, y por lo tanto, tienen que competir por el conjunto de recursos comunes. Estas relaciones de competencia pueden causar lo que se conoce como punto muerto o deadlock. 

En términos generales, un deadlock es un estado del sistema en el cual algunos procesos de trabajo nunca pueden terminarse. Generalmente, detrás de un problema de deadlock existe una situación de espera circular para acceder a un conjunto de recursos. Este estado de punto muerto o deadlock provoca que el sistema quede bloqueado y no pueda continuar con su producción, por lo que diseñar un sistema asegurando que esto no suceda, es de suma importancia. 

En el Laboratorio de Arquitecturas de Computadoras se han estado llevando adelante diferentes proyectos que intentan automatizar la detección y eliminación del deadlock en el diseño de un sistema mediante la adición de supervisores. Estos son representados por plazas en las redes de Petri que se añaden para evitar que determinados sifones no se vacíen y así asegurar que el diseño del sistema estará libre de deadlock durante toda su ejecución. Todas las investigaciones realizadas hasta el momento han sido sobre redes de Petri que hemos definido como redes conocidas. Estas son redes recolectadas en los últimos cinco años que modelan diferentes sistemas y que se han utilizado en diferentes papers y estudios para lograr el mismo objetivo de evitar el deadlock en un sistema. 

Hasta el momento se han logrado buenos resultados en los diferentes proyectos llevados a cabo, pero siempre verificando los mismos  utilizando el conjunto de redes conocidas. Para continuar avanzando y mejorando los resultados obtenidos hasta ahora es necesario contar con un conjunto mayor de redes de Petri, tanto para verificar que las soluciones propuestas hasta el momento son aplicables a diferentes sistemas, como así también para tener la posibilidad de comenzar a aplicar Inteligencia Artificial en la detección de los supervisores necesarios con el fin de evitar el deadlock. Sumado a la necesidad de generar redes en cantidad, es necesario también generar variedad, modelando sistemas con diferentes características tales como:

\begin{itemize}
\item Cantidad de recursos compartidos entre procesos.
\item Cantidad de recursos propios a cada proceso. 
\item Cantidad de procesos en paralelo.
\item Cantidad de procesos.
\item Variedad de figuras.
\end{itemize}

\section{Formulación del Problema}
El problema de programación de Sistemas de Fabricación Flexible ha sido estudiado desde variados enfoques que se pueden clasificar en las siguientes categorías: programación matemática [Aanen et al., 1993], [Sawik 1990]; inteligencia artificial y sistemas expertos [Bensana et al., 1988], [Chryssolouris et al.,1988]; inteligencia artificial - redes neuronales [Jain 2001], [Sabuncuoglu, 1998]; técnicas de simulación y reglas de despacho [Li et al., 1997], [Min et al., 1998]; lógica difusa y reglas de despacho [Custodio et al., 1994], [Grabot, Geneste, 1994]; búsqueda tabú [Basnet, 1996], [Logendran, Sonthinen, 1997]; métodos heurísticos [Kimemia, Gershwin, 1983], [Liu, MacCarthy, 1999].

A pesar del evidente interés que despierta este problema, la programación de FMS es aún un problema abierto. Los anteriores enfoques presentan dificultades bien sea en capturar todas las características salientes de dichos sistemas o en presentar métodos de solución eficientes [Lee, DiCesare, 1994].

Otros autores han utilizado RdP para el modelaje de FMS y han aplicado un método heurístico para su programación. Lee y DiCesare [1994], así como Lloyd y Constas [1995], utilizaron una búsqueda de ramificación y acotación para minimizar el tiempo de flujo total (makespan). Saitou et al. [2002] utilizaron RdP para modelar la asignación de los recursos y controlar la programación de la producción con el objeto de lograr programas robustos en FMS. Tiwari et al. [2001] modelan mediante RdP temporizadas el sistema de vehículos guiados  automáticamente de un FMS para asegurar que no se presenten conflictos y bloqueos en el sistema. Jain [2001] propone un software controlador de asignación de recursos para FMS usando RdP y reglas de despacho.

En los últimos trabajos mencionados, las RdP se han utilizado para modelar los FMS o controlar su evolución. En el LAC se ha estado trabajando bajo la misma premisa y con el objetivo de poder lograr agilizar los tiempos de desarrollo de los sistemas embebidos involucrados. Bajo este enfoque, una serie de estudios se encuentran en vías de desarrollo, sin embargo como se mencionó en secciones anteriores no se cuentan con suficientes redes para lograr resultados concluyentes.

\section{Objetivos}
Durante los últimos años el Laboratorio de Arquitectura de Computadoras ha llevado adelante diferentes proyectos e investigaciones con el propósito de lograr evitar el deadlock  en redes de Petri. Específicamente en un tipo de redes que modelan a Sistemas de Fabricación Flexible denominadas S3PR.

Estos proyectos han permitido recopilar a lo largo del tiempo diferentes redes de Petri con variadas características, que modelan distintos sistemas. Todas estas han sido extraídas de diferentes papers e investigaciones donde se intentaban resolver problemas relacionados a la temática de evitar el deadlock en las redes de Petri.  

El conjunto de redes recolectado lo denominamos redes conocidas y cuenta con diferentes redes, las cuales han sido de enorme utilidad para los proyectos realizados hasta el momento. A medida que se ha avanzado en la investigación, uno de los principales problemas es que no se cuenta con la cantidad necesaria de redes para validar los avances obtenidos en diferentes escenarios o para llevar adelante nuevas investigaciones que requieren tener grandes cantidades de datos, como puede ser aplicar Inteligencia Artificial. 

\subsection{Objetivos Generales}
A partir de las problemáticas descritas anteriormente, surgen tres objetivos claves para llevar adelante este proyecto, los cuales detallamos a continuación: 

\begin{itemize}
\item Generar grandes conjuntos de Redes de Petri.
\item Especificar las características que deben contener las Redes de Petri generadas. 
\item Explorar las combinaciones de características que permiten obtener Redes de Petri que cuenten con deadlock. 
\end{itemize}

Estos tres objetivos tienen la finalidad de atacar la problemática explicada en la sección anterior para permitir a futuros proyectos avanzar en nuevas líneas investigación donde se cuente con una mayor cantidad de datos, como así también, comprobar que las diferentes soluciones desarrolladas se adaptan correctamente a Redes de Petri con diferentes características, lo que representa diferentes configuraciones de los sistemas FMS.

\subsection{Objetivos Específicos}

Los objetivos generales permiten tener en claro las metas a cumplir en el desarrollo de este  trabajo. Se decidió extender estos mismos en objetivos específicos que posibiliten comprender de una manera más detallada el propósito que se busca alcanzar:

\begin{itemize}
\item Caracterizar de forma exhaustiva las especificaciones de los sistemas FMS y del subtipo de red de Petri S3PR asociado.
\item Generar redes de Petri en forma automática, aleatoria y configurable.
\item Explorar y definir combinaciones en las configuraciones de las redes de Petri generadas para que estas cuenten con deadlock. 
\item Clasificar las redes de Petri generadas, según sus propiedades.
\item Desarrollar diferentes análisis para verificar la solución propuesta.
\end{itemize}

\section{Requerimientos}
En ingeniería, los requerimientos se utilizan como datos de entrada en la etapa de diseño del producto. Establecen qué debe hacer el sistema, aunque no especifican la manera en que debe hacerlo. La fase de captura y registro de requisitos puede estar precedida por una fase de análisis conceptual del proyecto. A continuación se detalla un listado de los requerimientos definidos para este proyecto. 

\subsection{Listado de requerimientos}
\reqinit
\reqstart
	\item Se debe utilizar el software Petrinator para obtener los datos necesarios relacionados a la Red de Petri modelada en un archivo de extensión .pflow. Además de determinar sus capacidades y alcance.
	\item La integración con el software Petrinator debe ser automática, es decir, el usuario no necesita interactuar con la herramienta Petrinator.
	\item El programa debe ser capaz de generar una cantidad de redes de Petri especificada por el usuario i.
	\item El programa debe permitir al usuario definir las características de las redes de Petri a generar. 
	\item El programa debe producir como salida un conjunto de archivos de extensión .pflow que modelan cada una las redes de Petri generadas.
	\item La arquitectura del programa debe ser modular para permitir su fácil escalabilidad a través de nuevos módulos.
	\item El programa debe ser capaz de clasificar y agrupar en conjuntos definidos las redes de Petri generadas.
	\item El programa debe ser capaz de generar un 80\% o más  del total de las redes de Petri del tipo S3PR.
	\item El programa debe ser capaz de generar al menos 100 redes de Petri de 50 plazas y 50 transiciones en un tiempo menor a 10 segundos.
	\item El tiempo de aprendizaje de uso para un usuario sin entrenamiento previo, con conocimientos en Redes de Petri deberá ser menor a 30 minutos. 	
\reqend

\section{Análisis de riesgos}
Un riesgo es un evento o condición incierta que, en caso de ocurrir, tendrá consecuencias negativas sobre al menos uno de los requerimientos del proyecto. Por esta razón es importante identificarlos con anticipación para mitigarlos.

\subsection{Listado de riesgos}

\begin{table}[ht]
\centering
\begin{tabular}{|l|l|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{ID}} & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}\textbf{Descripción}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
R1 & Herramienta inadecuada                \\ \hline
\rowcolor[HTML]{FFFFFF} 
R2 & Mecanismo de generación inadecuado    \\ \hline
\rowcolor[HTML]{FFFFFF} 
R3 & Escalabilidad limitada                \\ \hline
\rowcolor[HTML]{FFFFFF} 
R4 & Tiempo de aprendizaje de uso excesivo \\ \hline
\rowcolor[HTML]{FFFFFF} 
R5 & Configuraciones inadecuadas           \\ \hline
\rowcolor[HTML]{FFFFFF} 
R6 & Tiempo de respuesta inadecuado        \\ \hline
\end{tabular}
\caption{Tabla de riesgos}
\label{tab:riesg}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R1 - Herramienta inadecuada}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Elección incorrecta de la herramienta para clasificar las Redes de Petri o utilización errónea. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} La herramienta no se adapta a nuestros requerimientos.               \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Sustitución de la herramienta por otra que cumpla nuestros requerimientos. \\ \hline
\end{tabular}%
}
\caption{R1 - Herramienta inadecuada}
\label{tab:r1}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R2 - Mecanismo de generación inadecuado}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Desarrollar un generador que no produzca de manera adecuada Redes de Petri. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Redes de Petri que no cumplen con las especificaciones necesarias.                \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Incremento del tiempo dedicado en el desarrollo para la readecuación del generador. \\ \hline
\end{tabular}%
}
\caption{R2 - Mecanismo de generación inadecuado}
\label{tab:r2}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R3 - Escalabilidad limitada}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} El generador desarrollado no permite añadir fácilmente nuevos módulos y funcionalidades. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} El generador no es reutilizable para nuevos proyectos o investigaciones porque no es escalable.                 \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Limitación del generador para ciertos casos de estudio ó reconstrucción total del generador. \\ \hline
\end{tabular}%
}
\caption{R3 - Escalabilidad limitada}
\label{tab:r3}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R4 - Tiempo de aprendizaje excesivo}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Un nuevo usuario requiere demasiado tiempo en comprender cómo se utiliza el generador. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Retraso en la investigación a desarrollar y pérdida de tiempo en la comprensión de la herramienta.                \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Readecuación del generador para reducir la dificultad de uso.   \\ \hline
\end{tabular}%
}
\caption{R4 - Tiempo de aprendizaje excesivo}
\label{tab:r4}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R5 - Configuraciones inadecuadas}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Las configuraciones de las Redes de Petri a generar no permiten producir redes del tipo S3PR ó redes con deadlock. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Las Redes de Petri generadas no cumplen con la condición de ser del tipo S3PR ó contar con deadlock.                 \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Incumplimiento del principal objetivo.   \\ \hline
\end{tabular}%
}
\caption{R5 - Configuraciones inadecuadas}
\label{tab:r5}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R6 - Tiempo de respuesta inadecuado}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Calidad deficiente del generador de Redes de Petri. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Generar conjuntos de un centenar de Redes de Petri ó más consume demasiado tiempo.          \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto: }Incumplimiento del principal objetivo.    \\ \hline
\end{tabular}%
}
\caption{R6 - Tiempo de respuesta inadecuado}
\label{tab:r6}
\end{table}
\clearpage

\section{Organización del Informe}
El presente informe consta de 5 capítulos y se dividió en cuatro partes. Cada una de las partes contiene un capítulo, además del capítulo introductorio que concluye con la presente sección.

En el primer capítulo de este informe se introduce al lector en la temática abordada, haciéndole notar cuales son los antecedentes y la relevancia del mismo. Luego se exponen las motivaciones junto con una revisión bibliográfica del tema abordado a modo de formulación del problema. Se detallan también los objetivos y los requerimientos tanto de modo global como particular y se exponen algunos de los riesgos identificados antes de su desarrollo.

La primera parte, desarrollada en el segundo capítulo abarca el marco teórico en el cual se desarrollan todos los conceptos necesarios para comprender el funcionamiento de las redes de Petri así como también una definición formal del subtipo S3PR.

En la segunda parte, denominada Marco Metodológico, contenida en el tercer capítulo se detalla en forma exhaustiva cómo se diseñó la solución propuesta, haciendo uso de una arquitectura modular. Además, se describen en forma detallada cada uno de los módulos que la componen.

En la tercera parte o de resultados, presentada en el capítulo cuarto se explican las pruebas realizadas sobre el sistema, se detalla la composición de cada una de ellas y se elaboran conclusiones parciales a raíz de los resultados obtenidos. Estos son luego resumidos en una tabla general y expuestos en diferentes gráficas.

Finalmente, en la cuarta y última parte se elaboran conclusiones generales a partir de los resultados obtenidos y se realizan propuestas de mejora a futuro.
\end{document}