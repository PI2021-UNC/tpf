\documentclass[../main.tex]{subfiles}
\RequirePackage{rotating}
\begin{document}

La programación modular es un paradigma o estilo de programación que consiste en dividir un programa muy grande en programas de menor tamaño o subprogramas con la finalidad de hacerlo más legible y manejable ya que, durante el desarrollo, el mismo puede tornarse muy largo. Para esto se implementa el uso de clases, funciones y módulos.

Se infiere que Python es un lenguaje ideal para este tipo de programas y es por ello que decidió usarlo en este proyecto integrador.

Un módulo es un objeto que sirve como unidad para organizar el código de un proyecto desarrollado en Python. Los módulos tienen en su contenido los objetos (funciones, clases, definiciones, etc) que se definan en él. Para utilizar el contenido de un módulo es necesario importarlo.\\

A continuación se describirá exhaustivamente cada uno de los módulos principales que componen esta tesis. Los archivos correspondientes a éstos  pueden ser accedidos a través del siguiente link al repositorio \url{https://gitlab.com/PI2021-UNC/tpf/-/tree/our/petri_generator}. 

\section{Generación}
En la presente sección se explica el diseño estructural, la implementación y desarrollo del generador aleatorio de redes de Petri configurable.\\

Como se mencionó en secciones anteriores, las redes que precisamos generar son redes del tipo S3PR (Systems of Simple Sequential Processes with Resources), dado que modelan la ejecución concurrente de procesos de trabajo. La finalización de alguno de estos puede iniciar más de una nueva operación. Estas redes cumplen con las siguientes características básicas:

\begin{itemize}
\item Cada proceso debe contener al menos una plaza operacional, una plaza de recursos y una plaza idle (también operacional), en donde las plazas idle no pertenecen a un recurso.
\item Todos los recursos deben tener transacciones en común con las plazas operacionales.
\item La plaza posterior y anterior a cada recurso debe ser una plaza operacional.
\item Todos los recursos deben tener una transición de entrada y salida diferente.
\item La plaza posterior y anterior a la idle no debe ser un recurso.
\item El conjunto de transiciones de un proceso debe ser un T-Invariante, pero un T-invariante no necesariamente siempre modela un proceso.
\item Cada circuito contiene una plaza idle.
\item Las plazas compartidas entre distintos circuitos deben ser recursos.
\end{itemize}

Como resultado de estas características, pueden ocurrir dos situaciones: \textbf{conflicto} y \textbf{deadlock}. 

\begin{itemize}
\item El \textbf{conflicto} puede ocurrir cuando dos o más procesos requieren un recurso común al mismo tiempo. Por ejemplo, dos estaciones de trabajo pueden compartir un sistema de transporte común o necesitar acceso al mismo almacenamiento. Una forma sencilla de resolver el conflicto es asignar un nivel de prioridad a cada uno de los procesos.
\item El \textbf{deadlock} puede suceder al compartir dos recursos entre los dos procesos. En este caso, se puede alcanzar un estado en el que ninguno de los procesos puede continuar. Teniendo en cuenta que uno de los procesos puede continuar si el conflicto se puede resolver, mientras que en el caso de deadlock no se puede hacer nada para que el sistema vuelva a funcionar.
\end{itemize}

Se decidió estructurar el generador en 4 partes bien definidas, con el fin de lograr un correcto desacoplamiento de las funcionalidades y así disponer de una mayor escalabilidad.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/component_gen.png}
	\caption{Diagrama de componentes de la solución propuesta.}
	\label{fig:component_gen}
  \end{figure}

\subsection{Generador}
Toda la lógica propia del generador se encuentra en este archivo, contiene todos los métodos necesarios para generar un archivo de extensión .pflow que contiene distintas figuras.
Estas figuras son generadas en base a las especificaciones de las redes tipo S3PR antes mencionadas y están basadas en 2 formas básicas: \textbf{trenes} y \textbf{circuitos}.

\begin{itemize}
\item \textbf{Trenes:} está compuesto por una secuencia de transiciones y plazas. Cada tren se crea con un número de transiciones aleatorio contenido dentro un rango configurable. La cantidad de plazas generadas dependen del número de transiciones. Además, se generan aleatoriamente recursos privados a partir de la cantidad de plazas del mismo. Estos recursos son propios de un tren y están conectados únicamente a 2 transiciones del mismo invariante. Por ejemplo, si un tren tiene 7 plazas, se generarán 7 o menos recursos privados.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./img/capitulo2/train.png}
	\caption{Trenes.}
	\label{fig:train}
  \end{figure}

\item \textbf{Circuitos:} está compuesto por una plaza idle conectada al inicio y final de un tren. Las plazas idle a diferencia de las plazas que componen el tren (excluyendo los recursos), poseen tokens cuya cantidad se determina aleatoriamente dentro de un rango configurable.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./img/capitulo2/circuit.png}
	\caption{Circuitos.}
	\label{fig:circuit}
  \end{figure}  
\end{itemize}

A partir de estas 2 figuras base se construyen figuras más complejas: \\

\begin{itemize}
\item \textbf{Fork-join:} esta figura se compone por 2 trenes conectados al comienzo y al final. Fork hace referencia a la parte del proceso donde se divide en dos y join donde se unifica. Para realizar las conexiones de los trenes se crean 2 plazas adicionales, una plaza que se conecta al comienzo de los 2 trenes y otra al final de los mismos. 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/fork-join.png}
	\caption{Fork-join.}
	\label{fig:fork-join}
  \end{figure}

\item \textbf{Circuito Fork-join:} de forma similar como sucede con las formas anteriores, para generar los circuitos se añade una plaza idle con tokens, y 2 transiciones, una previa a la plaza inicial y otra posterior a la plaza final a la que se conecta la plaza idle.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/cir-fork-join.png}
	\caption{Circuito Fork-join.}
	\label{fig:cir-fork-join}
  \end{figure}

\item \textbf{Circuito Fork-join con tren inicial:} esta figura es una variante de la figura Circuito Fork-join. Está compuesta por una figura Fork-join base, donde la plaza inicial se conecta a la transición final de un tren. Finalmente, para cerrar el circuito, se añade una transición conectada a la plaza final de la figura Fork-join y una plaza idle con tokens aleatorios que se conecta al comienzo del tren y a la transición posterior a la plaza final del Fork-join.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/init-train-fj.png}
	\caption{Circuito Fork-join con tren inicial.}
	\label{fig:init-train-fj}
  \end{figure}

\item \textbf{Circuito Fork-join con tren final:} está compuesta por una figura de un tren y un Fork-join igual que el anterior, pero en este caso se conectan de manera inversa. La transición inicial del tren se encuentra conectada a la plaza final de la figura Fork-join mientras que se crea una transición que se conecta a la plaza inicial de este. Por último se genera la plaza idle con tokens aleatorios, conectada a la transición inicial agregada y a la transición final del tren.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/final-train-fj.png}
	\caption{Circuito Fork-join con tren final.}
	\label{fig:final-train-fj}
  \end{figure}

\item \textbf{Circuito Fork-join con tren inicial y final:} esta figura es una combinación de las 2 anteriores. Comienza con tren, cuya transición final se conecta a la plaza inicial de una figura Fork-join, y finaliza con otro tren cuya transición inicial está conectada a la plaza final de la figura Fork-join. Se añade una plaza idle con tokens aleatorios conectada a la transición inicial del tren inicial y a la transición final del tren final.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/init-final-fj.png}
	\caption{Circuito Fork-join con tren inicial y final.}
	\label{fig:init-final-fj}
  \end{figure}
\end{itemize}

Estas figuras por sí solas no cumplen con todas las características necesarias de las redes del tipo S3PR, como por ejemplo que las plazas compartidas entre distintos circuitos deben ser recursos, pero es posible lograrlo combinando estas figuras y añadiendo recursos compartidos que las interconectan. 
Para esto se decidió generar 2 tipos de recursos compartidos diferentes con el objetivo de lograr mayor diversidad de redes, los cuales son generados y conectados de manera aleatoria entre las distintas figuras.\\

\begin{itemize}
\item \textbf{Recursos compartidos simples:} esta clase de recursos, tal como lo especifica su nombre, son recursos simples que se encuentran conectados a 2 invariantes distintos. Por defecto, las redes se generan con esta clase de recursos, donde la cantidad a generar es un valor aleatorio contenido dentro un rango parametrizable.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.43]{./img/capitulo2/simple_src.png}
	\caption{Red con recursos compartidos simples.}
	\label{fig:simple_src}
  \end{figure}

\item \textbf{Recursos compartidos complejos:} a diferencia de los anteriores, estos recursos conectan como mínimo 3 invariantes diferentes y como máximo la cantidad total de invariantes que posee la red. Por defecto, las redes generadas no contienen este tipo de recursos a menos que se especifique por parámetro.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.41]{./img/capitulo2/cpx_src.png}
	\caption{Red con recursos compartidos complejos.}
	\label{fig:cpx_src}
  \end{figure}
\end{itemize}

Se intentó dar un enfoque totalmente parametrizable y aleatorio, de forma tal que combinando las distintas figuras disponibles en distintas cantidades, variando los rangos de cantidades de tokens y recursos se puedan generar un sinnúmero de redes distintas. Estos parámetros adicionales que se mencionan son rango mínimo y máximo de tokens idle en las plazas idle y rango mínimo y máximo de transiciones por tren, ambos parametrizables desde el código del generador.\\

Gracias a todas estas herramientas contenidas, es posible generar redes de Petri que cumplan con todas las características para ser consideradas del tipo S3PR.

\subsection{Generador masivo}
\label{ss:gen-mas}
Con el objetivo de aislar la lógica propia de la generación de las redes de Petri, se decidió crear un módulo adicional encargado de, utilizando el módulo de generación, crear masivamente redes con una configuración determinada.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/generator_seq.png}
	\caption{Diagrama de secuencia del generador masivo.}
	\label{fig:generator_seq}
  \end{figure}

Este generador masivo saca provecho del enfoque parametrizable que se le dió al módulo de generación, de forma que recibiendo los siguientes parámetros nos permite automatizar la generación:

\begin{itemize}
	\item \textit{“-n” o “--net\_num”}
	\subitem \textbf{Descripción:} Cantidad de redes de Petri a generar (obligatorio).
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} sí.
	\subitem \textbf{Valor por defecto:} 1.\\

	\item \textit{“-o” o “--out\_dir”}
	\subitem \textbf{Descripción:} Directorio donde se almacenarán las redes generadas (./gen\_nets por defecto).
	\subitem \textbf{Tipo:} Ruta de directorio.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} ‘/gen\_nets’\\

	\item \textit{“-c” o “--circuits”}
	\subitem \textbf{Descripción:} Cantidad circuitos aleatorios a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} 0.\\

	\item \textit{“-ifj” o “--init\_fj”}
	\subitem \textbf{Descripción:} Cantidad circuitos Fork-join con tren inicial aleatorios a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido}: No.
	\subitem \textbf{Valor por defecto:} 0.\\

	\item \textit{“-ffj” o “--final\_fj”}
	\subitem \textbf{Descripción:} Cantidad circuitos con Fork-join con tren final aleatorios a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} 0.\\

	\item \textit{“-iffj” o “--init\_final\_fj”}
	\subitem \textbf{Descripción:} Cantidad circuitos con Fork-join con tren inicial y final aleatorios a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} 0.\\

	\item \textit{“-fj” o “--fork\_join”}
	\subitem \textbf{Descripción:} Cantidad circuitos con Fork-join aleatorios a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} 0.\\

	\item \textit{“-cr” o “--cmpx\_src”}
	\subitem \textbf{Descripción:} Cantidad de recursos compartidos complejos a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} 0.\\

	\item \textit{“-msr” o “--min\_shrd\_src”}
	\subitem \textbf{Descripción:} Cantidad mínima de recursos compartidos simples a generar en la red. 
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto:} cantidad de figuras solicitadas más 3.

	\item \textit{“-Msr” o “--max\_shrd\_src”}
	\subitem \textbf{Descripción:} Cantidad máxima de recursos compartidos simples a generar en la red.
	\subitem \textbf{Tipo:} número entero.
	\subitem \textbf{Requerido:} No.
	\subitem \textbf{Valor por defecto: }doble de la cantidad mínima de recursos compartidos.
\end{itemize}

Al ser la capa con la que interactúa el usuario que pretende generar redes de Petri, se incluyeron ciertos manejos de errores y ayudas en el script.\\

Al ingresar el comando \textbf{“-h o --help”} el script muestra por pantalla el listado de parámetros disponible con una breve descripción tal como se enumeraron anteriormente. Si la cantidad de figuras que se intenta generar es menor que 2, el script mostrará un mensaje de error advirtiendo que dicha cantidad debe ser mayor o igual a 2. 

Tal como se especifica en el listado de parámetros, en caso de no especificarse valores parar la cantidad mínima y máxima de recursos compartidos simples, los mismos se generarán en base a la cantidad de figuras, donde la cantidad mínima quedará definida por la cantidad de figuras incrementada en 3 y la cantidad máxima por el doble de la cantidad mínima antes definida.\\

En cuanto a la salida de dicho script, las redes solicitadas se generan cada una dentro de un directorio nuevo en el directorio especificado con el argumento \textit{“-o o --out\_dir”} o \textit{“/gen\_nets”} por defecto si este no se especifica. La carpeta contenedora de cada red recibe como nombre la fecha-hora en que se se generó en formato \textit{“YYYYMMDD-HHMMSS\_I”}, donde \textit{“I”} especifica el número de red generado. Finalmente esta es la carpeta que contiene un archivo \textit{“net.pflow”}, que es la red generada.

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.0]{./img/capitulo2/gen_directory_tree.png}
	\caption{Arbol de directorio de redes generadas.}
	\label{fig:gen_directory_tree}
  \end{figure}

\section{Clasificación}
\subsection{Clasificador}
Con el fin de obtener métricas sobre la calidad de redes generadas, se desarrolló un módulo adicional para determinar las características principales de las mismas. Este módulo clasifica las redes dependiendo si no poseen deadlock, no son del tipo S3PR, cumplen ambas condiciones anteriores o no pudieron ser clasificadas por algún error en Petrinator.

\subsubsection{Integración con Petrinator}
\label{ss:integra_petri}
El clasificador hace uso de una aplicación externa para poder calcular las características antes mencionadas, es decir si la red posee deadlock o no, como así también saber si es del tipo S3PR.\\

Existen varias herramientas con estas capacidades en la actualidad, pero en nuestro caso hemos decidido utilizar el software Petrinator, que ha sido desarrollado a través de varias tesis de compañeros.\\

Petrinator es un editor y analizador de redes de Petri que se presentó en el año 2017. Está compuesto por los siguientes componentes:
\begin{itemize}
\item Un editor de redes de Petri.
\item Un conjunto de algoritmos que se utilizan para analizar redes de Petri.
\item Un monitor de redes de petri temporales, que se utiliza como motor de ejecución para el simulador.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.9]{./img/capitulo2/comp_petrinator.png}
	\caption{Diagrama de componentes de software Petrinator simplificado.}
	\label{fig:simple_src}
  \end{figure}
  
En este proyecto se trabajó sobre la última versión estable contenida en este repositorio: https://github.com/nadaol/Petrinator\_2021. Con el fin de utilizar únicamente los análisis de la aplicación que se necesitan para verificar las propiedades de la red, se añadió la capacidad de recibir ciertos argumentos adicionales al programa los cuales se detallan a continuación:

\begin{itemize}
\item \textbf{pflow\_file:} ruta al archivo pflow que se desea analizar.
\item \textbf{output\_folder:} ruta a la carpeta de salida.
\item \textbf{bring\_gui:} esta bandera permite habilitar o deshabilitar la GUI o Interfaz Gráfica de Petrinator cada vez que se lanza.
\item \textbf{auto\_close:} esta bandera permite realizar un cierre automático del Petrinator una vez que concluyó el análisis para el cual fue ejecutado.
\item \textbf{current\_iteration:} se le envía el número de iteración para que pueda decidir si tiene que correr análisis inicial (verificación de deadlock y tipo S3PR).
\item \textbf{classify: }esta bandera permite utilizar Petrinator como clasificador de redes de Petri.
\item \textbf{redirect\_output: }permite redirigir los logs de salida generados durante su ejecución.
\end{itemize}

De esta forma se construye el siguiente comando, el cual es ejecutado por el clasificador para cada una de las redes como un subproceso.\\

$java\_command<string> petrinator\_path<string> pflow\_file<string> output\_folder<string> bring\_gui<boolean> auto\_close<boolean> current\_iteration<int> clasify<boolean> redirect\_output<string>$

\subsubsection{Clasificaciones definidas}
Durante la clasificación, se genera un archivo dependiendo del resultado del subproceso descrito anteriormente, por lo tanto, una red puede ser clasificada de la siguiente manera:

\begin{itemize}
\item \textbf{FAILED:} se produjo un error de Petrinator al intentar clasificar la red.
\item \textbf{NO\_CLASSIFIED:} no pudo clasificarse la red por alguna causa. (Ej: timeout).
\item \textbf{NO\_DEADLOCK:} la red no posee deadlock.
\item \textbf{NO\_S3P3:} la red no es del tipo S3PR.
\item \textbf{OK:} la red cumple con las condiciones de poseer deadlock y ser del tipo S3PR.
\end{itemize}

En esta primera instancia el módulo de clasificación tiene por foco a las redes que son del tipo S3PR y poseen deadlock inicial, ya que podrían ser útiles para el desarrollo de trabajos futuros en los que se requiera utilizar este tipo específico de redes. Sin embargo, es posible realizar ajustes en el módulo para incluir nuevas posibles clasificaciones.

\subsection{Clasificador masivo}
\label{ss:clasf-mas}
Habiendo ya logrado generar masivamente redes de Petri y desarrollado un módulo que clasifica individualmente cada red, el próximo paso fue desarrollar un nuevo módulo encargado de realizar esta clasificación de manera masiva.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./img/capitulo2/classifier_seq.png}
	\caption{Diagrama de secuencia del clasificador masivo.}
	\label{fig:classifier_seq}
  \end{figure}

El módulo funciona recibiendo el siguiente argumento:

\begin{itemize}
\item \textit{“-nd” o “--networks\_directory”:} Ruta a la carpeta contenedora de las redes a clasificar. (Por defecto “./test\_networks”).
\end{itemize}

\subsubsection{Funcionamiento básico}
Basándose en la estructura básica de directorios de salida del módulo de generación de redes de Petri, el clasificador masivo recorre uno a uno los subdirectorios dentro del directorio especificado en el parámetro \textbf{“-nd” o “--networks\_directory”} o el directorio por defecto si no se especifica, y ejecuta el módulo de clasificación por cada una de las redes generando el correspondiente archivo de salida para cada una en su subdirectorio.\\

Una vez finalizada la clasificación de todas las redes contenidas en el directorio analizado, se reorganizan los subdirectorios dentro de 5 carpetas distintas dependiendo de la clasificación. Los nombres de dichas carpetas coinciden con los nombres de los archivos generados por el clasificador:

\begin{itemize}
\item \textbf{FAILED:} redes que no pudieron clasificarse debido a un error de Petrinator.
\item \textbf{NO\_CLASSIFIED:} redes que no pudieron clasificarse por alguna otra causa. (Ej: timeout).
\item \textbf{NO\_DEADLOCK:} redes que no poseen deadlock inicial.
\item \textbf{NO\_S3P3:} redes que no son del tipo S3PR.
\item \textbf{OK:} redes que cumplen con las condiciones de poseer deadlock y ser del tipo S3PR.
\end{itemize}
Gracias a esta reorganización, es mucho más simple disponer de las redes con un tipo específico de clasificación para trabajar con ellas.

\subsubsection{Generación de estadísticas}
Otra de las funciones destacadas del módulo, es la posibilidad de generar estadísticas que permiten rápidamente visualizar las características del set de redes analizados.
Luego de haberse reorganizado el directorio, el módulo recorre los directorios en los que se han diferenciado por características las distintas redes y obtiene las métricas, entre las cuales se pueden mencionar:

\begin{itemize}
	\item Duración del análisis individual de cada una de las redes.
	\item Cantidad de redes analizadas.
	\item Cantidad de redes que pudieron clasificarse.
	\item Cantidad de redes que no poseen deadlock.
	\item Cantidad de redes que no son del tipo S3PR.
	\item Cantidad de redes que no pudieron clasificarse por timeout de Petrinator.
	\item Cantidad de redes que no pudieron clasificarse por fallas de Petrinator.
	\item Cantidad de redes que cumple las condiciones de poseer deadlock y ser tipo S3PR.
\end{itemize}

Además se exponen las siguientes métricas:

\begin{itemize}
\item \textbf{Calidad del generador:} Valor porcentual de las redes que cumplen las condiciones esperadas (deadlock y ser tipo S3PR).
\item \textbf{Calidad del generador/muestra:} Valor porcentual de las redes enviadas para analizar que no eran del tipo S3PR o no contenían DEADLOCK inicialmente.
\item \textbf{Incidencia debido a factores externos:} Porcentaje de redes sobre las cuales no se pudo completar el análisis debido a fallas en el Petrinator.
\end{itemize}

Finalmente, el módulo muestra la duración total del análisis para todas las redes. 
Toda esta información se almacena en un archivo de salida con con el siguiente formato:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.51]{./img/capitulo2/stats.png}
	\caption{Log con estadísticas.}
	\label{fig:stats}
  \end{figure}

\end{document}