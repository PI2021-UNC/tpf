\documentclass[../main.tex]{subfiles}
\begin{document}

\begin{quote}
  La \textit{Inteligencia Artificial (Artificial Intelligence)} se define como el estudio de los “agentes inteligentes”, i.e. cualquier dispositivo que perciba su entorno y tome medidas que maximicen sus posibilidades de lograr con éxito sus objetivos.
  
  \hfill \cite{poole1998}
\end{quote}

Esta definición nos da la idea de que la IA es un sistema reactivo, que reacciona a cambios externos y actúa en consecuencia.

\section{Aprendizaje automático} \label{machinelearning}

\begin{quote}
  El \textit{aprendizaje automático (Machine Learning)} es el estudio científico de algoritmos y modelos estadísticos que los sistemas informáticos utilizan para realizar una tarea específica sin utilizar instrucciones explícitas, sino que se basan en patrones e inferencia. Es visto como un subcampo de inteligencia artificial. Los algoritmos de aprendizaje automático crean un modelo matemático basado en datos de muestra, conocidos como "datos de entrenamiento", para hacer predicciones o decisiones sin ser programado explícitamente para realizar la tarea.
  
  \hfill \cite{bishop2006pattern}
\end{quote}

%El Aprendizaje Automático (\textit{Machine Learning}) es un conjunto de técnicas desarrolladas y aplicadas para generar nuevo conocimiento, la extracción de este se basa en datos o información ya existentes. En los últimos años, el crecimiento y disponibilidad de los datos ha crecido exponencialmente, fomentado por la evolución de la capacidad de cómputo a menor precio.  Esta herramienta nos permite extraer patrones a partir de grandes muestras de datos logrando solucionar diversos problemas de la ingeniería. 

\begin{figure}[H]
	\begin{center}				
	\includegraphics[width=1\textwidth]{./img/capitulo2/tesis_1.png}
  	\caption{Diagrama de flujo de una aplicación de \textit{Machine Learning}.}
  	\label{fig:flowchartml.}
  	\end{center}
\end{figure}

Por tanto el Aprendizaje Automático es la generación de un modelo de predicción de salida a partir de grandes cantidades de datos de entrada, realizando un tratamiento de los mismos a través de diferentes etapas bien definidas, como se pueden apreciar en la Fig.~\ref{fig:flowchartml.}, las cuales iremos desarrollando en diferentes secciones.

Es importante destacar la independencia del aprendizaje automático al momento de tomar decisiones a partir de los datos proporcionados sin intervención externa, es decir que no hay una especificación de reglas que dictan cómo deben ser tomadas estas decisiones. A su vez, los modelos obtenidos a partir de los algoritmos de \textit{Machine Learning} deben tener la capacidad de predecir a partir de nuevos datos, nunca antes procesados por el modelo, a esto se lo conoce como \textbf{generalización}. 

\subsection{Preprocesamiento} \label{preprocessing}
Este punto es vital para cualquier proyecto que utiliza algoritmos de \textit{Machine Learning}, debido a que los datos incluidos en los conjuntos conocidos como \textit{datasets}, no suelen presentarse en condiciones para obtener el óptimo rendimiento de los algoritmos de aprendizaje. Estos datos suelen estar desbalanceados, haya faltantes o sean demasiado ruidosos, etc. 

Por lo tanto, una vez que se obtiene el \textit{dataset} de entrada, es primordial investigar, limpiar y transformar los datos con diversas técnicas, de forma que presentemos un conjunto de datos que esté en condiciones de ser entrenado y luego el modelo resultante, al momento de ser probado con datos desconocidos, tenga un desempeño óptimo.  

Para lograr este objetivo se aplican técnicas tales como normalización, reescalado, reducción de dimensionalidad, discretización, tratamiento de anomalías y \textit{outliers}.

\section{Aprendizaje por refuerzo} \label{reinforcement}

El aprendizaje por refuerzo o (RL, por sus siglas en inglés) es una rama del aprendizaje automático  donde el aprendizaje ocurre a través de la interacción con un entorno. Es un aprendizaje orientado a objetivos en el que no se le enseña a la inteligencia que acciones tomar, sino que ésta aprende de las consecuencias de sus acciones. Está creciendo rápidamente con una amplia variedad de algoritmos y es una de las áreas de investigación más activas en inteligencia artificial (IA).

\subsection{Caso de estudio} 

Considere que le está enseñando a su perro a atrapar la pelota, pero no puede enseñarle explícitamente a atrapar la pelota; en su lugar, simplemente lanzará una pelota, y cada vez que el perro atrape la pelota, le dará una galleta. Si no atrapa la pelota, no le dará una galleta. El perro descubrirá qué acciones le hicieron recibir una galleta y repetirá esas acciones. 

Del mismo modo, en un entorno de RL, no le enseñara al agente que hacer o cómo hacerlo, sino que le dará una recompensa por cada acción que realice. La recompensa puede ser positiva o negativa. Luego, el agente comenzará a realizar acciones que le hicieron recibir una recompensa positiva. Por lo tanto, es un proceso de prueba y error. En la analogía anterior, el perro representa al agente. Darle una galleta al perro al atrapar la pelota es una recompensa positiva, y no darle la galleta es una recompensa negativa. 

Puede haber recompensas atrasadas en el tiempo. Es posible que no obtenga una recompensa en cada paso. Se puede otorgar una recompensa solo después de completar una tarea. En algunos casos, obtiene una recompensa en cada paso para saber si está cometiendo algún error.

El agente de RL puede \emph{explorar} diferentes acciones que podrían proporcionar una buena recompensa o puede \emph{explotar} (usar) la acción anterior que resultó en una buena recompensa.

Si el agente de RL explora diferentes acciones, existe una gran posibilidad de que reciba una recompensa pobre ya que no todas las acciones van a ser las mejores.

Si el agente de RL explota solo la mejor acción conocida, también existe una gran posibilidad de perderse una mejor acción, lo que podría proporcionar una mejor recompensa.

Siempre existe una compensación entre exploración y explotación. \emph{No podemos realizar exploración y explotación al mismo tiempo}. En secciones posteriores se discute el dilema exploración-explotación en detalle. 

\subsection{Algoritmo}

Los pasos para implementar un algoritmo de RL son los siguientes:

\begin{enumerate}
    \item El agente interactúa con el entorno realizando una acción.
    \item Luego de realizar la acción se mueve de un estado a otro.
    \item Recibe una recompensa basada en la acción que realizó. 
    \item De acuerdo a la recompensa, el agente aprenderá si la acción fue buena o mala.
    \item Si la acción fue buena, es decir, si el agente recibió una recompensa positiva, entonces el agente preferirá realizar esa acción o, de lo contrario, el agente intentará realizar otra acción que resulte en una recompensa positiva. Entonces, es básicamente un proceso de aprendizaje de prueba y error.
\end{enumerate}

\subsection{Aprendizaje por refuerzo vs. otros paradigmas del aprendizaje automático}

En el aprendizaje supervisado, la máquina (agente) aprende de los datos de entrenamiento que tienen un conjunto etiquetado de entrada y salida.

El objetivo es que el modelo extrapole y generalice su aprendizaje para que se pueda aplicar bien a los datos invisibles. Hay un supervisor externo que tiene una base de conocimientos completa del entorno y supervisa al agente para completar la tarea. 
En el aprendizaje no supervisado, proporcionamos al modelo datos de entrenamiento que solo tienen un conjunto de entradas; el modelo aprende a determinar el patrón oculto en la entrada. Existe un malentendido común de que RL es un tipo de aprendizaje no supervisado, pero no lo es. En el aprendizaje no supervisado, el modelo aprende la estructura oculta, mientras que en RL el modelo aprende maximizando las recompensas. 

Digamos que queremos sugerir nuevas películas al usuario. El aprendizaje no supervisado analiza las películas similares que la persona ha visto y sugiere películas, mientras que RL recibe constantemente comentarios del usuario, comprende sus preferencias de películas y crea una base de conocimientos sobre ellas y sugiere una nueva película.

\subsection{Elementos del aprendizaje por refuerzo}
\subsubsection{Agente}
Los agentes son los programas de software que toman decisiones inteligentes y básicamente son aprendices en el aprendizaje por refuerzo. Actúan interactuando con el entorno y reciben recompensas basadas en sus acciones, por ejemplo, un automóvil desplazándose por un camino. 

\subsubsection{Política} \label{politica} 
Define el comportamiento del agente en un entorno. La forma en que el agente decide que acción realizar depende de la política. Digamos que quiere llegar a su oficina desde su casa; habrá diferentes rutas para llegar a su oficina, algunas rutas serán cortas, mientras que otras serán más largas. Estas rutas se denominan políticas porque representan la forma en que elegimos realizar una acción para alcanzar nuestro objetivo. A menudo, una política se indica con el símbolo $\pi$. Una política puede tener la forma de una tabla de búsqueda o un proceso de búsqueda complejo.

\subsubsection{Función valor}
Denota que tan bueno es para un agente estar en un estado particular. Depende de la política y, a menudo, se indica mediante $\nu$(s). Es igual a la recompensa total esperada recibida por el agente a partir del estado inicial. Puede haber varias funciones de valor; la función valor óptima es la que tiene el valor más alto para todos los estados en comparación con otras funciones de valor. De manera similar, una política óptima es aquella que tiene la función valor óptima. 

\subsubsection{Modelo}
Es la representación de un entorno por parte del agente. El aprendizaje puede ser de dos tipos: aprendizaje basado en modelos y aprendizaje libre de modelos. En el aprendizaje basado en modelos, el agente aprovecha la información previamente aprendida para realizar una tarea, mientras que en el aprendizaje sin modelos, el agente simplemente se basa en una experiencia de prueba y error para realizar la acción correcta. Supongamos que desea llegar a su oficina desde su casa lo más rápido posible. En el aprendizaje basado en modelos, simplemente usa una experiencia previamente aprendida (mapa) para llegar a la oficina más rápido, mientras que en el aprendizaje sin modelos no usará una experiencia previa y probará todas las rutas diferentes y elegirá la más rápida. 

\subsubsection{Interfaz del entorno del agente}
Los agentes realizan acciones, $A_{t}$, en un tiempo, $t$, para pasar de un estado $S_{t}$, a otro estado $A_{t+1}$. En función de las acciones, los agentes reciben una recompensa numérica, $R$, del entorno. En última instancia, RL trata de encontrar las acciones óptimas que aumentarán la recompensa numérica.

\begin{figure} [H]
    \centering
    \includegraphics[scale=0.8]{./img/capitulo2/1.PNG}
    \caption{Interacción entre Agente y Entorno}
    \label{fig:1}
\end{figure}

Para comprender el concepto del RL podemos utilizar como ejemplo un laberinto: 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{./img/capitulo2/2.PNG}
	\caption{Laberinto}
	\label{fig:2}
\end{figure}

El objetivo de un laberinto es llegar al destino sin atascarse en los obstáculos. El flujo de trabajo sería el siguiente: 
\begin{itemize}
	\item El agente es quien viaja a través del laberinto, que es nuestro programa de software/algoritmo de RL.
	\item El entorno es el laberinto.
	\item El estado es la posición en la que se encuentra actualmente el agente.
	\item El agente realiza una acción pasando de un estado a otro. 
	\item Un agente recibe una recompensa positiva cuando su acción no se atasca con ningún obstáculo y recibe una recompensa negativa cuando su acción se atasca en obstáculos por lo que no puede llegar al destino. 
	\item El objetivo es resolver el laberinto y llegar a destino. 
\end{itemize}

\subsection{Tipos de entornos en aprendizaje por refuerzo}
Todo aquello con lo que interactúan los agentes se denomina entorno. El entorno es el mundo exterior. Comprende todo lo que está fuera del agente. Existen diferentes tipos de entorno, que se describen en las siguientes secciones. 

\subsubsection{Entorno determinista}
Se  dice que un entorno es determinista cuando conocemos el resultado en función del estado actual. Por ejemplo, en un juego de ajedrez, sabemos el resultado exacto de mover a cualquier jugador. 

\subsubsection{Entorno estocástico}
Se dice que un entorno es estocástico cuando no podemos determinar el resultado en función del estado actual. Habrá un mayor nivel de incertidumbre. Por ejemplo, nunca sabemos qué número aparecerá al lanzar un dado. 

\subsubsection{Entorno completamente observable}
Cuando un agente puede determinar el estado del sistema en todo momento, se denomina totalmente observable. Por ejemplo, en una partida de ajedrez, el estado del sistema, es decir, la posición de todos los jugadores en el tablero de ajedrez, está disponible todo el tiempo para que el jugador pueda tomar una decisión óptima. 

\subsubsection{Entorno parcialmente observable}
Cuando un agente no puede determinar el estado del sistema en todo momento, se denomina parcialmente observable. Por ejemplo, en un juego de póker, no tenemos idea de las cartas que tiene el oponente. 

\subsubsection{Entorno discreto}
Cuando solo hay un conjunto finito de acciones posibles para pasar de un estado a otro, se denomina entorno discreto. Por ejemplo, en un juego de ajedrez, solo tenemos un conjunto finito de movimientos. 

\subsubsection{Entorno continuo}
Cuando hay un estado finito de acciones disponibles para pasar de un estado a otro, se denomina entorno continuo. Por ejemplo, tenemos varias rutas disponibles para viajar desde el origen hasta el destino. 

\subsubsection{Entorno episódico y no episódico}
El entorno episódico también se denomina entorno no secuencial. En un entorno episódico, la acción actual de un agente no afectará una acción futura, mientras que en un entorno no episódico, la acción actual de un agente afectará una acción futura y también se denomina entorno secuencial. Es decir, el agente realiza las tareas independientes en el entorno episódico, mientras que en el entorno no episódico todas las acciones de los agentes están relacionadas. 

\subsubsection{Entorno de agente único y multiagente}
Los entornos de agentes múltiples se utilizan ampliamente al realizar tareas complejas. Habrá diferentes agentes actuando en entornos completamente diferentes. Los agentes en un entorno diferente se comunicaran entre sí. Un entorno de agentes múltiples será principalmente estocástico, ya que tiene un mayor nivel de incertidumbre. 

\subsection{El proceso de decisión de Markov}
El proceso de decisión de Markov (MDP, por sus siglas en inglés) proporciona un marco matemático para resolver el problema de aprendizaje por refuerzo. Casi todos los problemas de RL se pueden modelar como MDP. MDP se usa ampliamente para resolver varios problemas de optimización. 

\subsubsection{Cadenas de Markov y procesos de Markov}
Antes de explicar el proceso de decisión de Markov, comprendamos que son las cadenas de Markov y los procesos de Markov, que forman la base del proceso de decisión de Markov.

La propiedad de Markov establece que el futuro depende solo del presente y no del pasado. La cadena de Markov es un  modelo probabilístico que depende únicamente del estado actual para predecir el estado siguiente y no de los estados anteriores, es decir, el futuro es condicionalmente independiente del pasado. La cadena de Markov sigue estrictamente la propiedad de Markov. 

Por ejemplo, si sabemos que el estado actual es nublado, podemos predecir que el próximo estado podría ser lluvioso. Llegamos a esta conclusión sólo considerando el estado actual (nublado) y no los estados pasados, que podrían ser soleado, ventoso, etc. Sin embargo, la propiedad de Markov no es válida para todos los procesos. Por ejemplo, al lanzar un dado, el número que aparezca (siguiente estado), no depende del número anterior (estado actual).

Pasar de un estado a otro se denomina transición y su probabilidad se denomina probabilidad de transición. Podemos formular las probabilidades de transición en forma de tabla, como se muestra a continuación, en lo que  se denomina tabla de Markov. Muestra, dado el estado actual, cual es la probabilidad de pasar al siguiente estado:

\begin{center}
\begin{table}[ht]
\centering
\begin{tabular}{|c|c|c|}
\hline
\textbf{Estado actual} & \textbf{Próximo estado} & \textbf{Probabilidad de transición} \\ \hline
Nublado                & Lluvioso            & 0.6                             \\ \hline
Lluvioso               & Lluvioso            & 0.2                             \\ \hline
Soleado                & Nublado             & 0.1                             \\ \hline
Lluvioso               & Soleado             & 0.1                             \\ \hline
\end{tabular}
\caption{Tabla de Markov}
\label{tab:markov-table}
\end{table}
\end{center}

También podemos representar la cadena de Markov en forma de diagrama de estados que muestra la probabilidad de transición: 

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.0]{./img/capitulo2/3.PNG}
	\caption{Diagrama de estados de una cadena de Markov}
	\label{fig:3}
\end{figure}

La cadena de Markov se basa en el concepto central de que el futuro depende solo del presente y no del pasado. Un proceso estocástico se denomina proceso de Markov si sigue la propiedad de Markov. 

\subsubsection{Proceso de decisión de Markov}
MDP es una extensión de la cadena de Markov. Proporciona un marco matemático para modelar situaciones de toma de decisiones. Casi todos los problemas de aprendizaje por refuerzo se pueden modelar con MDP. 
MDP se compone de cinco importantes elementos: 

\begin{itemize}
	\item Un conjunto de estados $(S)$ en los que el agente puede estar.
	\item Un conjunto de acciones $(A)$ que pueden ser ejecutadas por el agente, para moverse de un estado a otro. 
	\item Una probabilidad de transición $\left(P_{\text {ss }^{\prime}}^{a}\right)$), que es la probabilidad de movernos de un estado S a otro estado S' ejecutando alguna acción a.
	\item Una probabilidad de recompensa $\left(R_{\text {ss }^{\prime}}^{a}\right)$, que es la probabilidad de que el agente obtenga una recompensa por moverse de un estado $S$ a otro estado $S'$ ejecutando alguna acción $a$. 
	\item Un factor de descuento ($\gamma$), que controla la importancia de las recompensas inmediatas y futuras. Discutiremos esto en detalle en las próximas secciones. 
\end{itemize}

\subsubsection{Retorno y recompensas}
Como hemos aprendido, en un entorno de aprendizaje por refuerzo, un agente interactúa con el entorno realizando una acción y se mueve de un estado a otro. En función de la acción que realiza, recibe una recompensa. Una recompensa no es más que un valor numérico, digamos, $+1$ por una buena acción y $-1$ por una mala acción. 

¿Cómo decidimos si una acción es buena o mala? 
En un juego de laberinto, una buena acción es cuando el agente hace un movimiento para que no golpee una pared del laberinto, mientras que una mala acción es donde el agente se mueve y golpea la pared del laberinto.  

Un agente intenta maximizar la cantidad total de recompensas (recompensas acumulativas) que recibe del entorno en lugar de recompensas inmediatas. La cantidad total de recompensas que recibe el agente del entorno se denomina retorno. Entonces, podemos formular la cantidad total de recompensas (retorno) recibidas por los agentes de la siguiente manera: 

\begin{equation}
R_{t}=r_{t+1}+r_{t+2}+\ldots \ldots+r_{T}
\end{equation}

$r_{t+1}$ es la recompensa que recibe el agente en un instante de tiempo $t_{0}$ luego de ejecutar la una acción $a_{0}$ para pasar de un estado a otro. $r_{t+2}$ es la recompensa que recibe el agente en un instante de tiempo $t_{1}$ luego de ejecutar una acción para pasar de un estado a otro. De las misma manera, $r_{T}$ es la recompensa recibida por el agente en el instante de tiempo final $T$ luego de ejecutar una acción para pasar de un estado a otro.

\subsubsection{Tareas episódicas y tareas continuas}
Las tareas episódicas son aquellas que tienen un estado terminal o de finalización. En RL, los episodios son considerados como interacciones entre el entorno y el agente desde un estado inicial hasta un estado final. 

Por ejemplo, en un video juego de autos de carrera, el juego comienza (estado inicial) y uno juega hasta que el juego termina (estado final). Esto es llamado un episodio. Una vez que el juego termina, se puede comenzar otro episodio reiniciando el juego, y este comenzará desde el estado inicial independientemente de la posición en la que se encontraba en el episodio anterior. 

En un tarea continua, no existe un estado terminal o de finalización. Es decir, la tarea nunca termina. Por ejemplo, un robot de asistencia personal no tiene un estado terminal.

\subsubsection{Factor de descuento}
Hemos observado que el objetivo de un agente es maximizar el retorno. Para una tarea episódica, podemos definir nuestro retorno como $R_{t}=r_{t+1}+r_{t+2}+\ldots \ldots+r_{T}$, donde $T$ es el estado final de ese episodio, y lo que intentamos es maximizar el retorno $r_{T}$.

Para una tarea continua, como no tenemos un estado terminal, podemos definir nuestro retorno como sumas infinitas $R_{t}=r_{t+1}+r_{t+2}+\ldots$. El dilema es cómo maximizamos el retorno si  las sumas nunca se detienen. 

Por esto se introduce el concepto de factor de descuento. 
Podemos definir nuestro retorno con un factor de descuento $(\gamma)$, de la siguiente manera: 

\begin{equation}
R_{t}=r_{t+1}+\gamma r_{t+2}+\gamma^{2} r_{t+3}+\gamma^{3} r_{t+4}+\ldots
\end{equation} 

\begin{equation}
=\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+1}
\end{equation} 

El factor de descuento decide la importancia que le damos a las recompensas futuras y las recompensas inmediatas. El valor del factor de descuento se encuentra entre $0$ y $1$. Un factor de descuento en $0$ significa que las recompensas inmediatas son más importantes, mientras que un factor de descuento en $1$ significa que las recompensas futuras son más importantes que las inmediatas. 

Un factor de descuento de $0$ nunca aprenderá considerando solo las recompensas inmediatas; de la misma manera, un factor de descuento de $1$ aprenderá siempre buscando la recompensa futura, lo que puede llevar al infinito. Por lo tanto, el valor óptimo del factor de descuento se encuentra entre $0,2$ y $0,8$.

La importancia de las recompensas inmediatas y futuras depende del caso de uso. En algunos casos, las recompensas futuras son más deseables que las inmediatas y viceversa. En un juego de ajedrez, el objetivo es derrotar al rey del oponente. Si le damos importancia a la recompensa inmediata, que se adquiere mediante acciones como que nuestro peón derrote a cualquier jugador oponente y así sucesivamente, el agente aprenderá a realizar este subobjetivo en lugar de aprender a alcanzar el objetivo real. Entonces, en este caso, damos importancia a las recompensas futuras, mientras que en otros casos se prefieren las recompensas inmediatas. 

\subsubsection{La política}
Como definimos en la sección \ref{politica}, la politica se denota con $\pi$ y relaciona los estados con las acciones.

La política se puede representar como $\pi(s): S \rightarrow A$, lo que indica un mapeo de estados a acciones. Entonces, básicamente, la política nos indica qué acción realizar en cada estado. Nuestro objetivo final radica en encontrar la política óptima que especifique la acción correcta a realizar en cada estado, maximizando la recompensa. 

\subsubsection{Función de valor}

Esta función específica que tan bueno es para un agente estar en un estado particular con una política $\pi$. Una función de valor a menudo se denota con $V(s)$. Denota el valor de un estado que sigue una política. 

Podemos definir la función de valor como:

\begin{equation}
\label{eqn:1}
V^{\pi}(s)=\mathbb{E}_{\pi}\left[R_{t} \mid s_{t}=s\right]
\end{equation}

Esto especifica el retorno esperado a partir del estado $S$ de acuerdo con la política $\pi$. Podemos sustituir el valor de $R_{t}$ la función de valor \ref{eqn:1} con lo siguiente: 

\begin{equation}
V^{\pi}(s)=\mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+1} \mid s_{t}=s\right]
\end{equation}

Hay que tener en cuenta que la función de valor depende de la política y el estado y varía según la política que elijamos. 

Podemos ver las funciones valor en una tabla. Digamos que tenemos dos estados y ambos siguen la política $\pi$. Según el valor de estos dos estados, podemos decir que tan bueno es para nuestro agente estar en ese estado siguiendo una política. Cuanto mayor sea el valor, mejor será el estado:

\begin{center}
\begin{table}[ht]
\centering
\begin{tabular}{|c|c|}
\hline
\textbf{Estado} & \textbf{Valor} \\ \hline
Estado 1        & 0.3            \\ \hline
Estado 2        & 0.9            \\ \hline
\end{tabular}
\caption{Tabla funciones valor}
\label{tab:valor-function}
\end{table}
\end{center}

Basándonos en la tabla anterior, podemos decir que es bueno estar en el estado $2$, ya que tiene un valor más alto. Veremos cómo estimar estos valores de forma intuitiva en las próximas secciones. 

\subsubsection{Función de valor estado-acción (Función Q)}
\label{ss:qfunction}
La función de valor estado-acción también se denomina función Q. Esta especifica que tan bueno es para un agente realizar una acción particular en un estado con una política $\pi$. La función $Q$ se denota con $Q(s)$ , podemos definirla de la siguiente manera: 

\begin{equation}
Q^{\pi}(s, a)=\mathbb{E}_{\pi}\left[R_{t} \mid s_{t}=s, a_{t}=a\right]
\end{equation}

Esto especifica el retorno esperado a partir del estado $s$ ejecutando la acción $a$ de acuerdo con la política $\pi$. Podemos sustituir el valor de $R_{t}$ la función $Q$ de la siguiente manera: 

\begin{equation}
Q^{\pi}(s, a)=\mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+1} \mid s_{t}=s, a_{t}=a\right]
\end{equation}

La diferencia entre la función de valor y la función $Q$ es que la función de valor especifica la bondad de un estado, mientras que una función $Q$ especifica la bondad de aplicar una acción en un estado. 

Al igual que las funciones de valor, las funciones $Q$ se pueden ver en un tabla, la cual se denomina tabla $Q$. Digamos que tenemos dos estados y dos acciones, nuestra tabla $Q$ queda de la siguiente manera: 

\begin{center}
\begin{table}[ht]
\centering
\begin{tabular}{|c|c|c|}
\hline
\textbf{Estado} & \textbf{Accion} & \textbf{Valor} \\ \hline
Estado 1        & Accion 1        & 0.03           \\ \hline
Estado 1        & Accion 2        & 0.02           \\ \hline
Estado 2        & Accion 1        & 0.5            \\ \hline
Estado 2        & Accion 2        & 0.9            \\ \hline
\end{tabular}
\caption{Tabla $Q$}
\label{tab:q-table}
\end{table}
\end{center}

Como vemos, la tabla $Q$ muestra el valor de todos los posibles pares de acciones-estados. Entonces, al mirar esta tabla, podemos llegar a la conclusión  de que realizar la acción $1$ en el estado $1$ y la acción $2$ en el estado $2$ es la mejor opción, ya que tiene el valor más alto. Siempre que decimos función valor $V(S)$ o función $Q$ $Q(S, a)$, en realidad significa la tabla de valores y la tabla $Q$, respectivamente, como se mostró anteriormente. 

\subsubsection{Ecuación de Bellman y la optimalidad}
La ecuación de Bellman, que lleva el nombre de Richard Bellman, matemático estadounidense, nos ayuda a resolver MDP. Cuando decimos resolver MDP, en realidad significa encontrar las políticas óptimas y las funciones de valor. Puede haber muchas funciones de valor diferentes según las diferentes políticas. La función de valor óptimo $V^{*}(s)$ es la que produce el máximo valor en comparación con todas las demas funciones de valor: 

\begin{equation}
V^{*}(s)=\max _{\pi} V^{\pi}(s)
\end{equation}

De manera similar, la política óptima es la que da como resultado una función de valor óptima. 
Dado que la función de valor óptimo $V^{*}(s)$ es la que tiene un valor más alto en comparación con todas las demás funciones de valor (es decir, máximo retorno), será el máximo de la función $Q$. Por lo tanto, la función de valor óptimo se puede calcular fácilmente tomando el máximo de la función $Q$ de la siguiente manera: 

\begin{equation}
\label{eqn:2}
V^{*}(s)=\max _{a} Q^{*}(s, a)
\end{equation}

La ecuación de Bellman para la función de valor se puede representar como:

\begin{equation}
V^{\pi}(s)=\sum_{a} \pi(s, a) \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a}\left[\mathcal{R}_{s s^{\prime}}^{a}+\gamma V^{\pi}\left(s^{\prime}\right)\right]
\end{equation}

Lo que indica la relación recursiva entre un valor de un estado y su estado sucesor, y el promedio de todas las posibilidades.  
De la misma manera, la ecuación de Bellman para la función $Q$ se puede representar de la siguiente manera: 

\begin{equation}
\label{eqn:3}
Q^{\pi}(s, a)=\sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a}\left[\mathcal{R}_{s s^{\prime}}^{a}+\gamma \sum_{a^{\prime}} Q^{\pi}\left(s^{\prime}, a^{\prime}\right)\right]
\end{equation}

Sustituyendo la ecuación \ref{eqn:3} en \ref{eqn:2}, obtenemos: 

\begin{equation}
V^{*}(s)=\max _{a} \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a}\left[\mathcal{R}_{s s^{\prime}}^{a}+\gamma \sum_{a^{\prime}} Q^{\pi}\left(s^{\prime}, a^{\prime}\right)\right]
\end{equation}

La ecuación anterior se conoce como la ecuación de optimalidad de Bellman. 

\subsubsection{Derivando la ecuación de Bellman para la función de valor y la función $Q$}
Primero, definimos a $P_{s s^{\prime}}^{a}$ como la probabilidad de transición al pasar de un estado $s$ a otro estado $s'$ luego de ejecutar una acción $a$:

\begin{equation}
P_{s s^{\prime}}^{a}=p r\left(s_{t+1}=s^{\prime} \mid s_{t}=s, a_{t}=a\right)
\end{equation}

Definimos a $R_{s s^{\prime}}^{a}$ de recibir una recompensa  al pasar de un estado $s$ a otro estado $s'$ luego de ejecutar una acción $a$:

\begin{equation}
R_{s s^{\prime}}^{a}=\mathbb{E}\left(R_{t+1} \mid s_{t}=s, s_{t+1}=s^{\prime}, a_{t}=a\right)
\end{equation}

\begin{equation}
\label{eqn:4}
=\gamma \mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t+1}=s^{\prime}\right]
\end{equation}

Sabemos que la función de valor está representada por:

\begin{equation}
V^{\pi}(s)=\mathbb{E}_{\pi}\left[R_{t} \mid s_{t}=s\right]
\end{equation}

\begin{equation}
V^{\pi}(s)=\mathbb{E}_{\pi}\left[r_{t+1}+\gamma r_{t+2}+\gamma^{2} r_{t+3}+\ldots \mid s_{t}=s\right]
\end{equation}

Podemos reescribir la función de valor sacando la primer recompensa: 

\begin{equation}
\label{eqn:5}
V^{\pi}(s)=\mathbb{E}_{\pi}\left[r_{t+1}+\gamma \sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t}=s\right]
\end{equation}

Las expectativas en la función de valor especifican el rendimiento esperado si estamos en el estado $s$, realizando una acción a con la política $\pi$.

Por lo que podemos reescribir nuestra expectativa explícitamente resumiendo todas las acciones y recompensas posibles de la siguiente manera: 

\begin{equation}
\mathbb{E}_{\pi}\left[r_{t+1} \mid s_{t}=s\right]=\sum_{a} \pi(s, a) \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a} \mathcal{R}_{s s^{\prime}}^{a}
\end{equation}

Si sustituimos $R_{s s^{\prime}}^{a}$ por la ecuación \ref{eqn:4}, tenemos: 

\begin{equation}
\sum_{a} \pi(s, a) \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a} \gamma \mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t+1}=s^{\prime}\right]
\end{equation}

Por otro lado, si sustituimos $r_{t+1}$ por la ecuación \ref{eqn:1} tenemos: 

\begin{equation}
\mathbb{E}_{\pi}\left[\gamma \sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t}=s\right]
\end{equation}

Entonces, nuestra ecuación de expectativa final se convierte en: 

\begin{equation}
\label{eqn:6}
\mathbb{E}_{\pi}\left[\gamma \sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t}=s\right]=\sum_{a} \pi(s, a) \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a} \gamma \mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t+1}=s^{\prime}\right]
\end{equation}

Ahora sustituimos nuestra expectativa \ref{eqn:6} en la función de valor \ref{eqn:5} y obtenemos: 

\begin{equation}
V^{\pi}(s)=\sum_{a} \pi(s, a) \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a}\left[\mathcal{R}_{s s^{\prime}}^{a}+\gamma \mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t+1}=s^{\prime}\right]\right]
\end{equation}

En lugar de $$\mathbb{E}_{\pi}\left[\sum_{k=0}^{\infty} \gamma^{k} r_{t+k+2} \mid s_{t+1}=s^{\prime}\right]$$ podemos utilizar $V^{\pi}\left(s^{\prime}\right)$ de la ecuación \ref{eqn:5}, y nuestra función de valor final queda como: 

\begin{equation}
V^{\pi}(s)=\sum_{a} \pi(s, a) \sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a}\left[\mathcal{R}_{s s^{\prime}}^{a}+\gamma V^{\pi}\left(s^{\prime}\right)\right]
\end{equation}

De manera muy similar podemos derivar la ecuación de Bellman para función $Q$, lo que nos da la ecuación final siguiente:

\begin{equation}
Q^{\pi}(s, a)=\sum_{s^{\prime}} \mathcal{P}_{s s^{\prime}}^{a}\left[\mathcal{R}_{s s^{\prime}}^{a}+\gamma \sum_{a^{\prime}} Q^{\pi}\left(s^{\prime}, a^{\prime}\right)\right]
\end{equation}

Ahora que tenemos una ecuación de Bellman tanto para la función de valor como para la función Q, necesitamos descifrar cómo encontrar las políticas más óptimas. 

\subsection{Resolucion de la ecuación de Bellman}

Para resolver la ecuación de Bellman es posible utilizar dos eficientes algoritmos basados en programación dinámica. 

\subsubsection{Iteración por valores}
En la iteración por valores, comenzamos con una función de valor aleatorio. Obviamente, la función de valor aleatorio podría no ser óptima, por lo que buscamos una nueva función de valor mejorada de manera iterativa hasta que encontremos la función de valor óptima. Una vez que encontramos la función de valor óptimo, podemos derivar fácilmente una política óptima de ella: 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{./img/capitulo2/diagram1.PNG}
	\caption{Diagrama de flujo iteración por valores}
	\label{fig:diagram1}
\end{figure}

Las pasos involucrados en el algoritmo son los siguientes: 
\begin{enumerate}
    \item Primero inicializamos la función de valor aleatorio, es decir, el  valor aleatorio para cada estado.
    \item Luego calculamos la función $Q$ para todos los pares de estados-acciones de $Q(s, a)$.
    \item Luego actualizamos nuestra función de valor con el valor máximo de $Q(s, a)$. 
    \item Repetimos estos pasos hasta que el cambio en la función de valor sea muy pequeño.
\end{enumerate}

\subsubsection{Iteración por políticas}
A diferencia de la iteración por valores, en la iteración por políticas comenzamos con la política aleatoria, luego encontramos la función de valor de esa política; si la función de valor no es óptima, encontramos la nueva política mejorada. Repetimos este proceso hasta encontrar la política óptima. 

Existen dos pasos en la iteración por políticas:
\begin{enumerate}
    \item Evaluación de políticas. Evaluación de la función de valor de una política estimada aleatoriamente. 
	\item Mejora de la política:  Al evaluar la función de valor, si no es óptima, encontramos una nueva política mejorada:
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{./img/capitulo2/diagram2.PNG}
	\caption{Diagrama de flujo iteración por políticas}
	\label{fig:diagram2}
\end{figure}

Los pasos involucrados en el algoritmo son los siguientes:
\begin{enumerate}
    \item Primero, inicializamos una política aleatoria.
    \item Luego, encontramos la función de valor para esa política aleatoria y la evaluamos para verificar si es óptima, lo que se denomina evaluación de políticas. 
    \item Si no es óptimo, encontramos una nueva política mejorada, este paso se llama mejora de la política. 
    \item Repetimos este paso hasta encontrar una política óptima. 
\end{enumerate}

\end{document}