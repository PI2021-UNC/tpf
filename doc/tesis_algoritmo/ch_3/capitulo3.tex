\documentclass[../main.tex]{subfiles}
\begin{document}

Para comenzar con el diseño de la solución debemos tener en claro cuales son los  objetivos planteados y seguir los requerimientos definidos en secciones anteriores. Nuestro principal objetivo es lograr evitar que un sistema modelado a través de Redes de Petri llegue a un estado no deseado, donde no pueda seguir evolucionando y quede totalmente bloqueado. En las Redes de Petri esto se conoce como “punto muerto” o “deadlock” y es una problemática que hace tiempo se intenta resolver, por lo que ha sido fuente de diversos estudios que han llevado a plantear diferentes soluciones (Ver sección \ref{ss:fprob}).

Existen diferentes tipos de Redes de Petri, para modelar diferentes sistemas con distintas características. En este proyecto nos centraremos en las Redes de Petri del tipo S3PR, definidas en la sección \ref{ss:ds3pr},  dado que modelan la ejecución concurrente de procesos de trabajo. Debido a las características dinámicas de dichas redes, es muy probable que se produzca deadlock, dejando al sistema en un estado de bloqueo y con la imposibilidad de continuar con la ejecución de los procesos de trabajo.

En los Proyectos Integradores denominados “Determinación automática del control de una Red de Petri” \cite{tesissalvatierra}, se planteó una primera solución para evitar el deadlock de redes S3PR mediante el agregado de plazas de control denominadas supervisores. Esta solución implica el desarrollo de un algoritmo para detectar los posibles supervisores a agregar, seleccionarlos, agregarlos y verificar si la red sigue teniendo o no deadlock. El algoritmo consta de los siguientes pasos: 

\begin{enumerate}
    \item Obtener los sifones vacíos en el estado inicial; estos sifones deben ignorarse dado que una vez vacíos permanecerán así por el resto de los estados alcanzables.
    \item Obtener los estados en deadlock con sus respectivos sifones vacíos, los cuales inicialmente estaban marcados.
    \item Seleccionando uno de los sifones mencionados en el ítem anterior:
        \begin{enumerate}
            \item Se obtiene su marcado inicial para posteriormente definir el marcado de su correspondiente supervisor.
            \item Se localizan las transiciones sensibilizadas en el estado idle (para el marcado inicial). 
            \item Se obtienen las plazas complemento del mismo.
                \begin{enumerate}
                    \item Se buscan las transiciones que quitan y agregan tokens a estas plazas. 
                    \item Las transiciones que agregan más tokens de los que quitan al sifón son las que nos interesan.
                \end{enumerate}
            \item Se verifica si hay transiciones en conflicto, de ser así se utilizan los \break T-invariantes para verificar si la ejecución de la misma se encuentra en el camino de las plazas del sifón. De no ser así, estas transiciones serán también de interés.
        \end{enumerate}

    \item Agregar una nueva plaza de control (perteneciente al supervisor) a la red puede producir un nuevo sifón mínimo no controlado y un nuevo estado de bloqueo. Por lo tanto, debemos volver al punto 1 calculando nuevamente el árbol de alcanzabilidad y repetir todo el algoritmo, atacando la totalidad de los sifones vacíos en estado de deadlock o mejor conocidos como bad siphons, hasta alcanzar una red viva. El algoritmo finaliza cuando no es posible encontrar un nuevo punto muerto en la red de Petri, es decir, se resuelve el deadlock de la misma. Sin embargo, puede darse la situación en donde el algoritmo no converge a una solución dado que sugiere supervisores con marcado igual a 0 o supervisores ya colocados.

    \item Agregar o quitar arcos. Se comienza con la verificación de que cada T-invariante esté realizando la devolución del token tomado del supervisor $V(s)$, en caso contrario, probar desde cuál transición del T-invariante tendría que devolver el mismo. Esta prueba se realizó de la siguiente manera: comenzando desde la última transición que forma parte del T-invariante agregando un arco que le devuelva el token al supervisor. Si el arco que devuelve el token llega al inicio sin que se provoque deadlock, esto quiere decir, que ese T-invariante no necesita utilizar el token del supervisor $V(s)$ por lo que el arco hacia la transición idle que extrae tokens del mismo debe eliminarse.

\end{enumerate}

\noindent Las fórmulas para calcular el supervisor para un sifón son las siguientes:
\begin{enumerate}
    \item $m(V_s) = m(BS_i) - 1$
    \item $Arco_1 = \{(V_s, t) \ / \ t \in P_0 \bullet \}$
    \item $Arco_2 = \{(t, V_s) \ / \ t \in C_s \bullet \}$
    \item $Arco_3 = \{(t, V_s) \ / \ t \in conflicto \wedge t \notin T_{inv_{BS_i}} \}$
\end{enumerate}

\noindent siendo:
\begin{enumerate}
    \item $V_s$ = plaza supervisor
    \item $P_0$ = plazas marcadas idle
    \item $C_s$ = complemento sifón
    \item $BS_i$ = bad siphon
    \item $t$ = conjunto de transiciones
\end{enumerate}

Se toma uno de los supervisores, se incorpora a la red en el software Petrinator realizando el análisis correspondiente en búsqueda de verificar que el deadlock de la red haya desaparecido; de no ser así se exportan nuevamente los archivos y se realiza la ejecución del algoritmo nuevamente. Y así iterativamente hasta lograr que el deadlock de la red desaparezca.\\

El algoritmo iterativo para lograr una red de Petri sin deadlock se muestra a continuación

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.58]{img/capitulo3/diagrama_flujo.png}
	\caption{Diagrama de flujo del algoritmo para destrabar red}
	\label{fig:diag_flujo}
 \end{figure} 
 
Cabe aclarar que dicho algoritmo, necesita de un diseñador que tome la decisión de cuál supervisor de la lista agregar. Si el diseñador selecciona un supervisor incorrectamente o comete algún error al importar los archivos, el proceso tendrá que comenzar de cero nuevamente. Esto claramente es un punto a mejorar, por lo que se propone automatizar la toma de decisiones, tanto la acción a escoger como qué supervisor agregar.\\

Además, la integración con la herramienta Petrinator no es completa, por lo que una vez que el diseñador decide qué supervisor agregar, deberá abrir en la herramienta Petrinator el nuevo archivo .pflow generado, generar los archivos .html que cuentan con la información necesaria y colocarlos en una carpeta específica para poder continuar con la ejecución. Esta metodología es propensa a errores por parte del usuario y además agrega complejidad al proceso, por lo que se propone automatizar este flujo de trabajo con el objetivo de agilizar y evitar errores en los análisis de las redes. 

\section{Metodología}
Siguiendo el concepto de programación modular, donde un procedimiento se divide en módulos bien definidos que cumplen una función específica, utilizamos diferentes módulos desarrollados en el proyecto "Determinación automática del control de una Red de Petri" \cite{tesissalvatierra} y diseñamos algunos nuevos para implementar una metodología que utiliza el aprendizaje por refuerzo. El primero lo denominamos deadlock\_avoidance\_RL.

El mismo  hace uso de una aplicación externa para poder calcular el árbol de alcanzabilidad y detectar si la red posee deadlock inicial o no, como así también saber si es del tipo S3PR. Existen varias herramientas con estas capacidades en la actualidad, pero en nuestro caso hemos decidido utilizar el software Petrinator, que ha sido desarrollado y mejorado durante más de 5 años por diferentes compañeros en el laboratorio de Arquitectura de Computadoras. Hemos trabajado sobre la última versión estable publicada en este repositorio https://github.com/nadaol/Petrinator\_2021. 

La integración con esta herramienta permite al módulo deadlock\_avoidance\_RL funcionar de manera automática, sin la necesidad de la intervención de ninguna persona, lo que mejora la performance. Además, permiten generar toda la información necesaria de la red de Petri para mantener actualizado el \emph{Entorno} cada vez que el \emph{Agente} ejecuta una \emph{Acción}. 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{./img/capitulo3/diagrama_componentes.png}
	\caption{Diagrama de componentes de la metodología desarrollada}
	\label{fig:diagrama_componentes}
  \end{figure}

\subsection{Funcionamiento}
El módulo deadlock\_avoidance\_RL se encarga de inicializar el entorno a partir de un archivo .pflow que contiene la red de Petri que se desea desbloquear, aplica las acciones definidas en la sección anterior para modificar dicho entorno y recibir una recompensa. Además, contiene la tabla Q, donde registra las recompensas recibidas en cada par acción - estado. 

Para utilizarlo son necesarios algunos argumentos,  que se detallan a continuación: 

\begin{itemize}
	\item \textit{“-pf” o “--pflow\_file”}
	\subitem \textbf{Descripción:} Ruta al archivo net.pflow que se usará durante el análisis
	\subitem \textbf{Tipo:} Ruta de archivo 
	\subitem \textbf{Requerido:} Si 
	\subitem \textbf{Valor por defecto si se omite:} "../test\_path/net.pflow”\
	
	\item \textit{“-of” o “--output\_folder”}
	\subitem \textbf{Descripción:} Ruta a la carpeta de salida donde se escribirá el archivo net.pflow procesado.
	\subitem \textbf{Tipo:} Ruta de directorio
	\subitem \textbf{Requerido:} Si
	\subitem \textbf{Valor por defecto si se omite:} "../test\_path”

	\item \textit{“-hp” o “--html\_path”}
	\subitem \textbf{Descripción:} Ruta a la carpeta de salida donde se escribirán los archivos html generados por petrinator.
	\subitem \textbf{Tipo:} Ruta de directorio
	\subitem \textbf{Requerido:} Si
	\subitem \textbf{Valor por defecto si se omite:} "../test\_path”

	\item \textit{“-tp” o “--txt\_path”}
	\subitem \textbf{Descripción:} Ruta a la carpeta de salida donde se escribirán los archivos txt generados por nuestro algoritmo.
	\subitem \textbf{Tipo:} Ruta de directorio
	\subitem \textbf{Requerido:} Si
	\subitem \textbf{Valor por defecto si se omite:} "../test\_path/txt\_info"
\end{itemize}

A continuación se detalla el funcionamiento del módulo con diferentes diagramas de secuencia.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./img/capitulo3/sequence_alg.png}
	\caption{Diagrama de secuencia general de la metodología desarrollada}
	\label{fig:sequence_alg}
  \end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./img/capitulo3/step_seq1.png}
	\caption{Diagrama de secuencia de acciones posibles: parte 1}
	\label{fig:step_alg1}
  \end{figure}
  
  \begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./img/capitulo3/step_seq2.png}
	\caption{Diagrama de secuencia de acciones posibles: parte 2}
	\label{fig:step_alg2}
  \end{figure}
  
  \begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./img/capitulo3/delete_seq.png}
	\caption{Diagrama de secuencia de eliminación de supervisores redundantes}
	\label{fig:delete_seq}
  \end{figure}
  
La ejecución continúa hasta que la Red de Petri analizada no contiene más deadlock o hasta que se cumple un número especificado de iteraciones. 

\section{Ejecutor Masivo}

Una vez desarrollado el módulo deadlock\_avoidance\_RL, como solo analiza de a una red en cada ejecución, decidimos implementar un nuevo módulo que permita la ejecución iterativa del mismo. Este tiene como objetivo recibir un directorio con diferentes redes y ejecutar el módulo deadlock\_avoidance\_RL para cada una de ellas. A su vez, se agregaron diferentes métricas para detectar si la metodología desarrollada cumplía su función. 

\subsection{Funcionamiento}
\label{ss:capitulo3_funcionamiento}
El módulo massive\_run recibe un solo argumento para su ejecución, el cual es:

\begin{itemize}
	\item \textit{“-nd” o “--networks\_directory”}
	\subitem \textbf{Descripción:} Ruta a la carpeta contenedora de todos los casos
	\subitem \textbf{Tipo:} Ruta de directorio
	\subitem \textbf{Requerido:} No
	\subitem \textbf{Valor por defecto si se omite:} networks\_directory
\end{itemize}

Además, al finalizar la ejecución, el módulo clasifica todas las redes analizadas, moviendo estas a diferentes directorios, y así tener una mejor visibilidad de los distintos tipos de redes. La clasificación se realiza de la siguiente manera: 

\begin{itemize}
	\item \textbf{NO S3PR:} Durante la primera iteración siempre se realiza un análisis preliminar, en caso de no tratarse de una red del tipo S3PR, se abandona el análisis y se clasifica la red bajo esta categoría.
	\item \textbf{NO DEADLOCK:} Lo mismo ocurre para el caso en que la red no presente deadlock inicial, se abandona el análisis y se clasifica la red bajo esta categoría.
	\item \textbf{NO UNLOCKED:} Debido a fines prácticos, incluimos un límite de iteraciones (Default: 25) y un límite de tiempo de ejecución del análisis mediante petrinator (Default: 10 minutos), ambos configurables. En caso de que la red que se está analizando alcance alguna de estas condiciones, se abandona el análisis y se clasifica la red bajo esta categoría.
	\item \textbf{UNLOCKED:} Aquí encontraremos las redes que han sido exitosamente desbloqueadas.
	\item \textbf{FAILED:} Notamos que bajo algunas circunstancias, generalmente cuando la red a analizar cuenta con demasiados estados. Se produce un error en el software Petrinator, cuando esto ocurre la red se clasifica como failed. Notar aquí que la red podría ser válida, solo que por una limitación externa no se logra completar el análisis. Estas redes quedan catalogadas en caso de ser necesarias para trabajos futuros que busquen mejorar el rendimiento del software Petrinator. 
\end{itemize}

Además, se incorporó una funcionalidad dentro del ejecutor masivo que permite saltarse redes con la combinación de teclas CTRL + C en caso de que se desee interrumpir un análisis sin cortar el set de pruebas completo.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./img/capitulo3/ctrlC.jpeg}
	\caption{Salida del log producido cuando se interrumpe alguna ejecucion}
	\label{fig:ctrlC}
  \end{figure}
  
Otra de las funciones destacadas es la presentación de estadísticas que permiten rápidamente visualizar, independientemente del resultado final obtenido para cada red, la siguiente información sobre el análisis para medir la calidad de la metodología ejecutada en deadlock\_avoidance\_RL:

\begin{itemize}
	\item Duración del análisis individual de cada una de las redes.
	\item Número de iteraciones realizadas durante la ejecución.
	\item Número de supervisores agregados.
	\item Número de sifones inicial.
	\item Número de plazas inicial.
	\item Número de transiciones iniciales.
\end{itemize}

Además se exponen las siguientes métricas:
\begin{itemize}
	\item \textbf{Calidad de la metodología:} Valor porcentual de las redes que pudieron ser desbloqueadas.
	\item \textbf{Calidad del generador/muestra:} Valor porcentual de las redes enviadas para analizar que no eran del tipo S3PR o no contenían DEADLOCK inicialmente.
	\item \textbf{Incidencia debido a factores externos:} Porcentaje de redes sobre las cuales no se pudo completar el análisis debido a fallas en Petrinator.
\end{itemize}

En el apéndice \ref{app:apendice_I} se aprecian en detalle las diferentes métricas aquí expuestas.
  
A continuación se detalla el funcionamiento de dicho módulo en un diagrama de secuencia. 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{./img/capitulo3/massive_seq.png}
	\caption{Diagrama de secuencia del ejecutor masivo}
	\label{fig:massive_seq}
  \end{figure}
  
\end{document}