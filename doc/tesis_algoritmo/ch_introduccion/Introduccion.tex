\documentclass[../main.tex]{subfiles}

\begin{document}

El presente informe tiene por objetivo describir el trabajo realizado como Proyecto Integrador de la carrera Ingeniería en Computación de la Facultad de Ciencias Exactas, Físicas y Naturales (FCEFyN) de la Universidad Nacional de Córdoba (UNC). A continuación, se desarrollará la formulación del problema que se pretende abordar, los objetivos propuestos, la metodología empleada para alcanzarlos, como así también los resultados y conclusiones a los cuales se arribaron.

Las Redes de Petri son una herramienta gráfica y formalizada, que permiten describir y analizar sistemas concurrentes, asíncronos, distribuidos, paralelos, no deterministicos y/o estocásticos. Son utilizadas ampliamente en el campo del modelado de sistemas de fabricación flexible (FMS) debido a sus características intrínsecas como concurrencia, sincronización, conflictos y activación de eventos.

Las propiedades de estas redes nos permiten detectar fenómenos de interés o errores de funcionamiento en el sistema que modelan. Entre ellos se encuentran: Vivacidad, Ciclicidad, Limitación, Conservación, Conflictividad, Exclusión mutua. 

En este trabajo final se pone especial atención al estudio de los interbloqueos del sistema o deadlock mediante técnicas de análisis basadas en el árbol de alcanzabilidad de un subtipo particular de redes denominadas S3PR. Se presenta una metodología que aplica inteligencia artificial para liberar al diseñador en el proceso de incorporación de mecanismos de control con el fin de detectar, prevenir y solventar de forma automatizada y eficiente los interbloqueos.

% introducción general
\section{Antecedentes}
\label{ss:antec}
Este trabajo se encuentra fuertemente ligado a varios proyectos integradores (PI) que fueron desarrollados en el ámbito del Laboratorio de Arquitecturas de Computadoras con anterioridad, e incluso algunos de ellos que aún se encuentran en vías de desarrollo.

Surge como evolución de lo expuesto en los PI “Determinación automática del control de una red de Petri” del autor Navarro, y continuado luego por los autores Izquierdo y Salvatierra \cite{tesissalvatierra}, ambos presentados en el año 2020. En ellos, se introdujo un mecanismo para identificar y corregir deadlocks basado en la teoría de sifones de manera manual. La principal dificultad allí radica en que sólo una persona con un buen conocimiento de la problemática representada a través de la red puede decidir correctamente qué modificación resulta conveniente incorporar en cada situación.

Por otro lado, este proyecto se encuentra completamente integrado al software Petrinator \cite{petrinator}, también desarrollado por diferentes estudiantes a lo largo de más de 5 años dentro del laboratorio. Particularmente, hemos hecho uso de la última versión desarrollada en el proyecto integrador “Integración Módulos Petrinator” de los autores Bosack y Oliva \cite{tesisbosack} desarrollada en el año 2021. En esta versión se introducen mejoras necesarias para el presente trabajo, como por ejemplo validar si la red analizada cumple con la condiciones suficientes para  ser considerada del subtipo S3PR.

Sin embargo, nótese que la solución que aquí se presenta es independiente del software Petrinator, pudiendo este ser reemplazado por cualquier otro software analizador de redes de Petri disponible.

Finalmente, en el transcurso del desarrollo de este proyecto surgió la necesidad de disponer de grandes cantidades de redes de Petri del tipo S3PR para poder validar la solución propuesta. Esto derivó en el PI “Generador automático, aleatorio y configurable de redes de Petri para el modelado de sistemas de fabricación flexible” publicado en forma paralela por el autor Monsierra \cite{tesismonsierra}. Tanto en este último como en los otros dos proyectos mencionados anteriormente se trabajó colaborativamente aportando ideas, conocimientos y experiencias para alcanzar objetivos comunes. Esto ha permitido a través del tiempo mejorar la calidad del ecosistema de herramientas que se disponen en el LAC para estudios relacionados con las redes de Petri en general.

\section{Relevancia}
\label{ss:relev}

Este estudio se basa principalmente en la ejecución concurrente de procesos de trabajo en sistemas de fabricación flexible (FMS) a través de una clase especial de redes de Petri que se denominan S3PR. Además en la actualidad,la mayoría de los procesos informáticos utilizados en sistemas embebidos emplean y comparten recursos mientras realizan una tarea colaborativa por lo que se asemejan mucho a los FMS y también pueden ser modelados mediante redes de ese tipo. 

Por lo general un sistema FMS está estructurado como un grupo de estaciones de trabajo interconectadas por medio de un sistema de transporte de materiales automatizado. El sistema de transporte, así como otros sistema de almacenamiento que pueden utilizarse deben ser automáticos. Los procesos de trabajo en un FMS son ejecutados al mismo tiempo, y por lo tanto, tienen que competir por el conjunto de recursos comunes. Estas relaciones de competencia pueden causar interbloqueos.  

En términos generales, el deadlock o interbloqueo es un estado del sistema en el cual algunos procesos de trabajo nunca pueden terminarse. Generalmente, detrás de un problema de deadlock existe una situación de espera circular para acceder a un conjunto de recursos. El análisis de las características dinámicas de este subtipo de redes planteadas nos permite identificar situaciones de deadlock en términos de marcado cero para algunos elementos estructurales llamados sifones.

Cuando las situaciones de deadlock aparecen en un sistema, es importante caracterizarlas para poder evitar que el sistema alcance ese estado o lograr que el sistema se recupere de ellos. Con este objetivo en mente, se pone el foco de atención en la prevención y evasión de situaciones de deadlock. La técnica utilizada es la la incorporación de ciertos mecanismos de control denominados supervisores de recursos que se basa en la adición de nuevas plazas a la red imponiendo restricciones que previenen la presencia de sifones sin marcar (causa directa de los interbloqueos), preservando en todo momento la funcionalidad original.

\section{Motivaciones}
\label{ss:motiv}

Al diseñar un procedimiento, se tiene la incertidumbre si este permitirá, luego de su aplicación, obtener los resultados esperados. Es muy frecuente que, durante su aplicación, emerjan dificultades de coordinación entre las diferentes actividades, tareas y  pasos. 

Las redes de Petri fueron utilizadas inicialmente para el análisis de algoritmos en la computación paralela o concurrente, pero dada la complejidad de los procesos productivos actuales, estas constituyen un método alternativo de diseño tanto para el proceso industrial como para el controlador. La fortaleza del modelado de las RdP radica en sus propiedades, que se dividen en dos grandes áreas, las dependientes del marcado inicial llamadas propiedades dinámicas o del comportamiento y las propiedades independientes del marcado, llamadas estructurales o estáticas.

En términos generales, el deadlock es un estado del sistema en el cual algunos procesos de trabajo nunca pueden terminarse. Generalmente, detrás de un problema de deadlock existe una situación de espera circular para acceder a un conjunto de recursos. Este estado de punto muerto o deadlock provoca que el sistema quede bloqueado y no pueda continuar con su producción, por lo que diseñar un sistema asegurando que esto no suceda, es de suma importancia. 

En el Laboratorio de Arquitecturas de Computadoras se han estado llevando adelante diferentes proyectos que intentan automatizar la detección y eliminación del deadlock en el diseño de un sistema mediante la adición de supervisores. Estos son representados por plazas en las redes de Petri que se añaden para evitar que determinados sifones no se vacíen y así asegurar que el diseño del sistema estará libre de deadlock durante toda su ejecución. Todas las investigaciones realizadas hasta el momento han sido sobre redes de Petri que hemos definido como redes conocidas. Estas son redes recolectadas en los últimos cinco años que modelan diferentes sistemas y que se han utilizado en diferentes papers o estudios afines. 

Hasta el momento se han logrado buenos resultados en los diferentes proyectos llevados a cabo en el LAC, pero siempre verificando los mismos utilizando el conjunto de redes conocidas. Para continuar avanzando, se necesita extender estos análisis a un grupo de redes más amplio y variado. Sobre este nuevo set compuesto por redes conocidas y auto generadas se desea aplicar Inteligencia Artificial a fines de incorporar automáticamente los supervisores necesarios para evitar el deadlock, liberando al diseñador del proceso de toma de decisiones para lograrlo. Sumado a la resolución del interbloqueo es necesario que la solución garantice una cantidad mínima de supervisores procurando además  alcanzar el máximo índice de paralelismo estimado para esa red y combinación de supervisores resultante de forma que las restricciones introducidas sobre la red original tengan el menor impacto en el desempeño posible.

\section{Formulación del Problema}
\label{ss:fprob}

En 1962, Petri inventó un enfoque teórico de la red para modelar y analizar los sistemas de comunicación en su tesis \cite{petri1962kommunikation}. Este modelo se basó en los conceptos de funcionamiento asíncrono y concurrente de las partes de un sistema y en la comprensión de que las relaciones entre las partes podían representarse mediante una red. Se ha realizado una gran cantidad de investigaciones tanto sobre la naturaleza como sobre la aplicación de las redes de Petri, la cuál parece estar en expansión.\\
Se ha demostrado que las redes de Petri son muy útiles en el modelado, análisis, simulación y control de sistemas concurrentes.

\par El flujo simultáneo de varios procesos en un sistema de asignación de recursos (RAS), que compiten por un conjunto finito de recursos, puede conducir a un punto muerto. Un interbloqueo (deadlock) ocurre cuando un conjunto de procesos se encuentra en un estado de ``espera circular''\footnote{Definido en el Marco Teórico (Sección \ref{sec:Interbloqueo})}, donde cada proceso del conjunto está esperando que un recurso sea liberado por otro proceso del conjunto mientras ocupa un recurso que, a su vez, es necesario por uno de los otros procesos. La noción de deadlock parcial o total es frecuente y es preferible la validación antes de la implementación para reducir los riesgos \cite{LIU2016198}.

Los estado de deadlock son una situación bastante indeseable en un sistema de fabricación automatizado. Su ocurrencia a menudo deteriora la utilización de recursos y pueden conducir a resultados catastróficos en sistemas críticos para la seguridad. Las redes de Petri son una herramienta matemática importante para manejar problemas de interbloqueo en sistemas de asignación de recursos.

A partir de la bibliografía estudiada vemos como diversos autores han abordado el tema con diferentes estrategias, las cuales se resumen a continuación.

\subsection{Estrategias de manejo de deadlock}

\subsubsection{Ignorar (Deadlock ignoring)}
Ignorar los estados de deadlock, que se conoce como el algoritmo de Ostrich \footnote{Concepto informático para denominar el procedimiento de algunos sistemas operativos. Esta teoría, acuñada por A. S. Tanenbaum, señala que dichos sistemas, en lugar de enfrentar el problema de los bloqueos mutuos asumen que estos nunca ocurrirán.}, se emplea en un sistema de asignación de recursos si la probabilidad de que se produzcan puntos muertos es mínima y la aplicación de otras estrategias de control de deadlock es técnicamente difícil. En un FMS o AMS, ignorar el estado deadlock es factible y razonable desde el punto de vista técnico y económico si el grado de intercambio de recursos es bajo.

\subsubsection{Prevenir (Deadlock prevention)}
Se logra controlando la solicitud de recursos y garantizando que nunca se produzcan estados de deadlock. Los recursos se otorgan a los procesos de tal manera que una solicitud de un recurso nunca conduce a situaciones de deadlock. El objetivo es imponer limitaciones a la evolución de un sistema. En este caso, el calculo se realiza offline de forma estática y una vez establecida una política de control, el sistema ya no puede alcanzar esos estados indeseables.
Una ventaja importante de los algoritmos de prevención de interbloqueo es que no requieren ningún costo de tiempo de ejecución, ya que los problemas se resuelven en las etapas de diseño y planificación del sistema.La principal crítica es que tienden a ser demasiado conservadores, lo que reduce la utilización de recursos y la productividad del sistema.

\subsubsection{Evitar (Deadlock avoidance)}
Para evitar los estados de deadlock se concede un recurso a un proceso solo si el estado resultante es seguro. Un estado se denomina seguro si existe al menos una secuencia de ejecución que permite que todos los procesos se ejecuten hasta su finalización. Para decidir si el próximo estado es seguro si se asigna un recurso a un proceso se debe realizar un seguimiento del estado del sistema global. Esto significa que son necesarios un gran almacenamiento y una amplia capacidad de comunicación.

\subsubsection{Detectar y recuperar (Deadlock detection and recovery)}
Se otorgan recursos a un proceso sin ningún control. El estado de la asignación de recursos y las solicitudes se examinan periódicamente para determinar si un conjunto de procesos está bloqueado. Este examen se realiza mediante un algoritmo de detección de interbloqueo. Si se encuentra un interbloqueo, el sistema se recupera abortando uno o más procesos interbloqueados y entregandole los recursos liberados a otros procesos. En la práctica de fabricación, a menudo se necesitan operadores humanos para esta estrategia y, por lo tanto, puede resultar muy costoso.

\subsection{Revisión de la literatura}
Las políticas de prevención de deadlock se han logrado ampliamente y han dado lugar a una gran cantidad de resultados. En esta sección, se revisan las estrategias de \textbf{prevención} de deadlock mediante el uso de redes de Petri y se desarrollan en base a diferentes técnicas como el análisis estructural y el análisis del grafo de alcanzabilidad.

\subsubsection{Métodos de análisis estructural}
Ezpeleta et al. \cite{paperezpeleta} utilizó una clase de redes de Petri, las del tipo S³PR y propuso un algoritmo para la asignación de recursos en FMS. El algoritmo propuesto agregó nuevos lugares a la red para imponer ciertas restricciones que prohíben la presencia de sifones vacíos.

Los trabajos de Huang et al. \cite{YiShengBook} y Huang y et al. \cite{YHuangandJeng} presentan una nueva política de prevención de deadlock para la clase de redes de Petri, donde los estados de deadlock están relacionados con sifones sin marcar. Se agregan dos tipos de lugares de control al modelo original para un sistema de fabricación flexible llamado lugar de control ordinario y lugar de control ponderado para evitar que los sifones se desmarquen.

En Li y Wei \cite{LiandWei} , se introduce el concepto de sifones elementales para diseñar un supervisor de red de Petri que haga cumplir la vivacidad para el mismo modelo de red de Petri. Basado en sifones elementales y conceptos de P-invariantes en redes de Petri, Li y Wei introducen un algoritmo de prevención de interbloqueo para una clase específica de redes de Petri que pueden modelar adecuadamente varios FMS \cite{LiandWei}, los sifones en un modelo de red de Petri se clasifican en dependientes y sifones elementales, y se agregan lugares de control para todos los sifones elementales, de modo que los sifones están controlados de forma invariante.

Huang \cite{Huang2007} propone una nueva metodología para sintetizar supervisores para la asignación de recursos en los FMS; se considera la clase de red Petri, a saber, S³PR, donde los puntos muertos están relacionados con sifones mínimos no marcados; todos los sifones mínimos deben controlarse agregando plazas de control. En este estudio, el número de plazas de control se reduce utilizando el concepto de sifón elemental.

Chen et al. \cite{paperchen}, presenta un algoritmo de prevención de deadlock y el concepto de extracción por sifón se utiliza para calcular sifones no marcados para el modelo de red de Petri. Primero, un algoritmo de extracción de sifón obtiene un sifón no marcado máximo, divide los lugares en él y determina un sifón necesario de los lugares divididos; esto se lleva a cabo para todos los sifones sin marcar. Luego, el algoritmo diseña un monitor conveniente para marcar cada sifón necesario hasta que el modelo de red de Petri controlado esté activo.

Liu et al. \cite{LiuanLi2013} propuso una variedad de algoritmos de control de interbloqueo para AMS con recursos no confiables, los monitores y las subredes de recuperación están diseñadas para sifones mínimos estrictos (sifón vacío) y recursos no confiables, respectivamente, y se utilizan dos tipos de arcos que son normales e inhibidores para conectar monitores con subredes de recuperación. Para obtener un supervisor con complejidad estructural, los sifones elementales extraídos de todos los sifones mínimos estrictos están obviamente controlados. 


\subsubsection{Enfoques basados en el grafo de alcanzabilidad}
Viswanadham et al. \cite{Viswanadham} presentó métodos de asignación de recursos estáticos para eliminar los puntos muertos; En este estudio, se utiliza un grafo de alcanzabilidad del modelo de red de Petri para llegar al método de asignación de recursos estático. Se implementa un algoritmo de prevención de interbloqueo para un sistema de fabricación de tamaño pequeño que consta de una máquina y un vehículo guiado automatizado (AGV). Los autores observaron que el algoritmo propuesto se puede aplicar de manera efectiva sólo para sistemas de fabricación de tamaño pequeño.

\par El trabajo de Uzam \cite{uzam2004, uzam2002} propone y mejora el método basado en una teoría de regiones para diseñar un supervisor de red de Petri óptimo. El tamaño del grafo de alcanzabilidad del modelo de red de Petri es un problema importante para aplicar la política de prevención de interbloqueos a un modelo de red de Petri muy grande. Por lo tanto, propusieron un algoritmo de reducción para simplificar modelos de redes de Petri muy grandes con el fin de facilitar los cálculos necesarios. Basado en la teoría de las regiones, Uzam y Zhou \cite{uzamandzhou2004} desarrollaron una política iterativa de prevención de interbloqueos para FMS. En su estudio, el grafo de alcanzabilidad de un modelo de red de Petri se divide en dos partes: zona libre de bloqueo (zona activa, LZ) y zona de bloqueo (DZ). La zona viva se desarrolla ya que el componente máximo fuertemente conectado contiene la marca inicial. La zona de interbloqueo contiene marcas desde las que no se puede alcanzar la marca inicial. FBM está definido como una marca en la zona de interbloqueo. Los interbloqueos se pueden eliminar prohibiendo el disparo de las transiciones habilitadas en FBM. En su trabajo, el enfoque presentado tiene dos problemas. Primero, no se puede garantizar un supervisor óptimo en general, incluso si existe un supervisor óptimo. En segundo lugar, en cada iteración se requiere el cálculo del grafo de alcanzabilidad total para verificar si las marcas en la zona de interbloqueo son accesibles. Este enfoque es fácil de usar y simple si el grafo de alcanzabilidad de un sistema es pequeño pero no puede garantizar la optimización del comportamiento del supervisor. Los monitores redundantes en el supervisor de red de Petri pueden existir cuando el supervisor está diseñado mediante un enfoque de control de sifón iterativo. Por tanto, Uzam et al. \cite{uzammuratli2007} introdujo un enfoque para identificar y eliminar los monitores redundantes mediante el cálculo del gráfico de accesibilidad de un modelo de red de Petri controlado. Si la red de Petri controlada no pierde la vivacidad cuando se eliminan los monitores redundantes, entonces los monitores redundantes se pueden eliminar del supervisor.\\

El objetivo de esta sección fue presentar una revisión de la literatura sobre los distintos enfoques planteados hasta el momento respecto a la prevención de los estados de deadlock.
Un enfoque híbrido de control de interbloqueos se refiere a aquel que combina distintas de estas planificaciones para tratarlos. La motivación esencial de plantear una estrategia de este tipo es aprovechar las ventajas o formalismos de múltiples de estas, evitando sus desventajas. Esto fue puntualmente lo que nos motivó a no regirnos por una única estrategia o estudio planteado.

\section{Objetivos}
Durante los últimos años el Laboratorio de Arquitectura de Computadoras ha llevado adelante diferentes proyectos e investigaciones con el propósito de lograr evitar el deadlock  en redes de Petri. Específicamente en un tipo de redes que modelan a Sistemas de Fabricación Flexible denominadas S3PR.

Estos proyectos han permitido recopilar a lo largo del tiempo diferentes redes de Petri con variadas características, que modelan distintos sistemas. Todas estas han sido extraídas de diferentes papers e investigaciones en donde se intentaban resolver problemas relacionados a las redes de Petri. El conjunto de redes recolectado lo denominamos redes conocidas, y cuenta con diferentes redes, las cuales han sido de enorme utilidad para los proyectos realizados hasta el momento.  

Lo que se pretende en este trabajo final es extender las investigaciones enfocadas en la solución de problemas de deadlock a un grupo mayor de redes con características particulares, pero siempre encuadradas dentro del subgrupo de interés original.

La manera en que se abordará será implementando Inteligencia Artificial con el objetivo de liberar al diseñador de la toma de decisiones en el proceso de incorporación de modificaciones a la red para evitar el interbloqueo.

Esta solución se basa en la integración de mecanismos de control, denominados supervisores. Una vez alcanzado el estado deseado, se realiza un análisis y posterior reducción de los mismos en los casos que se detecten redundancias. De esta forma, procuramos resolver el deadlock a través de una red que cuente con una cantidad mínima de supervisores y que al mismo tiempo logre el mayor índice de paralelismo estimado posible.

Cabe destacar el carácter exploratorio e investigativo de este proyecto, donde además de incursionar en uno de los campos de inteligencia artificial de mayor potencial de explotación se busca profundizar en el conocimiento de las propiedades dinámicas de las redes de Petri.

\subsection{Objetivos Generales}
A partir de las problemáticas descritas anteriormente, surgen cuatro objetivos claves para llevar adelante este proyecto, los cuales detallamos a continuación: 

\begin{itemize}
\item Desarrollar un algoritmo para automatizar el control del interbloqueo de las redes de Petri.
\item Desarrollar un método para detectar y eliminar supervisores redundantes.
\item Analizar métricas para determinar la solución óptima.
\item Explorar posibles implementaciones utilizando redes neuronales.
\end{itemize}

Estos cuatro objetivos tienen la finalidad de atacar la problemática explicada en la sección anterior para permitir a futuros proyectos avanzar en nuevas líneas investigación donde se cuente con una mayor cantidad de información, como así también, comprobar que las diferentes soluciones desarrolladas se adaptan correctamente a redes de Petri con diferentes características, lo que representa diferentes configuraciones de los sistemas FMS.

\subsection{Objetivos Específicos}
Los objetivos principales permiten tener en claro las metas a cumplir en el desarrollo de este  trabajo. Se decidió extender estos mismos en objetivos específicos que posibiliten comprender de una manera más detallada el propósito que se busca  alcanzar:

\begin {itemize}
\item Caracterizar de forma exhaustiva las especificaciones de los sistemas FMS y del subtipo de red de Petri S3PR asociado.
\item Resolver el problema de deadlock en redes de Petri del tipo S3PR usando Inteligencia Artificial.
\item Explorar y definir métricas para evaluar las alternativas de las soluciones encontradas. 
\item Clasificar los resultados de los análisis obtenidos.
\item Analizar los supervisores agregados y desechar aquellos en los que se puedan haber introducido redundancias.
\end{itemize}

Al tratarse de un trabajo de investigación en el cual se fue evolucionando sobre distintos emergentes surgidos a lo largo de su desarrollo, se fueron incorporando diferentes objetivos intermedios mediante los que se logró alcanzar gradualmente los objetivos principales antes mencionados.

\section{Requerimientos}
En ingeniería, los requerimientos se utilizan como datos de entrada en la etapa de diseño del producto. Establecen qué debe hacer el sistema, aunque no especifican la manera en que debe hacerlo. La fase de captura y registro de requisitos puede estar precedida por una fase de análisis conceptual del proyecto. A continuación se detalla un listado de los requerimientos definidos para este proyecto.

\subsection{Listado de requerimientos}
\reqinit
\reqstart
	\item Se debe utilizar el software Petrinator para obtener los datos necesarios relacionados a la Red de Petri modelada en un archivo de extensión .pflow. Además de determinar sus capacidades y alcance.
	\item La integración con el software Petrinator debe ser automática, es decir, el usuario no necesita interactuar con la herramienta Petrinator.
	\item La metodología propuesta debe converger en redes de Petri del tipo S3PR.
	\item La metodología propuesta se debe ejecutar de forma automática usando Inteligencia Artificial. 
	\item La metodología propuesta debe ser capaz de analizar redes de forma conjunta.
	\item La metodología propuesta debe proveer métricas sobre los resultados.
	\item El programa debe ser capaz de clasificar y agrupar en conjuntos definidos las redes de Petri analizadas.
	\item La metodología debe ser capaz de detectar y eliminar supervisores redundantes.
	\item La metodología debe proveer una solución que cumpla con las especificaciones deseadas, en base a cantidad de supervisores y paralelismo estimado.	
\reqend

\section{Análisis de riesgos}
Un riesgo es un evento o condición incierta que, en caso de ocurrir, tendrá consecuencias negativas sobre al menos uno de los requerimientos del proyecto. Por esta razón es importante identificarlos con anticipación para mitigarlos.

\subsection{Listado de riesgos}

\begin{table}[ht]
\centering
\begin{tabular}{|l|l|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{ID}} & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}\textbf{Descripción}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
R1 & Herramienta inadecuada                \\ \hline
\rowcolor[HTML]{FFFFFF} 
R2 & Mecanismo de IA inadecuado                  \\ \hline
\rowcolor[HTML]{FFFFFF} 
R3 & Integración limitada                \\ \hline
\rowcolor[HTML]{FFFFFF} 
R4 & Tiempo de aprendizaje de uso excesivo \\ \hline
\rowcolor[HTML]{FFFFFF} 
R5 & Métricas inadecuadas           \\ \hline
\rowcolor[HTML]{FFFFFF} 
R6 & Tiempo de respuesta inadecuado        \\ \hline
\end{tabular}
\caption{Tabla de riesgos}
\label{tab:riesg}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R1 - Herramienta inadecuada}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Elección incorrecta de la herramienta para clasificar las Redes de Petri o utilización errónea. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} La herramienta no se adapta a nuestros requerimientos.               \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Sustitución de la herramienta por otra que cumpla nuestros requerimientos. \\ \hline
\end{tabular}%
}
\caption{R1 - Herramienta inadecuada}
\label{tab:r1}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R2 - Mecanismo de IA inadecuado}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} El mecanismo de IA no se ajusta a las necesidades. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Demora en el cumplimiento de los objetivos.                \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Implementar un nuevo método que se ajuste a los requisitos. \\ \hline
\end{tabular}%
}
\caption{R2 - Mecanismo de IA inadecuado}
\label{tab:r2}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R3 - Integración limitada}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} La integración con el software elegido para generar la información de las redes es deficiente. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Errores y demoras durante los análisis.                 \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Análisis limitados, sobre set de redes específicas. \\ \hline
\end{tabular}%
}
\caption{R3 - Integración limitada}
\label{tab:r3}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R4 - Tiempo de aprendizaje excesivo}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Un nuevo usuario requiere demasiado tiempo en comprender cómo se utiliza el sistema. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Retraso en la investigación a desarrollar y pérdida de tiempo en la comprensión de la herramienta.                \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Readecuación del generador para reducir la dificultad de uso.   \\ \hline
\end{tabular}%
}
\caption{R4 - Tiempo de aprendizaje de uso excesivo}
\label{tab:r4}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R5 - Métricas inadecuadas}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Las métricas generadas son inaprovechables. \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Conclusiones erróneas y dificultad en aplicar mejoras.                \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto:} Incumplimiento de los objetivos.   \\ \hline
\end{tabular}%
}
\caption{R5 - Métricas inadecuadas}
\label{tab:r5}
\end{table}

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|p{14,5cm}|}
\hline
\rowcolor[HTML]{EFEFEF} 
\multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{R6 - Tiempo de respuesta inadecuado}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Condición:} Calidad deficiente del análisis \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Consecuencia:} Los análisis de las redes de Petri consumen demasiado tiempo.          \\ \hline
\rowcolor[HTML]{FFFFFF} 
\textbf{Efecto: }Obstaculiza el correcto entrenamiento de redes neuronales y demora la investigación de los resultados.    \\ \hline
\end{tabular}%
}
\caption{R6 - Tiempo de respuesta inadecuado}
\label{tab:r6}
\end{table}
\clearpage
	
\section{Organización del Informe}
El presente informe consta de seis capítulos, más un apartado de apéndices y se divide en cuatro partes, las cuales se listan a continuación: Marco teórico, Marco Metodológico, Resultados y Conclusiones.

En el primer capítulo de este informe se introduce al lector en la temática abordada, haciéndole notar cuales son los antecedentes y la relevancia del mismo. Luego se exponen las motivaciones junto con una revisión bibliográfica del tema abordado a modo de formulación del problema. Se detallan también los objetivos y los requerimientos tanto de modo global como particular y se exponen algunos de los riesgos identificados antes de su desarrollo.

La primera parte, dividida en dos capítulos, abarca el marco teórico en el cual se exponen primero en el capítulo dos, todos los conceptos necesarios para comprender el funcionamiento de las redes de Petri así como también una definición formal del subtipo S3PR. Luego, en el tercer capítulo, se realiza una introducción a las diferentes técnicas de IA conocidas, profundizando en conceptos de aprendizaje por refuerzo (técnica elegida para el desarrollo de este PI).

En la segunda parte, denominada Marco Metodológico, se encuentran contenidos el cuarto y quinto capítulo. En el primero de ellos, Diseño de la solución,  se detalla en forma exhaustiva cómo se ideó la solución propuesta, haciendo uso de una arquitectura modular. Además, se describen en forma detallada cada uno de los módulos que la componen. El capítulo siguiente, Desarrollo, incluye estudios realizados sobre redes conocidas usando la metodología base y como eso fue derivando en las diferentes mejoras y corrección de bugs que se implementaron.

En la tercera parte, o de Resultados, se explican las pruebas realizadas sobre el sistema luego de concluir su desarrollo. Aquí se contrastan las diferencias entre la metodología base utilizada y la metodología final con mejoras y corrección de errores aplicadas, las cuales se exponen en diferentes gráficas y tablas. Además se elaboran conclusiones parciales a raíz de los resultados obtenidos para cada una de ellas.

En la cuarta parte y última parte, o de Conclusiones, desarrollada en el capítulo seis se elaboran conclusiones generales a partir de los resultados obtenidos y se realizan propuestas de mejora a futuro.

Sobre el final del documento se encuentra la sección de apéndices, en la cual se profundiza sobre algunas cuestiones que podrían resultar de interés al lector y ampliar su comprensión sobre esta tesis.

\end{document}