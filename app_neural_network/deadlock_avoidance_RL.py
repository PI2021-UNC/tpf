#!/usr/bin/python3

"""
Determinacion automática del control de una red de Petri

Analiza las RdP y determina los supervisores para el control de los deadlock presentes en dicha red.

Autores:
- Lenta, Luis Alejandro
- Monsierra, Lucas Gabriel
- Nicolaide, Christian
"""

import os
import sys
import argparse
import subprocess
import numpy as np
import html_txt_all as hta
import filter_data as filterdata
import new_red
import arcs as arcosrdp
from gym import Env
from custom_print import *
from distutils.dir_util import copy_tree

# TODO ver la forma de mejorar custom print para no tener que pasarle siempre el path
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # Solo loguea errores de TensorFlow. Sino se imprime info de debug como la de CUDA.


# PETRINATOR OPTIONS: Se construye el comando para ejecutar la red: java_command<string> petrinator_path<string>
# pflow_file<string> output_folder<string> bring_gui<boolean> auto_close<boolean> current_iteration<int> clasify<boolean> redirect_output<string>
java_command = "java -Xms8G -Xmx8G -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=20 -XX:ConcGCThreads=5 -XX:InitiatingHeapOccupancyPercent=70 -jar"  # Comando java
petrinator_path = "../auto_petrinator/Petrinator-1.0-jar-with-dependencies.jar"   # Path al jar automizado
pflow_file = "../test_path/net.pflow"                                             # Ruta al pflow por defecto (ojo esta hardcodeado net.pflow en el jar)
output_folder = "../test_path"                                                    # Carpeta de salida al pflow (ojo esta hardcodeado net.pflow en el jar)
bring_gui = "false"                                                               # Habilitar o deshabilitar la gui de petrinator para ver la red
auto_close = "true"                                                               # Con auto_close en false y bring_gui en true se puede hacer debug paso a paso viendo evolucion graficamente
current_iteration = 0                                                             # Para saber si se corre el analisis S3PR y deadlock o no
clasify = "false"                                                                 # Para clasificacion de autogenerador de redes
redirect_output = "> /dev/null"                                                   # Quitar "> /dev/null" para debug del jar
petrinator_timeout = 300                                                          # Si tarda mas que esto, corta la ejecucion de petrinator y marca la red como NO.UNLOCKED

# ALGORITHM OPTIONS: Variables que usa el algoritmo para operar
id = 0                                                                            # ID del sifon (creo)
name_pflow = ""                                                                   # Nombre del pflow
q_values = np.zeros((5, 4))                                                       # Random values de q
html_path = "../test_path"                                                        # Carpeta de salida de los html
txt_path = "../test_path/txt_info"                                                # Carpeta de salida de los txt
step_execution = False                                                            # Flag para pedir enter para actualizar red y despues de cada accion
createdir = True                                                                  # Permite creacion automatica del output_folder, html_path o txt_path si no existen
trainning_num = 1                                                                 # Numero de iteraciones del algoritmo
iteration_limit = 75                                                              # Limite de iteraciones para intentar destrabar la red
train_networks_directory = "../train_networks"                                    # Carpeta con redes para entrenar la red neuronal.
episodes_directory = "../train_episodes"                                          # Carpeta con todos los pasos del entrenamiento
print_traceback = False                                                           # Flag de impresion de traceback en caso de excepcion
cant_supervisores = 0
ACTIONS = ['Agregar supervisor', 'Agregar o quitar arcos', 'Eliminar supervisor']


# CLASS DEFINITIONS: q significa quantity; m significa matrix
class Network:
    def __init__(self):
        global html_path, txt_path

        # Inicializo variables para el save STAT del massive run
        self.q_initial_siphons = 0
        self.q_initial_places = 0
        self.q_initial_transitions = 0
        self.q_initial_states = 0
        self.q_states = 0

        # Lista de supervisores agregados
        self.added_supervisors = []
        # Lista de supervisores eliminados
        self.deleted_supervisors = []
        self.save_state()
        self.petrinator_failed = False

        self.call_petrinaitor(0) # Zero indicate first time
        self.not_deadlock = False
        self.not_s3pr = False

        if self.petrinator_failed:
            return

        # Si no es S3PR abandono el analisis
        if os.path.isfile(output_folder + "/NO.S3PR"):
            c_print("[ERROR] La red ingresada NO ES S3PR, abandonando analisis", "\n", log_path=txt_path)
            self.not_s3pr = True
            return

        # Si no hay DEADLOCK abandono el analisis
        if os.path.isfile(output_folder + "/NO.DEADLOCK"):
            c_print("[ERROR] La red ingresada NO TIENE DEADLOCK, abandonando analisis", "\n", log_path=txt_path)
            self.not_deadlock = True
            return

        self.initial_mark = [] # Marcado inicial

        try:
            hta.main(html_path, txt_path)
        except Exception as e:
            c_print("[ERROR] Se produjo un FALLO INESPERADO en la ejecucion del petrinator, abandonando analisis", "\n", log_path=txt_path)
            subprocess.Popen("touch " + output_folder + "/FAILED", shell=True).communicate()
            self.petrinator_failed = True
            if (print_traceback):
                print(e.with_traceback())
            return

        # Filtrado de archivos provenientes del Petrinator
        (self.q_states, self.q_places, self.q_transitions, self.m_states_transitions, self.m_states_places, self.state_deadlock) = filterdata.main(txt_path)

        # Seteamos si tiene deadlock en el estado inicial
        self.initial_deadlock = False
        if len(self.state_deadlock) == 1:
            if self.state_deadlock[0] == 'S0':
                self.initial_deadlock = True

        # Cantidad de sifones y trampas
        self.q_siphons = 0
        self.q_traps = 0

        # Matrices
        self.m_pos = []
        self.m_pre = []
        self.m_siphons = np.zeros((self.q_siphons, self.q_places))
        self.m_traps = np.zeros((self.q_traps, self.q_places))
        self.siphons_traps()
        self.m_pre_pos()

        # T-invariantes
        self.t_invariant = self.invariants()
        self.t_invariant_original_net = self.invariants()

        # Guardo valores iniciales
        self.q_initial_siphons = self.q_siphons
        self.q_initial_places = self.q_places
        self.q_initial_transitions = self.q_transitions
        self.q_initial_states = self.q_states

        # Lista de sifones: Cada item tiene estado ID, Nro, plazas que componen el sifon
        self.siphons = []

        # Lista de sifones marcados en estado deadlock
        # Cada item tiene estado de deadlock, numero de sifon y marcado del sifon
        self.deadlock_siphons = []
        # Lista de supervisores
        self.supervisors = []
        self.q_supervisors = 0

        # Obtenemos la cantidad de plazas de la red original
        file_plazas = open(txt_path + "/cantidad_plazas_red_original.txt", "w")
        file_plazas.write(str(len(self.m_states_places[0])))
        self.q_places_original_net = len(self.m_states_places[0])
        file_plazas.close()

        # Guardamos los T-invariantes de la red original
        file_t_inv_orig = open(txt_path + "/invariante_red_original.txt", "w")
        for i in range(0, len(self.t_invariant)):
            for j in range(0, len(self.t_invariant[0])):
                file_t_inv_orig.write(str(self.t_invariant[i][j]) + ' ')
            file_t_inv_orig.write("\n")
        file_t_inv_orig.close()

        file_t_conflict_orig = open(txt_path + "/t_conflict_red_original.txt", "w")
        self.t_conflict_orig = []
        file_t_conflict_orig.close()

        self.find_supervisors()

        # Guardo estadistica
        self.save_state()

        # Guardo cantidad de tokens en plazas de acciones
        self.tokens_doing_something = self.m_states_places[:, self.m_states_places[0, :] == 0].sum()

    def siphons_traps(self):
        """
        Devuelve una matriz de [sifones x plazas] y otra de [trampas x plazas]. Tambien devuelve cantidad de sifones
        y de trampas.

        Parameters
        ----------
            cantidad_plazas     -- Cantidad de plazas de la RdP

        Returns
        -------
            matriz_sifones      -- Matriz de sifones
            matriz_traps        -- Matriz de trampas
            cantidad_sifones    -- Cantidad de sifones
            cantidad_traps      -- Cantidad de trampas
        """

        # Apertura de archivos resultantes de la conversion de archivos .html to .txt obtenidos del SW Petrinator,
        # para su siguiente manipulacion y filtrado.
        pasi = open(txt_path + "/siphons_traps.txt", "r")
        i = 0
        aux_s = 0
        aux_t = 0

        for line in pasi:  # Obtiene la cantidad de trampas y sifones que contiene la RdP
            i = i + 1
            if i > 1:
                aux_s = aux_s + 1
                aux_t = aux_t + 1
                if line.find("Minimal traps") == 1:
                    self.q_siphons = aux_s - 1
                    aux_t = 0
                if line.find("Analysis") == 1:
                    self.q_traps = aux_t - 1

        pasi.seek(0)  # Vuelve el cabezal al principio del archivo
        aux_s = self.q_siphons
        aux_t = self.q_traps

        s_flag = 0
        t_flag = 0

        siphons_aux = []
        traps_aux = []

        for line in pasi:
            if (s_flag == 1 and aux_s != 0):  # Obtiene los sifones de la RdP
                siphons_aux.append(line)
                aux_s = aux_s - 1

            if (t_flag == 1 and aux_t != 0):  # Obtiene las trampas de la RdP
                traps_aux.append(line)
                aux_t = aux_t - 1

            if (line.find("Minimal siphons") == 1):
                s_flag = 1
                # print("Sifones")

            if (line.find("Minimal traps") == 1):
                t_flag = 1
                # print("Trampas")

        siphons = []
        traps = []
        for i in range(len(siphons_aux)):  # Elimina los espacios del string y agrega cada sifon
            siphons.append(str(siphons_aux[i]).split())

        for i in range(len(traps_aux)):  # Elimina los espacios del string y agrega cada trampa
            traps.append(str(traps_aux[i]).split())

        # Creamos la matriz que representa por fila la cantidad de sifones o traps y por columna plazas
        # hay un 1 en las plazas que conforman esos sifones o traps
        self.m_siphons = np.zeros((self.q_siphons, self.q_places))
        self.m_traps = np.zeros((self.q_traps, self.q_places))

        for i in range(0, len(siphons)):
            for j in range(0, len(siphons[i])):
                self.m_siphons[i][int(siphons[i][j]) - 1] = 1

        for i in range(0, len(traps)):
            for j in range(0, len(traps[i])):
                self.m_traps[i][int(traps[i][j]) - 1] = 1

        pasi.close()

    def invariants(self):
        """
        Devuelve la matriz de invariantes de transicion, mediante la apertura de un archivo txt previamente convertido.

        Parameters
        ----------
            cantidad_transiciones   -- Cantidad de transiciones de la RdP

        Returns
        -------
            M_TI                    -- Matriz de T-invariante
        """
        file = open(txt_path + "/invariante.txt", "r")
        cont = 0
        TInvariantes = []
        PInvariantes = []
        for line in file:
            if (cont == 0):
                TInvariantes = line
                cont = cont + 1
            elif (cont == 1):
                PInvariantes = line
                cont = cont + 1

        aux_I = TInvariantes.split(' ')
        TInvariantes = []
        for i in range(self.q_transitions, len(aux_I) - 2):
            TInvariantes.append(aux_I[i])

        M_TI = np.zeros(
            (int(len(TInvariantes) / self.q_transitions), self.q_transitions))
        m = 0
        for i in range((int(len(TInvariantes) / self.q_transitions))):
            for j in range(self.q_transitions):
                M_TI[i][j] = TInvariantes[m]
                m = m + 1

        file.close()
        return M_TI.astype(int)

    def m_pre_pos(self):
        """
        Devuelve la matriz pre y post a partir de un archivo txt previamente convertido.

        Parameters
        ----------
            cantidad_plazas         -- Cantidad de plazas de la RdP
            cantidad_transiciones   -- Cantidad de transiciones de la RdP

        Returns
        -------
            matriz_I_pos            -- Matriz Post (I+)
            matriz_I_neg            -- Matriz Pre  (I-)
        """
        # Apertura de archivos resultantes de la conversion de archivos .html to .txt
        # obtenidos del SW Petrinator, para su siguiente manipulacion y filtrado.
        m_identity = open(txt_path + "/matricesI.txt", "r")
        self.m_pos = np.loadtxt(m_identity, delimiter=' '' ', skiprows=3,
                                max_rows=self.q_places, dtype=bytes).astype(str)
        self.m_pre = np.loadtxt(m_identity, delimiter=' '' ', skiprows=2, max_rows=self.q_places + 1,
                                dtype=bytes).astype(str)

        aux_pos = []
        aux_neg = []
        for i in range(self.q_places):
            aux_pos.append(self.m_pos[i].split(" "))

        for i in range(self.q_places):
            aux_neg.append(self.m_pre[i].split(" "))

        # La ultima fila del archivo tiene el marcado inicial
        aux_initial_mark = self.m_pre[self.q_places].split(" ")
        # Elimino la palabra initial
        aux_initial_mark.pop(0)
        # Elimino el espacio del final
        aux_initial_mark.pop(self.q_places)
        # Guardo la info
        for mark in aux_initial_mark:
            self.initial_mark.append(int(mark))

        aux_pos = np.delete(aux_pos, 0, 1)
        aux_neg = np.delete(aux_neg, 0, 1)
        aux_pos = np.delete(aux_pos, self.q_transitions, 1)
        aux_neg = np.delete(aux_neg, self.q_transitions, 1)

        self.m_pos = aux_pos.astype(int)
        self.m_pre = aux_neg.astype(int)

        m_identity.close()

    def conflict_t_invariant(self, t_conflict, complement_siphon_places, t_in):
        """
        Obtiene las transiciones en conflicto que le tienen que devolver algun token al supervisor.

        Parameters
        ----------
            t_conflict                  -- Transiciones en conflicto
            t_invariant                 -- T-Invariante
            matriz_pos                  -- Matriz Post (I+)
            plazas_sifon_complemento    -- Plazas complemento del sifon a controlar
            t_in                        -- Transiciones de entrada al supervisor
        """
        for ii in range(0, len(t_conflict)):
            flag_sifon = 0
            for jj in range(0, len(self.t_invariant)):
                # La T en conflicto forma parte del T invariante?
                if (int(self.t_invariant[jj][t_conflict[ii]]) == 1):
                    aux_t = np.copy(self.t_invariant[jj])  # Guardamos el T invariante
                    for aa in range(0, len(aux_t)):
                        if (int(aux_t[aa]) == 1):  # Buscamos la T que forma parte del T-invariante
                            for bb in range(0, len(self.m_pos)):
                                if (int(self.m_pos[bb][aa]) == 1):
                                    # La T alimenta a alguna plaza del sifon'
                                    if (int(complement_siphon_places[bb]) == 1):
                                        flag_sifon = 1

            if flag_sifon == 0:
                # custom_print.custom_print("[INFO] Transicion input:", int(t_conflict[ii]) + 1)
                t_in.append("T" + str(int(t_conflict[ii]) + 1))

    def path_conflict(self, t_idle, t_analyze, flag_idle, complement_siphon_places, t_in):
        """
        Se obtienen las transiciones que forman parte del conflicto. \n

        Parameters \n
        ----------
            t_idle                      -- Transiciones idle
            t_analizar                  -- Transicion a analizar
            flag_idle                   -- Indica que es una t-idle
            plazas_sifon_complemento    -- Plazas complementos del sifon a controlar
            matriz_pre                  -- Matriz Post (I+)
            matriz_pos                  -- Matriz Pre  (I-)
            cantidad_plazas             -- Cantidad de plazas de la RdP
            cantidad_transiciones       -- Cantidad de tranciones de la RdP
            t_invariant                 -- T-Invariante
            t_in                        -- Transiciones input al supervisor
        """

        if (t_idle != t_analyze or flag_idle == 1):
            flag_idle = 0
            p_idle = []  # Plaza a las que le pone tokens la transicion
            for jj in range(0, self.q_places):
                # A que plazas esta alimentando esa transicion(t_analizar)
                if (int(self.m_pos[jj][t_analyze]) != 0):
                    p_idle.append(int(jj))
            # print(p_idle)
            for ii in range(0, len(p_idle)):
                t_conflict = []  # Plaza que alimenta a las transiciones en conflicto
                for mm in range(0, self.q_transitions):
                    if (self.m_pre[p_idle[ii]][mm] == 1):
                        # Transiciones en conflicto sensibilizadas por esa plaza
                        t_conflict.append(mm)
                if (len(t_conflict) > 1):  # La plaza sensibiliza a mas de una transicion? Hay conflicto
                    file_t_conflict_orig = open(txt_path + "/t_conflict_red_original.txt", "w")
                    for ij in range(0, len(t_conflict)):
                        file_t_conflict_orig.write(str(t_conflict[ij]) + ' ')
                        if t_conflict[ij] not in self.t_conflict_orig:
                            self.t_conflict_orig.append(t_conflict[ij])
                    file_t_conflict_orig.close()

                    self.conflict_t_invariant(t_conflict, complement_siphon_places, t_in)
                else:  # no hay conflicto
                    if (t_conflict[0] < t_analyze):
                        continue
                    else:
                        self.path_conflict(t_idle, t_conflict[0], flag_idle, complement_siphon_places, t_in)

    def supervisor(self, siphon):
        """
        Define el supervisor que va a controlar el bad-siphon. Esta funcion define el marcado de la plaza supervisor y las transiciones de entrada y salida del mismo. \n

        Parameters \n
        ----------
            cantidad_transiciones   -- Cantidad de transiciones de la RdP
            cantidad_plazas         -- Cantidad de plazas de la RdP
            sifon                   -- bad siphon a controlar. Compuesto por 3 elementos: estado deadlock[0], numero sifon[1], marcado sifon[2]
            matriz_es_tr            -- Matriz [estado x transiciones]
            matriz_pos              -- Matriz Pos (I+)
            matriz_pre              -- Matriz Pre (I-)
            matriz_sifones          -- Matriz de sifones
            t_invariant             -- T-invariantes
            lista_supervisores      -- Contiene el listado de los posibles supervisores a agregar
            lista_sifones           -- Array que contiene la informacion del sifon a controlar. Contiene el id, el numero de sifon a controlar y un array con las plazas que lo componen
        """
        global id
        # print("\nid=", id)
        info_sifon = [id]
        id = id + 1
        trans_idle = []  # Transiciones habilitadas en el marcado inicial
        # Es la posicion 2 debido que el sifon esta declarado estado deadlock[0], numero sifon[1], marcado sifon[2]
        marcado_supervisor = siphon[2] - 1
        # Marcado del supervisor
        # print("Sifon a controlar: ", sifon[1] + 1)
        info_sifon.append(siphon[1] + 1)
        plazas_sifon_aux = np.copy(self.m_siphons[siphon[1]])
        sif_aux = []
        for i in range(len(plazas_sifon_aux)):
            if plazas_sifon_aux[i] != 0:
                sif_aux.append(f"P{i + 1}")
        # print("Plazas del Sifon: ", sif_aux)
        info_sifon.append(sif_aux)
        # print("Marcado del supervisor", marcado_supervisor)
        self.siphons.append(info_sifon)
        # Transiciones que salen del estado idle, le quitan tokens a los supervisores
        # estas se encuentran sensibilizadas en el estado inicial (0) y son las transiciones
        # que son distintan de -1 en la matriz matriz_es_tr (estado-transicion) --> Transiciones 1 de Ezpeleta
        t_out = []
        for ii in range(self.q_transitions):
            if self.m_states_transitions[0][ii] != -1:
                trans_idle.append(ii)
                t_out.append("T" + str(ii + 1))
                # +1 Por problemas de indice en petrinator empieza en 1
                # print("Transicion output:", ii + 1)

        # Vector que indica que transiciones sacan/ponen tokens en el sifon
        tran_sifon = np.zeros(self.q_transitions)

        # Vectors de todas las plazas del sistema, en 1 se encuentran las plazas que componen nuestro sifon
        plazas_sifon = np.copy(self.m_siphons[siphon[1]])
        # print(plazas_sifon)
        # Localizamos transiciones que colocan tokens al supervisor-->Transiciones 2 de Ezpeleta
        for i in range(0, self.q_places):
            if (plazas_sifon[i] == 1):  # Es una plaza del sifon
                for j in range(0, self.q_transitions):
                    if (int(self.m_pre[i][j]) == 1):
                        # Le quita tokens al sifon
                        tran_sifon[j] = tran_sifon[j] - 1
                    if (int(self.m_pos[i][j]) == 1):
                        # Le agrega tokens al sifon
                        tran_sifon[j] = tran_sifon[j] + 1
        t_in = []
        for i in range(0, self.q_transitions):
            # Si es mayor a 0 significa que esta transicion coloca mas tokens a los sifones de los que le quitan
            if (tran_sifon[i] > 0):
                # Petrinator empieza en 1 y no en cero por eso el +1
                # print("Transicion input:", i + 1)
                t_in.append("T" + str(i + 1))
        # Usado para calcular la 3er transicion de Ezpeleta
        plazas_sifon_complemento = np.copy(plazas_sifon)

        # Obtenemos los complementos
        for i in range(0, len(tran_sifon)):
            if (tran_sifon[i] > 0):
                for j in range(0, self.q_places):
                    # son las plazas que habilitan transiciones que agregan mas
                    if (int(self.m_pre[j][i]) == 1):
                        # tokens de los que sacan del sifon.
                        plazas_sifon_complemento[j] = 1

        for tt in range(0, len(trans_idle)):
            # indica en cuantos T-invariantes aparece la Transiciones habilitadas en estado idle
            cont_t_invariante = 0
            # de ser =>2 implica que esta en conflicto
            for yy in range(0, len(self.t_invariant)):
                if (self.t_invariant[yy][trans_idle[tt]] == 1):
                    cont_t_invariante = cont_t_invariante + 1
            if (cont_t_invariante >= 2):
                # El 1 indica que es flag_idle
                self.path_conflict(trans_idle[tt], trans_idle[tt], 1, plazas_sifon_complemento, t_in)

        if (marcado_supervisor > 0) & (len(t_in) > 0):
            # El supervisor se compone de la plaza a agregar, el marcado, transiciones de entrada y salida
            # y el sifon a controlar
            self.supervisors.append(["P" + str(self.q_places + 1), str(marcado_supervisor), t_in, t_out, sif_aux])

    # Estos supervisores son menos restrictivos que los agregados con supervisor
    # ya que no utilizan las transiciones en estado idle
    def supervisor_3(self, siphon):
        """
        Define el supervisor que va a controlar el bad-siphon.
        Esta funcion define el marcado de la plaza supervisor y las transiciones de entrada y salida del mismo.
        Las transiciones de entrada al supervisor son aquellas que tienen mas arcos de entrada al sifon que de salida
        Las transiciones de salida del supervisor son aquellas que no tienen arcos de entrada al sifon.

        Parameters \n
        ----------
            siphon                   -- bad siphon a controlar. Compuesto por 3 elementos: estado deadlock[0], numero sifon[1], marcado sifon[2]
        """
        global id
        # print("\nid=", id)
        info_sifon = [id]
        id = id + 1
        trans_idle = []  # Transiciones habilitadas en el marcado inicial
        # Es la posicion 2 debido que el sifon esta declarado estado deadlock[0], numero sifon[1], marcado sifon[2]
        marcado_supervisor = siphon[2] - 1
        # Marcado del supervisor
        # print("Sifon a controlar: ", sifon[1] + 1)
        info_sifon.append(siphon[1] + 1)
        plazas_sifon_aux = np.copy(self.m_siphons[siphon[1]])
        sif_aux = []
        for i in range(len(plazas_sifon_aux)):
            if (plazas_sifon_aux[i] != 0):
                sif_aux.append(f"P{i + 1}")
        info_sifon.append(sif_aux)
        self.siphons.append(info_sifon)
        # Vector que indica que transiciones sacan/ponen tokens en el sifon
        tran_sifon = np.zeros(self.q_transitions)
        # Vectors de todas las plazas del sistema, en 1 se encuentran las plazas que componen nuestro sifon
        plazas_sifon = np.copy(self.m_siphons[siphon[1]])
        # Transiciones que sacan tokens del sifon
        extract = np.zeros(self.q_transitions)
        # Transiciones que ponen tokens al sifon
        put = np.zeros(self.q_transitions)

        for i in range(0, self.q_places):
            if plazas_sifon[i] == 1:  # Es una plaza del sifon
                for j in range(0, self.q_transitions):
                    if int(self.m_pre[i][j]) == 1:
                        # Le quita tokens al sifon
                        tran_sifon[j] = tran_sifon[j] - 1
                        extract[j] = extract[j] + 1
                    if int(self.m_pos[i][j]) == 1:
                        # Le agrega tokens al sifon
                        tran_sifon[j] = tran_sifon[j] + 1
                        put[j] = put[j] + 1
        t_in = []
        t_out = []
        for i in range(0, self.q_transitions):
            total = put[i] - extract[i]
            if total > 0:
                t_in.append("T" + str(i + 1))
            # Si solo extrae tokens del sifon es una transicion de salida del supervisor
            if put[i] == 0 and extract[i] >= 1:
                t_out.append("T" + str(i + 1))

        # Controlo que desde el mismo invariante no existan dos transiciones
        for invariant in self.t_invariant:
            count = 0
            for index, transition_invariant in enumerate(invariant):
                if transition_invariant == 1:
                    if "T" + str(index + 1) in t_in:
                        count += 1
                        # Si hay mas de una transicion del mismo invariante, tengo que borrarla
                        if count > 1:
                            t_in.remove("T" + str(index + 1))
        # Hago lo mismo con las transiciones de salida
        for invariant in self.t_invariant:
            count = 0
            for index, transition_invariant in enumerate(invariant):
                if transition_invariant == 1:
                    if "T" + str(index + 1) in t_out:
                        count += 1
                        # Si hay mas de una transicion del mismo invariante, tengo que borrarla
                        if count > 1:
                            t_out.remove("T" + str(index + 1))

        # Chequeo si el sifon que se intenta controlar incluye los supervisores agregados.
        # En caso de ser asi, resto al marcado la cantidad de tokens del supervisor.
        supervisors_added = []
        for sup_added in self.added_supervisors:
            if sup_added[1] in sif_aux:
                marcado_supervisor = marcado_supervisor - int(sup_added[2])

        if (marcado_supervisor > 0) & (len(t_in) > 0):
            # El supervisor se compone de la plaza a agregar, el marcado, transiciones de entrada y salida
            # y el sifon a controlar
            self.supervisors.append(["P" + str(self.q_places + 1), str(marcado_supervisor), t_in, t_out, sif_aux])

    # Genera supervisores que tienen como salida las transiciones sensibilizadas y como entradas las transiciones que
    # ponen token en el sifon
    def supervisor_2(self, siphon):
        global id

        info_sifon = [id]
        id = id + 1

        # Es la posicion 2 debido que el sifon esta declarado estado deadlock[0], numero sifon[1], marcado sifon[2]
        marcado_supervisor = siphon[2] - 1

        info_sifon.append(siphon[1] + 1)
        plazas_sifon_aux = np.copy(self.m_siphons[siphon[1]])
        sif_aux = []
        for i in range(len(plazas_sifon_aux)):
            if plazas_sifon_aux[i] != 0:
                sif_aux.append(f"P{i + 1}")
        # print("Plazas del Sifon: ", sif_aux)
        info_sifon.append(sif_aux)
        self.siphons.append(info_sifon)

        # Vector que indica que transiciones sacan/ponen tokens en el sifon
        tran_sifon = np.zeros(self.q_transitions)
        # Vectors de todas las plazas del sistema, en 1 se encuentran las plazas que componen nuestro sifon
        plazas_sifon = np.copy(self.m_siphons[siphon[1]])
        # print(plazas_sifon)
        # Localizamos transiciones que colocan tokens al supervisor-->Transiciones 2 de Ezpeleta
        for i in range(0, self.q_places):
            if plazas_sifon[i] == 1:  # Es una plaza del sifon
                for j in range(0, self.q_transitions):
                    if int(self.m_pre[i][j]) == 1:
                        # Le quita tokens al sifon
                        tran_sifon[j] = tran_sifon[j] - 1
                    if int(self.m_pos[i][j]) == 1:
                        # Le agrega tokens al sifon
                        tran_sifon[j] = tran_sifon[j] + 1
        t_in = []
        places_before_t_in = []
        t_out = []
        for transition_index in range(0, self.q_transitions):
            # Si es mayor a 0 significa que esta transicion coloca mas tokens a los sifones de los que le quitan
            if tran_sifon[transition_index] > 0:
                # Petrinator empieza en 1 y no en cero por eso el +1
                # print("Transicion input:", i + 1)
                t_in.append("T" + str(transition_index + 1))
                # Buscamos las plazas que ponen tokens a la transicion de entrada al supervisor y que tienen marcado 0
                # Intentamos identificar cual es la transicion anterior en el invariante.
                for place_index in range(0, self.q_places):
                    if int(self.m_pre[place_index][transition_index]) > 0:
                        if self.initial_mark[place_index] == 0:
                            places_before_t_in.append(place_index)

        for place_index in places_before_t_in:
            for transition_index in range(0, self.q_transitions):
                if int(self.m_pos[place_index][transition_index]) > 0:
                    t_out.append("T" + str(transition_index + 1))

        if (marcado_supervisor > 0) & (len(t_in) > 0):
            # El supervisor se compone de la plaza a agregar, el marcado, transiciones de entrada y salida
            # y el sifon a controlar
            self.supervisors.append(["P" + str(self.q_places + 1), str(marcado_supervisor), t_in, t_out, sif_aux])

    def find_deadlock_siphons(self, state, idle, idle_siphon):
        """
        Devuelve los sifones que se vacian en ese estado de deadlock. \n
        Apartir de matriz de Estados x Plazas = [Marcado] se recorre la fila de la matriz donde se encuentra el estado deadlock,
        colocando un "1" en aquellas plazas donde el marcado sea >=1. \n
        Se realiza un and entre esa fila de la matriz y el sifon, si la and = 0 implica que ese sifon se encuentra vacio para ese estado de deadlock.

        Parameters \n
        ----------
            estado          -- Estado que posee Deadlock.
            matriz_sifones  -- [Marcado de plazas que componen el sifon]
            matriz_es_pl    -- EstadosxPlazas = [Marcado para ese estado].
            idle            -- Indica si se agrega a la lista de sifon_idle o sifon_deadlock
        """

        aux = np.zeros(self.q_places)
        flag_sifon_idle = 0
        for j in range(0, self.q_places):
            # Obtenemos las plazas(marcadas) del estado deadlock
            if self.m_states_places[state][j] >= 1:
                aux[j] = 1

        for i in range(0, self.q_siphons):
            cont = 0
            for j in range(0, self.q_places):
                if (int(self.m_siphons[i][j] and aux[j]) == 1):
                    cont = cont + 1  # Si el contador es distinto de cero, el sifon no esta vacio
            if (cont == 0):
                marcado = 0
                for j in range(0, self.q_places):
                    if (self.m_siphons[i][j] == 1):
                        # Es 0 en fila, porque es el estado inicial en el que se encontraban las plazas de los sifones
                        marcado = marcado + self.m_states_places[0][j]
                if (idle == 0):
                    for jj in range(0, len(idle_siphon)):
                        # El sifon vacio en deadlock esta vacio en idle?
                        if (idle_siphon[jj] == i):
                            flag_sifon_idle = 1
                    if (flag_sifon_idle == 0):  # El sifon no estaba vacio en idle
                        sifon_agregado = 1
                        # Verifica si el sifon ya se incluyó en la lista, de no ser así lo agrega
                        for index in range(0, len(self.deadlock_siphons)):
                            if self.deadlock_siphons[index][1] == i:
                                sifon_agregado = 0
                        if sifon_agregado:
                            # Devuelve el sifon y su marcado inicial, para ese estado deadlock
                            self.deadlock_siphons.append([state, i, marcado])
                else:
                    idle_siphon.append[i]

    def find_supervisors(self):
        idle_siphon = []  # Estado_idle sifon
        self.deadlock_siphons = []  # Estado_deadlock-sifon-marcado

        idle = 1  # Sifones vacios estado inicial
        self.find_deadlock_siphons(0, idle, idle_siphon)

        # Llamada recursiva a find_deadlock en busqueda de caminos que dirigen al deadlock
        idle = 0  # Sifones en estado deadlock
        for i in range(0, len(self.state_deadlock)):
            self.find_deadlock_siphons(self.state_deadlock[i], idle, idle_siphon)

        for i in range(0, len(self.deadlock_siphons)):
            # Nos quedamos con un solo sifon
            siphon = np.copy(self.deadlock_siphons[i])

            # Agregamos el supervisor del bad-sifon
            # self.supervisor(siphon)
            # TODO la accion eliminar deberia eliminar supervisores que no controlaron sifones
            # TODO agregar otra accion que sea elegir el supervisor que mas veces se repite
            self.supervisor_3(siphon)

        # Elimina archivo temporal
        os.remove(txt_path + "/filtrado_prueba.txt")

    def add_quit_arcs(self):

        array_supervisor = []
        for i in range(self.q_places_original_net, len(self.m_states_places[0])):
            array_supervisor.append(i)

        trans_idle = []  # Transiciones habilitadas en el marcado inicial

        # Transiciones que salen del estado idle
        for ii in range(self.q_transitions):
            if self.m_states_transitions[0][ii] != -1:
                trans_idle.append(ii)

        # Buscamos eliminar los arcos de las transiciones idle cuyo T-invariante al que pertenece no le devuelve token
        # al supervisor. (i.e arcos innecesarios)
        # Cantidad de trans_idle
        for i in range(len(trans_idle)):
            # cantidad de t-invariantes
            for j in range(len(self.t_invariant_original_net)):
                # La transicion idle forma parte del t-invariantes
                if (int(self.t_invariant_original_net[j][trans_idle[i]]) == 1):
                    for m in range(len(array_supervisor)):
                        cont_sup = 0
                        return_token_transition = []
                        for l in range(len(self.t_invariant_original_net[j])):
                            if (int(self.t_invariant_original_net[j][l]) == 1):
                                # El T-invariante de la transicion idle le devuelve token al supervisor?
                                if (int(self.m_pos[array_supervisor[m]][l]) == 1):
                                    cont_sup = cont_sup + 1  # si devuelve
                                    # TODO borrar si no se usa
                                    return_token_transition.append(l)
                        if (cont_sup == 0):  # no devuelve
                            cont = 0
                            for k in range(len(self.t_conflict_orig)):
                                aux = int(self.t_conflict_orig[k])
                                # La transicion en conflicto forma parte del T-invariante por lo tanto debe devolver el token
                                if (int(self.t_invariant_original_net[j][aux]) == 1):
                                    cont = cont + 1

                                    c_print("[ACTION] La transicion en conflicto T" + str(aux + 1)
                                            + " le tiene que devolver un token al supervisor P"
                                            + str(array_supervisor[m] + 1), log_path=txt_path)

                                    # Se agrega el arco
                                    arcosrdp.agregararco(name_pflow, aux + 1, array_supervisor[m] + 1)
                                    c_print("[INFO] Se agrego un arco desde T" + str(aux + 1)
                                            + " hasta P" + str(array_supervisor[m] + 1), log_path=txt_path)
                                    break

                            if (cont == 0):
                                if (int(self.m_pre[int(array_supervisor[m])][int(trans_idle[i])]) == 1):
                                    c_print("[ACTION] Eliminar arco desde P" + str(
                                        array_supervisor[m] + 1) + " hasta T" + str(
                                        trans_idle[i] + 1), log_path=txt_path)

                                    # Se elimina el arco
                                    arcosrdp.eliminararco(name_pflow, str(f'P{array_supervisor[m] + 1}'),
                                                          str(f'T{trans_idle[i] + 1}'))
                                    c_print("[INFO] Se elimino el arco desde P" + str(
                                        array_supervisor[m] + 1) + " hasta T" + str(trans_idle[i] + 1),
                                            log_path=txt_path)

                        elif cont_sup > 1:  # TODO borrar si no se usa
                            # Acá me parece que es al revés, hay que eliminar desde la transición a la plaza
                            delete_transition = min(return_token_transition)
                            c_print("[ACTION] Eliminar arco desde T" + str(
                                delete_transition + 1) + " hasta P" + str(
                                array_supervisor[m] + 1), log_path=txt_path)

                            # Se elimina el arco
                            arcosrdp.eliminararco(name_pflow, str(f'T{delete_transition + 1}'),
                                                  str(f'P{array_supervisor[m] + 1}'))

                            c_print("[INFO] Se elimino el arco con metodo 2 desde T" + str(
                                delete_transition + 1) + " hasta P" + str(array_supervisor[m] + 1), log_path=txt_path)

    def get_environment(self):
        global html_path, txt_path
        hta.main(html_path, txt_path)

        # Filtrado de archivos provenientes del Petrinator
        (self.q_states, self.q_places, self.q_transitions, self.m_states_transitions, self.m_states_places,
         self.state_deadlock) = filterdata.main(txt_path)

        # Seteamos si tiene deadlock en el estado inicial
        self.initial_deadlock = False
        if len(self.state_deadlock) == 1:
            if self.state_deadlock[0] == 'S0':
                self.initial_deadlock = True

        # Cantidad de sifones y trampas
        self.q_siphons = 0
        self.q_traps = 0

        # Matrices
        self.siphons_traps()
        self.m_pre_pos()

        # T-invariantes
        self.t_invariant = self.invariants()

        # Lista de sifones
        self.siphons = []

        # Lista de supervisores
        self.supervisors = []
        self.q_supervisors = 0

        # Lista de sifones marcados en estado deadlock
        self.deadlock_siphons = []

        self.find_supervisors()

    def add_supervisor(self, index):
        global cant_supervisores

        # Si el marcado es mayor a cero
        if int(self.supervisors[index][1]) > 0:
            cant_supervisores += 1
            # Agrego numero de supervisor agregado, plaza,  marcado, transiciones de entrada y transiciones de salida
            self.added_supervisors.append([cant_supervisores, self.supervisors[index][0], self.supervisors[index][1],
                                           self.supervisors[index][2], self.supervisors[index][3]])
            new_red.main(self.supervisors[index][0], self.supervisors[index][1], self.supervisors[index][2],
                         self.supervisors[index][3], name_pflow)

    def delete_supervisor(self, index):
        global cant_supervisores

        # Busco el supervisor a eliminar
        for i in range(len(self.added_supervisors)):
            # Si es el supervisor a eliminar, elimino todos los arcos
            if i == index:
                # Elimino todos los arcos de entrada
                for t_in in self.added_supervisors[i][3]:
                    arcosrdp.eliminararco(name_pflow, t_in, self.added_supervisors[i][1])
                # Elimino todos los arcos de salida
                for t_out in self.added_supervisors[i][4]:
                    arcosrdp.eliminararco(name_pflow, self.added_supervisors[i][1], t_out)

                arcosrdp.delete_place(name_pflow, self.added_supervisors[i][1])
                self.deleted_supervisors.append(self.added_supervisors[i])
                self.added_supervisors.pop(index)

    # Agrega el supervisor con la menor cantidad de arcos de salida. Si no existe ninguno,
    # agrega el primero de la lista
    def add_sup_min_t_out(self):
        global cant_supervisores
        index_supervisor = 0

        for index, sup in enumerate(self.supervisors):
            if len(self.supervisors[index_supervisor][3]) > len(sup[3]):
                index_supervisor = index
        c_print("[INFO] Supervisor con menor cantidad de arcos de salida agregado" + str(index_supervisor), log_path=txt_path)
        self.add_supervisor(index_supervisor)

    def call_petrinaitor(self, first_time=None):

        # Armo el comando a ejecutar
        if first_time is None:
            first_time = 1

        petrinator_cmd = java_command + " " + petrinator_path + " " + pflow_file + " " + output_folder + " " \
                         + bring_gui + " " + auto_close + " " + str(first_time) + " " + clasify + " " + redirect_output

        c_print("\n--------------------------------------------------------------------------", log_path=txt_path)
        c_print("[INFO] Ejecutando petrinator para conocer estado de la red...", log_path=txt_path)
        c_print("--------------------------------------------------------------------------\n", log_path=txt_path)

        # Lo ejecuto, y controlo si se llega al timeout
        petrinator = subprocess.Popen(petrinator_cmd, shell=True)
        try:
            petrinator.wait(petrinator_timeout)
        except subprocess.TimeoutExpired:
            petrinator.kill()
            c_print("[ERROR] Se alcanzo el limite de " + str(petrinator_timeout) + " segundos para la ejecucion de petrinator, abandonando analisis", "\n", log_path=txt_path)
            subprocess.Popen("touch " + output_folder + "/NO.UNLOCKED", shell=True).communicate()
            self.petrinator_failed = True #CHECK
            return

    # TODO para mantener funcionamiento de analisis al final de la ejecucion del massive_run.py
    def save_state(self):
        with open(output_folder + "/STATS", 'w') as f:
            f.write(str(current_iteration) + "\n")              # Nro de iteraciones realizados por el algoritmo
            f.write(str(len(self.added_supervisors)) + "\n")    # Cantidad de supervisores agregados
            f.write(str(self.q_initial_siphons) + "\n")         # Cantidad de sifones inicial
            f.write(str(self.q_initial_places) + "\n")          # Cantidad de plazas inicial
            f.write(str(self.q_initial_transitions) + "\n")     # Cantidad de transiciones inicial
            f.write(str(self.q_states) + "\n")                  # Cantidad de estados inicial

    # TODO sifones marcados al iniciar
    # TODO sifones NO marcados
    def get_state(self):
        return np.array([
            self.q_initial_siphons,
            self.q_initial_places,
            self.q_initial_transitions,
            self.q_initial_states,
            self.q_siphons,
            self.q_places,
            self.q_transitions,
            self.q_states,
            len(self.state_deadlock),
            len(self.t_invariant)
        ])


class NetworkEnv(Env):
    """
    Descripcion:
        El entorno de una red de petri son todos los datos que podemos obtener de la red sin disparar ninguna
        transicion.Estos son:

        - Cantidad de sifones iniciales
        - Cantidad de plazas iniciales
        - Cantidad de transiciones iniciales
        - Cantidad de estados iniciales
        - Cantidad de sifones actuales
        - Cantidad de plazas actuales
        - Cantidad de transiciones actuales
        - Cantidad de estados actuales
        - Estados en deadlock
        - Cantidad de invariantes
        -

        TODO terminar descripcion siguiendo
        https://github.com/openai/gym/blob/master/gym/envs/classic_control/cartpole.py
    """

    def __init__(self):

        # Estado de la red, representado por varios parametros
        # TODO mejorar para no estar consumiendo una variable global
        # Se podria hacer metiendo todas las funciones que traen datos de la red en esta clase
        self.network = Network()
        # TODO descomentar cuando implementemos NN
        # self.state = self.network.get_state()
        self.state = 0

    def step(self, action):
        c_print("[INFO] SIFONES", log_path=txt_path)
        print_list_sifons(self.network.siphons, "\n", log_path=txt_path)
        c_print("[INFO] T INVARIANTES", log_path=txt_path)
        print_t_invariants(self.network.t_invariant, "\n", log_path=txt_path)
        c_print("[INFO] Supervisores para agregar", log_path=txt_path)
        print_list_supervisors(self.network.supervisors, "\n", log_path=txt_path)
        c_print("[INFO] Supervisores agregados", log_path=txt_path)
        print_added_supervisors(self.network.added_supervisors, "\n", log_path=txt_path)
        c_print("[INFO] Supervisores eliminados", log_path=txt_path)
        print_added_supervisors(self.network.deleted_supervisors, "\n", log_path=txt_path)
        c_print("[INFO] Ejecutando accion...", log_path=txt_path)

        # Guardo cantidad de supervisores agregados, para controlar mas adelante en las recompensas
        quantity_supervisors_added = len(self.network.added_supervisors)
        previous_siphons = []
        siphon_to_control = None
        # El indice del sifon a controlar coincide con el indice del supervisor a agregar
        # Ya que el listado de sifones
        # La tercer columna son las plazas del sifon
        for siphon in self.network.siphons:
            previous_siphons.append(siphon[2])

        if action == 3:
            if len(self.network.supervisors) == 1:
                c_print("[ACTION] Agrego supervisor Numero: 0", log_path=txt_path)
                # La quinta columna es el sifon a controlar
                siphon_to_control = self.network.supervisors[0][4]
                self.network.add_supervisor(0)
            elif len(self.network.supervisors) > 1:
                random_number = np.random.randint(len(self.network.supervisors))
                c_print("[ACTION] Agrego supervisor aleatorio Numero:", str(random_number),
                        log_path=txt_path)
                siphon_to_control = self.network.supervisors[random_number][4]
                self.network.add_supervisor(random_number)
            else:
                # O penalizamos la accion o no hacemos nada y dejamos que en algun momento ejecute la otra accion
                # y que sirva ejecutarla.
                c_print("[ERROR] No hay supervisores en la lista, deberiamos penalizar la accion con -50",
                        log_path=txt_path)
        elif action == 1:
            c_print("[ACTION] Agregar o quitar arcos", log_path=txt_path)
            self.network.add_quit_arcs()
        elif action == 2:
            c_print("[ACTION] Eliminar el ultimo supervisor agregado", log_path=txt_path)
            # Elimino siempre el ultimo supervisor agregado
            self.network.delete_supervisor(self.network.delete_supervisor(len(self.network.added_supervisors) - 1))
        elif action == 0:
            c_print("[ACTION] Agrego supervisor con menor numero de arcos de salida (MNAS)", log_path=txt_path)
            self.network.add_sup_min_t_out()
        else:
            c_print("[ERROR] Fallo get_next_action", log_path=txt_path)

        # Seteamos la recompensa
        reward = 0

        # Para poder calcular la recompensa necesito datos del nuevo estado de la red luego de ejecutar la accion
        # TODO mejorar esto, para que cuando tire una exception podamos continuar la ejecucion
        # La idea es devolver un reward = -100 e indicar que se llego a un estado terminal con
        # done = False
        try:
            self.network.call_petrinaitor()
        except Exception as e:
            if print_traceback:
                print(e.with_traceback())
            # Un estado no deseado
            reward = -100
            # Estado terminal
            done = True
            info = {}
            return self.state, reward, done, info

        self.network.get_environment()
        self.state = self.network.get_state()
        # Calculamos la recompensa
        # Cuando se ejecuta la accion de agregar un supervisor
        #   Si el sifon a controlar no se vacia en estado de deadlock la recompensa es -1
        #   Si el sifon se sigue vaciando en estado de deadlock la recompensa es -100
        # Cuando se realiza la accion de agregar o quitar arcos
        #   Si algun sifon que estaba controlado se vacia en estado deadlock -100
        #   Si algun sifon del diablo se controla -1
        c_print("--------------------------------------------------------------------------\n", log_path=txt_path)
        c_print("[INFO] Calculando recompensa..", log_path=txt_path)
        # Si hay deadlock_inicial
        if self.network.initial_deadlock:
            c_print("[REWARD] Hay deadlock en el estado inicial, recompensa -100", log_path=txt_path)
            reward -= 100
        # Si no hay mas deadlock 100
        if len(self.network.state_deadlock) == 0:
            c_print("[REWARD] No hay mas deadlock, recompensa 100", log_path=txt_path)
            reward += 100
        elif action == 3:
            # Busco si sigue existiendo el sifon que se intento controlar al agregar el supervisor
            siphon_controlled = True
            for siphon in self.network.siphons:
                if siphon[2] == siphon_to_control:
                    c_print(
                        "[REWARD] Sigue existiendo el sifon del diablo que se quiso controlar, recompensa -100",
                        log_path=txt_path)
                    reward -= 100
                    siphon_controlled = False
                    break

            if siphon_controlled:
                c_print("[REWARD] El sifon se controlo correctamente, recompensa -1", log_path=txt_path)
                reward -= 1
        elif action == 1:
            # Verificar que es mejor, si > solo o >=
            if len(previous_siphons) < len(self.network.siphons):
                c_print("[REWARD] Hay mas sifones vacios que en el estado anterior, recompensa -100", log_path=txt_path)
                reward -= 100
            else:
                c_print(
                    "[REWARD] Hay menos o igual cantidad de sifones vacios que en el estado anterior, recompensa -1",
                    log_path=txt_path)
                reward -= 1
        elif action == 2:
            if quantity_supervisors_added > 0:
                c_print("[INFO] Se elimino supervisor correctamente ", log_path=txt_path)
                if len(previous_siphons) < len(self.network.siphons):
                    c_print("[REWARD] Hay mas sifones vacios que en el estado anterior, recompensa -10",
                            log_path=txt_path)
                    reward -= 10
                else:
                    c_print(
                        "[REWARD] Hay menos o igual cantidad de sifones vacios que en el estado anterior, recompensa -1",
                        log_path=txt_path)
                    reward -= 1
            else:
                c_print("[REWARD] NO habia supervisores para eliminar, recompensa -50", log_path=txt_path)
                reward -= 50
        elif action == 0:
            # Busco si sigue existiendo el sifon que se intento controlar al agregar el supervisor
            siphon_controlled = True
            for siphon in self.network.siphons:
                if siphon[2] == siphon_to_control:
                    c_print(
                        "[REWARD] Sigue existiendo el sifon del diablo que se quiso controlar, recompensa -100",
                        log_path=txt_path)
                    reward -= 100
                    siphon_controlled = False
                    break

            if siphon_controlled:
                c_print("[REWARD] El sifon se controlo correctamente, recompensa -1", log_path=txt_path)
                reward -= 1
        # Verificamos si llegamos a un estado terminal
        done = False
        c_print("\n--------------------------------------------------------------------------\n",
                log_path=txt_path)
        if self.network.initial_deadlock:
            c_print(
                "[STATE] El estado inicial de la red tiene deadlock. Se llego a un estado terminal NO DESEADO\n",
                log_path=txt_path)
            c_print("--------------------------------------------------------------------------\n", log_path=txt_path)
            done = True
        elif len(self.network.deadlock_siphons) == 0:
            c_print("[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK", log_path=txt_path)
            c_print("\n--------------------------------------------------------------------------\n", log_path=txt_path)
            subprocess.Popen("touch " + output_folder + "/UNLOCKED", shell=True).communicate()
            done = True

        if not done:
            # get_entorno devuelve todos -1 cuando el estado inicial de la red tiene deadlock
            if self.network.initial_deadlock:
                c_print("[STATE] Cambie al estado 4: Estado terminal. O se quito el deadlock o hay deadlock inicial", log_path=txt_path)
                self.state = 4
                deadlock_inicial = True
            # Si hay mas sifones vacios en estado de deadlock despues de la accion
            elif len(self.network.deadlock_siphons) > len(previous_siphons):
                c_print("[STATE] Cambie al estado 1: Hay mas sifones vacios en deadlock", log_path=txt_path)
                self.state = 1
            # Si hay menos sifones vacios en estado de deadlock despues de la accion
            elif len(self.network.deadlock_siphons) < len(previous_siphons):
                c_print("[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock", log_path=txt_path)
                self.state = 2
            # Si hay igual cantidad de sifones vacios en estado de deadlock despues de la accion
            else:
                c_print("[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock", log_path=txt_path)
                self.state = 3

        info = {}

        return self.state, reward, done, info

    def render(self, mode='human'):
        pass

    def reset(self):
        pass


# define an epsilon greedy algorithm that will choose which action to take next (i.e., where to move next)
def get_next_action(estado, epsilon):
    # if a randomly chosen value between 0 and 1 is less than epsilon,
    # then choose the most promising value from the Q-table for this state.
    if estado == 0:
        return 3
    if np.random.random() < epsilon:
        return np.argmax(q_values[estado])
        #if q_values[estado][0] > q_values[estado][1]:
        #    return 0
        #else:
        #    return 1
    else:  # choose a random action
        return np.random.randint(4)


def train():
    global iteration_limit
    env = NetworkEnv()
    if env.network.not_deadlock is True \
            or env.network.not_s3pr is True \
            or env.network.petrinator_failed:
        return
    c_print("[INFO] SIFONES", log_path=txt_path)
    print_list_sifons(env.network.siphons, "\n", log_path=txt_path)
    c_print("[INFO] T INVARIANTES", log_path=txt_path)
    print_t_invariants(env.network.t_invariant, "\n", log_path=txt_path)
    c_print("[INFO] Supervisores para agregar", log_path=txt_path)
    print_list_supervisors(env.network.supervisors, "\n", log_path=txt_path)
    c_print("[INFO] Supervisores agregados", log_path=txt_path)
    print_added_supervisors(env.network.added_supervisors, "\n", log_path=txt_path)
    c_print("[INFO] Supervisores eliminados", log_path=txt_path)
    print_added_supervisors(env.network.deleted_supervisors, "\n", log_path=txt_path)
    c_print("[INFO] Metricas...", log_path=txt_path)

    print("Plazas: " + str(env.network.q_places))
    print("Tokens: " + str(env.network.tokens_doing_something))
    print("Sifones en deadlock: " + str(env.network.deadlock_siphons))
    print("Sifones: " + str(len(env.network.siphons)))
    print("Estados: " + str(env.network.q_states))

    num_episodes = 1
    epsilon = 0.9
    discount = 0.9  # Change to 1 to simplify Q-value results

    for episode in range(num_episodes):
        done = False
        state = 0
        iteration = 0
        while not done:  # Run until the end of the episode
            iteration = iteration + 1
            # Select action
            action = get_next_action(state, epsilon)
            next_state, reward, done, info = env.step(action)
            # Improve Q-values with Bellman Equation
            if done:
                q_values[state][action] = reward
            else:
                q_values[state][action] = reward + discount * max(q_values[next_state])
            state = next_state
            print_q_table(q_values, "\n", log_path=txt_path)
            if iteration >= iteration_limit:
                c_print("[ERROR] Se alcanzo SIN EXITO el limite de " + str(
                    iteration_limit) + " iteraciones para intentar destrabar la red, abandonando analisis \n", log_path=txt_path)
                subprocess.Popen("touch " + output_folder + "/NO.UNLOCKED", shell=True).communicate()
                exit(1)
    # Print Q-values to see if action right is always better than action left
    # except for states 0 and 6, which are terminal states and you cannot take
    # any action from them, so it does not matter.
    # print('Q-values are:')
    # print(q_values)
    # action_dict = {0: 'agregar supervisor', 1: 'agregar o quitar arcos'}
    # state = 0
    # for Q_vals in q_values:
    #    print('Best action for state {} is {}'.format(state, action_dict[np.argmax(Q_vals)]))
    #    state += 1


def file_path(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)


def main(pf, of, hp, tp):
    global name_pflow, pflow_file, output_folder, html_path, txt_path

    pflow_file = pf
    output_folder = of
    html_path = hp
    txt_path = tp

    # Para crear directorios si no existen y auto_createdir = True
    dir_path(html_path, createdir)
    dir_path(txt_path, createdir)

    c_print("[DEBUG] pflow_file: ", pflow_file, log_path=txt_path)
    c_print("[DEBUG] output_folder: ", output_folder, log_path=txt_path)
    c_print("[DEBUG] html_path: ", html_path, log_path=txt_path)
    c_print("[DEBUG] txt_path: ", txt_path, log_path=txt_path)

    # El algoritmo se ejecuta iterativamente hasta que se controla la red.
    # De no ser así se dice que el algoritmo no converge
    name_pflow = html_path + "/net.pflow"
    train()


if __name__ == "__main__":
    # Si se corre el script sin argumentos usa los default del setup inicial
    # si no verifica que se pasen los 4 argumentos

    if len(sys.argv) > 1:
        ap = argparse.ArgumentParser(description='Paths de entrada y salida del algoritmo')
        required = ap.add_argument_group('Argummentos requeridos')

        required.add_argument("-pf", "--pflow_file", type=file_path, required=True, default=pflow_file,
                              help="Ruta al archivo net.pflow que se usara durante el analis")
        required.add_argument("-of", "--output_folder", type=dir_path, required=True, default=output_folder,
                              help="Directorio de salida donde se escribira net.pflow procesado")
        required.add_argument("-hp", "--html_path", type=dir_path, required=True, default=html_path,
                              help="Directorio de salida donde se escribiran los archivos html")
        required.add_argument("-tp", "--txt_path", type=dir_path, required=True, default=txt_path,
                              help="Directorio de salida donde se escribiran los archivos txt")

        args = vars(ap.parse_args())

        pflow_file = args['pflow_file']
        output_folder = args['output_folder']
        html_path = args['html_path']
        txt_path = args['txt_path']
        main(pflow_file, output_folder, html_path, txt_path)
    else:
        main(pflow_file, output_folder, html_path, txt_path)
