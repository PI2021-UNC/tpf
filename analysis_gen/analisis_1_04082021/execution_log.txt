06/08/2021 18:56:49[INFO] Iniciando la clasificación serial de todos las redes... 
  
06/08/2021 18:59:56[INFO] Clasificando los resultados... 
  
06/08/2021 19:00:06[INFO] Generando estadísticas... 
  
06/08/2021 19:00:06[RESULT] Se analizaron 50 redes de petri, de las cuales:   
06/08/2021 19:00:06[RESULT] 20 se pudieron clasificar   
06/08/2021 19:00:06[RESULT] 20 no contenian DEADLOCK inicialmente   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210806-185633_31 |    00h:00m:02s    |
| 20210806-185633_14 |    00h:00m:02s    |
| 20210806-185633_24 |    00h:00m:02s    |
| 20210806-185633_19 |    00h:00m:02s    |
| 20210806-185633_15 |    00h:00m:02s    |
| 20210806-185633_44 |    00h:00m:03s    |
| 20210806-185633_16 |    00h:00m:03s    |
| 20210806-185633_18 |    00h:00m:03s    |
| 20210806-185633_42 |    00h:00m:03s    |
| 20210806-185633_7  |    00h:00m:03s    |
| 20210806-185633_27 |    00h:00m:03s    |
| 20210806-185633_25 |    00h:00m:03s    |
| 20210806-185633_37 |    00h:00m:03s    |
| 20210806-185633_4  |    00h:00m:03s    |
| 20210806-185633_35 |    00h:00m:03s    |
| 20210806-185634_48 |    00h:00m:03s    |
| 20210806-185633_39 |    00h:00m:03s    |
| 20210806-185633_26 |    00h:00m:03s    |
| 20210806-185633_9  |    00h:00m:04s    |
| 20210806-185633_38 |    00h:00m:05s    |
+--------------------+-------------------+ 

06/08/2021 19:00:06[RESULT] 0 no eran del tipo S3PR   
06/08/2021 19:00:06[RESULT] 0 no pudieron clasificarse debido a corte por timeout del petrinator   
06/08/2021 19:00:06[RESULT] 30 expirementaron fallas inesperadas durante el análisis   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210806-185633_46 |    00h:00m:03s    |
| 20210806-185633_6  |    00h:00m:03s    |
| 20210806-185633_13 |    00h:00m:03s    |
| 20210806-185633_1  |    00h:00m:03s    |
| 20210806-185633_20 |    00h:00m:03s    |
| 20210806-185633_45 |    00h:00m:03s    |
| 20210806-185633_8  |    00h:00m:03s    |
| 20210806-185633_33 |    00h:00m:03s    |
| 20210806-185633_41 |    00h:00m:03s    |
| 20210806-185633_3  |    00h:00m:03s    |
| 20210806-185633_5  |    00h:00m:03s    |
| 20210806-185633_43 |    00h:00m:03s    |
| 20210806-185633_28 |    00h:00m:03s    |
| 20210806-185633_32 |    00h:00m:03s    |
| 20210806-185633_36 |    00h:00m:03s    |
| 20210806-185633_17 |    00h:00m:03s    |
| 20210806-185633_2  |    00h:00m:03s    |
| 20210806-185633_10 |    00h:00m:03s    |
| 20210806-185633_22 |    00h:00m:03s    |
| 20210806-185633_23 |    00h:00m:03s    |
| 20210806-185633_0  |    00h:00m:03s    |
| 20210806-185633_47 |    00h:00m:04s    |
| 20210806-185633_21 |    00h:00m:04s    |
| 20210806-185633_40 |    00h:00m:04s    |
| 20210806-185633_29 |    00h:00m:04s    |
| 20210806-185633_30 |    00h:00m:04s    |
| 20210806-185633_34 |    00h:00m:04s    |
| 20210806-185633_12 |    00h:00m:04s    |
| 20210806-185633_11 |    00h:00m:04s    |
| 20210806-185634_49 |    00h:00m:04s    |
+--------------------+-------------------+ 

06/08/2021 19:00:06[RESULT] 0 cumplen las condiciones de tener DEADLOCK y ser tipo S3PR   
06/08/2021 19:00:06[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
06/08/2021 19:00:06[RESULT] - 0.00% de las redes válidas cumplen las condiciones esperadas -- Calidad del Algoritmo   
06/08/2021 19:00:06[RESULT] - 40.00% no cumplen condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
06/08/2021 19:00:06[RESULT] - 60.00% se debieron descartar por problemas petrinator -- Incidencias debido a factores externos   
06/08/2021 19:00:06[INFO] La ejecución masiva ha concluído exitosamente, luego de 00h:03m:17s 
  
