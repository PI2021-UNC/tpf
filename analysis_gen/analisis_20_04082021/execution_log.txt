07/08/2021 21:29:10[INFO] Iniciando la clasificación serial de todos las redes... 
  
07/08/2021 21:31:53[INFO] Clasificando los resultados... 
  
07/08/2021 21:32:03[INFO] Generando estadísticas... 
  
07/08/2021 21:32:03[RESULT] Se analizaron 50 redes de petri, de las cuales:   
07/08/2021 21:32:03[RESULT] 50 se pudieron clasificar   
07/08/2021 21:32:03[RESULT] 3 no contenian DEADLOCK inicialmente   
+-------------------+-------------------+
|        Red        | Duracion Analisis |
+-------------------+-------------------+
| 20210807-212714_9 |    00h:00m:02s    |
| 20210807-212714_5 |    00h:00m:02s    |
| 20210807-212714_4 |    00h:00m:03s    |
+-------------------+-------------------+ 

07/08/2021 21:32:03[RESULT] 0 no eran del tipo S3PR   
07/08/2021 21:32:03[RESULT] 0 no pudieron clasificarse debido a corte por timeout del petrinator   
07/08/2021 21:32:03[RESULT] 0 expirementaron fallas inesperadas durante el análisis   
07/08/2021 21:32:03[RESULT] 47 cumplen las condiciones de tener DEADLOCK y ser tipo S3PR   
+--------------------+-------------------+
|        Red         | Duracion Analisis |
+--------------------+-------------------+
| 20210807-212714_7  |    00h:00m:02s    |
| 20210807-212714_6  |    00h:00m:02s    |
| 20210807-212714_2  |    00h:00m:02s    |
| 20210807-212714_21 |    00h:00m:02s    |
| 20210807-212714_47 |    00h:00m:02s    |
| 20210807-212714_46 |    00h:00m:02s    |
| 20210807-212714_19 |    00h:00m:02s    |
| 20210807-212714_35 |    00h:00m:02s    |
| 20210807-212714_22 |    00h:00m:02s    |
| 20210807-212714_32 |    00h:00m:02s    |
| 20210807-212714_44 |    00h:00m:02s    |
| 20210807-212714_27 |    00h:00m:02s    |
| 20210807-212714_43 |    00h:00m:02s    |
| 20210807-212714_49 |    00h:00m:02s    |
| 20210807-212714_36 |    00h:00m:02s    |
| 20210807-212714_29 |    00h:00m:02s    |
| 20210807-212714_38 |    00h:00m:02s    |
| 20210807-212714_24 |    00h:00m:02s    |
| 20210807-212714_3  |    00h:00m:02s    |
| 20210807-212714_15 |    00h:00m:02s    |
| 20210807-212714_17 |    00h:00m:03s    |
| 20210807-212714_48 |    00h:00m:03s    |
| 20210807-212714_31 |    00h:00m:03s    |
| 20210807-212714_45 |    00h:00m:03s    |
| 20210807-212714_37 |    00h:00m:03s    |
| 20210807-212714_34 |    00h:00m:03s    |
| 20210807-212714_25 |    00h:00m:03s    |
| 20210807-212714_11 |    00h:00m:03s    |
| 20210807-212714_40 |    00h:00m:03s    |
| 20210807-212714_8  |    00h:00m:03s    |
| 20210807-212714_0  |    00h:00m:03s    |
| 20210807-212714_20 |    00h:00m:03s    |
| 20210807-212714_42 |    00h:00m:03s    |
| 20210807-212714_30 |    00h:00m:03s    |
| 20210807-212714_18 |    00h:00m:03s    |
| 20210807-212714_26 |    00h:00m:03s    |
| 20210807-212714_41 |    00h:00m:03s    |
| 20210807-212714_13 |    00h:00m:03s    |
| 20210807-212714_10 |    00h:00m:03s    |
| 20210807-212714_33 |    00h:00m:03s    |
| 20210807-212714_14 |    00h:00m:03s    |
| 20210807-212714_39 |    00h:00m:03s    |
| 20210807-212714_16 |    00h:00m:03s    |
| 20210807-212714_1  |    00h:00m:04s    |
| 20210807-212714_12 |    00h:00m:04s    |
| 20210807-212714_23 |    00h:00m:04s    |
| 20210807-212714_28 |    00h:00m:05s    |
+--------------------+-------------------+ 

07/08/2021 21:32:03[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
07/08/2021 21:32:03[RESULT] - 94.00% de las redes válidas cumplen las condiciones esperadas -- Calidad del Algoritmo   
07/08/2021 21:32:03[RESULT] - 6.00% no cumplen condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
07/08/2021 21:32:03[RESULT] - 0.00% se debieron descartar por problemas petrinator -- Incidencias debido a factores externos   
07/08/2021 21:32:03[INFO] La ejecución masiva ha concluído exitosamente, luego de 00h:02m:52s 
  
