- [Proyecto Final de la Carrera de Ingeniería en Computación 2021 - FCEFyN (UNC)](#proyecto-final-de-la-carrera-de-ingeniería-en-computación-2021---fcefyn-unc)
  - [Integrantes](#integrantes)
    - [Directores](#directores)
    - [Alumnos](#alumnos)
  - [Antecedentes](#antecedentes)
    - [PI relevantes](#pi-relevantes)
    - [Otros proyectos relevantes](#otros-proyectos-relevantes)
    - [Papers consultados](#papers-consultados)
  - [RL y DRL](#rl-y-drl)
    - [Material Audiovisual](#material-audiovisual)

# Proyecto Final de la Carrera de Ingeniería en Computación 2021 - FCEFyN (UNC)

## Integrantes

### Directores
- Ing. Luis Orlando Ventre - [luis.ventre@unc.edu.ar ](mailto:luis.ventre@unc.edu.ar )
- Dr. Ing. Orlando Micolini - [orlando.micolini@unc.edu.ar ](mailto:orlando.micolini@unc.edu.ar)

### Alumnos
- [Luis Lenta](https://gitlab.com/LuisLenta) - [luislenta@mi.unc.edu.ar](luislenta@mi.unc.edu.ar)
- [Christian Nicolaide](https://gitlab.com/cnicolaide) - [cnicolaide@mi.unc.edu.ar](mailto:cnicolaide@mi.unc.edu.ar)
- [Lucas Monsierra](https://gitlab.com/LucasMonsierra) - [lucas.monsierra@mi.unc.edu.ar](mailto:lucas.monsierra@mi.unc.edu.ar)

## Antecedentes

### PI relevantes

Como punto de partida para nuestro PI nos basamos en los siguientes trabajos finales o practicas profesionales supervisadas llevadas a cabo por companeros en diferentes momentos en el Laboratorio de Arquitectura de Computadoras:

- [1. Java Petri Concurrency Monitor (2017 - Rabinovich)](https://github.com/airabinovich/java_petri_concurrency_monitor)
    - **Relevancia:** Monitor de concurrencia escrito en Java 8, se desarrollo a modo de libreria para ser importando en otros proyectos. Core de Petrinator
- [2. Editor, simulador y analizador de Redes de Petri (2017 - Felici/Asson)](https://github.com/joaquinfelici/Petri_Net_Simulator)
    - **Relevancia:** Es la primera version del Petrinator e implementa el monitor de concurrencia de Rabinovich y otros algoritmos ademas de brindar una GUI.
- [3. Baboon framework (2018 - Arce)](https://github.com/juanjoarce7456/baboon)
    - **Relevancia:** Framework escrito en Java 8, que gestiona las acciones del código del usuario utilizando una red de Petri como lógica del sistema.
- [4. Determinación automática del control de una red de Petri (2020 - Navarro/Izquierdo/Salvatierra)](https://github.com/MatiasNavarro/ProyectoIntegrador_FCEFyN)
    - **Relevancia:** Aqui desarrollan un algoritmo para detectar y corregir deadlocks, el cual nosotros pretendemos utilizar en la implementacion RL.
- [5. Determinacion automatica de la politica de una Red de Petri(2021 - Arce/Altuna)](https://github.com/Jnaltuna/HybridMachine)
    - **Relevancia:** Utiliza redes de petri en combianacion con automatas de aprendizaje (redes bayesianas).
- [6. Predicción de cantidad de defectos graves en vehículos utilitarios en planta automotriz (2021 - Collante)](https://github.com/GeraCollante/tesis-icomp-machinelearning)
    - **Relevancia:** El marco teorico nos ha resultado util para introducirnos en el mundo del Machine Learning.
- [7. Petrinator simulator for Petri Networks (En desarrollo - Valenzuela)](https://github.com/GabrielEValenzuela/Petrinator)
    - **Relevancia:** Este proyecto es el que introduce el soporte para redes de petri de mas de 1000x1000 (actualmente una limitacion de Petrinator).
- [8. Integración y desarrollo del editor/simulador Petrinator para análisis de sistemas mediante de redes de petri (En desarrollo - Bosack/Oliva)](https://github.com/nadaol/Petrinator_2021)
    - **Relevancia:** Aqui desarrollan el soporte para integrar Petrinator a otros sistemas. Nos resulto particularmente util la nueva funcionalidad de exportar todo. [Link a LateX](https://es.overleaf.com/project/60baa5e7c59bfd33314774f1)

### Otros proyectos relevantes

A continuacion se listan algunos proyectos que hemos utilizado como referencias para el desarrollo de este PI.

- [1. A quick implementation of a Petri net runner in python](https://github.com/Nikolaj-K/petri-net)
- [2. HPetriSim is a graphical editor which provides basic editing and simulation of Petri Nets](https://github.com/Uzuul23/HPetriSim)


### Papers consultados

A continuacion se detallan los papers que utilizamos como referencia. Además se agrega:

* Una breve descripción.
* La conclusión a la que llega.
* Por qué nos resultan de utilidad en nuestro PI.
* La fuente desde donde fueron obtenidos (para conocer datos relevantes, como por ejemplo cuantas veces fue citado o leído).

 - [**1. Deep Reinforcement Learning aplicado a politicas de Red de Petri**](https://gitlab.com/PI2021-UNC/tpf/-/blob/our/papers/Petri-net-based_dynamic_scheduling_of_flexible_manufacturing_system_via_deep_reinforcement_learning_with_graph_convolutional_network.pdf) 

  **Descripcion:** Aplica DRL a las politicas de la conocida Red de Petri de manufactura, es decir, la red que cuenta con lineas de montaje que comparten brazos robots, una red de tipo S3PR. Lo que hace es aplicar el RL, en particular el deep Q-Learning, para "aprender" y decidir de que manera aplicar las politicas para que la red no sufra de deadloock. Para Q-Learning lo que utiliza son lo que denomina Petri-net convolution (PNC).Esta capa se compone de otras dos capas denominadas P2T (Place to transition) y T2P (Transition to place)  Al utilizar esta capa PNC combinada con plazas temporales, logra "aprender" cuando tiene que disparar o no ciertas transiciones dependiendo de cuanta demora introduzcan en futuros disparos y cuan probable es que genere un deadlock dicho disparo. Es decir, aprende a modificar las politicas para que no se provoquen deadlocks y para intentar lograr el mayor paralelismo posible. Para comprobar esto, lleva a cabo tres experimentos distintos, comparando el nuevo metodo con metodos heuristicos ya desarrollados. 

  **Conclusion:** Concluye en que definitivamente el metodo aplicado es mejor que los metodos heuristicos. Y que tiene la capacidad de adaptarse a los cambios de distribucion de las piezas entrantes.

  **Utilidad:** Nos sirve para reconocer como aplicar RL a nuestro problema

  **Fuente:** [researchgate.net](https://www.researchgate.net/publication/340357187_Petri-net-based_dynamic_scheduling_of_flexible_manufacturing_system_via_deep_reinforcement_learning_with_graph_convolutional_network)

  - [**2. Cálculo de sifones mínimos estrictos (SMS) para redes S3PR basado en la teoría de descomposición del problema**](https://gitlab.com/PI2021-UNC/tpf/-/blob/our/papers/Computation%20of%20strict%20minimal%20siphons%20in%20a%20class%20of%20Petri%20nets.pdf) 

  **Descripcion:** Desarrollo de un nuevo método para la detección de sifones mínimos estrictos. Lo primero que hace es describir y citar las referencias de todos los papers donde se proponen algoritmos para detectar SMS. En el peor de los casos la cantidad de sifones puede crecer de manera exponencial con el tamaño de la red, por lo que afecta considerablemente a los algoritmos de detección de sifones. Según este paper, la mayoría de los métodos tienen problema cuando las dimensiones de las redes son grandes, por ejemplo, una red de 72 plazas por 64 transiciones no puede ser resuelta por uno de los métodos. Lo que buscan en este paper es lograr una complejidad polinomial en el método de detección de sifones, algo que no encontraron en ningun otro paper. 
  El método propuesto consiste en detectar lo que llama subredes de recursos ideales (no pude entender que son) las cuales se corresponden uno a uno con los sifones. 

  **Conclusion:** Mediante la teoría de descomposición, la que consiste en separar un problema grande en distintos problemas más pequeños, detecta las subredes de recursos ideales mediante funciones recursivas, logrando así detectar todos los sifones que tiene la red. Logra obtener un método de complejidad polinomial para obtener los sifones, lo que permite aplicarlo a redes con una gran cantidad de sifones. 

  **Utilidad:** Podemos utilizar el algoritmo propuesto por este paper para comparar con el método aplicado por nosotros y controlar si es mejor o no. Además, tiene citas a muchos paper que intentan hacer lo mismo, es decir, detectar los sifones para prevenir los deadlocks. 

  **Fuente:** [researchgate.net](https://www.researchgate.net/publication/316803386_Computation_of_Strict_Minimal_Siphons_in_a_Class_of_Petri_Nets_Based_on_Problem_Decomposition)

### Documentos en Drive
- [Bitacora](https://docs.google.com/document/d/1nMkXr4sykBexbJPZBoZcMB-okOmIHI1aMqltPt7gb4s/edit)
- [Analisis de funcionamiento](https://docs.google.com/document/d/1Kru6f7bNQdqBTpNRpzbXAv7J8ufPTYiy5Qw1vlzpOBg/edit)
- [Draft de tesis](https://docs.google.com/document/d/1MgIYdHg83N3C61Nlr2TKCq6y-DQvE5vLHFAa5zlrkXA/edit)

## RL y DRL

Creemos que RL o DRL son dos caminos que podrian ayudar a resolver este problema, aqui planteamos algunas referencias bibliograficas de utilidad.
- [Aprendizaje por refuerzo](https://www.aprendemachinelearning.com/aprendizaje-por-refuerzo/)
- [Deep Petri nets of unsupervised and
supervised learning](https://journals.sagepub.com/doi/pdf/10.1177/0020294020923375)
- [Petri-net-based dynamic scheduling of flexible manufacturing system via deep reinforcement learning with graph convolutional network](https://www.sciencedirect.com/science/article/abs/pii/S0278612520300145?via%3Dihub)
- [Deep Reinforcement Learning Course](https://simoninithomas.github.io/deep-rl-course/)
- [Reliable Reinforcement Learning Implementations](https://araffin.github.io/post/sb3/)
- [Top 10 Free Resources to learn Reinforcement Learning](https://analyticsindiamag.com/top-10-free-resources-to-learn-reinforcement-learning/)
- [Curso del MIT](http://introtodeeplearning.com/)
- [Ejemplo de aplicacion de RL](https://github.com/aamini/introtodeeplearning/blob/master/lab3/RL.ipynb)
- [Libreria para entrenar agentes de RL](https://gym.openai.com/)

### Material Audiovisual
- [Aprendizaje reforzado: guia definitiva](https://youtu.be/qBtB-xcJp4c)
- [Aprendizaje reforzado con Python](https://youtu.be/a4-WL9nC84c)

## Instrucciones de instalacion
1. Instalar la ultima version disponible de python3 
    ```apt-get install python3```
2. Instalar/actualizar la ultima version disponible de pip3
    ```python3 -m pip install pip; pip3 install --upgrade pip```
3. Instalar las dependencias del proyecto
    ```"pip3 install -r app_2/requirements.txt"```
