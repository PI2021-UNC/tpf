25/07/2021 16:42:17[INFO] Guardando una copia del directorio ../analysis/analisis_15_25072021/gen_nets en../analysis/analisis_15_25072021/gen_nets_original 
  
25/07/2021 16:42:17[INFO] Iniciando la ejecucion serial de todos los casos... 
  
25/07/2021 16:44:00[INFO] Clasificando los resultados... 
  
25/07/2021 16:44:10[INFO] Generando estadisticas... 
  
25/07/2021 16:44:10[RESULT] Se analizaron 25 redes de petri, de las cuales:   
25/07/2021 16:44:10[RESULT] 0 se pudieron desbloquear   
25/07/2021 16:44:10[RESULT] 13 no contenian deadlock inicialmente   
+--------------------+-------------------+---------------+-----------------+-------------------+------------------+------------------------+-------------------+
|        Red         | Duracion Analisis | N Iteraciones | N Sup Agregados | N Sifones Inicial | N Plazas Inicial | N Transiciones Inicial | N Estados Inicial |
+--------------------+-------------------+---------------+-----------------+-------------------+------------------+------------------------+-------------------+
| 20210725-164143_4  |    00h:00m:02s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_5  |    00h:00m:02s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_6  |    00h:00m:02s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_2  |    00h:00m:02s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_18 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_0  |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_1  |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_17 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_11 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_3  |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_16 |    00h:00m:04s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_13 |    00h:00m:05s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_10 |    00h:00m:06s    |       1       |        0        |         0         |        0         |           0            |         0         |
+--------------------+-------------------+---------------+-----------------+-------------------+------------------+------------------------+-------------------+ 

25/07/2021 16:44:10[RESULT] 0 no eran del tipo s3pr   
25/07/2021 16:44:10[RESULT] 0 no pudieron destrabarse debido a corte por timeout del petrinator o limite de iteraciones   
25/07/2021 16:44:10[RESULT] 12 expirementaron fallas inesperadas durante el analisis   
+--------------------+-------------------+---------------+-----------------+-------------------+------------------+------------------------+-------------------+
|        Red         | Duracion Analisis | N Iteraciones | N Sup Agregados | N Sifones Inicial | N Plazas Inicial | N Transiciones Inicial | N Estados Inicial |
+--------------------+-------------------+---------------+-----------------+-------------------+------------------+------------------------+-------------------+
| 20210725-164143_20 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_24 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_9  |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_23 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_21 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_15 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_7  |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_19 |    00h:00m:03s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_12 |    00h:00m:04s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_14 |    00h:00m:04s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_22 |    00h:00m:05s    |       1       |        0        |         0         |        0         |           0            |         0         |
| 20210725-164143_8  |    00h:00m:12s    |       1       |        0        |         0         |        0         |           0            |         0         |
+--------------------+-------------------+---------------+-----------------+-------------------+------------------+------------------------+-------------------+ 

25/07/2021 16:44:10[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
25/07/2021 16:44:10[RESULT] - 0.00% de las redes validas pudo ser desbloqueado -- Calidad del Algoritmo   
25/07/2021 16:44:10[RESULT] - 52.00% no cumplian condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
25/07/2021 16:44:10[RESULT] - 48.00% se debieron descartar por problemas petrinator y/o algoritmo py original -- Incidencias debido a factores externos   
25/07/2021 16:44:10[INFO] La ejecucion masiva ha concluido exitosamente, luego de 00h:01m:53s 
  
