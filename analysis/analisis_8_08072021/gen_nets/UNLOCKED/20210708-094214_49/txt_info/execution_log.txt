18/07/2021 21:19:10[DEBUG] pflow_file:  ../analysis/analisis_8_08072021/gen_nets2/20210708-094214_49/net.pflow  
18/07/2021 21:19:10[DEBUG] output_folder:  ../analysis/analisis_8_08072021/gen_nets2/20210708-094214_49/  
18/07/2021 21:19:10[DEBUG] html_path:  ../analysis/analisis_8_08072021/gen_nets2/20210708-094214_49/  
18/07/2021 21:19:10[DEBUG] txt_path:  ../analysis/analisis_8_08072021/gen_nets2/20210708-094214_49/txt_info  

--------------------------------------------------------------------------   
18/07/2021 21:19:10[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
+----+-----+-------------------------------------------------+
| ID | Nro |                      Plazas                     |
+----+-----+-------------------------------------------------+
| 0  |  1  | ['P3', 'P7', 'P11', 'P15', 'P24', 'P25', 'P26'] |
| 1  |  26 |     ['P3', 'P7', 'P10', 'P15', 'P24', 'P25']    |
| 2  |  27 |            ['P3', 'P7', 'P25', 'P27']           |
| 3  |  2  |     ['P2', 'P7', 'P11', 'P15', 'P24', 'P26']    |
+----+-----+-------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
18/07/2021 21:19:35[INFO] Supervisores para agregar   
+-----+---------+---------------------+--------------+
|  ID | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
| P31 |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
| P31 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P31 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P31 |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:19:35[INFO] Supervisores agregados   
+-----+---------+-------------+------------+
| Nro | Marcado | T - Entrada | T - Salida |
+-----+---------+-------------+------------+
+-----+---------+-------------+------------+
18/07/2021 21:19:35[INFO] Ejecutando accion...   
18/07/2021 21:19:35[INFO] Ejecutando accion...   
18/07/2021 21:19:35[ACTION] Agrego supervisor aleatorio Numero: 3  

--------------------------------------------------------------------------   
18/07/2021 21:19:35[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   

--------------------------------------------------------------------------
   
18/07/2021 21:20:06[INFO] Calculando recompensa..   
18/07/2021 21:20:06[REWARD] El sifon se controlo correctamente, recompensa -1   

--------------------------------------------------------------------------
   
18/07/2021 21:20:06[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -1.0        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+-------------------------------------------------+
| ID | Nro |                      Plazas                     |
+----+-----+-------------------------------------------------+
| 4  |  1  | ['P3', 'P7', 'P11', 'P15', 'P24', 'P25', 'P26'] |
| 5  |  27 |     ['P3', 'P7', 'P10', 'P15', 'P24', 'P25']    |
| 6  |  28 |            ['P3', 'P7', 'P25', 'P27']           |
| 7  |  2  |     ['P2', 'P7', 'P11', 'P15', 'P24', 'P26']    |
+----+-----+-------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
18/07/2021 21:20:06[INFO] Supervisores para agregar   
+-----+---------+---------------------+--------------+
|  ID | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
| P32 |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
| P32 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P32 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P32 |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:20:06[INFO] Supervisores agregados   
+-----+---------+---------------------+--------------+
| Nro | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
|  1  |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:20:06[INFO] Ejecutando accion...   
18/07/2021 21:20:06[ACTION] Agregar o quitar arcos   
18/07/2021 21:20:06[ACTION] Eliminar arco desde T6 hasta P31   
18/07/2021 21:20:06[INFO] Se elimino el arco con metodo 2 desde T6 hasta P31   
18/07/2021 21:20:06[ACTION] La transicion en conflicto T13 le tiene que devolver un token al supervisor P31   
18/07/2021 21:20:06[INFO] Se agrego un arco desde T13 hasta P31   

--------------------------------------------------------------------------   
18/07/2021 21:20:06[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   

--------------------------------------------------------------------------
   
18/07/2021 21:20:29[INFO] Calculando recompensa..   
18/07/2021 21:20:29[REWARD] Hay menos o igual cantidad de sifones vacios que en el estado anterior, recompensa -1   

--------------------------------------------------------------------------
   
18/07/2021 21:20:29[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -1.0        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          -1.0          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+-------------------------------------------------+
| ID | Nro |                      Plazas                     |
+----+-----+-------------------------------------------------+
| 8  |  1  | ['P3', 'P7', 'P11', 'P15', 'P24', 'P25', 'P26'] |
| 9  |  27 |     ['P3', 'P7', 'P10', 'P15', 'P24', 'P25']    |
| 10 |  28 |            ['P3', 'P7', 'P25', 'P27']           |
+----+-----+-------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
18/07/2021 21:20:29[INFO] Supervisores para agregar   
+-----+---------+---------------------+--------------+
|  ID | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
| P32 |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
| P32 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P32 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:20:29[INFO] Supervisores agregados   
+-----+---------+---------------------+--------------+
| Nro | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
|  1  |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:20:29[INFO] Ejecutando accion...   
18/07/2021 21:20:29[ACTION] Agregar o quitar arcos   

--------------------------------------------------------------------------   
18/07/2021 21:20:29[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   

--------------------------------------------------------------------------
   
18/07/2021 21:20:51[INFO] Calculando recompensa..   
18/07/2021 21:20:51[REWARD] Hay menos o igual cantidad de sifones vacios que en el estado anterior, recompensa -1   

--------------------------------------------------------------------------
   
18/07/2021 21:20:51[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -1.0        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          -1.0          |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          -1.0          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+-------------------------------------------------+
| ID | Nro |                      Plazas                     |
+----+-----+-------------------------------------------------+
| 11 |  1  | ['P3', 'P7', 'P11', 'P15', 'P24', 'P25', 'P26'] |
| 12 |  27 |     ['P3', 'P7', 'P10', 'P15', 'P24', 'P25']    |
| 13 |  28 |            ['P3', 'P7', 'P25', 'P27']           |
+----+-----+-------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
18/07/2021 21:20:51[INFO] Supervisores para agregar   
+-----+---------+---------------------+--------------+
|  ID | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
| P32 |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
| P32 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P32 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:20:51[INFO] Supervisores agregados   
+-----+---------+---------------------+--------------+
| Nro | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
|  1  |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:20:51[INFO] Ejecutando accion...   
18/07/2021 21:20:51[INFO] Ejecutando accion...   
18/07/2021 21:20:51[ACTION] Agrego supervisor aleatorio Numero: 0  

--------------------------------------------------------------------------   
18/07/2021 21:20:51[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   

--------------------------------------------------------------------------
   
18/07/2021 21:21:16[INFO] Calculando recompensa..   
18/07/2021 21:21:16[REWARD] El sifon se controlo correctamente, recompensa -1   

--------------------------------------------------------------------------
   
18/07/2021 21:21:16[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -1.0        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          -1.0          |
| Igual cantidad de sifónes vacíos en estado deadlock |        -1.0        |          -1.0          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+-------------------------------------------------+
| ID | Nro |                      Plazas                     |
+----+-----+-------------------------------------------------+
| 14 |  1  | ['P3', 'P7', 'P11', 'P15', 'P24', 'P25', 'P26'] |
| 15 |  28 |     ['P3', 'P7', 'P10', 'P15', 'P24', 'P25']    |
| 16 |  29 |            ['P3', 'P7', 'P25', 'P27']           |
+----+-----+-------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
18/07/2021 21:21:16[INFO] Supervisores para agregar   
+-----+---------+---------------------+--------------+
|  ID | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
| P33 |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
| P33 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P33 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:21:16[INFO] Supervisores agregados   
+-----+---------+---------------------+--------------+
| Nro | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
|  1  |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
|  2  |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:21:16[INFO] Ejecutando accion...   
18/07/2021 21:21:16[ACTION] Agregar o quitar arcos   
18/07/2021 21:21:16[ACTION] Eliminar arco desde T6 hasta P32   
18/07/2021 21:21:16[INFO] Se elimino el arco con metodo 2 desde T6 hasta P32   
18/07/2021 21:21:16[ACTION] La transicion en conflicto T13 le tiene que devolver un token al supervisor P32   
18/07/2021 21:21:16[INFO] Se agrego un arco desde T13 hasta P32   

--------------------------------------------------------------------------   
18/07/2021 21:21:16[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   

--------------------------------------------------------------------------
   
18/07/2021 21:21:40[INFO] Calculando recompensa..   
18/07/2021 21:21:40[REWARD] Hay menos o igual cantidad de sifones vacios que en el estado anterior, recompensa -1   

--------------------------------------------------------------------------
   
18/07/2021 21:21:40[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -1.0        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          -1.0          |
| Igual cantidad de sifónes vacíos en estado deadlock |        -1.0        |          -1.9          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+-------------------------------------------------+
| ID | Nro |                      Plazas                     |
+----+-----+-------------------------------------------------+
| 17 |  1  | ['P3', 'P7', 'P11', 'P15', 'P24', 'P25', 'P26'] |
| 18 |  28 |     ['P3', 'P7', 'P10', 'P15', 'P24', 'P25']    |
| 19 |  29 |            ['P3', 'P7', 'P25', 'P27']           |
+----+-----+-------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
18/07/2021 21:21:40[INFO] Supervisores para agregar   
+-----+---------+---------------------+--------------+
|  ID | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
| P33 |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
| P33 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
| P33 |    1    |     ['T3', 'T6']    | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:21:40[INFO] Supervisores agregados   
+-----+---------+---------------------+--------------+
| Nro | Marcado |     T - Entrada     |  T - Salida  |
+-----+---------+---------------------+--------------+
|  1  |    1    | ['T2', 'T6', 'T11'] | ['T1', 'T5'] |
|  2  |    2    | ['T3', 'T6', 'T11'] | ['T1', 'T5'] |
+-----+---------+---------------------+--------------+
18/07/2021 21:21:40[INFO] Ejecutando accion...   
18/07/2021 21:21:40[INFO] Ejecutando accion...   
18/07/2021 21:21:40[ACTION] Agrego supervisor aleatorio Numero: 2  

--------------------------------------------------------------------------   
18/07/2021 21:21:40[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   

--------------------------------------------------------------------------
   
18/07/2021 21:22:06[INFO] Calculando recompensa..   
18/07/2021 21:22:06[REWARD] No hay mas deadlock, recompensa 100   

--------------------------------------------------------------------------
   
18/07/2021 21:22:06[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK   

--------------------------------------------------------------------------
   
18/07/2021 21:22:07[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -1.0        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          -1.0          |
| Igual cantidad de sifónes vacíos en estado deadlock |       100.0        |          -1.9          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
