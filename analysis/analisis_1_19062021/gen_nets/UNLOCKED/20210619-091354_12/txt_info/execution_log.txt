+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        0.0         |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+--------------------------------------------------------------+
| ID | Nro |                            Plazas                            |
+----+-----+--------------------------------------------------------------+
| 0  |  9  | ['P2', 'P4', 'P7', 'P16', 'P18', 'P19', 'P22', 'P24', 'P25'] |
| 1  |  14 | ['P2', 'P4', 'P7', 'P11', 'P15', 'P18', 'P19', 'P22', 'P25'] |
| 2  |  1  |    ['P2', 'P5', 'P16', 'P18', 'P19', 'P21', 'P24', 'P25']    |
| 3  |  2  |       ['P1', 'P4', 'P16', 'P18', 'P19', 'P24', 'P25']        |
| 4  |  6  |           ['P1', 'P4', 'P13', 'P15', 'P19', 'P25']           |
| 5  |  16 |       ['P2', 'P5', 'P15', 'P18', 'P19', 'P21', 'P25']        |
| 6  |  13 | ['P2', 'P4', 'P7', 'P13', 'P15', 'P18', 'P19', 'P22', 'P25'] |
+----+-----+--------------------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  |  1  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
+-----+---------+-----------------------------------------+---------------------+
|  ID | Marcado |               T - Entrada               |      T - Salida     |
+-----+---------+-----------------------------------------+---------------------+
| P26 |    2    | ['T2', 'T5', 'T9', 'T13', 'T14', 'T16'] | ['T1', 'T4', 'T19'] |
| P26 |    1    |           ['T2', 'T11', 'T16']          | ['T1', 'T4', 'T19'] |
| P26 |    2    | ['T2', 'T5', 'T9', 'T13', 'T14', 'T16'] | ['T1', 'T4', 'T19'] |
| P26 |    1    |        ['T2', 'T5', 'T13', 'T16']       | ['T1', 'T4', 'T19'] |
| P26 |    1    |                 ['T13']                 | ['T1', 'T4', 'T19'] |
| P26 |    1    |        ['T2', 'T5', 'T13', 'T16']       | ['T1', 'T4', 'T19'] |
| P26 |    2    |           ['T2', 'T13', 'T16']          | ['T1', 'T4', 'T19'] |
+-----+---------+-----------------------------------------+---------------------+
+----+-----+--------------------------------------------------------------+
| ID | Nro |                            Plazas                            |
+----+-----+--------------------------------------------------------------+
| 7  |  10 | ['P2', 'P4', 'P7', 'P16', 'P18', 'P19', 'P22', 'P24', 'P25'] |
| 8  |  15 | ['P2', 'P4', 'P7', 'P11', 'P15', 'P18', 'P19', 'P22', 'P25'] |
| 9  |  1  |    ['P2', 'P5', 'P16', 'P18', 'P19', 'P21', 'P24', 'P25']    |
| 10 |  2  |       ['P1', 'P4', 'P16', 'P18', 'P19', 'P24', 'P25']        |
| 11 |  6  |           ['P1', 'P4', 'P13', 'P15', 'P19', 'P25']           |
| 12 |  17 |       ['P2', 'P5', 'P15', 'P18', 'P19', 'P21', 'P25']        |
| 13 |  14 | ['P2', 'P4', 'P7', 'P13', 'P15', 'P18', 'P19', 'P22', 'P25'] |
+----+-----+--------------------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
+-----+---------+-----------------------------------------+---------------------+
|  ID | Marcado |               T - Entrada               |      T - Salida     |
+-----+---------+-----------------------------------------+---------------------+
| P27 |    2    | ['T2', 'T5', 'T9', 'T13', 'T14', 'T16'] | ['T1', 'T4', 'T19'] |
| P27 |    1    |           ['T2', 'T11', 'T16']          | ['T1', 'T4', 'T19'] |
| P27 |    2    | ['T2', 'T5', 'T9', 'T13', 'T14', 'T16'] | ['T1', 'T4', 'T19'] |
| P27 |    1    |        ['T2', 'T5', 'T13', 'T16']       | ['T1', 'T4', 'T19'] |
| P27 |    1    |                 ['T13']                 | ['T1', 'T4', 'T19'] |
| P27 |    1    |        ['T2', 'T5', 'T13', 'T16']       | ['T1', 'T4', 'T19'] |
| P27 |    2    |           ['T2', 'T13', 'T16']          | ['T1', 'T4', 'T19'] |
+-----+---------+-----------------------------------------+---------------------+
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+--------------------------------------------------------------+
| ID | Nro |                            Plazas                            |
+----+-----+--------------------------------------------------------------+
| 14 |  10 | ['P2', 'P4', 'P7', 'P16', 'P18', 'P19', 'P22', 'P24', 'P25'] |
| 15 |  15 | ['P2', 'P4', 'P7', 'P11', 'P15', 'P18', 'P19', 'P22', 'P25'] |
| 16 |  1  |    ['P2', 'P5', 'P16', 'P18', 'P19', 'P21', 'P24', 'P25']    |
| 17 |  2  |       ['P1', 'P4', 'P16', 'P18', 'P19', 'P24', 'P25']        |
| 18 |  6  |           ['P1', 'P4', 'P13', 'P15', 'P19', 'P25']           |
| 19 |  17 |       ['P2', 'P5', 'P15', 'P18', 'P19', 'P21', 'P25']        |
| 20 |  14 | ['P2', 'P4', 'P7', 'P13', 'P15', 'P18', 'P19', 'P22', 'P25'] |
+----+-----+--------------------------------------------------------------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
+-----+---------+-----------------------------------------+---------------------+
|  ID | Marcado |               T - Entrada               |      T - Salida     |
+-----+---------+-----------------------------------------+---------------------+
| P27 |    2    | ['T2', 'T5', 'T9', 'T13', 'T14', 'T16'] | ['T1', 'T4', 'T19'] |
| P27 |    1    |           ['T2', 'T11', 'T16']          | ['T1', 'T4', 'T19'] |
| P27 |    2    | ['T2', 'T5', 'T9', 'T13', 'T14', 'T16'] | ['T1', 'T4', 'T19'] |
| P27 |    1    |        ['T2', 'T5', 'T13', 'T16']       | ['T1', 'T4', 'T19'] |
| P27 |    1    |                 ['T13']                 | ['T1', 'T4', 'T19'] |
| P27 |    1    |        ['T2', 'T5', 'T13', 'T16']       | ['T1', 'T4', 'T19'] |
| P27 |    2    |           ['T2', 'T13', 'T16']          | ['T1', 'T4', 'T19'] |
+-----+---------+-----------------------------------------+---------------------+
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          -0.9          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
+----+-----+--------+
| ID | Nro | Plazas |
+----+-----+--------+
+----+-----+--------+
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        90.0        |          -0.9          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+
