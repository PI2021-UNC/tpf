21/06/2021 12:39:35[INFO] Iniciando la ejecucion serial de todos los casos... 
  
21/06/2021 12:49:01[INFO] Clasificando los resultados... 
  
21/06/2021 12:49:11[INFO] Generando estadisticas... 
  
21/06/2021 12:49:11[RESULT] Se analizaron 25 redes de petri, de las cuales:   
21/06/2021 12:49:11[RESULT] 11 se pudieron desbloquear   
+--------------------+-------------------+--------------------+
|        Red         | Duracion Analisis | Nro de Iteraciones |
+--------------------+-------------------+--------------------+
| 20210621-121849_11 |     10.83 seg     |         3          |
| 20210621-121849_19 |     11.02 seg     |         3          |
| 20210621-121849_14 |     179.24 seg    |         9          |
| 20210621-121849_23 |     20.70 seg     |         3          |
| 20210621-121849_10 |     20.91 seg     |         4          |
| 20210621-121849_6  |     20.99 seg     |         4          |
| 20210621-121849_0  |     27.86 seg     |         6          |
| 20210621-121849_16 |     39.52 seg     |         8          |
| 20210621-121849_22 |     53.20 seg     |         11         |
| 20210621-121849_17 |     57.06 seg     |         8          |
| 20210621-121849_7  |     99.88 seg     |         19         |
+--------------------+-------------------+--------------------+ 

21/06/2021 12:49:11[RESULT] 11 no contenian deadlock inicialmente   
+--------------------+-------------------+--------------------+
|        Red         | Duracion Analisis | Nro de Iteraciones |
+--------------------+-------------------+--------------------+
| 20210621-121849_4  |      1.42 seg     |         1          |
| 20210621-121849_8  |      1.42 seg     |         1          |
| 20210621-121849_24 |      1.47 seg     |         1          |
| 20210621-121849_3  |      1.47 seg     |         1          |
| 20210621-121849_1  |      1.52 seg     |         1          |
| 20210621-121849_15 |      1.52 seg     |         1          |
| 20210621-121849_5  |      1.68 seg     |         1          |
| 20210621-121849_18 |      1.78 seg     |         1          |
| 20210621-121849_21 |      2.12 seg     |         1          |
| 20210621-121849_12 |      2.47 seg     |         1          |
| 20210621-121849_13 |      2.59 seg     |         1          |
+--------------------+-------------------+--------------------+ 

21/06/2021 12:49:11[RESULT] 3 no eran del tipo s3pr   
+--------------------+-------------------+--------------------+
|        Red         | Duracion Analisis | Nro de Iteraciones |
+--------------------+-------------------+--------------------+
| 20210621-121849_2  |      1.52 seg     |         1          |
| 20210621-121849_9  |      1.52 seg     |         1          |
| 20210621-121849_20 |      1.77 seg     |         1          |
+--------------------+-------------------+--------------------+ 

21/06/2021 12:49:11[RESULT] 0 no pudieron destrabarse debido a corte por timeout del petrinator o limite de iteraciones   
21/06/2021 12:49:11[RESULT] 0 expirementaron fallas inesperadas durante el analisis   
21/06/2021 12:49:11[RESULT] Del set de redes propuesto, observar los siguientes indicadores:   
21/06/2021 12:49:11[RESULT] - 100.00% de las redes validas pudo ser desbloqueado -- Calidad del Algoritmo   
21/06/2021 12:49:11[RESULT] - 56.00% no cumplian condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra   
21/06/2021 12:49:11[RESULT] - 0.00% se debieron descartar por problemas petrinator y/o algoritmo py original -- Incidencias debido a factores externos 
  
21/06/2021 12:49:11[INFO] La ejecucion masiva ha concluido exitosamente, luego de 575.70 segundos 
  
