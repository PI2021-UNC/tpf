21/06/2021 18:49:54[DEBUG] pflow_file:  ../analisis_5_21062021/gen_nets/20210621-175515_19/net.pflow  
21/06/2021 18:49:54[DEBUG] output_folder:  ../analisis_5_21062021/gen_nets/20210621-175515_19/  
21/06/2021 18:49:54[DEBUG] html_path:  ../analisis_5_21062021/gen_nets/20210621-175515_19/  
21/06/2021 18:49:54[DEBUG] txt_path:  ../analisis_5_21062021/gen_nets/20210621-175515_19/txt_info  

--------------------------------------------------------------------------   
21/06/2021 18:49:54[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:50:07[INFO] Leyendo archivos...   
21/06/2021 18:50:09[INFO] Cantidad de estados: 1839  
21/06/2021 18:50:09[INFO] Cantidad de transiciones: 19  
21/06/2021 18:50:09[INFO] Cantidad de plazas: 27  
21/06/2021 18:50:09[INFO] Cantidad de sifones: 50  
21/06/2021 18:50:09[INFO] Cantidad de trampas: 40  

--------------------------------------------------------------------------
   
21/06/2021 18:50:09[INFO] Cantidad de entrenamientos a realizar:  1  
21/06/2021 18:50:09[INFO] Entrenamiento numero: 0 
 
21/06/2021 18:50:09[INFO] Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        0.0         |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------   
21/06/2021 18:50:09[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:50:22[INFO] Importando datos desde html...   
21/06/2021 18:50:24[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:50:24[INFO] Cantidad de transiciones: 19  
21/06/2021 18:50:24[INFO] Estados de deadlock: [15, 17, 25, 43]  
21/06/2021 18:50:24[INFO] Cantidad de sifones: 50  
21/06/2021 18:50:24[INFO] Cantidad de sifones vacios en estado de deadlock: 5 
 
21/06/2021 18:50:24[INFO] SIFONES   
+----+-----+-----------------------------------------------+
| ID | Nro |                     Plazas                    |
+----+-----+-----------------------------------------------+
| 0  |  3  |    ['P2', 'P7', 'P13', 'P19', 'P24', 'P27']   |
| 1  |  15 |       ['P2', 'P7', 'P13', 'P24', 'P25']       |
| 2  |  21 |    ['P2', 'P7', 'P8', 'P13', 'P24', 'P25']    |
| 3  |  31 |    ['P2', 'P3', 'P7', 'P13', 'P24', 'P25']    |
| 4  |  43 | ['P2', 'P3', 'P7', 'P8', 'P13', 'P24', 'P25'] |
+----+-----+-----------------------------------------------+
21/06/2021 18:50:24[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  |  1  |  1  |  1  |  0  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
21/06/2021 18:50:24[INFO] POSIBLES SUPERVISORES A AGREGAR   
+-----+---------+--------------------------+---------------------+
|  ID | Marcado |       T - Entrada        |      T - Salida     |
+-----+---------+--------------------------+---------------------+
| P28 |    1    |   ['T2', 'T6', 'T12']    | ['T1', 'T5', 'T18'] |
| P28 |    1    |       ['T2', 'T6']       | ['T1', 'T5', 'T18'] |
| P28 |    1    |    ['T2', 'T6', 'T7']    | ['T1', 'T5', 'T18'] |
| P28 |    1    |    ['T2', 'T3', 'T6']    | ['T1', 'T5', 'T18'] |
| P28 |    1    | ['T2', 'T3', 'T6', 'T7'] | ['T1', 'T5', 'T18'] |
+-----+---------+--------------------------+---------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:50:24[INFO] Ejecutando accion...   
21/06/2021 18:50:24[ACTION] Agregar supervisor    
21/06/2021 18:50:24[ACTION] Agrego supervisor Numero: 1  

--------------------------------------------------------------------------   
21/06/2021 18:50:24[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:50:34[INFO] Importando datos desde html...   
21/06/2021 18:50:34[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:50:34[INFO] Cantidad de sifones vacios en estado de deadlock ANTES: 5  
21/06/2021 18:50:34[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA: 1  
21/06/2021 18:50:34[INFO] Cantidad de transiciones: 19  
21/06/2021 18:50:34[INFO] Estados de deadlock: [20]  
21/06/2021 18:50:34[INFO] Cantidad de sifones: 38  
21/06/2021 18:50:34[INFO] Cantidad de sifones vacios en estado de deadlock: 1 
 
21/06/2021 18:50:34[INFO] SIFONES   
+----+-----+---------------------+
| ID | Nro |        Plazas       |
+----+-----+---------------------+
| 5  |  10 | ['P1', 'P6', 'P28'] |
+----+-----+---------------------+
21/06/2021 18:50:34[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
21/06/2021 18:50:34[INFO] POSIBLES SUPERVISORES A AGREGAR   
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
21/06/2021 18:50:34[INFO] Actualizando estado de la red..   
21/06/2021 18:50:34[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   

--------------------------------------------------------------------------
   
21/06/2021 18:50:34[INFO] Calculando recompensa..   
21/06/2021 18:50:34[REWARD] El sifon se controlo correctamente, recompensa -1   

--------------------------------------------------------------------------
   
21/06/2021 18:50:34[INFO] NEW Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:50:34[INFO] Ejecutando accion...   
21/06/2021 18:50:34[ACTION] Agregar o quitar arcos   
21/06/2021 18:50:34[ACTION] La transicion en conflicto T9 le tiene que devolver un token al supervisor P28   
21/06/2021 18:50:34[INFO] Se agrego un arco desde T9 hasta P28   
21/06/2021 18:50:34[ACTION] La transicion en conflicto T14 le tiene que devolver un token al supervisor P28   
21/06/2021 18:50:34[INFO] Se agrego un arco desde T14 hasta P28   

--------------------------------------------------------------------------   
21/06/2021 18:50:34[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:50:47[INFO] Importando datos desde html...   
21/06/2021 18:50:48[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:50:48[INFO] Cantidad de sifones vacios en estado de deadlock ANTES: 1  
21/06/2021 18:50:48[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA: 0  
21/06/2021 18:50:48[INFO] Cantidad de transiciones: 19  
21/06/2021 18:50:48[INFO] Estados de deadlock: []  
21/06/2021 18:50:48[INFO] Cantidad de sifones: 26  
21/06/2021 18:50:48[INFO] Cantidad de sifones vacios en estado de deadlock: 0 
 
21/06/2021 18:50:48[INFO] SIFONES   
+----+-----+--------+
| ID | Nro | Plazas |
+----+-----+--------+
+----+-----+--------+
21/06/2021 18:50:48[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  |  1  |  1  |  1  |  0  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
21/06/2021 18:50:48[INFO] POSIBLES SUPERVISORES A AGREGAR   
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
21/06/2021 18:50:48[INFO] Actualizando estado de la red..   
21/06/2021 18:50:48[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   

--------------------------------------------------------------------------
   
21/06/2021 18:50:48[INFO] Calculando recompensa..   
21/06/2021 18:50:48[REWARD] No hay mas deadlock, recompensa 100   

--------------------------------------------------------------------------
   
21/06/2021 18:50:48[INFO] NEW Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          90.0          |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:50:48[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK   

--------------------------------------------------------------------------
   
21/06/2021 18:50:48[INFO] Training complete! 
  
