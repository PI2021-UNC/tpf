21/06/2021 18:53:41[DEBUG] pflow_file:  ../analisis_5_21062021/gen_nets/20210621-175515_25/net.pflow  
21/06/2021 18:53:41[DEBUG] output_folder:  ../analisis_5_21062021/gen_nets/20210621-175515_25/  
21/06/2021 18:53:41[DEBUG] html_path:  ../analisis_5_21062021/gen_nets/20210621-175515_25/  
21/06/2021 18:53:41[DEBUG] txt_path:  ../analisis_5_21062021/gen_nets/20210621-175515_25/txt_info  

--------------------------------------------------------------------------   
21/06/2021 18:53:41[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:53:53[INFO] Leyendo archivos...   
21/06/2021 18:53:54[INFO] Cantidad de estados: 745  
21/06/2021 18:53:54[INFO] Cantidad de transiciones: 19  
21/06/2021 18:53:54[INFO] Cantidad de plazas: 27  
21/06/2021 18:53:54[INFO] Cantidad de sifones: 17  
21/06/2021 18:53:54[INFO] Cantidad de trampas: 20  

--------------------------------------------------------------------------
   
21/06/2021 18:53:54[INFO] Cantidad de entrenamientos a realizar:  1  
21/06/2021 18:53:54[INFO] Entrenamiento numero: 0 
 
21/06/2021 18:53:54[INFO] Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        0.0         |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------   
21/06/2021 18:53:54[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:54:04[INFO] Importando datos desde html...   
21/06/2021 18:54:04[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:54:04[INFO] Cantidad de transiciones: 19  
21/06/2021 18:54:04[INFO] Estados de deadlock: [20]  
21/06/2021 18:54:04[INFO] Cantidad de sifones: 17  
21/06/2021 18:54:04[INFO] Cantidad de sifones vacios en estado de deadlock: 2 
 
21/06/2021 18:54:04[INFO] SIFONES   
+----+-----+------------------------------------------+
| ID | Nro |                  Plazas                  |
+----+-----+------------------------------------------+
| 0  |  5  | ['P2', 'P7', 'P11', 'P19', 'P24', 'P27'] |
| 1  |  12 | ['P2', 'P7', 'P11', 'P19', 'P24', 'P26'] |
+----+-----+------------------------------------------+
21/06/2021 18:54:04[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
21/06/2021 18:54:04[INFO] POSIBLES SUPERVISORES A AGREGAR   
+-----+---------+--------------+---------------------+
|  ID | Marcado | T - Entrada  |      T - Salida     |
+-----+---------+--------------+---------------------+
| P28 |    1    | ['T2', 'T7'] | ['T1', 'T6', 'T18'] |
| P28 |    1    | ['T2', 'T7'] | ['T1', 'T6', 'T18'] |
+-----+---------+--------------+---------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:54:04[INFO] Ejecutando accion...   
21/06/2021 18:54:04[ACTION] Agregar supervisor    
21/06/2021 18:54:04[ACTION] Agrego supervisor Numero: 0  

--------------------------------------------------------------------------   
21/06/2021 18:54:04[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:54:12[INFO] Importando datos desde html...   
21/06/2021 18:54:13[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:54:13[INFO] Cantidad de sifones vacios en estado de deadlock ANTES: 2  
21/06/2021 18:54:13[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA: 2  
21/06/2021 18:54:13[INFO] Cantidad de transiciones: 19  
21/06/2021 18:54:13[INFO] Estados de deadlock: [21]  
21/06/2021 18:54:13[INFO] Cantidad de sifones: 19  
21/06/2021 18:54:13[INFO] Cantidad de sifones vacios en estado de deadlock: 2 
 
21/06/2021 18:54:13[INFO] SIFONES   
+----+-----+---------------------------------------+
| ID | Nro |                 Plazas                |
+----+-----+---------------------------------------+
| 2  |  10 |          ['P1', 'P6', 'P28']          |
| 3  |  17 | ['P1', 'P2', 'P3', 'P4', 'P6', 'P28'] |
+----+-----+---------------------------------------+
21/06/2021 18:54:13[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
21/06/2021 18:54:13[INFO] POSIBLES SUPERVISORES A AGREGAR   
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
21/06/2021 18:54:13[INFO] Actualizando estado de la red..   
21/06/2021 18:54:13[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock   

--------------------------------------------------------------------------
   
21/06/2021 18:54:13[INFO] Calculando recompensa..   
21/06/2021 18:54:13[REWARD] El sifon se controlo correctamente, recompensa -1   

--------------------------------------------------------------------------
   
21/06/2021 18:54:13[INFO] NEW Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:54:13[INFO] Ejecutando accion...   
21/06/2021 18:54:13[ACTION] Agregar o quitar arcos   
21/06/2021 18:54:13[ACTION] La transicion en conflicto T9 le tiene que devolver un token al supervisor P28   
21/06/2021 18:54:13[INFO] Se agrego un arco desde T9 hasta P28   
21/06/2021 18:54:13[ACTION] La transicion en conflicto T13 le tiene que devolver un token al supervisor P28   
21/06/2021 18:54:13[INFO] Se agrego un arco desde T13 hasta P28   

--------------------------------------------------------------------------   
21/06/2021 18:54:13[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:54:23[INFO] Importando datos desde html...   
21/06/2021 18:54:23[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:54:23[INFO] Cantidad de sifones vacios en estado de deadlock ANTES: 2  
21/06/2021 18:54:23[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA: 0  
21/06/2021 18:54:23[INFO] Cantidad de transiciones: 19  
21/06/2021 18:54:23[INFO] Estados de deadlock: []  
21/06/2021 18:54:23[INFO] Cantidad de sifones: 19  
21/06/2021 18:54:23[INFO] Cantidad de sifones vacios en estado de deadlock: 0 
 
21/06/2021 18:54:23[INFO] SIFONES   
+----+-----+--------+
| ID | Nro | Plazas |
+----+-----+--------+
+----+-----+--------+
21/06/2021 18:54:23[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
21/06/2021 18:54:23[INFO] POSIBLES SUPERVISORES A AGREGAR   
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
21/06/2021 18:54:23[INFO] Actualizando estado de la red..   
21/06/2021 18:54:23[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   

--------------------------------------------------------------------------
   
21/06/2021 18:54:23[INFO] Calculando recompensa..   
21/06/2021 18:54:23[REWARD] No hay mas deadlock, recompensa 100   

--------------------------------------------------------------------------
   
21/06/2021 18:54:23[INFO] NEW Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          90.0          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:54:23[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK   

--------------------------------------------------------------------------
   
21/06/2021 18:54:23[INFO] Training complete! 
  
