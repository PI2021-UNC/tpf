21/06/2021 18:42:56[DEBUG] pflow_file:  ../analisis_5_21062021/gen_nets/20210621-175514_7/net.pflow  
21/06/2021 18:42:56[DEBUG] output_folder:  ../analisis_5_21062021/gen_nets/20210621-175514_7/  
21/06/2021 18:42:56[DEBUG] html_path:  ../analisis_5_21062021/gen_nets/20210621-175514_7/  
21/06/2021 18:42:56[DEBUG] txt_path:  ../analisis_5_21062021/gen_nets/20210621-175514_7/txt_info  

--------------------------------------------------------------------------   
21/06/2021 18:42:56[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:43:05[INFO] Leyendo archivos...   
21/06/2021 18:43:05[INFO] Cantidad de estados: 110  
21/06/2021 18:43:05[INFO] Cantidad de transiciones: 14  
21/06/2021 18:43:05[INFO] Cantidad de plazas: 21  
21/06/2021 18:43:05[INFO] Cantidad de sifones: 12  
21/06/2021 18:43:05[INFO] Cantidad de trampas: 12  

--------------------------------------------------------------------------
   
21/06/2021 18:43:05[INFO] Cantidad de entrenamientos a realizar:  1  
21/06/2021 18:43:05[INFO] Entrenamiento numero: 0 
 
21/06/2021 18:43:05[INFO] Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        0.0         |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------   
21/06/2021 18:43:05[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:43:13[INFO] Importando datos desde html...   
21/06/2021 18:43:13[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:43:13[INFO] Cantidad de transiciones: 14  
21/06/2021 18:43:13[INFO] Estados de deadlock: [76, 79]  
21/06/2021 18:43:13[INFO] Cantidad de sifones: 12  
21/06/2021 18:43:13[INFO] Cantidad de sifones vacios en estado de deadlock: 1 
 
21/06/2021 18:43:13[INFO] SIFONES   
+----+-----+-----------------------------------------+
| ID | Nro |                  Plazas                 |
+----+-----+-----------------------------------------+
| 0  |  12 | ['P2', 'P5', 'P9', 'P12', 'P16', 'P20'] |
+----+-----+-----------------------------------------+
21/06/2021 18:43:13[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
21/06/2021 18:43:13[INFO] POSIBLES SUPERVISORES A AGREGAR   
+-----+---------+--------------------+---------------------+
|  ID | Marcado |    T - Entrada     |      T - Salida     |
+-----+---------+--------------------+---------------------+
| P22 |    1    | ['T2', 'T5', 'T8'] | ['T1', 'T4', 'T13'] |
+-----+---------+--------------------+---------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:43:13[INFO] Ejecutando accion...   
21/06/2021 18:43:13[ACTION] Agregar supervisor    
21/06/2021 18:43:13[ACTION] Agrego supervisor Numero: 0   

--------------------------------------------------------------------------   
21/06/2021 18:43:13[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:43:22[INFO] Importando datos desde html...   
21/06/2021 18:43:22[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:43:22[INFO] Cantidad de sifones vacios en estado de deadlock ANTES: 1  
21/06/2021 18:43:22[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA: 1  
21/06/2021 18:43:22[INFO] Cantidad de transiciones: 14  
21/06/2021 18:43:22[INFO] Estados de deadlock: [36]  
21/06/2021 18:43:22[INFO] Cantidad de sifones: 13  
21/06/2021 18:43:22[INFO] Cantidad de sifones vacios en estado de deadlock: 1 
 
21/06/2021 18:43:22[INFO] SIFONES   
+----+-----+----------------------------------+
| ID | Nro |              Plazas              |
+----+-----+----------------------------------+
| 1  |  8  | ['P1', 'P4', 'P8', 'P13', 'P22'] |
+----+-----+----------------------------------+
21/06/2021 18:43:22[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  |  0  |  0  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
21/06/2021 18:43:22[INFO] POSIBLES SUPERVISORES A AGREGAR   
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
21/06/2021 18:43:22[INFO] Actualizando estado de la red..   
21/06/2021 18:43:22[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock   

--------------------------------------------------------------------------
   
21/06/2021 18:43:22[INFO] Calculando recompensa..   
21/06/2021 18:43:22[REWARD] El sifon se controlo correctamente, recompensa -1   

--------------------------------------------------------------------------
   
21/06/2021 18:43:22[INFO] NEW Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          0.0           |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:43:22[INFO] Ejecutando accion...   
21/06/2021 18:43:22[ACTION] Agregar o quitar arcos   
21/06/2021 18:43:22[ACTION] La transicion en conflicto T10 le tiene que devolver un token al supervisor P22   
21/06/2021 18:43:22[INFO] Se agrego un arco desde T10 hasta P22   

--------------------------------------------------------------------------   
21/06/2021 18:43:22[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
21/06/2021 18:43:29[INFO] Importando datos desde html...   
21/06/2021 18:43:30[INFO] Analizando cantidad de supervisores que se pueden agregar...   
21/06/2021 18:43:30[INFO] Cantidad de sifones vacios en estado de deadlock ANTES: 1  
21/06/2021 18:43:30[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA: 0  
21/06/2021 18:43:30[INFO] Cantidad de transiciones: 14  
21/06/2021 18:43:30[INFO] Estados de deadlock: []  
21/06/2021 18:43:30[INFO] Cantidad de sifones: 13  
21/06/2021 18:43:30[INFO] Cantidad de sifones vacios en estado de deadlock: 0 
 
21/06/2021 18:43:30[INFO] SIFONES   
+----+-----+--------+
| ID | Nro | Plazas |
+----+-----+--------+
+----+-----+--------+
21/06/2021 18:43:30[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
| 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 1  | 1  | 1  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+
21/06/2021 18:43:30[INFO] POSIBLES SUPERVISORES A AGREGAR   
+----+---------+-------------+------------+
| ID | Marcado | T - Entrada | T - Salida |
+----+---------+-------------+------------+
+----+---------+-------------+------------+
21/06/2021 18:43:30[INFO] Actualizando estado de la red..   
21/06/2021 18:43:30[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   

--------------------------------------------------------------------------
   
21/06/2021 18:43:30[INFO] Calculando recompensa..   
21/06/2021 18:43:30[REWARD] No hay mas deadlock, recompensa 100   

--------------------------------------------------------------------------
   
21/06/2021 18:43:30[INFO] NEW Q-TABLE   
+-----------------------------------------------------+--------------------+------------------------+
|                   Estados/Acciones                  | Agregar supervisor | Agregar o quitar arcos |
+-----------------------------------------------------+--------------------+------------------------+
|                        Start                        |        -0.9        |          0.0           |
|        Más sifónes vacíos en estado deadlock        |        0.0         |          0.0           |
|       Menos sifónes vacíos en estado deadlock       |        0.0         |          0.0           |
| Igual cantidad de sifónes vacíos en estado deadlock |        0.0         |          90.0          |
|                         End                         |        0.0         |          0.0           |
+-----------------------------------------------------+--------------------+------------------------+

--------------------------------------------------------------------------
   
21/06/2021 18:43:30[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK   

--------------------------------------------------------------------------
   
21/06/2021 18:43:30[INFO] Training complete! 
  
