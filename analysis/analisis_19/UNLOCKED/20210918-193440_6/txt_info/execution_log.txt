18/09/2021 19:55:15[DEBUG] pflow_file:  ../test_networks/20210918-193440_6/net.pflow  
18/09/2021 19:55:15[DEBUG] output_folder:  ../test_networks/20210918-193440_6/  
18/09/2021 19:55:15[DEBUG] html_path:  ../test_networks/20210918-193440_6/  
18/09/2021 19:55:15[DEBUG] txt_path:  ../test_networks/20210918-193440_6/txt_info  

--------------------------------------------------------------------------   
18/09/2021 19:55:15[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:55:23[INFO] SIFONES   
+----+-----+---------------------------------------------------------+
| ID | Nro |                          Plazas                         |
+----+-----+---------------------------------------------------------+
| 0  |  3  | ['P2', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32', 'P33'] |
| 1  |  15 |     ['P2', 'P12', 'P14', 'P21', 'P23', 'P31', 'P33']    |
| 2  |  4  |     ['P1', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32']    |
+----+-----+---------------------------------------------------------+ 

18/09/2021 19:55:23[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 | T20 | T21 | T22 | T23 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  |  0  |  0  |  0  |  0  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  1  |  1  |  1  |  1  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+ 

18/09/2021 19:55:23[INFO] Supervisores para agregar   
+-----+---------+-----------------------------------+-----------------------------------+---------------------------------------------------------+
|  ID | Marcado |            T - Entrada            |             T - Salida            |                    Sifon a controlar                    |
+-----+---------+-----------------------------------+-----------------------------------+---------------------------------------------------------+
| P34 |    2    | ['T2', 'T9', 'T13', 'T18', 'T21'] | ['T1', 'T8', 'T12', 'T17', 'T20'] | ['P2', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32', 'P33'] |
| P34 |    1    |        ['T2', 'T9', 'T21']        |        ['T1', 'T8', 'T20']        |     ['P2', 'P12', 'P14', 'P21', 'P23', 'P31', 'P33']    |
| P34 |    1    |       ['T13', 'T18', 'T21']       |       ['T12', 'T17', 'T20']       |     ['P1', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32']    |
+-----+---------+-----------------------------------+-----------------------------------+---------------------------------------------------------+ 

18/09/2021 19:55:23[INFO] Supervisores agregados   
+-----+-------+---------+-------------+------------+----------------+
| Nro | Plaza | Marcado | T - Entrada | T - Salida | MAL supervisor |
+-----+-------+---------+-------------+------------+----------------+
+-----+-------+---------+-------------+------------+----------------+ 

18/09/2021 19:55:23[INFO] Supervisores eliminados   
+-----+-------+---------+-------------+------------+----------------+
| Nro | Plaza | Marcado | T - Entrada | T - Salida | MAL supervisor |
+-----+-------+---------+-------------+------------+----------------+
+-----+-------+---------+-------------+------------+----------------+ 

18/09/2021 19:55:23[INFO] Ejecutando accion...   
18/09/2021 19:55:23[ACTION] Agrego supervisor con menor numero de arcos de salida (MNAS)   
18/09/2021 19:55:23[INFO] Supervisor con menor cantidad de arcos de salida agregado 2   

--------------------------------------------------------------------------   
18/09/2021 19:55:23[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
--------------------------------------------------------------------------
   
18/09/2021 19:55:33[INFO] Calculando recompensa..   
18/09/2021 19:55:33[REWARD] Agregue un supervisor que genero mas sifones vacios en deadlock, recompensa -10   

--------------------------------------------------------------------------
   
18/09/2021 19:55:33[STATE] Cambie al estado 1: Hay mas sifones vacios en deadlock   
+--------+---------+-------------------+----------------+---------+---------+------------------------+
| Plazas | Estados | Tokens trabajando | Sifones vacios | Sifones | Trampas | Supervisores agregados |
+--------+---------+-------------------+----------------+---------+---------+------------------------+
|   34   |   223   |        538        |       5        |   101   |    50   |           1            |
+--------+---------+-------------------+----------------+---------+---------+------------------------+ 
+------------------------------+------------+-----------------+--------------------+-----------------------+
|       Estados/Acciones       | + sup MNAS | + sup aleatorio | - sup MAL agregado | - ultimo sup agregado |
+------------------------------+------------+-----------------+--------------------+-----------------------+
|            Start             |   -10.0    |       0.0       |        0.0         |          0.0          |
| + sifónes vacíos en deadlock |    0.0     |       0.0       |        0.0         |          0.0          |
| - sifónes vacíos en deadlock |    0.0     |       0.0       |        0.0         |          0.0          |
| = sifónes vacíos en deadlock |    0.0     |       0.0       |        0.0         |          0.0          |
|             End              |    0.0     |       0.0       |        0.0         |          0.0          |
+------------------------------+------------+-----------------+--------------------+-----------------------+ 

18/09/2021 19:55:33[INFO] SIFONES   
+----+-----+----------------------------------------------------------------+
| ID | Nro |                             Plazas                             |
+----+-----+----------------------------------------------------------------+
| 3  |  3  |    ['P2', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32', 'P33']     |
| 4  |  20 |        ['P2', 'P12', 'P14', 'P21', 'P23', 'P31', 'P33']        |
| 5  |  7  |              ['P14', 'P20', 'P23', 'P29', 'P34']               |
| 6  |  13 |        ['P1', 'P15', 'P20', 'P23', 'P29', 'P32', 'P34']        |
| 7  |  17 | ['P2', 'P11', 'P15', 'P20', 'P23', 'P29', 'P32', 'P33', 'P34'] |
+----+-----+----------------------------------------------------------------+ 

18/09/2021 19:55:33[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 | T20 | T21 | T22 | T23 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  |  0  |  0  |  0  |  0  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  1  |  1  |  1  |  1  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+ 

18/09/2021 19:55:33[INFO] Supervisores para agregar   
+-----+---------+-----------------------------------+-----------------------------------+----------------------------------------------------------------+
|  ID | Marcado |            T - Entrada            |             T - Salida            |                       Sifon a controlar                        |
+-----+---------+-----------------------------------+-----------------------------------+----------------------------------------------------------------+
| P35 |    2    | ['T2', 'T9', 'T13', 'T18', 'T21'] | ['T1', 'T8', 'T12', 'T17', 'T20'] |    ['P2', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32', 'P33']     |
| P35 |    1    |        ['T2', 'T9', 'T21']        |        ['T1', 'T8', 'T20']        |        ['P2', 'P12', 'P14', 'P21', 'P23', 'P31', 'P33']        |
| P35 |    1    |           ['T13', 'T21']          |           ['T11', 'T20']          |        ['P1', 'P15', 'P20', 'P23', 'P29', 'P32', 'P34']        |
| P35 |    2    |        ['T2', 'T13', 'T21']       |        ['T1', 'T11', 'T20']       | ['P2', 'P11', 'P15', 'P20', 'P23', 'P29', 'P32', 'P33', 'P34'] |
+-----+---------+-----------------------------------+-----------------------------------+----------------------------------------------------------------+ 

18/09/2021 19:55:33[INFO] Supervisores agregados   
+-----+-------+---------+-----------------------+-----------------------+----------------+
| Nro | Plaza | Marcado |      T - Entrada      |       T - Salida      | MAL supervisor |
+-----+-------+---------+-----------------------+-----------------------+----------------+
|  1  |  P34  |    1    | ['T13', 'T18', 'T21'] | ['T12', 'T17', 'T20'] |     False      |
+-----+-------+---------+-----------------------+-----------------------+----------------+ 

18/09/2021 19:55:33[INFO] Supervisores eliminados   
+-----+-------+---------+-------------+------------+----------------+
| Nro | Plaza | Marcado | T - Entrada | T - Salida | MAL supervisor |
+-----+-------+---------+-------------+------------+----------------+
+-----+-------+---------+-------------+------------+----------------+ 

18/09/2021 19:55:33[INFO] Ejecutando accion...   
18/09/2021 19:55:33[ACTION] Agrego supervisor con menor numero de arcos de salida (MNAS)   
18/09/2021 19:55:33[INFO] Supervisor con menor cantidad de arcos de salida agregado 2   

--------------------------------------------------------------------------   
18/09/2021 19:55:33[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
--------------------------------------------------------------------------
   
18/09/2021 19:55:44[INFO] Calculando recompensa..   
18/09/2021 19:55:44[REWARD] Agregue un supervisor que genero menos sifones vacios en deadlock, recompensa -1   

--------------------------------------------------------------------------
   
18/09/2021 19:55:44[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock   
+--------+---------+-------------------+----------------+---------+---------+------------------------+
| Plazas | Estados | Tokens trabajando | Sifones vacios | Sifones | Trampas | Supervisores agregados |
+--------+---------+-------------------+----------------+---------+---------+------------------------+
|   34   |   223   |        538        |       5        |   101   |    50   |           1            |
|   35   |   220   |        530        |       2        |   117   |    78   |           2            |
+--------+---------+-------------------+----------------+---------+---------+------------------------+ 
+------------------------------+------------+-----------------+--------------------+-----------------------+
|       Estados/Acciones       | + sup MNAS | + sup aleatorio | - sup MAL agregado | - ultimo sup agregado |
+------------------------------+------------+-----------------+--------------------+-----------------------+
|            Start             |   -10.0    |       0.0       |        0.0         |          0.0          |
| + sifónes vacíos en deadlock |    -1.0    |       0.0       |        0.0         |          0.0          |
| - sifónes vacíos en deadlock |    0.0     |       0.0       |        0.0         |          0.0          |
| = sifónes vacíos en deadlock |    0.0     |       0.0       |        0.0         |          0.0          |
|             End              |    0.0     |       0.0       |        0.0         |          0.0          |
+------------------------------+------------+-----------------+--------------------+-----------------------+ 

18/09/2021 19:55:44[INFO] SIFONES   
+----+-----+---------------------------------------------------------+
| ID | Nro |                          Plazas                         |
+----+-----+---------------------------------------------------------+
| 8  |  3  | ['P2', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32', 'P33'] |
| 9  |  25 |     ['P2', 'P12', 'P14', 'P21', 'P23', 'P31', 'P33']    |
+----+-----+---------------------------------------------------------+ 

18/09/2021 19:55:44[INFO] T INVARIANTES   
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | T11 | T12 | T13 | T14 | T15 | T16 | T17 | T18 | T19 | T20 | T21 | T22 | T23 |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
| 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  |  0  |  0  |  0  |  0  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  1  |  1  |  1  |  1  |  1  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  1  |  0  |  0  |  0  |  1  |  1  |
| 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  1  |  1  |  1  |
+----+----+----+----+----+----+----+----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+ 

18/09/2021 19:55:44[INFO] Supervisores para agregar   
+-----+---------+-----------------------------------+-----------------------------------+---------------------------------------------------------+
|  ID | Marcado |            T - Entrada            |             T - Salida            |                    Sifon a controlar                    |
+-----+---------+-----------------------------------+-----------------------------------+---------------------------------------------------------+
| P36 |    2    | ['T2', 'T9', 'T13', 'T18', 'T21'] | ['T1', 'T8', 'T12', 'T17', 'T20'] | ['P2', 'P12', 'P15', 'P21', 'P23', 'P31', 'P32', 'P33'] |
| P36 |    1    |        ['T2', 'T9', 'T21']        |        ['T1', 'T8', 'T20']        |     ['P2', 'P12', 'P14', 'P21', 'P23', 'P31', 'P33']    |
+-----+---------+-----------------------------------+-----------------------------------+---------------------------------------------------------+ 

18/09/2021 19:55:44[INFO] Supervisores agregados   
+-----+-------+---------+-----------------------+-----------------------+----------------+
| Nro | Plaza | Marcado |      T - Entrada      |       T - Salida      | MAL supervisor |
+-----+-------+---------+-----------------------+-----------------------+----------------+
|  1  |  P34  |    1    | ['T13', 'T18', 'T21'] | ['T12', 'T17', 'T20'] |     False      |
|  2  |  P35  |    1    |     ['T13', 'T21']    |     ['T11', 'T20']    |     False      |
+-----+-------+---------+-----------------------+-----------------------+----------------+ 

18/09/2021 19:55:44[INFO] Supervisores eliminados   
+-----+-------+---------+-------------+------------+----------------+
| Nro | Plaza | Marcado | T - Entrada | T - Salida | MAL supervisor |
+-----+-------+---------+-------------+------------+----------------+
+-----+-------+---------+-------------+------------+----------------+ 

18/09/2021 19:55:44[INFO] Ejecutando accion...   
18/09/2021 19:55:44[ACTION] Agrego supervisor con menor numero de arcos de salida (MNAS)   
18/09/2021 19:55:44[INFO] Supervisor con menor cantidad de arcos de salida agregado 1   

--------------------------------------------------------------------------   
18/09/2021 19:55:44[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
--------------------------------------------------------------------------
   
18/09/2021 19:56:01[INFO] Calculando recompensa..   
18/09/2021 19:56:01[REWARD] No hay mas deadlock, recompensa 100   

--------------------------------------------------------------------------
   
18/09/2021 19:56:01[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK   

--------------------------------------------------------------------------
   
+--------+---------+-------------------+----------------+---------+---------+------------------------+
| Plazas | Estados | Tokens trabajando | Sifones vacios | Sifones | Trampas | Supervisores agregados |
+--------+---------+-------------------+----------------+---------+---------+------------------------+
|   34   |   223   |        538        |       5        |   101   |    50   |           1            |
|   35   |   220   |        530        |       2        |   117   |    78   |           2            |
|   36   |   217   |        522        |       0        |   192   |    83   |           3            |
+--------+---------+-------------------+----------------+---------+---------+------------------------+ 
+------------------------------+------------+-----------------+--------------------+-----------------------+
|       Estados/Acciones       | + sup MNAS | + sup aleatorio | - sup MAL agregado | - ultimo sup agregado |
+------------------------------+------------+-----------------+--------------------+-----------------------+
|            Start             |   -10.0    |       0.0       |        0.0         |          0.0          |
| + sifónes vacíos en deadlock |    -1.0    |       0.0       |        0.0         |          0.0          |
| - sifónes vacíos en deadlock |   100.0    |       0.0       |        0.0         |          0.0          |
| = sifónes vacíos en deadlock |    0.0     |       0.0       |        0.0         |          0.0          |
|             End              |    0.0     |       0.0       |        0.0         |          0.0          |
+------------------------------+------------+-----------------+--------------------+-----------------------+ 

18/09/2021 19:56:01[INFO] Comienza proceso de eliminacion de supervisores redundantes   
18/09/2021 19:56:01[INFO] Supervisor a eliminar   
+-----+-------+---------+-----------------------+-----------------------+----------------+
| Nro | Plaza | Marcado |      T - Entrada      |       T - Salida      | MAL supervisor |
+-----+-------+---------+-----------------------+-----------------------+----------------+
|  1  |  P34  |    1    | ['T13', 'T18', 'T21'] | ['T12', 'T17', 'T20'] |     False      |
+-----+-------+---------+-----------------------+-----------------------+----------------+ 
18/09/2021 19:56:01[INFO] Eliminando supervisor ...   

--------------------------------------------------------------------------   
18/09/2021 19:56:01[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:56:14[INFO] El supervisor eliminado no era redundante, volviendo a agregar supervisor   
18/09/2021 19:56:14[INFO] Se volvio a agregar correctamente el supervisor eliminado   

--------------------------------------------------------------------------   
18/09/2021 19:56:14[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:56:34[INFO] Supervisor a eliminar   
+-----+-------+---------+----------------+----------------+----------------+
| Nro | Plaza | Marcado |  T - Entrada   |   T - Salida   | MAL supervisor |
+-----+-------+---------+----------------+----------------+----------------+
|  2  |  P35  |    1    | ['T13', 'T21'] | ['T11', 'T20'] |     False      |
+-----+-------+---------+----------------+----------------+----------------+ 
18/09/2021 19:56:34[INFO] Eliminando supervisor ...   

--------------------------------------------------------------------------   
18/09/2021 19:56:34[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:56:49[INFO] El supervisor eliminado no era redundante, volviendo a agregar supervisor   
18/09/2021 19:56:49[INFO] Se volvio a agregar correctamente el supervisor eliminado   

--------------------------------------------------------------------------   
18/09/2021 19:56:49[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:57:08[INFO] Supervisor a eliminar   
+-----+-------+---------+---------------------+---------------------+----------------+
| Nro | Plaza | Marcado |     T - Entrada     |      T - Salida     | MAL supervisor |
+-----+-------+---------+---------------------+---------------------+----------------+
|  3  |  P36  |    1    | ['T2', 'T9', 'T21'] | ['T1', 'T8', 'T20'] |     False      |
+-----+-------+---------+---------------------+---------------------+----------------+ 
18/09/2021 19:57:08[INFO] Eliminando supervisor ...   

--------------------------------------------------------------------------   
18/09/2021 19:57:08[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:57:21[INFO] El supervisor eliminado no era redundante, volviendo a agregar supervisor   
18/09/2021 19:57:21[INFO] Se volvio a agregar correctamente el supervisor eliminado   

--------------------------------------------------------------------------   
18/09/2021 19:57:21[INFO] Ejecutando petrinator para conocer estado de la red...   
--------------------------------------------------------------------------
   
18/09/2021 19:57:39[INFO] Se eliminaron todos los supervisores redundantes   
+--------+---------+-------------------+----------------+---------+---------+------------------------+-------------------------+
| Plazas | Estados | Tokens trabajando | Sifones vacios | Sifones | Trampas | Supervisores agregados | Supervisores eliminados |
+--------+---------+-------------------+----------------+---------+---------+------------------------+-------------------------+
|   36   |   217   |        522        |       0        |   192   |    83   |           3            |            0            |
+--------+---------+-------------------+----------------+---------+---------+------------------------+-------------------------+ 
