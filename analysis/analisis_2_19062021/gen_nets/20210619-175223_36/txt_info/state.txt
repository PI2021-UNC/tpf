 p 28 
 t 17 
 Reachable states from S0 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 2, 1] 
 T1 => S1 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 0] 
 T6 => S6 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 0] 
 T16 => S15 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0] 
 
 Reachable states from S1 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 0] 
 T2 => S2 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 0, 1, 0] 
 
 Reachable states from S2 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 0, 1, 0] 
 T3 => S3 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 0, 1, 1, 0] 
 
 Reachable states from S3 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 0, 1, 1, 0] 
 T4 => S4 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 2, 0] 
 
 Reachable states from S4 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 2, 0] 
 T5 => S5 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 2, 0] 
 Deadlock on S5 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 2, 0] 
 
 Reachable states from S6 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 0] 
 T7 => S7 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 1, 0] 
 
 Reachable states from S7 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 1, 0] 
 T8 => S8 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 2, 1] 
 
 Reachable states from S8 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 2, 1] 
 T9 => S0 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 2, 1] 
 T1 => S9 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 1, 1, 1, 0] 
 T6 => S12 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 1, 0] 
 T16 => S14 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S9 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 1, 1, 1, 0] 
 T9 => S1 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 0] 
 T2 => S10 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 0, 1, 0] 
 
 Reachable states from S10 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 0, 1, 0] 
 T9 => S2 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 0, 1, 0] 
 T3 => S11 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 0, 1, 1, 0] 
 
 Reachable states from S11 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 0, 1, 1, 0] 
 T9 => S3 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 0, 1, 1, 0] 
 
 Reachable states from S12 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 1, 0] 
 T9 => S6 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 0] 
 T7 => S13 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0] 
 
 Reachable states from S13 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0] 
 T9 => S7 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 1, 0] 
 
 Reachable states from S14 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0] 
 T9 => S15 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0] 
 
 Reachable states from S15 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0] 
 T10 => S16 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0] 
 T13 => S57 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S16 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0] 
 T11 => S17 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0] 
 
 Reachable states from S17 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0] 
 T12 => S18 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1] 
 
 Reachable states from S18 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1] 
 T17 => S0 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 2, 1] 
 T1 => S19 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0] 
 T6 => S24 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
 T16 => S33 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0] 
 
 Reachable states from S19 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0] 
 T17 => S1 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 0] 
 T2 => S20 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0] 
 
 Reachable states from S20 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0] 
 T17 => S2 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 0, 1, 0] 
 T3 => S21 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0] 
 
 Reachable states from S21 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0] 
 T17 => S3 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 0, 1, 1, 0] 
 T4 => S22 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 2, 0] 
 
 Reachable states from S22 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 2, 0] 
 T17 => S4 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 2, 0] 
 T5 => S23 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 0] 
 
 Reachable states from S23 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 0] 
 T17 => S5 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 2, 0] 
 
 Reachable states from S24 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
 T17 => S6 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 0] 
 T7 => S25 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0] 
 
 Reachable states from S25 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0] 
 T17 => S7 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 1, 0] 
 T8 => S26 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 2, 1] 
 
 Reachable states from S26 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 2, 1] 
 T17 => S8 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 2, 1] 
 T9 => S18 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1] 
 T1 => S27 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0] 
 T6 => S30 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0] 
 T16 => S32 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S27 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0] 
 T17 => S9 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 1, 1, 1, 0] 
 T9 => S19 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0] 
 T2 => S28 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0] 
 
 Reachable states from S28 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0] 
 T17 => S10 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 0, 1, 0] 
 T9 => S20 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0] 
 T3 => S29 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0] 
 
 Reachable states from S29 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0] 
 T17 => S11 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 0, 1, 1, 0] 
 T9 => S21 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0] 
 
 Reachable states from S30 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0] 
 T17 => S12 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 1, 1, 1, 0] 
 T9 => S24 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
 T7 => S31 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0] 
 
 Reachable states from S31 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0] 
 T17 => S13 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0] 
 T9 => S25 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0] 
 
 Reachable states from S32 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0] 
 T17 => S14 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0] 
 T9 => S33 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0] 
 
 Reachable states from S33 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0] 
 T17 => S15 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0] 
 T10 => S34 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0] 
 T13 => S50 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S34 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0] 
 T17 => S16 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0] 
 T11 => S35 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0] 
 
 Reachable states from S35 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0] 
 T17 => S17 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0] 
 T12 => S36 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 2, 1] 
 
 Reachable states from S36 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 2, 1] 
 T17 => S18 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1] 
 T1 => S37 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1, 1, 1, 1, 1, 0] 
 T6 => S42 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 0] 
 
 Reachable states from S37 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1, 1, 1, 1, 1, 0] 
 T17 => S19 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0] 
 T2 => S38 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 0, 1, 0] 
 
 Reachable states from S38 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 0, 1, 0] 
 T17 => S20 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0] 
 T3 => S39 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 0, 1, 1, 0] 
 
 Reachable states from S39 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 0, 1, 1, 0] 
 T17 => S21 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0] 
 T4 => S40 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 1, 2, 0] 
 
 Reachable states from S40 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 1, 2, 0] 
 T17 => S22 [0, 0, 0, 1, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 2, 0] 
 T5 => S41 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 2, 0] 
 
 Reachable states from S41 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 2, 0] 
 T17 => S23 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 0] 
 
 Reachable states from S42 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 0] 
 T17 => S24 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
 T7 => S43 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 1, 0] 
 
 Reachable states from S43 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 1, 0] 
 T17 => S25 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0] 
 T8 => S44 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 1, 2, 1] 
 
 Reachable states from S44 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 1, 2, 1] 
 T17 => S26 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 2, 1] 
 T9 => S36 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 2, 1] 
 T1 => S45 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 1, 1, 1, 0] 
 T6 => S48 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S45 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 1, 1, 1, 0] 
 T17 => S27 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0] 
 T9 => S37 [1, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1, 1, 1, 1, 1, 0] 
 T2 => S46 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 0, 1, 0] 
 
 Reachable states from S46 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 0, 1, 0] 
 T17 => S28 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0] 
 T9 => S38 [0, 1, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 0, 1, 0] 
 T3 => S47 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 0, 1, 1, 0] 
 
 Reachable states from S47 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 0, 1, 1, 0] 
 T17 => S29 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0] 
 T9 => S39 [0, 0, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 0, 1, 1, 0] 
 
 Reachable states from S48 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 0, 1, 1, 1, 0] 
 T17 => S30 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0] 
 T9 => S42 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 0] 
 T7 => S49 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1, 0] 
 
 Reachable states from S49 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1, 0] 
 T17 => S31 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0] 
 T9 => S43 [0, 0, 0, 0, 1, 1, 1, 3, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 1, 0] 
 
 Reachable states from S50 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0] 
 T14 => S51 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 2, 1] 
 T17 => S57 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S51 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 2, 1] 
 T15 => S36 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 2, 1] 
 T6 => S52 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0] 
 T17 => S54 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 2, 1] 
 
 Reachable states from S52 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0] 
 T15 => S42 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 0] 
 T17 => S53 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0] 
 
 Reachable states from S53 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0] 
 T15 => S24 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
 
 Reachable states from S54 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 2, 1] 
 T15 => S18 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1] 
 T6 => S53 [0, 0, 0, 0, 1, 1, 1, 3, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0] 
 T16 => S55 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0] 
 
 Reachable states from S55 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0] 
 T15 => S33 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0] 
 T13 => S56 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0] 
 
 Reachable states from S56 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0] 
 T15 => S50 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0] 
 
 Reachable states from S57 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0] 
 T14 => S54 [0, 0, 0, 0, 1, 1, 1, 3, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 2, 1] 
