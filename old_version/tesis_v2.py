#!/usr/bin/python3

"""
Determinacion automática del control de una red de Petri

Analiza las RdP y determina los supervisores para el control de los deadlock presentes en dicha red.

Autores:
- Lenta, Luis Alejandro
- Monsierra, Lucas Gabriel
- Nicolaide, Christian
"""

import os
import sys
import argparse
import subprocess
import numpy as np
import html_txt_all as hta
import filter_data as filterdata
import new_red
import arcs as arcosrdp
from prettytable import PrettyTable
from datetime import datetime


##### INICIO CONFIGURACION GLOBAL #####

# PRINT OPTIONS: Se puede agregar o quitar labels de debug al igual que excepciones
custom_print_level = ["[INFO]", "[ACTION]", "[REWARD]", "[STATE]", "[INPUT]", "[ERROR]", "[DEBUG]",
                      "[RESULT]"]  # Tags que se desean loguear, omitimos [INSTRUCTION]
custom_print_exceptions = ["---", "\n"]  # Excepciones del custom_print
custom_print_datefmt = "%d/%m/%Y %H:%M:%S"  # Formato del timestamp del log
show_label = True  # Mostrar/ocultar el label del tag
log_activity = True  # Habilitar/deshabilitar el logueo a archivo de la salida por consola
format_text = True  # Habilitar/deshabilitar el formateo de texto en base a los tags
INFO = '\033[94m'  # Aazul
ACTION = '\033[96m'  # Cyan
REWARD = '\033[92m'  # Verde
STATE = '\033[93m'  # Amarillo
INPUT = '\033[0m'  # Sin formato especial
ERROR = '\033[91m'  # Rojo
DEBUG = '\033[0;37;45m'  # Fondo magenta letra blanca
RESULT = '\033[6;30;43m'  # Fondo amarillo letra negra
ENDC = '\033[0m'  # Clear format

# PETRINATOR OPTIONS: Se construye el comando para ejecutar la red: java_command<string> petrinator_path<string> pflow_file<string> output_folder<string> bring_gui<boolean> auto_close<boolean> current_iteration<int> redirect_output<string>
java_command = "java -Xms12G -Xmx12G -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=20 -XX:ConcGCThreads=5 -XX:InitiatingHeapOccupancyPercent=70 -jar"  # Comando java
petrinator_path = "../auto_petrinator/Petrinator-1.0-jar-with-dependencies.jar"  # Path al jar automizado
pflow_file = "../test_path/net.pflow"  # Ruta al pflow por defecto (ojo esta hardcodeado net.pflow en el jar)
output_folder = "../test_path"  # Carpeta de salida al pflow (ojo esta hardcodeado net.pflow en el jar)
bring_gui = "false"  # Habilitar o deshabilitar la gui de petrinator para ver la red
auto_close = "true"  # Con auto_close en false y bring_gui en true se puede hacer debug paso a paso viendo evolucion graficamente
current_iteration = 0  # Para saber si se corre el analisis S3PR y deadlock o no
clasify = "false"  # Flag para indicar si hago sólo la clasificación de redes o corro el análisis incial también
redirect_output = "> /dev/null"  # Quitar "> /dev/null" para debug del jar
petrinator_timeout = 300  # Si tarda mas que esto, corta la ejecucion de petrinator y marca la red como NO.UNLOCKED

# ALGORITHM OPTIONS: Variables que usa el algoritmo para operar
id = 0  # ID del sifon (creo)
name_pflow = ""  # Nombre del pflow
q_values = np.zeros((5, 2))  # Random values de q
html_path = "../test_path"  # Carpeta de salida de los html
txt_path = "../test_path/txt_info"  # Carpeta de salida de los txt
logfile_name = "/execution_log.txt"  # Nombre del archivo de log (se guarda en carpeta de txt)
step_execution = False  # Flag para pedir enter para actualizar red y despues de cada accion
multiple_runs = False  # Flag que permite seguir analizando otras redes cuando termina la actual
auto_createdir = True  # Permite creacion automatica del output_folder, html_path o txt_path si no existen
trainning_num = 1  # Numero de iteraciones del algoritmo
iteration_limit = 25  # Limite de iteraciones para intentar destrabar la red
train_networks_directory = "../train_networks"  # Carpeta con redes para entrenar la red neuronal.
episodes_directory = "../train_episodes"  # Carpeta con todos los pasos del entrenamiento

# STATS GLOBAL VARIABLES
cant_supervisores = 0
cant_sifones_inciales = 0
cant_plazas_iniciales = 0
cant_transiciones_iniciales = 0
cant_estados_iniciales = 0
network = None
RUN_NN = False  # Para correr entrenamiento de Neural Network
ACTIONS = ['Agregar supervisor', 'Agregar o quitar arcos']


###### FIN CONFIGURACION GLOBAL ######


def custom_print(text1, text2="", text3="", text4="", show_label=show_label, log_activity=log_activity,
                 format_text=format_text, log_path=None):
    """
    Imprime hasta 4 strings si el tag del primero esta incluido en la lista custom_print_level.

    Parameters
    ----------
        text1          -- Requerido: texto tageado a imprimir
        text2          -- Opcional: texto adicional (Default: "")
        text3          -- Opcional: texto adicional (Default: "")
        text4          -- Opcional: texto adicional (Default: "")
        show_label     -- Opcional: flag para activar/desactivar impresion del tag (Default: "True")
        log_activity   -- Opcional: flag para activar/desactivar logueo en archivo de texto (Default: "True")
        format_text    -- Opcional: flag para activar/desactivar el formateo de texto (Default: "True")
        log_path       -- Opcional: path al archivo de log que se quiere escribir
    """
    if log_path is None:
        dir_path(txt_path, True)
        log_path = txt_path + logfile_name

    # Registro la hora
    timestamp = datetime.now().strftime(custom_print_datefmt)

    # El label siempre viene en el primer string, entonces lo spliteo por espacios
    # y me quedo con el primer elemento del array que seria el tag
    substring = text1.split(" ")

    # Si el texto es una excepcion no hago el chequeo de tags
    for exception in custom_print_exceptions:
        # Solo verifico que sea un unico string y que la excepcion este contenida en ese texto
        if ((exception in text1) and text2 == "" and text3 == "" and text4 == ""):
            print(text1, text2, text3, text4)
            # Si el log esta activado, escribo archivo
            if (log_activity):
                with open(log_path, "a+") as log:
                    print(text1, text2, text3, text4, file=log)
            return

    # Le saco los corchetes al tag para luego llamar la variable global de mismo nombre que contiene el formato a usar
    format = substring[0].replace("[", "").replace("]", "")

    # Chequeo si el tag esta en la lista de lo que deseo imprimir
    if (substring[0] in (custom_print_level)):
        # Chequeo si tengo que imprimir etiqueta
        if (show_label):
            if (format_text):
                # En caso positivo, imprimo timestamp + formatodesado + text1..n + vuelvo formato default
                print(timestamp + globals()[format], text1, text2, text3, text4, ENDC)
            else:
                # En caso negativo, imprimo timestamp + text1..n
                print(timestamp + text1, text2, text3, text4)

            # Si el log esta activado, escribo archivo tambien (sin formato independientemente de format_text)
            if (log_activity):
                with open(log_path, "a+") as log:
                    print(timestamp + text1, text2, text3, text4, file=log)
        else:
            if (format_text):
                # En caso negativo, le saco el tag y el espacio separador despues de el y agrego timestamp y formato deseado
                print(timestamp + globals()[format], text1.replace(substring[0] + " ", ""), text2, text3, text4, ENDC)
            else:
                # En caso negativo, le saco el tag y el espacio separador despues de el y agrego timestamp
                print(timestamp + text1.replace(substring[0] + " ", ""), text2, text3, text4)

            # Si el log esta activado, escribo archivo tambien (sin formato independientemente de format_text)
            if (log_activity):
                with open(log_path, "a+") as log:
                    print(timestamp + text1.replace(substring[0] + " ", ""), text2, text3, text4, file=log)


def siphones_traps(cantidad_plazas):
    """
    Devuelve una matriz de [sifones x plazas] y otra de [trampas x plazas]. Tambien devuelve cantidad de sifones y de trampas.

    Parameters
    ----------
        cantidad_plazas     -- Cantidad de plazas de la RdP

    Returns
    -------
        matriz_sifones      -- Matriz de sifones
        matriz_traps        -- Matriz de trampas
        cantidad_sifones    -- Cantidad de sifones
        cantidad_traps      -- Cantidad de trampas
    """
    # Apertura de archivos resultantes de la conversion de archivos .html to .txt obtenidos del SW Petrinator, para su siguiente manipulacion y filtrado.
    pasi = open(txt_path + "/siphons_traps.txt", "r")
    i = 0
    aux_s = 0
    aux_t = 0

    for line in pasi:  # Obtiene la cantidad de trampa y sifones que contiene la RdP
        i = i + 1
        if (i > 1):
            aux_s = aux_s + 1
            aux_t = aux_t + 1
            if (line.find("Minimal traps") == 1):
                cantidad_sifones = aux_s - 1
                aux_t = 0
            if (line.find("Analysis") == 1):
                cantidad_traps = aux_t - 1

    pasi.seek(0)  # Vuelve el cabezal al principio del archivo
    aux_s = cantidad_sifones
    aux_t = cantidad_traps

    s_flag = 0
    t_flag = 0

    siphons_aux = []
    traps_aux = []

    for line in pasi:
        if (s_flag == 1 and aux_s != 0):  # Obtiene los sifones de la RdP
            siphons_aux.append(line)
            aux_s = aux_s - 1

        if (t_flag == 1 and aux_t != 0):  # Obtiene las trampas de la RdP
            traps_aux.append(line)
            aux_t = aux_t - 1

        if (line.find("Minimal siphons") == 1):
            s_flag = 1
            # print("Sifones")

        if (line.find("Minimal traps") == 1):
            t_flag = 1
            # print("Trampas")

    siphons = []
    traps = []
    for i in range(len(siphons_aux)):  # Elimina los espacios del string y agrega cada sifon
        siphons.append(str(siphons_aux[i]).split())

    for i in range(len(traps_aux)):  # Elimina los espacios del string y agrega cada trampa
        traps.append(str(traps_aux[i]).split())

    # Creamos la matriz que representa por fila la cantidad de sifones o traps y por columna plazas
    # hay un 1 en las plazas que conforman esos sifones o traps
    matriz_sifones = np.zeros((cantidad_sifones, cantidad_plazas))
    matriz_traps = np.zeros((cantidad_traps, cantidad_plazas))

    for i in range(0, len(siphons)):
        for j in range(0, len(siphons[i])):
            matriz_sifones[i][int(siphons[i][j]) - 1] = 1

    for i in range(0, len(traps)):
        for j in range(0, len(traps[i])):
            matriz_traps[i][int(traps[i][j]) - 1] = 1

    pasi.close()
    return matriz_sifones, matriz_traps, cantidad_sifones, cantidad_traps


def invariantes(cantidad_transiciones):
    """
    Devuelve la matriz de invariantes de transicion, mediante la apertura de un archivo txt previamente convertido.

    Parameters
    ----------
        cantidad_transiciones   -- Cantidad de transiciones de la RdP

    Returns
    -------
        M_TI                    -- Matriz de T-invariante
    """
    file = open(txt_path + "/invariante.txt", "r")
    cont = 0
    TInvariantes = []
    PInvariantes = []
    for line in file:
        if (cont == 0):
            TInvariantes = line
            cont = cont + 1
        elif (cont == 1):
            PInvariantes = line
            cont = cont + 1

    aux_I = TInvariantes.split(' ')
    TInvariantes = []
    for i in range(cantidad_transiciones, len(aux_I) - 2):
        TInvariantes.append(aux_I[i])

    M_TI = np.zeros(
        (int(len(TInvariantes) / cantidad_transiciones), cantidad_transiciones))
    m = 0
    for i in range((int(len(TInvariantes) / cantidad_transiciones))):
        for j in range(cantidad_transiciones):
            M_TI[i][j] = TInvariantes[m]
            m = m + 1

    file.close()
    return M_TI.astype(int)


def matriz_pre_pos(cantidad_plazas, cantidad_transiciones):
    """
    Devuelve la matriz pre y post a partir de un archivo txt previamente convertido.

    Parameters
    ----------
        cantidad_plazas         -- Cantidad de plazas de la RdP
        cantidad_transiciones   -- Cantidad de transiciones de la RdP

    Returns
    -------
        matriz_I_pos            -- Matriz Post (I+)
        matriz_I_neg            -- Matriz Pre  (I-)
    """
    # Apertura de archivos resultantes de la conversion de archivos .html to .txt
    # obtenidos del SW Petrinator, para su siguiente manipulacion y filtrado.
    matriz_I = open(txt_path + "/matricesI.txt", "r")
    matriz_I_pos = np.loadtxt(matriz_I, delimiter=' '' ', skiprows=3,
                              max_rows=cantidad_plazas, dtype=bytes).astype(str)
    matriz_I_neg = np.loadtxt(matriz_I, delimiter=' '' ', skiprows=2, max_rows=cantidad_plazas + 1, dtype=bytes).astype(
        str)

    aux_pos = []
    aux_neg = []
    for i in range(cantidad_plazas):
        aux_pos.append(matriz_I_pos[i].split(" "))

    for i in range(cantidad_plazas):
        aux_neg.append(matriz_I_neg[i].split(" "))

    aux_pos = np.delete(aux_pos, 0, 1)
    aux_neg = np.delete(aux_neg, 0, 1)
    aux_pos = np.delete(aux_pos, cantidad_transiciones, 1)
    aux_neg = np.delete(aux_neg, cantidad_transiciones, 1)

    matriz_I_pos = aux_pos.astype(int)
    matriz_I_neg = aux_neg.astype(int)

    matriz_I.close()

    return matriz_I_pos, matriz_I_neg


def conflict_t_invariante(t_conflict, t_invariant, matriz_pos, plazas_sifon_complemento, t_in):
    """
    Obtiene las transiciones en conflicto que le tienen que devolver algun token al supervisor.

    Parameters
    ----------
        t_conflict                  -- Transiciones en conflicto
        t_invariant                 -- T-Invariante
        matriz_pos                  -- Matriz Post (I+)
        plazas_sifon_complemento    -- Plazas complemento del sifon a controlar
        t_in                        -- Transiciones de entrada al supervisor
    """
    for ii in range(0, len(t_conflict)):
        flag_sifon = 0
        for jj in range(0, len(t_invariant)):
            # La T en conflicto forma parte del T invariante?
            if (int(t_invariant[jj][t_conflict[ii]]) == 1):
                aux_t = np.copy(t_invariant[jj])  # Guardamos el T invariante
                for aa in range(0, len(aux_t)):
                    if (int(aux_t[aa]) == 1):  # Buscamos la T que forma parte del T-invariante
                        for bb in range(0, len(matriz_pos)):
                            if (int(matriz_pos[bb][aa]) == 1):
                                # La T alimenta a alguna plaza del sifon'
                                if (int(plazas_sifon_complemento[bb]) == 1):
                                    flag_sifon = 1

        if (flag_sifon == 0):
            # custom_print("[INFO] Transicion input:", int(t_conflict[ii]) + 1)
            t_in.append("T" + str(int(t_conflict[ii]) + 1))


def path_conflict(t_idle, t_analizar, flag_idle, plazas_sifon_complemento, matriz_pre, matriz_pos, cantidad_plazas,
                  cantidad_transiciones, t_invariant, t_in):
    """
    Se obtienen las transiciones que forman parte del conflicto. \n

    Parameters \n
    ----------
        t_idle                      -- Transiciones idle
        t_analizar                  -- Transicion a analizar
        flag_idle                   -- Indica que es una t-idle
        plazas_sifon_complemento    -- Plazas complementos del sifon a controlar
        matriz_pre                  -- Matriz Post (I+)
        matriz_pos                  -- Matriz Pre  (I-)
        cantidad_plazas             -- Cantidad de plazas de la RdP
        cantidad_transiciones       -- Cantidad de tranciones de la RdP
        t_invariant                 -- T-Invariante
        t_in                        -- Transiciones input al supervisor
    """

    if (t_idle != t_analizar or flag_idle == 1):
        flag_idle = 0
        p_idle = []  # Plaza a las que le pone tokens la transicion
        for jj in range(0, cantidad_plazas):
            # A que plazas esta alimentando esa transicion(t_analizar)
            if (int(matriz_pos[jj][t_analizar]) != 0):
                p_idle.append(int(jj))
        # print(p_idle)
        for ii in range(0, len(p_idle)):
            t_conflict = []  # Plaza que alimenta a las transiciones en conflicto
            for mm in range(0, cantidad_transiciones):
                if (matriz_pre[p_idle[ii]][mm] == 1):
                    # Transiciones en conflicto sensibilizadas por esa plaza
                    t_conflict.append(mm)
            if (len(t_conflict) > 1):  # La plaza sensibiliza a mas de una transicion? Hay conflicto
                file_t_conflict_orig = open(txt_path + "/t_conflict_red_original.txt", "w")
                for ij in range(0, len(t_conflict)):
                    file_t_conflict_orig.write(str(t_conflict[ij]) + ' ')
                file_t_conflict_orig.close()

                conflict_t_invariante(t_conflict, t_invariant, matriz_pos, plazas_sifon_complemento, t_in)

            else:  # no hay conflicto
                if (t_conflict[0] < t_analizar):
                    continue
                else:
                    path_conflict(t_idle, t_conflict[0], flag_idle, plazas_sifon_complemento, matriz_pre, matriz_pos,
                                  cantidad_plazas, cantidad_transiciones, t_invariant, t_in)


def supervisor(cantidad_transiciones, cantidad_plazas, sifon, matriz_es_tr, matriz_pos, matriz_pre, matriz_sifones,
               t_invariant, lista_supervisores, lista_sifones):
    """
    Define el supervisor que va a controlar el bad-siphon. Esta funcion define el marcado de la plaza supervisor y las transiciones de entrada y salida del mismo. \n

    Parameters \n
    ----------
        cantidad_transiciones   -- Cantidad de transiciones de la RdP
        cantidad_plazas         -- Cantidad de plazas de la RdP
        sifon                   -- bad siphon a controlar. Compuesto por 3 elementos: estado deadlock[0], numero sifon[1], marcado sifon[2]
        matriz_es_tr            -- Matriz [estado x transiciones]
        matriz_pos              -- Matriz Pos (I+)
        matriz_pre              -- Matriz Pre (I-)
        matriz_sifones          -- Matriz de sifones
        t_invariant             -- T-invariantes
        lista_supervisores      -- Contiene el listado de los posibles supervisores a agregar
        lista_sifones           -- Array que contiene la informacion del sifon a controlar. Contiene el id, el numero de sifon a controlar y un array con las plazas que lo componen
    """
    global id
    # print("\nid=", id)
    info_sifon = [id]
    id = id + 1
    trans_idle = []  # Transiciones habilitadas en el marcado inicial
    # Es la posicion 2 debido que el sifon esta declarado estado deadlock[0], numero sifon[1], marcado sifon[2]
    marcado_supervisor = sifon[2] - 1
    # Marcado del supervisor
    # print("Sifon a controlar: ", sifon[1] + 1)
    info_sifon.append(sifon[1] + 1)
    plazas_sifon_aux = np.copy(matriz_sifones[sifon[1]])
    sif_aux = []
    for i in range(len(plazas_sifon_aux)):
        if (plazas_sifon_aux[i] != 0):
            sif_aux.append(f"P{i + 1}")
    # print("Plazas del Sifon: ", sif_aux)
    info_sifon.append(sif_aux)
    # print("Marcado del supervisor", marcado_supervisor)
    lista_sifones.append(info_sifon)
    # Transiciones que salen del estado idle, le quitan tokens a los supervisores
    # estas se encuentran sensibilizadas en el estado inicial (0) y son las transiciones
    # que son distintan de -1 en la matriz matriz_es_tr (estado-transicion) --> Transiciones 1 de Ezpeleta
    t_out = []
    for ii in range(cantidad_transiciones):
        if (matriz_es_tr[0][ii] != -1):
            trans_idle.append(ii)
            t_out.append("T" + str(ii + 1))
            # +1 Por problemas de indice en petrinator empieza en 1
            # print("Transicion output:", ii + 1)

    # Vector que indica que transiciones sacan/ponen tokens en el sifon
    tran_sifon = np.zeros(cantidad_transiciones)

    plazas_sifon = np.copy(matriz_sifones[sifon[
        1]])  # Vectors de todas las plazas del sistema, en 1 se encuentran las plazas que componen nuestro sifon
    # print(plazas_sifon)
    # Localizamos transiciones que colocan tokens al supervisor-->Transiciones 2 de Ezpeleta
    for i in range(0, cantidad_plazas):
        if (plazas_sifon[i] == 1):  # Es una plaza del sifon
            for j in range(0, cantidad_transiciones):
                if (int(matriz_pre[i][j]) == 1):
                    tran_sifon[j] = tran_sifon[j] - \
                                    1  # Le quita tokens al sifon
                if (int(matriz_pos[i][j]) == 1):
                    tran_sifon[j] = tran_sifon[j] + \
                                    1  # Le agrega tokens al sifon
    t_in = []
    for i in range(0, cantidad_transiciones):
        # Si es mayor a 0 significa que esta transicion coloca mas tokens a los sifones de los que le quitan
        if (tran_sifon[i] > 0):
            # Petrinator empieza en 1 y no en cero por eso el +1
            # print("Transicion input:", i + 1)
            t_in.append("T" + str(i + 1))
    # Usado para calcular la 3er transicion de Ezpeleta
    plazas_sifon_complemento = np.copy(plazas_sifon)

    # Obtenemos los complementos
    for i in range(0, len(tran_sifon)):
        if (tran_sifon[i] > 0):
            for j in range(0, cantidad_plazas):
                # son las plazas que habilitan transiciones que agregan mas
                if (int(matriz_pre[j][i]) == 1):
                    # tokens de los que sacan del sifon.
                    plazas_sifon_complemento[j] = 1

    for tt in range(0, len(trans_idle)):
        # indica en cuantos T-invariantes aparece la Transiciones habilitadas en estado idle
        cont_t_invariante = 0
        # de ser =>2 implica que esta en conflicto
        for yy in range(0, len(t_invariant)):
            if (t_invariant[yy][trans_idle[tt]] == 1):
                cont_t_invariante = cont_t_invariante + 1
        if (cont_t_invariante >= 2):
            path_conflict(trans_idle[tt], trans_idle[tt], 1, plazas_sifon_complemento, matriz_pre, matriz_pos,
                          cantidad_plazas, cantidad_transiciones, t_invariant, t_in)  # El 1 indica que es flag_idle

    if (marcado_supervisor > 0) & (len(t_in) > 0):
        lista_supervisores.append(["P" + str(cantidad_plazas + 1), str(marcado_supervisor), t_in, t_out, sif_aux])


def fun_sifones_deadlock(estado, matriz_sifones, matriz_es_pl, idle, cantidad_plazas, cantidad_sifones, sifon_idle,
                         sifon_deadlock):
    """
    Devuelve los sifones que se vacian en ese estado de deadlock. \n
    Apartir de matriz de Estados x Plazas = [Marcado] se recorre la fila de la matriz donde se encuentra el estado deadlock,
    colocando un "1" en aquellas plazas donde el marcado sea >=1. \n
    Se realiza un and entre esa fila de la matriz y el sifon, si la and = 0 implica que ese sifon se encuentra vacio para ese estado de deadlock.

    Parameters \n
    ----------
        estado          -- Estado que posee Deadlock.
        matriz_sifones  -- [Marcado de plazas que componen el sifon]
        matriz_es_pl    -- EstadosxPlazas = [Marcado para ese estado].
        idle            -- Indica si se agrega a la lista de sifon_idle o sifon_deadlock
    """

    aux = np.zeros(cantidad_plazas)
    flag_sifon_idle = 0
    for j in range(0, cantidad_plazas):
        # Obtenemos las plazas(marcadas) del estado deadlock
        if (matriz_es_pl[estado][j] >= 1):
            aux[j] = 1

    for i in range(0, cantidad_sifones):
        cont = 0
        for j in range(0, cantidad_plazas):
            if (int(matriz_sifones[i][j] and aux[j]) == 1):
                cont = cont + 1  # Si el contador es distinto de cero, el sifon no esta vacio
        if (cont == 0):
            marcado = 0
            for j in range(0, cantidad_plazas):
                if (matriz_sifones[i][j] == 1):
                    marcado = marcado + matriz_es_pl[0][
                        j]  # Es 0 en fila, porque es el estado inicial en el que se encontraban las plazas de los sifones
            if (idle == 0):
                for jj in range(0, len(sifon_idle)):
                    # El sifon vacio en deadlock esta vacio en idle?
                    if (sifon_idle[jj] == i):
                        flag_sifon_idle = 1
                if (flag_sifon_idle == 0):  # El sifon no estaba vacio en idle
                    sifon_agregado = 1
                    for index in range(0,
                                       len(sifon_deadlock)):  # Verifica si el sifon ya se incluyó en la lista, de no ser así lo agrega
                        if (sifon_deadlock[index][1] == i):
                            sifon_agregado = 0
                    if (sifon_agregado):
                        sifon_deadlock.append(
                            [estado, i, marcado])  # Devuelve el sifon y su marcado inicial, para ese estado deadlock
            else:
                sifon_idle.append[i]


# Devuelve todos los datos de la red
def get_entorno():
    call_petrinator()
    custom_print("[INFO] Importando datos desde html...")
    # Se agrega este catch debido al error en la recursividad. Ej: Ezpeleta_V2
    try:
        # Conversion de archivos html a txt
        hta.main(html_path, txt_path)

        # Filtrado de archivos provenientes del Petrinator
        (cantidad_estados, cantidad_plazas, cantidad_transiciones, matriz_es_tr, matriz_es_pl,
         state_deadlock) = filterdata.main(txt_path)
        # print("State deadlock", state_deadlock, "\n")
        # Si hay deadlock en el estado inicial, estoy en un estado terminal, devuelvo todos -1
        # No desanidar el if, porque si el len no es 1, puede que estemos en una situacion que no hay mas deadlock.
        if len(state_deadlock) == 1:
            if state_deadlock[0] == 'S0':
                return -1, -1, -1, -1, -1, -1, -1, -1, -1, -1

        (matriz_sifones, matriz_traps, cantidad_sifones, cantidad_traps) = siphones_traps(cantidad_plazas)
        (matriz_pos, matriz_pre) = matriz_pre_pos(cantidad_plazas, cantidad_transiciones)

        # T-invariantes
        t_invariant = invariantes(cantidad_transiciones)

        (sifon_deadlock, lista_supervisores, lista_sifones) = analisis_2(matriz_sifones, matriz_es_pl, cantidad_plazas,
                                                                         cantidad_sifones, state_deadlock,
                                                                         cantidad_transiciones,
                                                                         matriz_es_tr, matriz_pos, matriz_pre,
                                                                         t_invariant)

        return cantidad_sifones, t_invariant, state_deadlock, sifon_deadlock, lista_supervisores, cantidad_transiciones, matriz_es_tr, matriz_es_pl, matriz_pos, matriz_pre, lista_sifones

    except Exception as e:
        print(e.with_traceback())
        custom_print("[ERROR] Se produjo un error inesperado en la ejecucion del algoritmo, abandonando analisis", "\n")
        subprocess.Popen("touch " + output_folder + "/FAILED", shell=True).communicate()
        exit(1)


# Inicializa las variables necesarias.
def init():
    global cant_sifones_inciales, cant_plazas_iniciales, cant_transiciones_iniciales, cant_estados_iniciales

    # Corre petrinator automatizado para generar los archivos .html necesarios para el analisis del estado de la red.
    call_petrinator()

    # Conversion de archivos html a txt
    custom_print("[INFO] Leyendo archivos...")

    # Se agrega este try/catch debido al problema observado en la ejecucion de ciertas redes autogeneradas donde el archivo cov.html a veces se crea y aveces no
    try:
        hta.main(html_path, txt_path)
    except:
        custom_print("[ERROR] Se produjo un error inesperado en la ejecucion del petrinator, abandonando analisis",
                     "\n")
        subprocess.Popen("touch " + output_folder + "/FAILED", shell=True).communicate()
        exit(1)

    # Filtrado de archivos provenientes del Petrinator
    (cantidad_estados, cantidad_plazas, cantidad_transiciones, matriz_es_tr, matriz_es_pl,
     state_deadlock) = filterdata.main(txt_path)

    # Matrices
    (matriz_sifones, matriz_traps, cantidad_sifones,
     cantidad_traps) = siphones_traps(cantidad_plazas)

    (matriz_pos, matriz_pre) = matriz_pre_pos(
        cantidad_plazas, cantidad_transiciones)

    # T-invariantes
    t_invariant = invariantes(cantidad_transiciones)

    # Guardo info estadistica
    cant_sifones_inciales = cantidad_sifones
    cant_plazas_iniciales = cantidad_plazas
    cant_transiciones_iniciales = cantidad_transiciones
    cant_estados_iniciales = cantidad_estados

    custom_print("[INFO] Cantidad de estados:", cantidad_estados)
    custom_print("[INFO] Cantidad de transiciones:", cantidad_transiciones)
    custom_print("[INFO] Cantidad de plazas:", cantidad_plazas)
    custom_print("[INFO] Cantidad de sifones:", cantidad_sifones)
    custom_print("[INFO] Cantidad de trampas:", cantidad_traps)
    custom_print("\n--------------------------------------------------------------------------\n")

    # Obtenemos la cantidad de plazas de la red original
    file_plazas = open(txt_path + "/cantidad_plazas_red_original.txt", "w")
    file_plazas.write(str(len(matriz_es_pl[0])))
    file_plazas.close()

    # Guardamos los T-invariantes de la red original
    file_t_inv_orig = open(txt_path + "/invariante_red_original.txt", "w")
    for i in range(0, len(t_invariant)):
        for j in range(0, len(t_invariant[0])):
            file_t_inv_orig.write(str(t_invariant[i][j]) + ' ')
        file_t_inv_orig.write("\n")
    file_t_inv_orig.close()

    file_t_conflict_orig = open(txt_path + "/t_conflict_red_original.txt", "w")
    file_t_conflict_orig.close()

    # TODO borrar cuando estemos seguros que no hace falta
    # (sifon_deadlock, lista_supervisores, lista_sifones) = analisis_2(matriz_sifones, matriz_es_pl, cantidad_plazas,
    #                                                                cantidad_sifones, state_deadlock,
    #                                                                cantidad_transiciones,
    #                                                                matriz_es_tr, matriz_pos, matriz_pre, t_invariant)

    # return cantidad_sifones, t_invariant, state_deadlock, sifon_deadlock, lista_supervisores, cantidad_transiciones, matriz_es_tr, matriz_es_pl, matriz_pos, matriz_pre, lista_sifones


def analisis_2(matriz_sifones, matriz_es_pl, cantidad_plazas, cantidad_sifones, state_deadlock, cantidad_transiciones,
               matriz_es_tr, matriz_pos, matriz_pre, t_invariant):
    custom_print("[INFO] Analizando cantidad de supervisores que se pueden agregar...")

    sifon_idle = []  # Estado_idle sifon
    sifon_deadlock = []  # Estado_deadlock-sifon-marcado

    idle = 1  # Sifones vacios estado inicial
    fun_sifones_deadlock(0, matriz_sifones, matriz_es_pl, idle, cantidad_plazas, cantidad_sifones, sifon_idle,
                         sifon_deadlock)
    # print("Sifones vacios en idle",sifon_idle)

    # Llamada recursiva a fun_deadlock en busqueda de caminos que dirigen al deadlock
    idle = 0  # Sifones en estado deadlock
    for i in range(0, len(state_deadlock)):
        fun_sifones_deadlock(state_deadlock[i], matriz_sifones, matriz_es_pl, idle, cantidad_plazas, cantidad_sifones,
                             sifon_idle, sifon_deadlock)
    # print("Cantidad de estados con deadlock:", len(state_deadlock))
    # print("Cantidad de sifones vacios:", len(sifon_deadlock))
    # print("\nSIFONES VACIOS")
    # print_pretty_matrix(sifon_deadlock)

    lista_supervisores = []
    lista_sifones = []
    for i in range(0, len(sifon_deadlock)):
        # Nos quedamos con un solo sifon
        sifon = np.copy(sifon_deadlock[i])

        # Agregamos el supervisor del bad-sifon
        supervisor(cantidad_transiciones, cantidad_plazas, sifon, matriz_es_tr, matriz_pos, matriz_pre, matriz_sifones,
                   t_invariant, lista_supervisores, lista_sifones)

    # Elimina archivo temporal
    os.remove(txt_path + "/filtrado_prueba.txt")
    return sifon_deadlock, lista_supervisores, lista_sifones


# Segunda accion de agregar o quitar arcos


def analisis_3(cantidad_transiciones, matriz_es_tr, matriz_es_pl, matriz_pos, matriz_pre):
    file_plazas = open(txt_path + "/cantidad_plazas_red_original.txt", "r")
    cantidad_plazas_red_original = int(file_plazas.read())
    array_supervisor = []
    for i in range(cantidad_plazas_red_original, len(matriz_es_pl[0])):
        array_supervisor.append(i)

    trans_idle = []  # Transiciones habilitadas en el marcado inicial

    # Transiciones que salen del estado idle
    for ii in range(cantidad_transiciones):
        if (matriz_es_tr[0][ii] != -1):
            trans_idle.append(ii)

    # Guardamos los T-invariantes de la red original
    file_t_invariant_red_original = open(txt_path + "/invariante_red_original.txt", "r")

    t_invariant_red_original = []
    aux_t_inv = []

    for line in file_t_invariant_red_original:
        aux_t_inv.append(line)

    for i in range(len(aux_t_inv)):
        t_invariant_red_original.append(str(aux_t_inv[i]).split())

    # Guardamos los conflictos de la red original
    file_t_conflict_red_original = open(txt_path + "/t_conflict_red_original.txt", "r")

    t_conflict_red_original = []
    t_conflict_red_original_aux = []
    aux_conflic = []

    for line in file_t_conflict_red_original:
        aux_conflic.append(line)

    for i in range(len(aux_conflic)):
        t_conflict_red_original_aux.append(str(aux_conflic[i]).split())

    if (len(t_conflict_red_original_aux) != 0):
        t_conflict_red_original = t_conflict_red_original_aux[0]

    msjadd = []
    msjdel = []
    # Buscamos eliminar los arcos de las transiciones idle cuyo T-invariante al que pertenece no
    # le devuelve token al supervisor. (i.e arcos innecesarios)
    for i in range(len(trans_idle)):  # Cantidad de trans_idle
        for j in range(len(t_invariant_red_original)):  # cantidad de t-invariantes
            # La transicion idle forma parte del t-invariantes
            if (int(t_invariant_red_original[j][trans_idle[i]]) == 1):
                for m in range(len(array_supervisor)):
                    cont_sup = 0
                    return_token_transition = []
                    for l in range(len(t_invariant_red_original[j])):
                        if (int(t_invariant_red_original[j][l]) == 1):
                            # El T-invariante de la transicion idle le devuelve token al supervisor?
                            if (int(matriz_pos[array_supervisor[m]][l]) == 1):
                                cont_sup = 1  # si devuelve
                                # TODO borrar si no se usa
                                return_token_transition.append(l)
                    if (cont_sup == 0):  # no devuelve
                        cont = 0
                        for k in range(len(t_conflict_red_original)):
                            aux = int(t_conflict_red_original[k])
                            # La transicion en conflicto forma parte del T-invariante por lo tanto debe devolver el token
                            if (int(t_invariant_red_original[j][aux]) == 1):
                                cont = cont + 1

                                custom_print("[ACTION] La transicion en conflicto T" + str(
                                    aux + 1) + " le tiene que devolver un token al supervisor P" + str(
                                    array_supervisor[m] + 1))

                                # Se agrega el arco
                                arcosrdp.agregararco(name_pflow, aux + 1, array_supervisor[m] + 1)
                                custom_print("[INFO] Se agrego un arco desde T" + str(aux + 1) + " hasta P" + str(
                                    array_supervisor[m] + 1))
                                break

                        if cont == 0:
                            if int(matriz_pre[int(array_supervisor[m])][int(trans_idle[i])]) == 1:
                                custom_print("[ACTION] Eliminar arco desde P" + str(array_supervisor[m] + 1)
                                             + " hasta T" + str(trans_idle[i] + 1))

                                # Se elimina el arco
                                arcosrdp.eliminararco(name_pflow, str(f'P{array_supervisor[m] + 1}'),
                                                      str(f'T{trans_idle[i] + 1}'))
                                custom_print("[INFO] Se elimino el arco desde P" + str(
                                    array_supervisor[m] + 1) + " hasta T" + str(trans_idle[i] + 1))
                    elif cont_sup > 1:  # TODO borrar si no se usa
                        # Acá me parece que es al revés, hay que eliminar desde la transición a la plaza
                        delete_transition = min(return_token_transition)
                        custom_print("[ACTION] Eliminar arco desde T" + str(delete_transition + 1)
                                     + " hasta P" + str(array_supervisor[m] + 1))

                        # Se elimina el arco
                        arcosrdp.eliminararco(name_pflow, str(f'T{delete_transition + 1}'),
                                              str(f'P{array_supervisor[m] + 1}'))
                        custom_print("[INFO] Se elimino el arco con metodo 2 desde T" + str(
                            delete_transition + 1) + " hasta P" + str(array_supervisor[m] + 1))


def agregar_sup(lista_supervisores, id_int):
    global cant_supervisores

    cant_supervisores += 1
    if int(lista_supervisores[id_int][1]) > 0:
        new_red.main(lista_supervisores[id_int][0], lista_supervisores[id_int][1], lista_supervisores[id_int][2],
                     lista_supervisores[id_int][3], name_pflow)
        # TODO Ver si al cambiar la cantidad de invariantes se penaliza con -100


def calcular_recompensas(sifones_vacios_antes_accion, sifones_vacios, t_inv_anterior, t_inv, state_deadlock, action,
                         marcado_sup_agregado, deadlock_inicial):
    custom_print("\n--------------------------------------------------------------------------\n")
    custom_print("[INFO] Calculando recompensa..")
    # Si hay deadlock_inicial
    if deadlock_inicial:
        custom_print("[REWARD] Hay deadlock inicial, recompensa -100")
        return -100
    # Si no hay mas deadlock 100
    elif (len(state_deadlock) == 0):
        custom_print("[REWARD] No hay mas deadlock, recompensa 100")
        return 100
    elif (action == 0):
        # Si se agrego un supervisor con marcado 0 penalizo
        if marcado_sup_agregado < 1:
            custom_print("[REWARD] El marcado del supervisor es 0, recompensa -100")
            return -100
        # Busco si sigue exisitiendo el sifon que se intento controlar al agrear el supervisor
        for i in range(len(sifones_vacios)):
            # Busco si sigue existiendo ese estado en deadlock
            if sifones_vacios[i][0] in sifones_vacios_antes_accion:
                custom_print("[REWARD] Sigue existiendo el sifon del diablo que se quiso controlar, recompensa -100")
                return -100
        # Si no se cumple ninguna de las dos, agregue bien al supervisor
        custom_print("[REWARD] El sifon se controlo correctamente, recompensa -1")
        return -1
    elif (action == 1):
        # Verificar que es mejor, si > solo o >=
        if (len(sifones_vacios_antes_accion) < len(sifones_vacios)):
            custom_print("[REWARD] Hay mas sifones vacios que en el estado anterior, recompensa -100")
            return -100
        else:
            custom_print(
                "[REWARD] Hay menos o igual cantidad de sifones vacios que en el estado anterior, recompensa -1")
            return -1

    # Cuando se ejecuta la accion de agregar un supervisor analisis_2
    #   Si el sifon a controlar no se vacia en estado de deadlock la recompensa es -1
    #   Si el sifon se sigue vaciando en estado de deadlock la recompensa es -100
    #   Si el marcado del supervisor agregado es 0 -100
    # Cuando se realiza la accion de agregar o quitar arcos analisis_3
    #   Si algun sifon que estaba controlado se vacia en estado deadlock -100
    #   Si algun sifon del diablo se controla -1


# define a function that determines if the specified location is a terminal state
def is_terminal_state(sifones_vacios_deadlock, t_inv_anterior, t_inv, estado_inicial_deadlock):
    # if the reward for this location is -1, then it is not a terminal state (i.e., it is a 'white square')

    # print("Invariantes ", len(t_inv), len(t_inv_anterior))
    # print("Sifones vacios en deadklock ", len(sifones_vacios_deadlock))
    custom_print("\n--------------------------------------------------------------------------\n")
    if estado_inicial_deadlock:
        custom_print("[STATE] El estado inicial de la red tiene deadlock. Se llego a un estado terminal NO DESEADO\n")
        custom_print("--------------------------------------------------------------------------\n")

        if (multiple_runs):
            custom_print("[INFO] Al llegar a un estado terminal necesitamos actualizar los datos.\n")
            input(
                "[INPUT] Actualice los archivos con los datos originales de al red y presione enter para continuar con la siguiente iteracion: ")
            # Volvemos a cargar todos los datos de una nueva red, o de la misma pero sin supervisores.
            init()

        return True
    # if len(t_inv) != len(t_inv_anterior):
    # print("Los invariantes cambiaron, se llego a un estado terminal NO DESEADO")
    # input("\nActualice los archivos y presion enter para continuar con la siguiente iteracion: ")
    # return True
    if (len(sifones_vacios_deadlock) == 0):
        custom_print("[STATE] Se llego al estado deseado, NO HAY MAS DEADLOCK")
        custom_print("\n--------------------------------------------------------------------------\n")
        subprocess.Popen("touch " + output_folder + "/UNLOCKED", shell=True).communicate()

        if (multiple_runs):
            custom_print("[INFO] Al llegar a un estado terminal necesitamos actualizar los datos.\n")
            input(
                "[INPUT] Actualice los archivos con los datos originales de al red y presione enter para continuar con la siguiente iteracion: ")
            # Volvemos a cargar todos los datos de una nueva red, o de la misma pero sin supervisores.
            init()
        return True
    else:
        return False


# define an epsilon greedy algorithm that will choose which action to take next (i.e., where to move next)
def get_next_action(estado, epsilon):
    # if a randomly chosen value between 0 and 1 is less than epsilon,
    # then choose the most promising value from the Q-table for this state.
    if (estado == 0):
        return 0
    if np.random.random() < epsilon:
        # return np.argmax(q_values[estado])
        if q_values[estado][0] > q_values[estado][1]:
            return 0
        else:
            return 1
    else:  # choose a random action
        return np.random.randint(2)


# Estados
# Estado 0 -> Inicio
# Estado 1 -> Mas sifones vacios en estado deadlock
# Estado 2 -> Menos sifones vacios en estado deadlock
# Estado 3 -> Igual cantidad sifones vacios en estado deadlock
# Estado 4 -> Estado final


def print_and_log(q, lista=None):
    if lista is not None:
        for row in lista:
            q.add_row(row)

    print(q)
    # Si el log esta activado, escribo archivo
    if log_activity:
        with open(txt_path + logfile_name, "a") as log:
            print(q, file=log)


def print_q_table(q_table):
    q = PrettyTable()
    q.add_column("Estados/Acciones",
                 ["Start", "Más sifónes vacíos en estado deadlock", "Menos sifónes vacíos en estado deadlock",
                  "Igual cantidad de sifónes vacíos en estado deadlock", "End"])
    q.add_column("Agregar supervisor", q_table[:, 0])
    q.add_column("Agregar o quitar arcos", q_table[:, 1])
    print_and_log(q)


def print_list_supervisors(list_supervisors):
    q = PrettyTable()
    q.field_names = ["ID", "Marcado", "T - Entrada", "T - Salida", "Sifon a controlar"]
    print_and_log(q, list_supervisors)


def print_added_supervisors(list_supervisors):
    q = PrettyTable()
    q.field_names = ["Nro", "Marcado", "T - Entrada", "T - Salida"]
    print_and_log(q, list_supervisors)


def print_list_sifons(list_sifons):
    q = PrettyTable()
    q.field_names = ["ID", "Nro", "Plazas"]
    print_and_log(q, list_sifons)


def print_t_invariants(t_invariants):
    q = PrettyTable()
    field_names = []
    for row in t_invariants:
        for column_number in range(len(row)):
            field_names.append("T" + str(column_number))
        break  # No borrar el break

    q.field_names = field_names

    print_and_log(q, t_invariants)


def save_stats():
    with open(output_folder + "/STATS", 'w') as f:
        f.write(str(current_iteration) + "\n")  # Nro de iteraciones realizados por el algoritmo
        f.write(str(cant_supervisores) + "\n")  # Cantidad de supervisores agregados
        f.write(str(cant_sifones_inciales) + "\n")  # Cantidad de sifones inicial
        f.write(str(cant_plazas_iniciales) + "\n")  # Cantidad de plazas inicial
        f.write(str(cant_transiciones_iniciales) + "\n")  # Cantidad de transiciones inicial
        f.write(str(cant_estados_iniciales) + "\n")  # Cantidad de estados inicial


def call_petrinator():
    global current_iteration

    if current_iteration < iteration_limit:

        # Armo el comando a ejecutar
        petrinator_cmd = java_command + " " + petrinator_path + " " + pflow_file + " " + output_folder + " " + bring_gui + " " + auto_close + " " \
                         + str(current_iteration) + " " + clasify + " " + redirect_output

        # Actualizo numero de iteracion
        current_iteration += 1

        # Guardo estadistica
        save_stats()

        custom_print("\n--------------------------------------------------------------------------")
        custom_print("[INFO] Ejecutando petrinator para conocer estado de la red...")
        custom_print("--------------------------------------------------------------------------\n")

        # Lo ejecuto, y controlo si se llega al timeout
        petrinator = subprocess.Popen(petrinator_cmd, shell=True)
        try:
            petrinator.wait(petrinator_timeout)
        except subprocess.TimeoutExpired:
            petrinator.kill()
            custom_print("[ERROR] Se alcanzo el limite de " + str(
                petrinator_timeout) + " segundos para la ejecucion de petrinator, cerrando analisis", "\n")
            subprocess.Popen("touch " + output_folder + "/NO.UNLOCKED", shell=True).communicate()
            exit(1)

        # Si no es S3PR abandono el analisis
        if (os.path.isfile(output_folder + "/NO.S3PR")):
            custom_print("[ERROR] La red ingresada no es S3PR, cerrando analisis", "\n")
            exit(1)

        # Si no hay DEADLOCK abandono el analisis
        if (os.path.isfile(output_folder + "/NO.DEADLOCK")):
            custom_print("[ERROR] La red ingresada no tiene deadlock, cerrando analisis", "\n")
            exit(1)

        # Si no se pudo destrabar anteriormente abandono el analisis
        if (os.path.isfile(output_folder + "/NO.UNLOCKED")):
            custom_print("[ERROR] La red ingresada no se ha podido destrabar anteriormente, cerrando analisis", "\n")
            exit(1)
    else:
        custom_print("[ERROR] Se alcanzo SIN EXITO el limite de " + str(
            iteration_limit) + " iteraciones para intentar destrabar la red, cerrando analisis", "\n")
        subprocess.Popen("touch " + output_folder + "/NO.UNLOCKED", shell=True).communicate()
        exit(1)


def dir_path(string, auto_createdir=auto_createdir):
    if os.path.isdir(string):
        return string

    # Crear directorio si no existe y auto_createdir = True
    if ((not os.path.exists(string)) and auto_createdir):
        os.makedirs(string)
        return string
    else:
        raise NotADirectoryError(string)


def entrenar():
    # define training parameters
    # the percentage of time when we should take the best action (instead of a random action)
    epsilon = 0.9
    discount_factor = 0.9  # discount factor for future rewards
    learning_rate = 0.9  # the rate at which the AI agent should learn

    # run through 1000 training episodes
    for episode in range(trainning_num):

        custom_print("[INSTRUCTION] El entrenamiento comienza con un análisis sobre la red.")
        custom_print("[INSTRUCTION] Luego, ejecuta una de las siguientes acciones: ")
        custom_print("[INSTRUCTION] - Agregar supervisor.")
        custom_print("[INSTRUCTION] - Agregar o quitar arcos de un supervisor.")
        custom_print("[INSTRUCTION] Luego, actualiza la Q-TABLE y vuelve a evaluar el estado de la red")
        custom_print("[INSTRUCTION] Repite estos pasos hasta llegar a uno de los siguientes estados")
        custom_print("[INSTRUCTION] - El estado inicial de la red tiene deadlock.")
        custom_print("[INSTRUCTION] - La red ya no tiene mas deadlock.")
        custom_print(
            "[INSTRUCTION] Al llegar a un estado terminal vuelve a este punto y comienza un nuevo entrenamiento.")

        custom_print("[INFO] Cantidad de entrenamientos a realizar: ", trainning_num)
        custom_print("[INFO] Entrenamiento numero:", episode, "\n")
        custom_print("[INFO] Q-TABLE")
        print_q_table(q_values)

        # En cada episodio reinicio todo

        # Empiezo cada episodio en el estado inicial
        estado = 0
        deadlock_inicial = False
        if (step_execution):
            input(
                "\n[INPUT] Presione enter para realizar el análisis. Si necesita actualizar la red, este es el momento: ")

        # Corre analisis con petrinator para saber estado de la red
        (cantidad_sifones, t_invariant, state_deadlock, sifones_vacios_deadlock,
         lista_supervisores, cantidad_transiciones, matriz_es_tr, matriz_es_pl, matriz_pos, matriz_pre,
         lista_sifones) = get_entorno()

        # Imprimo estado de la red
        custom_print("[INFO] Cantidad de transiciones:", cantidad_transiciones)
        custom_print("[INFO] Estados de deadlock:", state_deadlock)
        custom_print("[INFO] Cantidad de sifones:", cantidad_sifones)
        custom_print("[INFO] Cantidad de sifones vacios en estado de deadlock:", len(sifones_vacios_deadlock), "\n")
        custom_print("[INFO] SIFONES")
        print_list_sifons(lista_sifones)
        print("")
        custom_print("[INFO] T INVARIANTES")
        print_t_invariants(t_invariant)
        print("")
        custom_print("[INFO] POSIBLES SUPERVISORES A AGREGAR")
        print_list_supervisors(lista_supervisores)

        # get_entorno devuelve todos -1 cuando existe deadlock inicial
        if cantidad_sifones == -1:
            deadlock_inicial = True

        # Guardo los t_invariantes antes de modificar la red
        t_inv_anterior = t_invariant
        marcado_sup_agregado = 0

        # continue taking actions (i.e., moving) until we reach a terminal state
        # (i.e., until we reach the item packaging area or crash into an item storage location)
        # Comparar con t_invariante de la red original, en vez del anterior. TODO
        while not is_terminal_state(sifones_vacios_deadlock, t_inv_anterior, t_invariant, deadlock_inicial):
            custom_print("[INFO] Ejecutando accion...")
            # choose which action to take (i.e., where to move next)
            action_index = get_next_action(estado, epsilon)
            if (action_index == 0):
                custom_print("[ACTION] Agregar supervisor ")
                if len(lista_supervisores) == 1:
                    marcado_sup_agregado = int(lista_supervisores[0][1])
                    custom_print("[ACTION] Agrego supervisor Numero: 0")
                    agregar_sup(lista_supervisores, 0)
                elif len(lista_supervisores) > 1:
                    num_aleatorio = np.random.randint(len(lista_supervisores))
                    custom_print("[ACTION] Agrego supervisor aleatorio Numero:", str(num_aleatorio))
                    marcado_sup_agregado = int(lista_supervisores[num_aleatorio][1])
                    agregar_sup(lista_supervisores, num_aleatorio)
                else:
                    # O penalizamos la accion o no hacemos nada y dejamos que en algun momento ejecute la otra accion
                    # y que sirva ejecutarla.
                    custom_print("[ERROR] No hay supervisores en la lista, deberiamos penalizar la accion con -50")
            elif (action_index == 1):
                custom_print("[ACTION] Agregar o quitar arcos")
                analisis_3(cantidad_transiciones, matriz_es_tr, matriz_es_pl, matriz_pos, matriz_pre)
            else:
                custom_print("[ERROR] Fallo get_next_action")

            if (step_execution):
                input("[INPUT] Accion ejecutada con exito, presione enter para continuar: ")

            # Guardo datos del estado anterior.
            t_inv_anterior = t_invariant
            sifones_vacios_antes = sifones_vacios_deadlock
            # Vuelvo a cargar el estado de la red, porque al ejecutar una accion este cambia.
            (cantidad_sifones, t_invariant, state_deadlock, sifones_vacios_deadlock, lista_supervisores,
             cantidad_transiciones, matriz_es_tr, matriz_es_pl, matriz_pos, matriz_pre, lista_sifones) = get_entorno()

            # Imprimo informacion
            custom_print("[INFO] Cantidad de sifones vacios en estado de deadlock ANTES:", len(sifones_vacios_antes))
            if sifones_vacios_deadlock != -1:
                custom_print("[INFO] Cantidad de sifones vacioes en estado de deadlock AHORA:",
                             len(sifones_vacios_deadlock))
            else:
                custom_print("[INFO] Deadlock en estado inicial, no se pueden calcular sifones vacios")

            custom_print("[INFO] Cantidad de transiciones:", cantidad_transiciones)
            custom_print("[INFO] Estados de deadlock:", state_deadlock)
            custom_print("[INFO] Cantidad de sifones:", cantidad_sifones)
            custom_print("[INFO] Cantidad de sifones vacios en estado de deadlock:", len(sifones_vacios_deadlock), "\n")
            custom_print("[INFO] SIFONES")
            print_list_sifons(lista_sifones)
            print("")
            custom_print("[INFO] T INVARIANTES")
            print_t_invariants(t_invariant)
            print("")
            custom_print("[INFO] POSIBLES SUPERVISORES A AGREGAR")
            print_list_supervisors(lista_supervisores)
            print("")
            custom_print("[INFO] Actualizando estado de la red..")

            # Actualizo el estado
            estado_anterior = estado

            # get_entorno devuelve todos -1 cuando el estado inicial de la red tiene deadlock
            if cantidad_sifones == -1:
                custom_print(
                    "[STATE] Cambie al estado 4: Estado terminal. O se quito el deadlock o hay deadlock inicial")
                estado = 4
                deadlock_inicial = True
            # Si hay mas sifones vacios en estado de deadlock despues de la accion
            elif len(sifones_vacios_deadlock) > len(sifones_vacios_antes):
                custom_print("[STATE] Cambie al estado 1: Hay mas sifones vacios en deadlock")
                estado = 1
            # Si hay menos sifones vacios en estado de deadlock despues de la accion
            elif len(sifones_vacios_deadlock) < len(sifones_vacios_antes):
                custom_print("[STATE] Cambie al estado 2: Hay menos sifones vacios en deadlock")
                estado = 2
            # Si hay igual cantidad de sifones vacios en estado de deadlock despues de la accion
            else:
                custom_print("[STATE] Cambie al estado 3: Hay igual cantidad de sifones vacios en deadlock")
                estado = 3
            reward = calcular_recompensas(sifones_vacios_antes, sifones_vacios_deadlock, t_inv_anterior,
                                          t_invariant, state_deadlock, action_index, marcado_sup_agregado,
                                          deadlock_inicial)
            # receive the reward for moving to the new state, and calculate the temporal difference
            # reward = rewards[row_index, column_index]
            old_q_value = q_values[estado_anterior, action_index]
            temporal_difference = reward + \
                                  (discount_factor * np.max(q_values[estado])) - old_q_value

            # update the Q-value for the previous state and action pair
            new_q_value = old_q_value + (learning_rate * temporal_difference)
            q_values[estado_anterior, action_index] = new_q_value
            custom_print("\n--------------------------------------------------------------------------\n")
            custom_print("[INFO] NEW Q-TABLE")
            print_q_table(q_values)

    # Condiciones de finalizacion: No hay mas deadlock o inguna transicion esta sensibilizada (deadlock inicial)
    custom_print("[INFO] Training complete!", "\n")


def run_algorithm():
    custom_print(
        "[INSTRUCTION] Bienvenido: Este programa es capas de aplicar Reinforcemente Learning sobre una red de petri para evitar el deadlock, en caso de tenerlo.")
    custom_print("[INSTRUCTION] Solo necesita: ")
    custom_print("[INSTRUCTION]    - Una red de petri en formato .pflow")
    custom_print("[INSTRUCTION]    - Programa Petrinator (version automatizada) para el análisis de la red.")
    custom_print(
        "[INSTRUCTION] Una vez ingresado el nombre del archivo .pflow, el programa obtiene la información de la red.")
    custom_print("[INSTRUCTION] Luego comienza con el entrenamiento, generando una Q-TABLE por cada iteracion")
    custom_print(
        "[INSTRUCTION] Este proceso es automático, hasta el momento que se logra destrabar la red o se llega a un estado terminal.")
    custom_print(
        "[INSTRUCTION] En ese momento se necesita volver a cargar la red que presentaba deadlock para comenzar de cero el analisis, y volver a entrenar")

    global name_pflow

    if (step_execution):
        input("[INPUT] Presione enter para iniciar el analisis:")

    name_pflow = html_path + "/net.pflow"

    init()
    entrenar()


def file_path(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)


def main(pf, of, hp, tp):
    global pflow_file, output_folder, html_path, txt_path

    pflow_file = pf
    output_folder = of
    html_path = hp
    txt_path = tp

    # Para crear directorios si no existen y auto_createdir = True
    dir_path(html_path)
    dir_path(txt_path)

    custom_print("[DEBUG] pflow_file: ", pflow_file)
    custom_print("[DEBUG] output_folder: ", output_folder)
    custom_print("[DEBUG] html_path: ", html_path)
    custom_print("[DEBUG] txt_path: ", txt_path)

    # El algoritmo se ejecuta iterativamente hasta que se controla la red.
    # De no ser así se dice que el algoritmo no converge
    while (1):
        choise = "0"
        id = 0

        run_algorithm()

        if (multiple_runs):
            print("\n-------------------------------------------------------------")
            print("Ingrese:")
            print("1 - Deadlock = true  - Volver a ejecutar el Algoritmo")
            print("0 - Deadlock = false - Finalizar ejecucion")
            choise = input("Opcion: ")
            print("-------------------------------------------------------------")

        if (choise == "0"):
            exit(0)


if __name__ == "__main__":

    # Si se corre el script sin argumentos usa los default del setup inicial
    # si no verifica que se pasen los 4 argumentos
    if len(sys.argv) > 1:
        ap = argparse.ArgumentParser(description='Paths de entrada y salida del algoritmo')
        required = ap.add_argument_group('Argummentos requeridos')

        required.add_argument("-pf", "--pflow_file", type=file_path, required=True, default=pflow_file,
                              help="Ruta al archivo net.pflow que se usara durante el analis")
        required.add_argument("-of", "--output_folder", type=dir_path, required=True, default=output_folder,
                              help="Directorio de salida donde se escribira net.pflow procesado")
        required.add_argument("-hp", "--html_path", type=dir_path, required=True, default=html_path,
                              help="Directorio de salida donde se escribiran los archivos html")
        required.add_argument("-tp", "--txt_path", type=dir_path, required=True, default=txt_path,
                              help="Directorio de salida donde se escribiran los archivos txt")

        args = vars(ap.parse_args())

        pflow_file = args['pflow_file']
        output_folder = args['output_folder']
        html_path = args['html_path']
        txt_path = args['txt_path']
        main(pflow_file, output_folder, html_path, txt_path)
    else:
        main(pflow_file, output_folder, html_path, txt_path)
