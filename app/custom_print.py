from prettytable import PrettyTable
from datetime import datetime
import time
import os

# PRINT OPTIONS: Se puede agregar o quitar labels de debug al igual que excepciones
custom_print_level = ["[INFO]", "[ACTION]", "[REWARD]", "[STATE]", "[INPUT]", "[ERROR]", "[DEBUG]",
                      "[RESULT]"]  # Tags que se desean loguear, omitimos [INSTRUCTION]
custom_print_exceptions = ["---", "\n"]                 # Excepciones del custom_print
custom_print_datefmt = "%d/%m/%Y %H:%M:%S"              # Formato del timestamp del log
logfile_name = "/execution_log.txt"                     # Nombre del archivo de log (se guarda en carpeta de txt)
label = True                                            # Mostrar/ocultar el label del tag
activity = True                                         # Habilitar/deshabilitar el logueo a archivo de la salida por consola
format = True                                           # Habilitar/deshabilitar el formateo de texto en base a los tags
INFO = '\033[94m'                                       # Azul
ACTION = '\033[96m'                                     # Cyan
REWARD = '\033[92m'                                     # Verde
STATE = '\033[93m'                                      # Amarillo
INPUT = '\033[0m'                                       # Sin formato especial
ERROR = '\033[91m'                                      # Rojo
DEBUG = '\033[0;37;45m'                                 # Fondo magenta letra blanca
RESULT = '\033[6;30;43m'                                # Fondo amarillo letra negra
ENDC = '\033[0m'                                        # Clear format
custom_date_fmt = "%Hh:%Mm:%Ss"


def dir_path(string, auto_createdir=False):
    if os.path.isdir(string):
        return string

    # Crear directorio si no existe y auto_createdir = True
    if (not os.path.exists(string)) and auto_createdir:
        os.makedirs(string)
        return string
    else:
        raise NotADirectoryError(string)


def c_print(text1, text2="", text3="", text4="", show_label=label, log_activity=activity,
            format_text=format, log_path=None):
    """
    Imprime hasta 4 strings si el tag del primero esta incluido en la lista custom_print_level.

    Parameters
    ----------
        text1          -- Requerido: texto tageado a imprimir
        text2          -- Opcional: texto adicional (Default: "")
        text3          -- Opcional: texto adicional (Default: "")
        text4          -- Opcional: texto adicional (Default: "")
        show_label     -- Opcional: flag para activar/desactivar impresion del tag (Default: "True")
        log_activity   -- Opcional: flag para activar/desactivar logueo en archivo de texto (Default: "True")
        format_text    -- Opcional: flag para activar/desactivar el formateo de texto (Default: "True")
        log_path       -- Opcional: path al archivo de log que se quiere escribir
    """
    if log_path is None:
        log_path = os.path.abspath(os.getcwd()) + logfile_name
    else:
        log_path = log_path + logfile_name

    # Registro la hora
    timestamp = datetime.now().strftime(custom_print_datefmt)

    # El label siempre viene en el primer string, entonces lo spliteo por espacios
    # y me quedo con el primer elemento del array que seria el tag
    substring = text1.split(" ")

    # Si el texto es una excepcion no hago el chequeo de tags
    for exception in custom_print_exceptions:
        # Solo verifico que sea un unico string y que la excepcion este contenida en ese texto
        if (exception in text1) and text2 == "" and text3 == "" and text4 == "":
            print(text1, text2, text3, text4)
            # Si el log esta activado, escribo archivo
            if log_activity:
                with open(log_path, "a+") as log:
                    print(text1, text2, text3, text4, file=log)
            return

    # Le saco los corchetes al tag para luego llamar la variable global de mismo nombre que contiene el formato a usar
    format_tag = substring[0].replace("[", "").replace("]", "")

    # Chequeo si el tag esta en la lista de lo que deseo imprimir
    if substring[0] in custom_print_level:
        # Chequeo si tengo que imprimir etiqueta
        if show_label:
            if format_text:
                # En caso positivo, imprimo timestamp + formatodesado + text1..n + vuelvo formato default
                print(timestamp + globals()[format_tag], text1, text2, text3, text4, ENDC)
            else:
                # En caso negativo, imprimo timestamp + text1..n
                print(timestamp + text1, text2, text3, text4)

            # Si el log esta activado, escribo archivo tambien (sin formato independientemente de format_text)
            if log_activity:
                with open(log_path, "a+") as log:
                    print(timestamp + text1, text2, text3, text4, file=log)
        else:
            if format_text:
                # En caso negativo, le saco el tag y el espacio separador despues de el
                # y agrego timestamp y formato deseado
                print(timestamp + globals()[format_tag], text1.replace(substring[0] + " ", ""), text2, text3, text4,
                      ENDC)
            else:
                # En caso negativo, le saco el tag y el espacio separador despues de el y agrego timestamp
                print(timestamp + text1.replace(substring[0] + " ", ""), text2, text3, text4)

            # Si el log esta activado, escribo archivo tambien (sin formato independientemente de format_text)
            if log_activity:
                with open(log_path, "a+") as log:
                    print(timestamp + text1.replace(substring[0] + " ", ""), text2, text3, text4, file=log)


def print_and_log(q, lista=None, last_line="", log_path=None):
    if lista is not None:
        for row in lista:
            q.add_row(row)

    if log_path is None:
        log_path = os.path.abspath(os.getcwd()) + logfile_name

    print(q, last_line)
    # Si el log esta activado, escribo archivo
    if activity:
        with open(log_path + logfile_name, "a") as log:
            print(q, last_line, file=log)


def print_q_table(q_table, last_line="", log_path=None):
    q = PrettyTable()
    q.add_column("Estados/Acciones",
                 ["Start", "Más sifónes vacíos en estado deadlock", "Menos sifónes vacíos en estado deadlock",
                  "Igual cantidad de sifónes vacíos en estado deadlock", "End"])
    q.add_column("Agregar supervisor", q_table[:, 0])
    q.add_column("Agregar o quitar arcos", q_table[:, 1])
    print_and_log(q, None, last_line, log_path=log_path)


def print_list_supervisors(list_supervisors, last_line="", log_path=None):
    q = PrettyTable()
    q.field_names = ["ID", "Marcado", "T - Entrada", "T - Salida", "Sifon a controlar"]
    print_and_log(q, list_supervisors, last_line, log_path=log_path)


def print_added_supervisors(list_supervisors, last_line="", log_path=None):
    q = PrettyTable()
    q.field_names = ["Nro", "Plaza", "Marcado", "T - Entrada", "T - Salida"]
    print_and_log(q, list_supervisors, last_line, log_path=log_path)


def print_list_sifons(list_siphons, last_line="", log_path=None):
    q = PrettyTable()
    q.field_names = ["ID", "Nro", "Plazas"]
    print_and_log(q, list_siphons, last_line, log_path=log_path)


def print_t_invariants(t_invariants, last_line="", log_path=None):
    q = PrettyTable()
    field_names = []
    for row in t_invariants:
        for column_number in range(len(row)):
            field_names.append("T" + str(column_number))
        break  # No borrar el break

    q.field_names = field_names

    print_and_log(q, t_invariants, last_line, log_path=log_path)


# Imprime la duracion de cada analisis, ordenada de menor a mayor y filtrada por tipo de resultado obtenido
def print_pretty_dict(target_result, dictionary, log_activity=activity, log_path=None):
    if log_path is None:
        log_path = os.path.abspath(os.getcwd()) + logfile_name
    else:
        log_path = log_path + logfile_name

    cantidad = 0

    t = PrettyTable(
        ["Red", "Duracion Analisis", "N Iteraciones", "N Sup Agregados", "N Sifones Inicial", "N Plazas Inicial",
         "N Transiciones Inicial", "N Estados Inicial"])
    for key, val in dictionary.items():
        if val[7] == target_result:
            t.add_row([key, time.strftime(custom_date_fmt, time.gmtime(val[0])), str(val[1]), str(val[2]), str(val[3]),
                       str(val[4]), str(val[5]), str(val[6])])
            cantidad += 1

    if cantidad > 0:
        # No me interesa imprimir la tabla vacia, por eso el flag de cantidad
        print(t.get_string(), "\n")

        # Si el log esta activado, escribo archivo
        if log_activity:
            with open(log_path, "a") as log:
                print(t.get_string(), "\n", file=log)


def print_metrics(metrics, last_line="", log_path=None):
    q = PrettyTable()
    q.field_names = ["Plazas", "Estados", "Tokens trabajando", "Sifones vacios", "Sifones", "Trampas",
                     "Cantidad de supervisores agregados"]
    print_and_log(q, metrics, last_line, log_path=log_path)