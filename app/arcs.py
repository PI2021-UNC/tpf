"""
Funciones adicionales para agregar/eliminar arcos de las redes de Petri (.pflow) creadas 
con el software Petrinator
"""

def eliminararco(name_pflow, aS, aD):
    """
    Elimina un arco de la red. Desde una plaza supervisor hasta una transición. \n
    
    Parameters \n
    ----------
        name_pflow  -- Nombre del la red  a modificar (.pflow) creada con el software Petrinator. \n
        aS          -- Fuente del arco a eliminar. \n
        aD          -- Destino del arco a eliminar. \n
    """
    ## Comento esto porque limita a eliminar arcos desde un plaza a una transición. No permite eliminar a la inversa.
    # Para manejar esto, lo hago genérico, desde la llamda especifico si la fuente es una plaza o una transición
    # con la letra correspondiente.
    # aS = str(f'P{aS}')  #Al arco fuente se le agrega una 'P' (plaza) para conformar la nomenclatura. Ej: P5 
    # aD = str(f'T{aD}')  #Al arco destino se le agrega una 'T' (transicion) para conformar la nomenclatura. Ej: T9
    
    arc = 0 
    pos = 0
    arco_eliminado = [] 

    f = open(name_pflow,"r")
    archivo = f.readlines()
    f.close()

    for i in range(len(archivo)):   #Elimina los espacios contenidos en el archivo y los guarda en 'arco_eliminado'
        arco_eliminado.append(archivo[i].strip())

    for i in range (len(arco_eliminado)): #Busca el arco a eliminar y guarda la posición para eliminarlo
        if(arco_eliminado[i] == ('<sourceId>'+aS+'</sourceId>') and arco_eliminado[i+1] == ('<destinationId>'+aD+'</destinationId>')):
            pos = i-2
            arc = 1 

    
    while(arc): #Si se encuentra el arco a el arco a eliminar modifica el archivo, de no ser así, no se realiza ninguna acción 
        if(arco_eliminado[pos] != '</arc>'):
            arco_eliminado.pop(pos)
        else:
            arco_eliminado.pop(pos)
            arc = 0

    with open(name_pflow, 'w') as f:  #Se reescribe el archivo '.pflow' 
        for item in arco_eliminado:
            f.write("%s\n" % item)
    f.close()


def agregararco(name_pflow, aS, aD): 
    """
    Agrega un arco de la red. Desde una transición hasta una plaza supervisor. \n
    
    Parameters \n
    ----------
        name_pflow  -- Nombre del la red a modificar (.pflow) creada con el software Petrinator. \n
        aS          -- Fuente del arco a agregar \n
        aD          -- Destino del arco a agregar \n
    """
    aS = str(f'T{aS}') #Al arco fuente se le agrega una 'T' (transicion) para conformar la nomenclatura. Ej: T9
    aD = str(f'P{aD}') #Al arco destino se le agrega una 'P' (plaza) para conformar la nomenclatura. Ej: P5 
    
    flag = 0
    archivo_sin_espacio=[]

    f = open(name_pflow,"r")
    archivo = f.readlines()
    f.close()

    for i in range(len(archivo)): #Elimina los espacios contenidos en el archivo y los guarda en 'archivo_sin_espacio'
        archivo_sin_espacio.append(archivo[i].strip())

        if(archivo_sin_espacio[i] == '</arc>' and flag == 0): #Busca el lugar del archivo donde se debe agregar el arco
            archivo_sin_espacio.append('<arc>\n<type>regular</type>\n<sourceId>'+ aS + '</sourceId>\n<destinationId>'+ aD +'</destinationId>\n<multiplicity>1</multiplicity>\n</arc>')
            flag = 1

    with open(name_pflow, 'w') as f: #Se reescribe el archivo '.pflow' 
        for item in archivo_sin_espacio:
            f.write("%s\n" % item)
    f.close()


# Elimino una plaza de la red. Este metodo se debe llamar luego de haber eliminado los arcos
def delete_place(name_pflow, place_id):
    f = open(name_pflow, "r")
    archivo = f.readlines()
    f.close()
    file_without_spaces = []
    # Elimina los espacios contenidos en el archivo y los guarda en 'arco_eliminado'
    for i in range(len(archivo)):
        file_without_spaces.append(archivo[i].strip())

    # Busco <id>P15</id>
    # Si la red se guarda con los tag <id> en otra posicion dentro de los tag <place> el metodo falla
    for index, item in enumerate(file_without_spaces):
        if item == "<id>" + place_id + "</id>":
            # tag <place>
            file_without_spaces.pop(index - 1)
            # tag <id>P15</id>
            file_without_spaces.pop(index - 1)
            # tag <x>363</x>
            file_without_spaces.pop(index - 1)
            # tag <y>9</y>
            file_without_spaces.pop(index - 1)
            # tag <label>P15</label>
            file_without_spaces.pop(index - 1)
            # tag <tokens>0</tokens>
            file_without_spaces.pop(index - 1)
            # tag <isStatic>false</isStatic>
            file_without_spaces.pop(index - 1)
            # tag </place>
            file_without_spaces.pop(index - 1)

    # Se reescribe el archivo '.pflow'
    with open(name_pflow, 'w') as f:
        for item in file_without_spaces:
            f.write("%s\n" % item)
    f.close()