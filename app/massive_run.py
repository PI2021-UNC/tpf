#!/usr/bin/python3

import os
import sys
import subprocess
import argparse
import time
import deadlock_avoidance_RL as algoritmo
from collections import OrderedDict
from importlib import reload
from custom_print import c_print, dir_path, print_pretty_dict
from prettytable import PrettyTable

##### INICIO CONFIGURACION GLOBAL #####

networks_directory = "../test_networks"  # Directorio contenedor de redes
log_activity = True  # Habilitar/deshabilitar el logueo a archivo de la salida por consola
log_path = networks_directory  # Directorio al archivo de log
nodeadlock_folder = networks_directory + "/NO_DEADLOCK"  # Subdirectorio donde se colocaran redes SIN DEADLOCK
nos3pr_folder = networks_directory + "/NO_S3P3"  # Subdirectorio donde se colocaran redes NO S3PR
nounlocked_folder = networks_directory + "/NO_UNLOCKED"  # Subdirectorio donde se colocaran redes NO DESTRABADAS
unlocked_folder = networks_directory + "/UNLOCKED"  # Subdirectorio donde se colocaran redes DESTRABADAS
failed_folder = networks_directory + "/FAILED"  # Subdirectorio donde se colocaran redes CON FALLAS
auto_createdir = True  # Permite creacion automatica de las carpetas que no existen
wait_time = 10  # Ventana de seguridad antes de clasificar (asegura que se terminaron las op de escritura antes)
total_networks = 0  # Contador de analisis de redes realizados
results = {}  # Diccionario de listas con resultados Ej. {'Ezpeleta': [22.545323610305786, 6, 'unlocked']
globalt_start = time.time()  # Tiempo de inicio ejecucion masiva
custom_datefmt = "%Hh:%Mm:%Ss"


###### FIN CONFIGURACION GLOBAL ######


# Recorre los archivos que estan en networks_directory
def serial_execute():
    for subdir, dirs, files in os.walk(networks_directory):
        dirs.sort()
        for filename in files:
            filepath = subdir + os.sep + filename

            if filepath.endswith("net.pflow"):
                output_folder = subdir + os.sep
                html_path = subdir + os.sep
                txt_path = subdir + os.sep + "txt_info"

                # Obtengo el nombre de la red
                network = subdir.split("/")[-1]

                # Capturo instante de inicio
                t_start = time.time()

                # Ejecuto nuestro algoritmo pasandole los parametros como argumento
                try:
                    algoritmo.main(filepath, output_folder, html_path, txt_path)
                except SystemExit:
                    pass
                except KeyboardInterrupt:
                    subprocess.Popen("touch " + output_folder + "/FAILED", shell=True).communicate()
                    c_print("[ERROR] Se interrumpio la ejecucion de la red: " + network + " mediante CTRL + C", "\n",
                            log_activity=log_activity, log_path=log_path)
                except Exception as e:
                    print(e.with_traceback())
                    subprocess.Popen("touch " + output_folder + "/FAILED", shell=True).communicate()
                    c_print("[ERROR] Se produjo una excepcion NO MANEJADA durante la ejecucion de tesis_V2 mientras se analizaba la red: " + network + " REVISAR Y CORREGIR!", "\n",
                        log_activity=log_activity, log_path=log_path)

                # Capturo instante de finalizacion
                t_taken = time.time() - t_start

                # Guardo la duracion y nro de iteraciones de cada uno de los casos de analisis en un diccionario
                results.setdefault(network, []).append(t_taken)

                with open(output_folder + "/STATS", 'r') as f:
                    lines = f.readlines()

                    # Cada linea del archivo corresponde a: current_iteration, cant_supervisores, cant_sifones_inciales, cant_plazas_iniciales, cant_transiciones_iniciales
                    for line in lines:
                        results.setdefault(network, []).append(line.strip())

                # Almaceno cantidad de redes a analizar (directorios)
                global total_networks
                total_networks += 1

                # Reload al modulo tesis_v2 para empezar de nuevo
                reload(algoritmo)


def classify_results():
    dir_path(nodeadlock_folder, auto_createdir)
    dir_path(nos3pr_folder, auto_createdir)
    dir_path(nounlocked_folder, auto_createdir)
    dir_path(unlocked_folder, auto_createdir)
    dir_path(failed_folder, auto_createdir)

    for subdir, dirs, files in os.walk(networks_directory):
        dirs.sort()
        for filename in files:
            if ((nodeadlock_folder in subdir) or (nos3pr_folder in subdir) or (unlocked_folder in subdir) or (
                    nounlocked_folder in subdir) or (failed_folder in subdir)):
                break
            else:
                filepath = subdir + os.sep + filename
                container_folder = subdir + os.sep
                # Obtengo el nombre de la red
                network = subdir.split("/")[-1]

                if filepath.endswith("NO.S3PR"):
                    subprocess.Popen("mv " + container_folder + " " + nos3pr_folder, shell=True).communicate()
                    results.setdefault(network, []).append("nos3pr")
                    break
                if filepath.endswith("NO.DEADLOCK"):
                    subprocess.Popen("mv " + container_folder + " " + nodeadlock_folder, shell=True).communicate()
                    results.setdefault(network, []).append("nodeadlock")
                    break
                if filepath.endswith("NO.UNLOCKED"):
                    subprocess.Popen("mv " + container_folder + " " + nounlocked_folder, shell=True).communicate()
                    results.setdefault(network, []).append("nounlocked")
                    break
                if filepath.endswith("UNLOCKED"):
                    subprocess.Popen("mv " + container_folder + " " + unlocked_folder, shell=True).communicate()
                    results.setdefault(network, []).append("unlocked")
                    break
                if filepath.endswith("FAILED"):
                    subprocess.Popen("mv " + container_folder + " " + failed_folder, shell=True).communicate()
                    results.setdefault(network, []).append("failed")
                    break


# Devuelve la cantidad de veces que ocurre el tipo de resultado que se le pida
def get_count(target_result):
    count = 0

    for network in results:
        count += results[network].count(target_result)

    return count


def get_stats():
    # Ordeno el diccionario por duracion de menor a mayor, porque el sortby de print_pretty_dict lo hace tomando el dato como string y no queda bien
    sorted_results = OrderedDict([(x, results[x]) for x in sorted(results, key=lambda x: results[x][
        0])])  # OrderedDict(sorted(results.items(), key=lambda e: e[1][2]))

    unlocked = get_count("unlocked")
    nodeadlock = get_count("nodeadlock")
    nos3pr = get_count("nos3pr")
    nounlocked = get_count("nounlocked")
    failed = get_count("failed")

    valid_networks = total_networks - nodeadlock - nos3pr  # las failed son validas por eso no las resto

    c_print("[RESULT] Se analizaron " + str(total_networks) + " redes de petri, de las cuales:",
            log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] " + str(unlocked) + " se pudieron desbloquear", log_activity=log_activity, log_path=log_path)
    print_pretty_dict("unlocked", sorted_results, log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] " + str(nodeadlock) + " no contenian deadlock inicialmente", log_activity=log_activity,
            log_path=log_path)
    print_pretty_dict("nodeadlock", sorted_results, log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] " + str(nos3pr) + " no eran del tipo s3pr", log_activity=log_activity, log_path=log_path)
    print_pretty_dict("nos3pr", sorted_results, log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] " + str(
        nounlocked) + " no pudieron destrabarse debido a corte por timeout del petrinator o limite de iteraciones",
            log_activity=log_activity, log_path=log_path)
    print_pretty_dict("nounlocked", sorted_results, log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] " + str(failed) + " expirementaron fallas inesperadas durante el analisis",
            log_activity=log_activity, log_path=log_path)
    print_pretty_dict("failed", sorted_results, log_activity=log_activity, log_path=log_path)

    c_print("[RESULT] Del set de redes propuesto, observar los siguientes indicadores:", log_activity=log_activity,
            log_path=log_path)
    c_print("[RESULT] - " + "{:.2f}%".format(((
                                                          unlocked / valid_networks) * 100) if valid_networks != 0 else 0) + " de las redes validas pudo ser desbloqueado -- Calidad del Algoritmo",
            log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] - " + "{:.2f}%".format((((
                                                           nodeadlock + nos3pr) / total_networks) * 100) if total_networks != 0 else 0) + " no cumplian condiciones iniciales (S3PR y DEADLOCK) -- Calidad del Generador/Muestra",
            log_activity=log_activity, log_path=log_path)
    c_print("[RESULT] - " + "{:.2f}%".format((((
                                                           nounlocked + failed) / total_networks) * 100) if total_networks != 0 else 0) + " se debieron descartar por problemas petrinator y/o algoritmo py original -- Incidencias debido a factores externos",
            log_activity=log_activity, log_path=log_path)
    print("")


def main():
    global networks_directory, log_path, nodeadlock_folder, nos3pr_folder, nounlocked_folder, unlocked_folder, failed_folder

    # Si se corre el script sin argumentos usa los default del setup inicial,
    # si no verifica que se pasen los 4 argumentos
    if len(sys.argv) > 1:
        ap = argparse.ArgumentParser(description='Paths de entrada y salida del algoritmo')
        required = ap.add_argument_group('Argummentos requeridos')

        required.add_argument("-nd", "--networks_directory", type=dir_path, required=True, default=networks_directory,
                              help="Ruta a la carpeta contenedora de todos los casos")

        args = vars(ap.parse_args())

        networks_directory = args['networks_directory']
        log_path = networks_directory  # Directorio al archivo de log
        nodeadlock_folder = networks_directory + "/NO_DEADLOCK"  # Subdirectorio donde se colocaran redes SIN DEADLOCK
        nos3pr_folder = networks_directory + "/NO_S3P3"  # Subdirectorio donde se colocaran redes NO S3PR
        nounlocked_folder = networks_directory + "/NO_UNLOCKED"  # Subdirectorio donde se colocaran redes NO DESTRABADAS
        unlocked_folder = networks_directory + "/UNLOCKED"  # Subdirectorio donde se colocaran redes DESTRABADAS
        failed_folder = networks_directory + "/FAILED"  # Subdirectorio donde se colocaran redes CON FALLAS

    # Checkeo que networks_directory exista
    dir_path(networks_directory)
    if os.path.isdir(networks_directory + "_original"):
        c_print(
            "[ERROR] Ya se ejecuto un analisis de este directorio anteriormente, renombre el directorio " + networks_directory + "_original y intente nuevamente",
            log_activity=log_activity, log_path=log_path)
        exit(1)
    else:
        c_print(
            "[INFO] Guardando una copia del directorio " + networks_directory + " en" + networks_directory + "_original",
            "\n", log_activity=log_activity, log_path=log_path)
        # Copio directorio viejo
        subprocess.Popen("cp " + networks_directory + " " + networks_directory + "_original -R",
                         shell=True).communicate()
        # Como ya se habia empezado a escribir el log tengo que borrarlo
        subprocess.Popen("find " + networks_directory + " " + networks_directory
                         + "_original -type f -name *.txt -exec rm {} \;", shell=True).communicate()

    c_print("[INFO] Iniciando la ejecucion serial de todos los casos...", "\n", log_activity=log_activity,
            log_path=log_path)
    # Ejecuto de manera secuencial todas las redes contenidas en el networks_directory
    serial_execute()

    c_print("[INFO] Clasificando los resultados...", "\n", log_activity=log_activity, log_path=log_path)
    # Espero unos segundos a que se terminen de guardar los archivos del ultima ejecucion
    # (evita problemas debido a I/O operations a la hora de hacer el mv)
    time.sleep(wait_time)
    # Clasifico los resultados del analisis de cada red en: desbloqueado, no desbloqueado, no s3pr, no deadlock y failed
    classify_results()

    c_print("[INFO] Generando estadisticas...", "\n", log_activity=log_activity, log_path=log_path)
    # Contabilizo la cantidad de tipo de resultados y muestro los tiempos en tablas con referencia
    # a cada red y sus tiempos
    get_stats()

    c_print("[INFO] La ejecucion masiva ha concluido exitosamente, luego de "
            + time.strftime(custom_datefmt, time.gmtime((time.time() - globalt_start))) + "\n"
            , log_activity=log_activity, log_path=log_path)
    exit(0)


if __name__ == "__main__":
    main()
