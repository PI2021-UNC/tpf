"""
Convierte el archivo de SIFONES Y TRAMPAS extraido del Petrinator (.html) a un archivo ".txt" que luego será utilizado en el algoritmo
"""

import os
from bs4 import BeautifulSoup


def clean_file(text, txt_path):
    """Elimina los salto de linea, espacios y palabras sobrantes en el archivo para luego ser procesado 
    correctamente en otro script de python. \n
    Crea un archivo temporal para trabajar y al final lo elimina.

    Parameters \n
    ----------
        text -- Archivo html que fue convertido a txt.
    """

    # En las siguiente lineas de código se abren y se cierran dos archivos, los cuales 
    # son leídos y escritos para dejar el archivo "limpio" para el próximo script. 
    f = open(txt_path + "/siphons_traps.txt", "w")
    f.write(text)
    f.close()
    
    f1 = open(txt_path + "/siphons_traps.txt", "r")
    f2 = open(txt_path + "/siphons_traps.txt.tmp", "w")
    for line in f1:
        f2.write(line.replace("\n", " "))
    f1.close()
    f2.close()

    f1 = open(txt_path + "/siphons_traps.txt.tmp", "r")
    f2 = open(txt_path + "/siphons_traps.txt", "w")
    for line in f1:
        f2.write(line.replace(" }", "\n"))
    f1.close()
    f2.close()
    f1 = open(txt_path + "/siphons_traps.txt", "r")
    f2 = open(txt_path + "/siphons_traps.txt.tmp", "w")
    for line in f1:
        f2.write(line.replace("siphons", "siphons \n"))
    f1.close()
    f2.close()

    f1 = open(txt_path + "/siphons_traps.txt.tmp", "r")
    f2 = open(txt_path + "/siphons_traps.txt", "w")
    for line in f1:
        f2.write(line.replace("traps", "traps \n"))
    f1.close()
    f2.close()

    f1 = open(txt_path + "/siphons_traps.txt", "r")
    f2 = open(txt_path + "/siphons_traps.txt.tmp", "w")
    for line in f1:
        f2.write(line.replace(",", ""))
    f1.close()
    f2.close()
    
    f1 = open(txt_path + "/siphons_traps.txt.tmp", "r")
    f2 = open(txt_path + "/siphons_traps.txt", "w")
    for line in f1:
        f2.write(line.replace("Siphons and Traps", ""))
    f1.close()
    f2.close()

    f1 = open(txt_path + "/siphons_traps.txt", "r")
    f2 = open(txt_path + "/siphons_traps.txt.tmp", "w")
    for line in f1:
        f2.write(line.replace(" {", ""))
    f1.close()
    f2.close()

    f1 = open(txt_path + "/siphons_traps.txt.tmp", "r")
    f2 = open(txt_path + "/siphons_traps.txt", "w")
    for line in f1:
        f2.write(line.replace("P", ""))
    f1.close()
    f2.close()

    # Elimina archivo temporal
    os.remove(txt_path + "/siphons_traps.txt.tmp")

def main(html_path, txt_path):
    # Abre un archivo html a partir del path indicado por consola, y lo carga en un archivo
    # BeatifulSoup para extraer los datos html de dicho archivo.
    # archivo = input("Path del archivo de Sifones(html): ")
    html = open(html_path + "/sif.html","r")
    soup = BeautifulSoup(html,"lxml")

    # Elimina todos elementos de estilo
    for script in soup(["style"]):
        script.extract()

    # Obtener texto del archivo BeautifulSoup
    text = soup.get_text(separator="\n", strip=True)

    # Elimina los saltos de lina y los espacios al principio y al final de cada línea
    lines = (line.strip() for line in text.split("\n"))

    # Separa varios encabezados en cada linea
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))

    # Elimina líneas en blanco
    text = "\n".join(chunk for chunk in chunks if chunk)

    # Llamada a la funcion clean_file()
    clean_file(text, txt_path)
